from skaro_experiment import *
import numpy as np
from utils import load_raw, load_json, save_raw, save_json
from learning_agents import InterestAgentV4
import copy

from scipy.spatial import cKDTree
import sys, random




#np.set_printoptions(suppress=True)

### TO CONTINUE

## Get maximum action size of dataset
def get_max_action_size(data):
    """
    Get the size of the longest action attempted.
    
    data DatasetV2: dataset to check
    """
    return len(data.a_spaces[0])

def get_all_max_action_size(l):
    return max(l)
    

## Counting raw repartition of actions per task spaces
def count_action_task_repartition(data):
    outDim = len(data.y_spaces)
    inDim = get_max_action_size(data)
    res = np.zeros((outDim, inDim))
    for idA, idY in zip(data.idA, data.idY):
        a_type = idA[0][1]
        for idy in idY:
            task = idy[0]
            res[task, a_type] += 1
    return res


def count_all_action_task_repartition(l):
    outDim = len(l[0])
    inDim = 0
    for li in l:
        inDim = max(inDim, len(li[0]))
    res = np.zeros((outDim, inDim))
    for li in l:
        res[:len(li),:len(li[0])] += li
    res /= float(len(l))
    return res


## Counting raw repartition of actions
def count_action_repartition(data):
    inDim = get_max_action_size(data)
    res = np.zeros(inDim)
    for idA in data.idA:
        a_type = idA[0][1]
        res[a_type] += 1
    return res


def count_all_action_repartition(l):
    inDim = 0
    for li in l:
        inDim = max(inDim, len(li))
    res = np.zeros(inDim)
    for li in l:
        res[:len(li)] += li
    res /= float(len(l))
    return res


## Counting will repartition of actions per task spaces
def count_will_action_task_repartition(la):
    """
    la LearningAgentV4: learner to check
    """
    outDim = len(la.dataset.y_spaces)
    inDim = get_max_action_size(data)
    res = np.zeros((outDim, inDim))
    i = 0
    for lp in la.learning_process:
        n_iter = lp[0]
        task = lp[3]
        for epmem in la._replayer_epmem_range(i, n_iter):
            idE = epmem-1
            a_type = la.dataset.idA[idE][0][1]
            res[task, a_type] += 1
        i = n_iter
    return res


def count_will_all_action_task_repartition(l):
    return count_all_action_task_repartition(l)


## Counting will repartition of actions
def count_will_action_repartition(la):
    """
    la LearningAgentV4: learner to check
    """
    inDim = get_max_action_size(la.dataset)
    res = np.zeros(inDim)
    for epmem in la.epmem_length[1:]:
        idE = epmem-1
        a_type = la.dataset.idA[idE][0][1]
        res[a_type] += 1
    return res


def count_will_all_action_repartition(l):
    return count_all_action_repartition(l)


## Computing number of cells reached for a task and action size

def init_grid(nb_dim, dimY):
    if dimY == 0:
        return 0
    grid = []
    for i in range(nb_dim):
        grid.append(init_grid(nb_dim, dimY-1))
    return grid


def get_indexes(y, step, nbdim):
    index = []
    for yi in y:
        index.append(min(int(np.floor((yi + 1.0) / step)), nbdim-1))
    return index


def set_ntab(tab, index, val):
    tmp = tab
    for i in index[:-1]:
        tmp = tmp[i]
    tmp[index[-1]] = val


def get_ntab(tab, index):
    tmp = tab
    for i in index:
        tmp = tmp[i]
    return tmp

def flatten(tab):
    try:
        i = 0
        while i < len(tab):
            for f in flatten(tab[i]):
                yield f
            i += 1
    except Exception:
        yield tab

def count_cells(data, task, grid_step):
    dimY = data.y_spaces[task].dim
    grid_cell = int(np.ceil(2.0/grid_step))
    grid = init_grid(grid_cell, dimY)
    grid_all = []
    for a_s in data.a_spaces[0]:
        grid_all.append(copy.deepcopy(grid))
    l = len(data.idA)
    for idA, idY in zip(data.idA, data.idY):
        a_type = idA[0][1]
        for (t, idy) in idY:
            if t != task:
                continue
            y = data.y_spaces[task].data[idy]
            indexes = get_indexes(y, grid_step, grid_cell)
            new_val = get_ntab(grid_all[a_type], indexes) + 1
            set_ntab(grid_all[a_type], indexes, new_val)
    grid_res = np.zeros(len(data.a_spaces[0]))
    for i, g in enumerate(grid_all):
        for gj in flatten(g):
            if gj > 0:
                grid_res[i] += 1
    return grid_res

## Computing repartitions of actions per task spaces using evaluation and distance

def compute_evaluation(data, testbench, task):
    """Compute evaluation for a given task."""
    
    actions = []
    for a in data.a_spaces:
        actions.append([])
        for a_s in a:
            actions[-1].append(0)
    
    # Create KD-Tree for evaluation
    tree = None
    if len(data.y_spaces[task].data) > 0:
        tree = cKDTree(np.array(data.y_spaces[task].data), balanced_tree=False)
    
    # Will contain all evaluation information
    eva = []
    # Error sum on the testbench
    error_sum = 0.
    # Sum of quadratic error on testbench
    quad_error_sum = 0.
    for i, point in enumerate(testbench):
        #sys.stdout.write("\rProgress: " + str(i) + "/" + str(len(testbench)))
        #sys.stdout.flush()
        if tree == None:
            dist = data.y_spaces[task].options['out_dist']
        else:
            dist,ids = tree.query(point, k=1, eps=0.0, p=2)
            idE = data.y_spaces[task].ids[ids]
            a_type = data.idA[idE][0]
            actions[a_type[0]][a_type[1]] += 1
        p = dist / data.y_spaces[task].options['max_dist']
        eva.append(p)
        error_sum += p
        quad_error_sum += p**2
    error_sum /= float(len(eva))
    quad_error_sum /= float(len(eva))
    std = np.sqrt(quad_error_sum - error_sum**2)
    
    #print ""
    
    return eva, actions


def compute_evaluation_perf(data, testbench, task):
    """Compute evaluation for a given task."""
    
    actions = []
    for a in data.a_spaces:
        actions.append([])
        for a_s in a:
            actions[-1].append(0)
    
    # Will contain all evaluation information
    eva = []
    # Error sum on the testbench
    error_sum = 0.
    # Sum of quadratic error on testbench
    quad_error_sum = 0.
    for i, point in enumerate(testbench):
        #sys.stdout.write("\rProgress: " + str(i) + "/" + str(len(testbench)))
        #sys.stdout.flush()
        if len(data.y_spaces[task].data) == 0:
            dist = data.y_spaces[task].options['out_dist']
            p = dist
        else:
            ids,dist = data.nn_y(point, task)
            p = dist[0]
            idE = data.y_spaces[task].ids[ids[0]]
            a_type = data.idA[idE][0]
            actions[a_type[0]][a_type[1]] += 1
        eva.append(p)
        error_sum += p
        quad_error_sum += p**2
    error_sum /= float(len(eva))
    quad_error_sum /= float(len(eva))
    std = np.sqrt(quad_error_sum - error_sum**2)
    
    #print ""
    
    return eva, actions

"""
testbench0 = []
for i in range(1000000):
    r = random.uniform(0, 1)
    theta = random.uniform(-np.pi, np.pi)
    z = random.uniform(-0.2, 1)
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    testbench0.append(np.array([x,y,z]))
testbench1 = []
for i in range(1000000):
    r = random.uniform(0, 1)
    theta = random.uniform(-np.pi, np.pi)
    z = random.uniform(-0.2, 1)
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    testbench1.append(np.array([x,y,z]))
testbench2 = []
for i in range(1000000):
    r1 = random.uniform(0, 1)
    theta1 = random.uniform(-np.pi, np.pi)
    x1 = r1 * np.cos(theta1)
    y1 = r1 * np.sin(theta1)
    r2 = random.uniform(0, 1)
    theta2 = random.uniform(-np.pi, np.pi)
    x2 = r2 * np.cos(theta2)
    y2 = r2 * np.sin(theta2)
    testbench2.append(np.array([x1,y1,x2,y2]))
"""

def compute_evaluation_atypes(data):
    _,actions0 = compute_evaluation(data, testbench0, 0)
    _,actions1 = compute_evaluation(data, testbench1, 1)
    _,actions2 = compute_evaluation(data, testbench2, 2)
    return [actions0, actions1, actions2]

## Computing repartitions of actions per task spaces using evaluation and performance

def compute_evaluation_perf_atypes(data):
    _,actions0b = compute_evaluation_perf(data, testbench0[:1000], 0)
    _,actions1b = compute_evaluation_perf(data, testbench1[:1000], 1)
    _,actions2b = compute_evaluation_perf(data, testbench2[:1000], 2)
    return [actions0b, actions1b, actions2b]

## Computing repartition of actions per task spaces using best locality ??

def compute_evaluation_interpol(data, testbench, task):
    """Compute evaluation for a given task."""
    
    actions = []
    for a in data.a_spaces:
        actions.append([])
        for a_s in a:
            actions[-1].append(0)
    
    # Will contain all evaluation information
    eva = []
    # Error sum on the testbench
    error_sum = 0.
    # Sum of quadratic error on testbench
    quad_error_sum = 0.
    for i, point in enumerate(testbench):
        #sys.stdout.write("\rProgress: " + str(i) + "/" + str(len(testbench)))
        #sys.stdout.flush()
        if len(data.y_spaces[task].data) == 0:
            dist = data.y_spaces[task].options['out_dist']
        else:
            _,_,a_type = data.best_locality(point, task)
            actions[a_type[0]][a_type[1]] += 1
    
    #print ""
    
    return actions

def compute_evaluation_interpol_atypes(data):
    actions0 = compute_evaluation_interpol(data, testbench0[:500], 0)
    actions1 = compute_evaluation_interpol(data, testbench1[:500], 1)
    actions2 = compute_evaluation_interpol(data, testbench2[:500], 2)
    return [actions0, actions1, actions2]


def compute_mean_evaluation_atypes(l):
    res = []
    for i, li in enumerate(l):  # For each actions repartition for all tasks (one sample)
        for j, a in enumerate(li): # For each task
            if len(res) <= j:
                res.append(np.zeros((1, 1)))
            res[j].resize((max(len(res[j]), len(a)), max(len(res[j][0]), len(a[0]))), refcheck=False)
            res[j][:len(a),:len(a[0])] += a
    for r in res:
        r /= float(len(l))
    return res


## Computing raw number of procedures

def count_procedures(data):
    return len(data.idEP)


def count_mean_procedures(l):
    return sum(l) / float(len(l))


## Computing raw number of procedures p_types

def count_procedures_ptype(data):
    proc = np.zeros((len(data.y_spaces), len(data.y_spaces)))
    if not hasattr(data, 'p_spaces'):
        return proc
    for i, p_s in enumerate(data.p_spaces):
        for j, p in enumerate(p_s):
            proc[i, j] = len(p.data)
    return proc


def count_all_procedures_ptype(l):
    return sum(l) / float(len(l))


## Computing raw number of procedures p_type per tasks

def count_procedures_task(data):
    proc = np.zeros((len(data.y_spaces), len(data.y_spaces), len(data.y_spaces)))
    if not hasattr(data, 'p_spaces'):
        return proc
    for idE in data.idEP:
        idP = data.idP[idE]
        idY = data.idY[idE]
        p_type = idP[0]
        for idy in idY:
            task = idy[0]
            proc[task, p_type[0], p_type[1]] += 1
    return proc


def count_all_procedures_task(l):
    return sum(l) / float(len(l))


## Computing will number of procedures per task

def count_will_procedures(la):
    """
    la LearningAgentV4: learner to check
    """
    outDim = len(la.dataset.y_spaces)
    res = np.zeros(outDim)
    i = 0
    for lp in la.learning_process:
        n_iter = lp[0]
        task = lp[3]
        for epmem in la._replayer_epmem_range(i, n_iter):
            idE = epmem-1
            if la.dataset.idP[idE] != None:
                res[task] += 1
        i = n_iter
    return res


def count_will_all_procedures(l):
    return sum(l)


## Computing will number of procedures per task

def count_will_p_types_tasks(la):
    """
    la LearningAgentV4: learner to check
    """
    outDim = len(la.dataset.y_spaces)
    proc = np.zeros((outDim, outDim, outDim))
    i = 0
    for lp in la.learning_process:
        n_iter = lp[0]
        task = lp[3]
        for epmem in la._replayer_epmem_range(i, n_iter):
            idE = epmem-1
            if la.dataset.idP[idE] != None:
                p_type = la.dataset.idP[idE][0]
                proc[task, p_type[0], p_type[1]] += 1
        i = n_iter
    return proc


def count_will_p_types_tasks_all(l):
    return sum(l) / float(len(l))


## Computing task repartition

def compute_task_repartition(la):
    outDim = len(la.dataset.y_spaces)
    res = np.zeros(outDim)
    for lp in la.learning_process:
        task = lp[3]
        res[task] += 1
    return res

def compute_all_task_repartition(l):
    return sum(l) / float(len(l))


## Computing task strategy repartition

def compute_mean_task_strat_repartition(l):
    return sum(l) / float(len(l))


# Compute progress history for each task

def compute_progress_history_task(la):
    prog = []
    for y_s in la.dataset.y_spaces:
        prog.append([])
    i = 0
    j = 1
    k = 0
    # For each learning episode
    while i < len(la.learning_process):
        lp = la.learning_process[i]
        n_iter = len(la.epmem_length)-1
        if i < len(la.learning_process) - 1:
            n_iter = la.learning_process[i+1][0]
        # Fo each attempt
        while j <= n_iter:
            size = la.epmem_length[j]
            attempt_prog = la.progresses[j-1]
            # For each part of the attempt
            for ide, p in zip(range(k, size), attempt_prog):
                for ((t,_), pp) in zip(la.dataset.idY[ide], p):
                    prog[t].append(pp)
            k = size
            j += 1
        prog[lp[3]].append(la.goal_progresses[i])
        i += 1
    return prog


# Compute progress history for each task with order remembered

def compute_progress_history_task_order(la):
    prog = []
    for y_s in la.dataset.y_spaces:
        prog.append([])
    i = 0
    j = 1
    k = 0
    # For each learning episode
    while i < len(la.learning_process):
        lp = la.learning_process[i]
        n_iter = len(la.epmem_length)-1
        if i < len(la.learning_process) - 1:
            n_iter = la.learning_process[i+1][0]
        # Fo each attempt
        while j <= n_iter:
            size = la.epmem_length[j]
            attempt_prog = la.progresses[j-1]
            # For each part of the attempt
            for ide, p in zip(range(k, size), attempt_prog):
                tasks = [True for p_s in prog]
                for ((t,_), pp) in zip(la.dataset.idY[ide], p):
                    prog[t].append(pp)
                    tasks[t] = False
                for t in range(len(tasks)):
                    if tasks[t]:
                        prog[t].append(0.0)
            k = size
            j += 1
        prog[lp[3]].append(la.goal_progresses[i])
        for t in range(len(prog)):
            if t != lp[3]:
                prog[t].append(0.0)
        i += 1
    return prog


# Compute evolution of strategy choice for a specific task


# Compute types of procedures chosen for evaluation

def compute_p_types_eval(data, testbench, task):
    """Compute evaluation for a given task."""
    
    proc = []
    for p in data.y_spaces:
        proc.append([])
        for p_s in data.y_spaces:
            proc[-1].append(0)
    for i, point in enumerate(testbench):
        #sys.stdout.write("\rProgress: " + str(i) + "/" + str(len(testbench)))
        #sys.stdout.flush()
        if len(data.y_spaces[task].data) == 0:
            break
        else:
            ids,dist = data.nn_y_procedural(point, task)
            if len(ids) == 0:
                break
            idE = data.y_spaces[task].ids[ids[0]]
            p_type = data.idP[idE][0]
            proc[p_type[0]][p_type[1]] += 1
    
    return proc


def compute_p_types_interpol(data, testbench, task):
    """Compute evaluation for a given task."""
    
    proc = []
    for p in data.y_spaces:
        proc.append([])
        for p_s in data.y_spaces:
            proc[-1].append(0)
    
    for i, point in enumerate(testbench):
        #sys.stdout.write("\rProgress: " + str(i) + "/" + str(len(testbench)))
        #sys.stdout.flush()
        if len(data.y_spaces[task].data) == 0:
            break
        else:
            y,_,p_type = data.best_locality_procedures(point, task)
            if len(y) == 0:
                break
            proc[p_type[0]][p_type[1]] += 1
    
    return proc


def compute_mean_evaluation_ptypes(l):
    res = []
    for i, li in enumerate(l):  # For each actions repartition for all tasks (one sample)
        for j, p in enumerate(li): # For each task
            if len(res) <= j:
                res.append(np.array(p))
            else:
                res[j] += np.array(p)
    for i in range(len(res)):
        res[i] = res[i] / float(len(l))
    return res


if __name__ == "__main__":
    
    import os, sys
    
    base = sys.argv[1]
    
    folder = "data/" + base + "/data/"

    analyzes = {}
    analyzes['max_a'] = []
    analyzes['a_repart'] = []
    analyzes['a_t_repart'] = []
    analyzes['will_a_repart'] = []
    analyzes['will_a_t_repart'] = []
    analyzes['eval_a_repart'] = []
    analyzes['eval_perf_a_repart'] = []
    analyzes['eval_inter_a_repart'] = []
    analyzes['nb_proc'] = []
    analyzes['nb_proc_type'] = []
    analyzes['proc_t_repart'] = []
    analyzes['will_proc_t'] = []
    analyzes['will_p_types_tasks'] = []

    interest = False
    #np.set_printoptions(suppress=True)
    
    for fi in os.listdir(folder):
        f = folder + fi
        try:
            eva = load_raw(f)
            print(f)
            la = eva.learning_agent
            data = la.dataset
            analyzes['max_a'].append(get_max_action_size(data))
            analyzes['a_repart'].append(count_action_repartition(data))
            analyzes['a_t_repart'].append(count_action_task_repartition(data))
            analyzes['will_a_repart'].append(count_will_action_repartition(la))
            analyzes['eval_a_repart'].append(compute_evaluation_atypes(data))
            analyzes['eval_perf_a_repart'].append(compute_evaluation_perf_atypes(data))
            analyzes['eval_inter_a_repart'].append(compute_evaluation_interpol_atypes(data))
            analyzes['nb_proc'].append(count_procedures(data))
            analyzes['nb_proc_type'].append(count_procedures_ptype(data))
            analyzes['proc_t_repart'].append(count_procedures_task(data))
            if isinstance(la, InterestAgentV4):
                analyzes['will_a_t_repart'].append(count_will_action_task_repartition(la))
                analyzes['will_proc_t'].append(count_will_procedures(la))
                analyzes['will_p_types_tasks'].append(count_will_p_types_tasks(la))
                interest = True
        except Exception:
            pass

    res = {}
    res['max_a'] = get_all_max_action_size(analyzes['max_a'])
    res['a_repart'] = count_all_action_repartition(analyzes['a_repart']).tolist()
    res['a_t_repart'] = count_all_action_task_repartition(analyzes['a_t_repart']).tolist()
    res['will_a_repart'] = count_will_all_action_repartition(analyzes['will_a_repart']).tolist()
    res['eval_a_repart'] = compute_mean_evaluation_atypes(analyzes['eval_a_repart'])
    for i in range(len(res['eval_a_repart'])):
        res['eval_a_repart'][i] = res['eval_a_repart'][i].tolist()
    res['eval_perf_a_repart'] = compute_mean_evaluation_atypes(analyzes['eval_perf_a_repart'])
    for i in range(len(res['eval_perf_a_repart'])):
        res['eval_perf_a_repart'][i] = res['eval_perf_a_repart'][i].tolist()
    res['eval_inter_a_repart'] = compute_mean_evaluation_atypes(analyzes['eval_inter_a_repart'])
    for i in range(len(res['eval_inter_a_repart'])):
        res['eval_inter_a_repart'][i] = res['eval_inter_a_repart'][i].tolist()
    res['nb_proc'] = count_mean_procedures(analyzes['nb_proc'])
    res['nb_proc_type'] = count_all_procedures_ptype(analyzes['nb_proc_type']).tolist()
    res['proc_t_repart'] = count_all_procedures_task(analyzes['proc_t_repart']).tolist()
    if interest:
        res['will_a_t_repart'] = count_will_all_action_task_repartition(analyzes['will_a_t_repart']).tolist()
        res['will_proc_t'] = count_will_all_procedures(analyzes['will_proc_t']).tolist()
        res['will_p_types_tasks'] = count_will_p_types_tasks_all(analyzes['will_p_types_tasks']).tolist()
    
    print(base + " analyzed!")
    save_json(res, base + "_analyze.json")
    
    
    
    
    


