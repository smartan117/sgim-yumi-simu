import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
import time

import copy
import random

from dataset import ActionSpace, OutcomeSpace, DatasetV2, SimpleDataset
from environment import EnvironmentV4
from evaluation_multiprocesses import EvaluationV4

from learning_strategies import RandomActionV4, RandomProcedure, DiscreteProceduralTeacher
from learning_agents import LearningAgentV4, InterestAgentV4, Mod, GoodRegionMod, GoodPointMod
from interest_models import InterestModelV3

from utils import load_json, save_json, load_raw, save_raw
from optparse import OptionParser

from scipy.spatial.distance import euclidean

import sys, os
sys.path.insert(0, "../poppycontrol")
import DMP





# Rules used to build procedural teachers


class VirtualArm2D:
    """Simulate a 2D robotic arm that can also be translated in z with no kinematic constraints."""
    
    def __init__(self, segments):
        """
        segments list of float: the length of each link of the arm
        """
        self.segments = segments
        self.angles = np.zeros(len(segments))
        self.anchor = np.zeros(2)
        # The robot has a joint at each end of a link
        #  the end effector is also among them
        #  each joint contains the following (x, y, erx, ery)
        #  where er is the output vector of the arm afterthe joint
        self.joints = np.zeros((len(segments)+1, 4))
        self.z = 0.0
        self.compute_end()
    
    def to_config(self):
        dico = {}
        dico['type'] = self.__class__.__name__
        dico['segments'] = self.segments.tolist()
        return dico
    
    @classmethod
    def from_config(cls, dico):
        return VirtualArm2D(np.array(dico['segments']))
    
    def set_anchor(self, anchor):
        """Set the position of the anchor of the robot."""
        self.anchor = anchor
        self.compute_end()
    
    def set_angles(self, angles):
        """Set the angles of the robot."""
        self.angles = angles
        self.compute_end()
    
    def set_z(self, z):
        """set the horizontal plan of the robot z."""
        self.z = z
    
    def compute_end(self):
        """Compute the position of each joint of the robot according to angles."""
        ex = np.array([1.0, 0.0])
        ey = np.array([0.0, 1.0])
        self.end = np.array(self.anchor, copy=True)
        
        for i in range(len(self.angles)):
            # Compute output unitary vectors
            e_r = np.cos(self.angles[i]) * ex + np.sin(self.angles[i]) * ey
            e_theta = -np.sin(self.angles[i]) * ex + np.cos(self.angles[i]) * ey
            ex = e_r
            ey = e_theta
            # Compute next joint position
            self.joints[i,0:2] = self.end
            self.joints[i,2:4] = e_r
            self.end += self.segments[i] * ex
        
        self.joints[-1,0:2] = self.end
    
    def plot(self, options = ['k', 'k.']):
        """Plot the arm in the 2D horizontal plan."""
        plt.plot(self.joints[:,0], self.joints[:,1], options[0])
        plt.plot(self.joints[0:-1,0], self.joints[0:-1,1], options[1])
    
    def plot3d(self, ax, options):
        """Plot the arm in the 3D space."""
        ax.plot(self.joints[:,0], self.joints[:,1], np.ones(len(self.joints)) * self.z, \
            color=options['color'][0], marker=options['marker'][0], linestyle=options['linestyle'][0])
        ax.plot(self.joints[0:-1,0], self.joints[0:-1,1], np.ones(len(self.joints)-1) * self.z, \
            color=options['color'][1], marker=options['marker'][1], linestyle=options['linestyle'][1])


class SceneObject:
    """An object in a 3D scene."""
    def __init__(self, name, x, y, z):
        self.name = name
        self.x = x
        self.y = y
        self.z = z
    
    def to_config(self):
        dico = {}
        dico['type'] = self.__class__.__name__
        dico['name'] = self.name
        dico['state'] = {}
        dico['state']['x'] = self.x
        dico['state']['y'] = self.y
        dico['state']['z'] = self.z
        return dico
    
    @classmethod
    def from_config(cls, dico):
        return SceneObject(dico['name'], dico['state']['x'], dico['state']['y'], dico['state']['z'])
    
    def plot(self, options='ok'):
        """Plot the object in the horizontal plan."""
        plt.plot(np.array(self.x), np.array(self.y), options)
    
    def plot3d(self, ax, options):
        """Plot the object in the 3D space."""
        ax.plot(np.array([self.x]), np.array([self.y]), np.array([self.z]), \
            color=options['color'], marker=options['marker'], linestyle=options['linestyle'])


class GrabableObject(SceneObject):
    """An object that can be grabed."""
    def __init__(self, name, x, y, z):
        SceneObject.__init__(self, name, x, y, z)
        # Reference to the object currently holding the grabable object
        self.graber = None
        # Indicate the state of the object (some object can be broken)
        self.working = True
    
    @classmethod
    def from_config(cls, dico):
        return GrabableObject(dico['name'], dico['state']['x'], dico['state']['y'], dico['state']['z'])
    
    def update_state(self):
        """Update the position of the object."""
        # A grabable object changes its position when grabed
        if self.graber != None:
            self.x = self.graber.x
            self.y = self.graber.y
            self.z = self.graber.z
    
    def grab(self, graber):
        """Grab the current object."""
        # The object cannot be grabed by two different objects at the same time."""
        if self.graber == None:
            self.graber = graber
    
    def release(self):
        """Release the object from its graber."""
        self.graber = None


class Joystick(GrabableObject):
    """A specific kind of grabable object that can only be moved inside a cubic space."""
    def __init__(self, name, x, y, z, bounds):
        """
        bounds dict of float arrays: the boundaries of the cubic space where the joystick can be moved
        """
        GrabableObject.__init__(self, name, x, y, z)
        # Contains two keys: min and max
        self.bounds = bounds
        # The given position is the anchor of the joystick (where it returns when released)
        self.anchor = (x, y, z)
    
    def to_config(self):
        dico = SceneObject.to_config(self)
        dico['bounds'] = {'min': self.bounds['min'].tolist(), 'max': self.bounds['max'].tolist()}
        return dico
    
    @classmethod
    def from_config(cls, dico):
        bounds = {'min': np.array(dico['bounds']['min']), 'max': np.array(dico['bounds']['max'])}
        return Joystick(dico['name'], dico['state']['x'], dico['state']['y'], dico['state']['z'], bounds)
    
    def be_released(self):
        """Check whether the joystick is inside its cubic space or not."""
        if self.x < self.bounds['min'][0] or self.x > self.bounds['max'][0]:
            return True
        if self.y < self.bounds['min'][1] or self.y > self.bounds['max'][1]:
            return True
        if self.z < self.bounds['min'][2] or self.z > self.bounds['max'][2]:
            return True
        return False
    
    def update_state(self):
        """Update the position of the joystick."""
        if self.graber != None:
            self.x = self.graber.x
            self.y = self.graber.y
            self.z = self.graber.z
            if self.be_released():
                self.graber.release()
                self.x = self.anchor[0]
                self.y = self.anchor[1]
                self.z = self.anchor[2]        


class Graber(SceneObject):
    """Object that can grab others."""
    def __init__(self, name, x, y, z):
        SceneObject.__init__(self, name, x, y, z)
        # Reference to the object currently grabed
        self.grabed = None
    
    def grab(self, obj):
        """Grab an object."""
        if self.grabed == None:
            # If no other object in hand just grab it
            obj.grab(self)
            if obj.graber == self:
                self.grabed = obj
        else: # Another item was grabed
            # Else both objects are destroyed
            #  but old object still in hand (other objects can still be destroyed!!!)
            self.grabed.working = False
            obj.working = False
    
    def release(self):
        """Release the currently grabed object."""
        if self.grabed != None:
            self.grabed.release()
            self.grabed = None


class Scene2D5:
    """The scene describing the environment with an arm, objects, a pen among them."""
    def __init__(self, arm, pen, options, fig_id, obj=None):
        """
        arm VirtualArm2D: the arm of the robot
        pen GrabableObject: pen usable by the robot to draw on the floor
        options dict: options of the scene
        fig_id int: the id of the figure on which to plot the scene (still mandatory)
        obj list of GrabableObject: objects present in the scene
        """
        self.arm = arm
        self.hand = Graber("Hand", self.arm.joints[-1,0], self.arm.joints[-1,1], self.arm.z)
        self.obj = obj
        self.pen = pen
        # Axis limits used when plotting the scene
        self.limits = [-1.5, 1.5, -1.5, 1.5, -1.5, 1.5]
        self.fig_id = fig_id
        # Options used to configure scene
        # z_max: max height of contact with floor
        # z_min: min height of contact with floor
        # z_broke: height where pen will break
        # d_pen: max distance to reach pen
        # d_obj: max distance to reach each object (list)
        # breakable: say if pen breakable or not
        self.options = options
        self.reset()
    
    def to_config(self):
        self.reset()
        dico = {}
        dico['type'] = self.__class__.__name__
        dico['arm'] = self.arm.to_config()
        dico['pen'] = self.pen.to_config()
        dico['objects'] = []
        for o in self.obj:
            dico['objects'].append(o.to_config())
        dico['options'] = self.options
        return dico
    
    @classmethod
    def from_config(cls, dico):
        arm = VirtualArm2D.from_config(dico['arm'])
        pen = GrabableObject.from_config(dico['pen'])
        obj = []
        for do in dico['objects']:
            obj.append(getattr(sys.modules[__name__], do['type']).from_config(do))
        return Scene2D5(arm, pen, dico['options'], 1, obj)
        
    def reset_state(self, anchor, angles, z, pen, obj=None):
        """Reset the scene to its initial state."""
        # Release whatever object is in the hand
        self.hand.release()
        # Reset the arm
        self.arm.set_anchor(anchor)
        self.arm.set_angles(angles)
        self.arm.set_z(z)
        # Reset the pen
        self.pen.x = pen[0]
        self.pen.y = pen[1]
        self.pen.z = pen[2]
        self.pen.working = True
        if self.obj:
            # Reset every object
            for i in range(len(self.obj)):
                self.obj[i].x = obj[i][0]
                self.obj[i].y = obj[i][1]
                self.obj[i].z = obj[i][2]
                self.obj[i].working = True
        self.reset()
        
    def reset(self):
        """Reset the positions' trackers."""
        self.drawing = []
        self.hand_pos = []
        self.pen_pos = []
        self.obj_pos = []
        if self.obj:
            for obj in self.obj:
                self.obj_pos.append([])
    
    def update(self, plot=True):
        """Update the state of the environment."""
        self.update_hand()
        self.update_pen()
        self.update_obj()
        if plot:
            self.plot()
        
    def update_hand(self):
        """Update the hand and store current position."""
        self.hand.x = self.arm.joints[-1,0]
        self.hand.y = self.arm.joints[-1,1]
        self.hand.z = max(self.arm.z, self.options['z_min'])
        self.hand_pos.append([self.hand.x, self.hand.y, self.hand.z])
    
    def update_obj(self):
        """Update the state and store the current position of each objects."""
        if not self.obj:
            return
        for i, obj in enumerate(self.obj):
            if obj.working:
                dist = math.sqrt((self.hand.x - obj.x)**2 + (self.hand.y - obj.y)**2 + (self.hand.z - obj.z)**2)
                
                if dist < self.options['d_obj'][i] and obj.graber == None:
                    self.hand.grab(obj)
                obj.update_state()
                self.obj_pos[i].append([obj.x, obj.y, obj.z])
            else:
                self.obj_pos[i] = []
    
    def update_pen(self):
        """Update the state of the pen and compute the drawings."""
        if self.pen.working:
            # Check distance hand - pen
            dist = math.sqrt((self.hand.x - self.pen.x)**2 + (self.hand.y - self.pen.y)**2 + (self.hand.z - self.pen.z)**2)
            
            # If distance low enough, grab pen
            if dist < self.options['d_pen'] and self.pen.graber == None:
                self.hand.grab(self.pen)
            self.pen.update_state()
            
            # If pen breakable, in hand and hand to low
            if self.options['breakable'] and self.pen.graber != None and self.arm.z < self.options['z_broke']:
                self.pen.working = False
            
            # If pen grabed and touching the floor
            if self.pen.graber != None and self.arm.z < self.options['z_max'] and self.pen.working:
                self.drawing.append([self.pen.x, self.pen.y])
            else:
                self.drawing.append(None)
            
            self.pen_pos.append([self.pen.x, self.pen.y, self.pen.z])
        else:
            self.pen_pos = []
    
    ##### All following plot functions are obsolete
    
    def plot_drawings(self, options):
        """Plot the last continuous drawing recorded in 2D."""
        i = 0
        while i < len(self.drawing):
            while i < len(self.drawing) and self.drawing[i] == None:
                i += 1
            p = []
            while i < len(self.drawing) and self.drawing[i] != None:
                p.append(self.drawing[i])
                i += 1
            if len(p) > 0:
                p = np.array(p)
                plt.plot(p[:,0], p[:,1], options)
    
    def plot_drawings3d(self, ax, options):
        """Plot the last continuous drawing recorded in 3D."""
        i = 0
        while i < len(self.drawing):
            while i < len(self.drawing) and self.drawing[i] == None:
                i += 1
            p = []
            while i < len(self.drawing) and self.drawing[i] != None:
                p.append(self.drawing[i])
                i += 1
            if len(p) > 0:
                p = np.array(p)
                ax.plot(p[:,0], p[:,1], -0.2 * ones(len(p)), color=options['color'], marker=options['marker'], linestyle=options['linestyle'])
    
    def plot_hand_pos(self, n=100, options='k.'):
        """Plot the trace of the recent hand positions."""
        l = min(n, len(self.hand_pos))
        p = np.array(self.hand_pos[(len(self.hand_pos)-l):len(self.hand_pos)])
        plt.plot(p[:,0], p[:,1], options)
    
    def plot_hand_pos3d(self, ax, options, n=100):
        """Plot the trace of the recent hand positions in 3D."""
        l = min(n, len(self.hand_pos))
        p = np.array(self.hand_pos[(len(self.hand_pos)-l):len(self.hand_pos)])
        ax.plot(p[:,0], p[:,1], p[:,2], color=options['color'], marker=options['marker'], linestyle=options['linestyle'])
    
    def plot_pen_pos(self, n=100, options='k.'):
        """Plot the trace of the recent pen positions."""
        l = min(n, len(self.pen_pos))
        p = np.array(self.pen_pos[(len(self.pen_pos)-l):len(self.pen_pos)])
        plt.plot(p[:,0], p[:,1], options)
    
    def plot_pen_pos3d(self, ax, options, n=100):
        """Plot the trace of the recent pen positions in 3D."""
        l = min(n, len(self.pen_pos))
        p = np.array(self.pen_pos[(len(self.pen_pos)-l):len(self.pen_pos)])
        ax.plot(p[:,0], p[:,1], p[:,2], color=options['color'], marker=options['marker'], linestyle=options['linestyle'])
    
    def plot(self):
        """Plot the scene in 2D."""
        plt.ion()
        plt.figure(self.fig_id)
        plt.show()
        plt.cla()
        self.plot_hand_pos()
        self.plot_pen_pos(options='r.')
        #self.plot_obj_pos(options='g.')
        self.arm.plot()
        self.hand.plot('ok')
        #self.obj.plot('og')
        if self.pen.working:
            self.pen.plot('or')
        else:
            self.pen.plot('xr')
        self.plot_drawings('b')
        plt.axis(self.limits[0:4])
        plt.draw()
    
    def plot3d(self):
        """Plot the scene in 3D."""
        plt.ion()
        fig = plt.figure(self.fig_id)
        plt.show()
        plt.clf()
        ax = fig.add_subplot(111, projection='3d')
        self.plot_hand_pos3d(ax, {'color': 'k', 'marker': '.', 'linestyle': 'None'})
        self.plot_pen_pos3d(ax, {'color': 'r', 'marker': '.', 'linestyle': 'None'})
        self.arm.plot3d(ax, {'color': ['k', 'k'], 'marker': ['', '.'], 'linestyle': ['solid','None']})
        self.hand.plot3d(ax, {'color': 'k', 'marker': 'o', 'linestyle': 'None'})
        if self.pen.working:
            self.pen.plot3d(ax, {'color': 'r', 'marker': 'o', 'linestyle': 'None'})
        else:
            self.pen.plot3d(ax, {'color': 'r', 'marker': 'x', 'linestyle': 'None'})
        self.plot_drawings3d(ax, {'color': 'b', 'marker': '', 'linestyle': 'solid'})
        ax.set_xlim(self.limits[0], self.limits[1])
        ax.set_ylim(self.limits[2], self.limits[3])
        ax.set_zlim(self.limits[4], self.limits[5])
        plt.draw()


class ComplexArmEnvironmentV5(EnvironmentV4):
    """Environment containing a scene with a virtual arm controllable by joint angular DMPs."""
    def __init__(self, a_spaces, y_spaces, scene, initial_pose, show=True):
        """
        a_spaces list of ActionSpace: list of all primitive action spaces available
        y_spaces list of OutcomeSpace: list of all outcome spaces in the environment
        scene Scene2D5: the scene containing the environment's state
        initial_pose float array: the position of the anchor of the virtual arm at each reset
        show boolean: indicates whether the scene will be plotted or not (TO NOT USE WHILE PLOT FUNCTIONS HAVEN'T BEEN UPDATED!!!)
        """
        EnvironmentV4.__init__(self, a_spaces, y_spaces)
        self.scene = scene
        self.initial_pose = initial_pose
        self.show = show
        # Contains all the objects initial position (for resetting the scene)
        self.obj = []
        for obj in self.scene.obj:
            self.obj.append([obj.x, obj.y, obj.z])
        self.obj = np.array(self.obj)
    
    def to_config(self):
        self.reset()
        dico = EnvironmentV4.to_config(self)
        dico['scene'] = self.scene.to_config()
        dico['initial_arm_pose'] = self.initial_pose.tolist()
        return dico
    
    @classmethod
    def from_config(cls, dico):
        a_spaces = []
        y_spaces = []
        for a_s in dico['a_spaces']:
            a_spaces.append(ActionSpace.from_config(a_s))
        for y_s in dico['y_spaces']:
            y_spaces.append(OutcomeSpace.from_config(y_s))
        scene = Scene2D5.from_config(dico['scene'])
        return ComplexArmEnvironmentV5(a_spaces, y_spaces, scene, np.array(dico['initial_arm_pose']), False)
    
    def reset(self):
        """Reset the environment."""
        self.reset_state(np.array([0.0, 0.0]), copy.deepcopy(self.initial_pose), 0.0, np.array([-0.3, 0.5, 1.0]), \
                self.obj)
    
    def reset_state(self, anchor, angles, z, pen, obj):
        """Reset the state of the scene."""
        self.scene.reset_state(anchor, angles, z, pen, obj)
    
    def a_to_dmp(self, a, a_type):
        """Extract DMP parameters from a primitive action."""
        assert a_type == 0
        n_bfs = 3
        cs = DMP.CanonicalSystem()
        dmp = DMP.DynamicSystem(cs=cs, tau=5.0)
        
        for j in range(len(self.scene.arm.angles)):
            ts = DMP.TransformationSystem(str(j), n_bfs=n_bfs, start=0., goal=np.pi * a[(n_bfs+1)*j + n_bfs], method="2012")
            weights = []
            for k in range(n_bfs):
                weights.append(100.0*a[(n_bfs+1)*j + k])
            c, h = ts.compute_bfs()
            ts.parameters = DMP.WeightedLinearRegression(c, h, np.array(weights))
            dmp.add_dmp(ts)
        z = a[len(a)-1]
        
        return dmp, z
    
    def execute_dmp(self, dmp, z):
        """Execute the DMP on the arm."""
        real_start = {}
        joints = []
        for i in range(len(self.scene.arm.angles)):
            real_start[str(i)] = self.scene.arm.angles[i]
            joints.append(str(i))
        #real_start["0"] = self.scene.arm.angles[0]
        #real_start["1"] = self.scene.arm.angles[1]
        #real_start["2"] = self.scene.arm.angles[2]
        dmp.set_start(real_start)
        dmp.reset()
        dt = 0.01
        if self.show:
            dt = 0.05
        
        while dmp.cs.t < 5.0:
            #self.scene.arm.set_angles(np.array(dmp.get_state(["0", "1", "2"])))
            self.scene.arm.set_angles(np.array(dmp.get_state(joints)))
            self.scene.arm.set_z(z)  # z >= 0.0
            self.scene.update(plot=self.show)
            if self.show:
                time.sleep(dt)
            dmp.step(dt)
    
    def compute_drawing(self):
        """Compute every pieces of drawing."""
        drawings = []
        i = 0
        while i < len(self.scene.drawing):
            while i < len(self.scene.drawing) and self.scene.drawing[i] == None:
                i += 1
            if i < len(self.scene.drawing):
                drawings.append([])
            while i < len(self.scene.drawing) and self.scene.drawing[i] != None:
                drawings[-1].append(self.scene.drawing[i])
                i += 1
        return drawings
    
    def retrieve_outcomes(self):
        y_list = []
        y_types = []
        
        y_hand = self.scene.hand_pos[-1]
        self.normalize_y(y_hand, 0)
        y_list.append(np.array(y_hand))
        y_types.append(0)
        
        if self.scene.pen.graber != None and self.scene.pen.working:
            y_pen = self.scene.pen_pos[-1]
            self.normalize_y(y_pen, 1)
            y_list.append(np.array(y_pen))
            y_types.append(1)
        
        for i, obj in enumerate(self.scene.obj):
            if isinstance(obj, GrabableObject) and obj.graber != None and obj.working:
                y_obj = np.array(self.scene.obj_pos[i][-1])
                if isinstance(obj, Joystick):
                    y_obj = (y_obj - obj.bounds['min']) * 2.0 / (obj.bounds['max'] - obj.bounds['min']) - 1.0
                self.normalize_y(y_obj, 3+i)
                y_list.append(y_obj)
                y_types.append(3+i)
        
        drawings = self.compute_drawing()
        if len(drawings) > 0 and self.scene.pen.working:
            y_draw = np.zeros(4)
            y_draw[0:2] = drawings[-1][0]
            y_draw[2:4] = drawings[-1][-1]
            self.normalize_y(y_draw, 2)
            y_list.append(y_draw)
            y_types.append(2)
        
        return y_list, y_types
    
    def execute(self, a, a_type):
        """Execute an action, given context and return them normalized plus the outcomes."""
        assert a_type < len(self.a_spaces)
        assert self.a_spaces[a_type].dim == len(a)
        
        # Create DMP from action
        self.normalize_a(a, a_type)
        dmp, z = self.a_to_dmp(a, a_type)
        
        # Execute DMP
        self.execute_dmp(dmp, z)
        
        # Retrieve outcomes
        return self.retrieve_outcomes()


class DirtyRemote:
    
    def __init__(self, remote1, remote2):
        self.remote1 = remote1
        self.remote2 = remote2
        self.reset()
    
    def to_config(self):
        dico = {}
        dico['type'] = self.__class__.__name__
        dico['remote1'] = self.remote1
        dico['remote2'] = self.remote2
        return dico
    
    @classmethod
    def from_config(cls, dico):
        return DirtyRemote(dico['remote1'], dico['remote2'])
    
    def reset(self):
        self.x = 0.0
        self.y = 0.0
    
    def move_character(self, y_list, y_types):
        moved = False
        for yi, ti in zip(y_list, y_types):
            if ti == self.remote1:
                self.x = yi[0]
                moved = True
            if ti == self.remote2:
                self.y = yi[1]
                moved = True
        return moved


class ComplexArmEnvironmentV6(ComplexArmEnvironmentV5):
    
    def __init__(self, a_spaces, y_spaces, scene, initial_pose, dirty, show=True):
        ComplexArmEnvironmentV5.__init__(self, a_spaces, y_spaces, scene, initial_pose, show)
        self.dirty = dirty
    
    def to_config(self):
        dico = ComplexArmEnvironmentV5.to_config(self)
        dico['dirty'] = []
        for d in self.dirty:
            dico['dirty'].append(d.to_config())
        return dico
    
    @classmethod
    def from_config(cls, dico):
        a_spaces = []
        y_spaces = []
        for a_s in dico['a_spaces']:
            a_spaces.append(ActionSpace.from_config(a_s))
        for y_s in dico['y_spaces']:
            y_spaces.append(OutcomeSpace.from_config(y_s))
        scene = Scene2D5.from_config(dico['scene'])
        dirty = []
        for d in dico['dirty']:
            dirty.append(DirtyRemote.from_config(d))
        return ComplexArmEnvironmentV6(a_spaces, y_spaces, scene, np.array(dico['initial_arm_pose']), dirty, False)
    
    def reset(self):
        ComplexArmEnvironmentV5.reset(self)
        for dirty in self.dirty:
            dirty.reset()
    
    def retrieve_outcomes(self):
        y_list, y_types = ComplexArmEnvironmentV5.retrieve_outcomes(self)
        for i, obj in enumerate(self.dirty):
            if isinstance(obj, DirtyRemote) and obj.move_character(y_list, y_types):
                y_obj = np.array([obj.x, obj.y])
                y_list.append(y_obj)
                y_types.append(3 + len(self.scene.obj) + i)
        
        return y_list, y_types


class ComplexArmEnvironmentV7(ComplexArmEnvironmentV6):
    
    def __init__(self, a_spaces, y_spaces, scene, initial_pose, dirty, show=True):
        ComplexArmEnvironmentV6.__init__(self, a_spaces, y_spaces, scene, initial_pose, dirty, show=show)
        self.n = len(y_spaces) - len(dirty) - len(self.scene.obj) - 3
    
    def retrieve_outcomes(self):
        y_list, y_types = ComplexArmEnvironmentV6.retrieve_outcomes(self)
        drawings = self.compute_drawing()
        if len(drawings) > 0 and self.scene.pen.working:
            y = drawings[0][0] + drawings[0][-1]
            for i in range(min(self.n, len(drawings)-1)):
                task = i + len(self.dirty) + len(self.scene.obj) + 3
                y += drawings[i+1][-1]
                yt = np.array(y)
                self.normalize_y(yt, task)
                y_list.append(yt)
                y_types.append(task)
        return y_list, y_types


class ComplexArmEnvironmentV8(ComplexArmEnvironmentV6):
    
    def __init__(self, a_spaces, y_spaces, scene, initial_pose, dirty, d_hole, show=True):
        ComplexArmEnvironmentV6.__init__(self, a_spaces, y_spaces, scene, initial_pose, dirty, show=show)
        self.d_hole = d_hole
        self.y_player = None
    
    def reset(self):
        ComplexArmEnvironmentV6.reset(self)
        self.y_player = None
    
    def retrieve_outcomes(self):
        y_list, y_types = ComplexArmEnvironmentV6.retrieve_outcomes(self)
        y_hole = None
        
        # Crafting a new outcome from drawing and player
        for y, task in zip(y_list, y_types):
            if task == 2:
                y_hole = y[2:]
            if task == 5:
                self.y_player = y
        if self.y_player != None and y_hole != None:
            d = euclidean(self.y_player, y_hole)
            if d < self.d_hole:
                y_types.append(len(self.y_spaces)-1)
                y_list.append(y_hole)
        
        return y_list, y_types
    
    def to_config(self):
        dico = ComplexArmEnvironmentV6.to_config(self)
        dico['d_hole'] = self.d_hole
        return dico
    
    @classmethod
    def from_config(cls, dico):
        a_spaces = []
        y_spaces = []
        for a_s in dico['a_spaces']:
            a_spaces.append(ActionSpace.from_config(a_s))
        for y_s in dico['y_spaces']:
            y_spaces.append(OutcomeSpace.from_config(y_s))
        scene = Scene2D5.from_config(dico['scene'])
        dirty = []
        for d in dico['dirty']:
            dirty.append(DirtyRemote.from_config(d))
        d_hole = dico['d_hole']
        return ComplexArmEnvironmentV8(a_spaces, y_spaces, scene, np.array(dico['initial_arm_pose']), dirty, d_hole, False)
    


######## TO KEEP IMPLEMENTING

class SkaroExperiment:
    
    def __init__(self):
        a1 = ActionSpace({'min': -np.ones(13), 'max': np.ones(13)}, 1, "A")
        
        y1 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Hand Pose")
        y2 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Pen pose")
        y3 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "Drawing")
        #y4 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Obj pose 0")
        #y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 0")
        
        arm = VirtualArm2D(np.array([0.5, 0.3, 0.2]))
        pen = GrabableObject("Pen", -0.3, 0.5, 1.0)
        
        obj0 = GrabableObject("Obj0", 0.2, 0.4, 0.0)
        bnds = {'min': np.array([-0.8, 0.2, 0.4]), 'max': np.array([-0.4, 0.6, 0.8])}
        #joy = Joystick("Joystick0", -0.6, 0.4 , 0.6, bnds)
        
        #obj = [obj0]
        
        scene_op = {'breakable': True, 'z_max': 0.0, \
            'z_min': -0.2, 'd_pen': 0.05, 'd_obj': [0.1, 0.08], 'z_broke': -0.3}
        scene = Scene2D5(arm, pen, scene_op, 1, [])
        initial_pose = np.array([np.pi/3.0, -np.pi/5.0, -np.pi/4.0])
        
        
        self.data = DatasetV2([a1], [y1, y2, y3])
        self.env = ComplexArmEnvironmentV5([a1], [y1, y2, y3], scene, initial_pose, False)
    
    @classmethod
    def create_testbench(cls):
        testbench = [[],[],[]]
        # Testbench for task space 0 and 1 and 3
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                for z in np.linspace(-1, 1, num=10):
                    testbench[0].append(np.array([x, y, z]))
                    testbench[1].append(np.array([x, y, z]))
        for x1 in np.linspace(-1, 1, num=10):
            for y1 in np.linspace(-1, 1, num=10):
                for x2 in np.linspace(-1, 1, num=10):
                    for y2 in np.linspace(-1, 1, num=10):
                        testbench[2].append(np.array([x1, y1, x2, y2]))
        return testbench
    
    @classmethod
    def save_default_eval_config(cls, testbench_filename):
        save_raw(cls.create_testbench(), testbench_filename)


class SkaroExperimentV2:
    t = [0, 100, 500, 1000, 2000, 4000, 6000, 8000, 10000, 15000, 20000, 25000]
    def __init__(self):
        a1 = ActionSpace({'min': -np.ones(13), 'max': np.ones(13)}, 1, "A")
        
        y1 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Hand Pose")
        y2 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Pen pose")
        y3 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "Drawing")
        y4 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Obj pose 0")
        y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 0")
        y6 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 1")
        y7 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "Player")
        
        arm = VirtualArm2D(np.array([0.5, 0.3, 0.2]))
        pen = GrabableObject("Pen", -0.3, 0.5, 1.0)
        
        obj0 = GrabableObject("Obj0", 0.2, 0.4, 0.0)
        bnds0 = {'min': np.array([-0.8, 0.2, 0.4]), 'max': np.array([-0.4, 0.6, 0.8])}
        bnds1 = {'min': np.array([-0.3, -0.7, 0.4]), 'max': np.array([0.1, -0.3, 0.8])}
        joy0 = Joystick("Joystick0", -0.6, 0.4 , 0.6, bnds0)
        joy1 = Joystick("Joystick1", -0.1, -0.5 , 0.6, bnds1)
        
        dirty = DirtyRemote(4, 5)
        
        obj = [obj0, joy0, joy1]
        
        scene_op = {'breakable': True, 'z_max': 0.0, \
            'z_min': -0.2, 'd_pen': 0.05, 'd_obj': [0.1, 0.08, 0.08], 'z_broke': -0.3}
        scene = Scene2D5(arm, pen, scene_op, 1, obj)
        initial_pose = np.array([np.pi/3.0, -np.pi/5.0, -np.pi/4.0])
        
        
        self.data = DatasetV2([a1], [y1, y2, y3, y4, y5, y6, y7])
        self.env = ComplexArmEnvironmentV6([a1], [y1, y2, y3, y4, y5, y6, y7], scene, initial_pose, [dirty], False)
    
    @classmethod
    def create_testbench(cls):
        testbench = [[],[],[],[],[],[],[]]
        # Testbench for task space 0 and 1 and 3
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                for z in np.linspace(-1, 1, num=10):
                    testbench[0].append(np.array([x, y, z]))
                    testbench[1].append(np.array([x, y, z]))
                    testbench[3].append(np.array([x, y, z]))
                    testbench[4].append(np.array([x, y, z]))
                    testbench[5].append(np.array([x, y, z]))
                    #testbench[6].append(np.array([x, y, z]))
        for x1 in np.linspace(-1, 1, num=10):
            for y1 in np.linspace(-1, 1, num=10):
                for x2 in np.linspace(-1, 1, num=10):
                    for y2 in np.linspace(-1, 1, num=10):
                        testbench[2].append(np.array([x1, y1, x2, y2]))
        for x in np.linspace(-1, 1, num=40):
            for y in np.linspace(-1, 1, num=40):
                testbench[6].append(np.array([x, y]))
        return testbench
    
    @classmethod
    def save_default_eval_config(cls, testbench_filename):
        save_raw(cls.create_testbench(), testbench_filename)


class SkaroExperimentV3:
    t = [0, 100, 500, 1000, 2000, 4000, 6000, 8000, 10000, 15000, 20000, 25000]
    def __init__(self):
        a1 = ActionSpace({'min': -np.ones(13), 'max': np.ones(13)}, 1, "A")
        
        y1 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Hand Pose")
        y2 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Pen pose")
        y3 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "Drawing")
        y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 0")
        y6 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 1")
        y7 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "Player")
        y8 = OutcomeSpace({'min': -np.ones(6), 'max': np.ones(6)}, "Drawing2")
        
        arm = VirtualArm2D(np.array([0.5, 0.3, 0.2]))
        pen = GrabableObject("Pen", -0.3, 0.5, 1.0)
        
        obj0 = GrabableObject("Obj0", 0.2, 0.4, 0.0)
        bnds0 = {'min': np.array([-0.8, 0.2, 0.4]), 'max': np.array([-0.4, 0.6, 0.8])}
        bnds1 = {'min': np.array([-0.3, -0.7, 0.4]), 'max': np.array([0.1, -0.3, 0.8])}
        joy0 = Joystick("Joystick0", -0.6, 0.4 , 0.6, bnds0)
        joy1 = Joystick("Joystick1", -0.1, -0.5 , 0.6, bnds1)
        
        dirty = DirtyRemote(4, 5)
        
        obj = [joy0, joy1]
        
        scene_op = {'breakable': True, 'z_max': 0.0, \
            'z_min': -0.2, 'd_pen': 0.05, 'd_obj': [0.08, 0.08], 'z_broke': -0.3}
        scene = Scene2D5(arm, pen, scene_op, 1, obj)
        initial_pose = np.array([np.pi/3.0, -np.pi/5.0, -np.pi/4.0])
        
        
        self.data = DatasetV2([a1], [y1, y2, y3, y5, y6, y7, y8])
        self.env = ComplexArmEnvironmentV6([a1], [y1, y2, y3, y5, y6, y7, y8], scene, initial_pose, [dirty], False)
    
    @classmethod
    def create_testbench(cls):
        testbench = [[],[],[],[],[],[],[]]
        # Testbench for task space 0 and 1 and 3
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                for z in np.linspace(-1, 1, num=10):
                    testbench[0].append(np.array([x, y, z]))
                    testbench[1].append(np.array([x, y, z]))
                    testbench[3].append(np.array([x, y, z]))
                    testbench[4].append(np.array([x, y, z]))
        for x1 in np.linspace(-1, 1, num=10):
            for y1 in np.linspace(-1, 1, num=10):
                for x2 in np.linspace(-1, 1, num=10):
                    for y2 in np.linspace(-1, 1, num=10):
                        testbench[2].append(np.array([x1, y1, x2, y2]))
        for x in np.linspace(-1, 1, num=40):
            for y in np.linspace(-1, 1, num=40):
                testbench[5].append(np.array([x, y]))
        for x1 in np.linspace(-1, 1, num=5):
            for y1 in np.linspace(-1, 1, num=5):
                for x2 in np.linspace(-1, 1, num=5):
                    for y2 in np.linspace(-1, 1, num=5):
                        for x3 in np.linspace(-1, 1, num=5):
                            for y3 in np.linspace(-1, 1, num=5):
                                testbench[6].append(np.array([x1, y1, x2, y2, x3, y3]))
        return testbench
    
    @classmethod
    def save_default_eval_config(cls, testbench_filename):
        save_raw(cls.create_testbench(), testbench_filename)


class SkaroExperimentV4:
    t = [0, 100, 500, 1000, 2000, 4000, 6000, 8000, 10000, 15000, 20000, 25000]
    def __init__(self, nb_joints):
        inDim = nb_joints*4 + 1
        a1 = ActionSpace({'min': -np.ones(inDim), 'max': np.ones(inDim)}, 1, "A")
        
        y1 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Hand Pose")
        y2 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Pen pose")
        y3 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "Drawing")
        y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 0")
        y6 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 1")
        y7 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "Player")
        
        arm = VirtualArm2D(np.ones(nb_joints) / float(nb_joints))
        pen = GrabableObject("Pen", -0.3, 0.5, 1.0)
        
        bnds0 = {'min': np.array([-0.8, 0.2, 0.4]), 'max': np.array([-0.4, 0.6, 0.8])}
        bnds1 = {'min': np.array([-0.3, -0.7, 0.4]), 'max': np.array([0.1, -0.3, 0.8])}
        joy0 = Joystick("Joystick0", -0.6, 0.4 , 0.6, bnds0)
        joy1 = Joystick("Joystick1", -0.1, -0.5 , 0.6, bnds1)
        
        dirty = DirtyRemote(3, 4)
        
        obj = [joy0, joy1]
        
        scene_op = {'breakable': True, 'z_max': 0.0, \
            'z_min': -0.2, 'd_pen': 0.05, 'd_obj': [0.05, 0.05], 'z_broke': -0.3}
        scene = Scene2D5(arm, pen, scene_op, 1, obj)
        initial_pose = np.ones(nb_joints) * np.pi / 8.0
        
        
        self.data = DatasetV2([a1], [y1, y2, y3, y5, y6, y7])
        self.env = ComplexArmEnvironmentV6([a1], [y1, y2, y3, y5, y6, y7], scene, initial_pose, [dirty], False)
    
    @classmethod
    def create_testbench(cls):
        testbench = [[],[],[],[],[],[]]
        # Testbench for task space 0 and 1 and 3
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                for z in np.linspace(-1, 1, num=10):
                    testbench[0].append(np.array([x, y, z]))
                    testbench[1].append(np.array([x, y, z]))
                    testbench[3].append(np.array([x, y, z]))
                    testbench[4].append(np.array([x, y, z]))
                    #testbench[6].append(np.array([x, y, z]))
        for x1 in np.linspace(-1, 1, num=10):
            for y1 in np.linspace(-1, 1, num=10):
                for x2 in np.linspace(-1, 1, num=10):
                    for y2 in np.linspace(-1, 1, num=10):
                        testbench[2].append(np.array([x1, y1, x2, y2]))
        for x in np.linspace(-1, 1, num=40):
            for y in np.linspace(-1, 1, num=40):
                testbench[5].append(np.array([x, y]))
        return testbench
    
    @classmethod
    def save_default_eval_config(cls, testbench_filename):
        save_raw(cls.create_testbench(), testbench_filename)


class SkaroExperimentV5:
    t = [0, 100, 500, 1000, 2000, 4000, 6000, 8000, 10000, 15000, 20000, 25000]
    def __init__(self, nb_joints):
        inDim = nb_joints*4 + 1
        a1 = ActionSpace({'min': -np.ones(inDim), 'max': np.ones(inDim)}, 1, "A")
        
        y1 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Hand Pose")
        y2 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Pen pose")
        y3 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "Drawing")
        y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 0")
        y6 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 1")
        y7 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "Player")
        y8 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "Hole")
        
        arm = VirtualArm2D(np.ones(nb_joints) / float(nb_joints))
        pen = GrabableObject("Pen", -0.3, 0.5, 1.0)
        
        bnds0 = {'min': np.array([-0.8, 0.2, 0.4]), 'max': np.array([-0.4, 0.6, 0.8])}
        bnds1 = {'min': np.array([-0.3, -0.7, 0.4]), 'max': np.array([0.1, -0.3, 0.8])}
        joy0 = Joystick("Joystick0", -0.6, 0.4 , 0.6, bnds0)
        joy1 = Joystick("Joystick1", -0.1, -0.5 , 0.6, bnds1)
        
        dirty = DirtyRemote(3, 4)
        
        obj = [joy0, joy1]
        
        scene_op = {'breakable': True, 'z_max': 0.0, \
            'z_min': -0.2, 'd_pen': 0.05, 'd_obj': [0.1, 0.1], 'z_broke': -0.3}
        scene = Scene2D5(arm, pen, scene_op, 1, obj)
        initial_pose = np.ones(nb_joints) * np.pi / 8.0
        
        
        self.data = DatasetV2([a1], [y1, y2, y3, y5, y6, y7, y8])
        self.env = ComplexArmEnvironmentV8([a1], [y1, y2, y3, y5, y6, y7, y8], scene, initial_pose, [dirty], 0.2, False)
    
    @classmethod
    def create_testbench(cls):
        testbench = [[],[],[],[],[],[],[]]
        # Testbench for task space 0 and 1 and 3
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                for z in np.linspace(-1, 1, num=10):
                    testbench[0].append(np.array([x, y, z]))
                    testbench[1].append(np.array([x, y, z]))
                    testbench[3].append(np.array([x, y, z]))
                    testbench[4].append(np.array([x, y, z]))
                    #testbench[6].append(np.array([x, y, z]))
        for x1 in np.linspace(-1, 1, num=10):
            for y1 in np.linspace(-1, 1, num=10):
                for x2 in np.linspace(-1, 1, num=10):
                    for y2 in np.linspace(-1, 1, num=10):
                        testbench[2].append(np.array([x1, y1, x2, y2]))
        for x in np.linspace(-1, 1, num=40):
            for y in np.linspace(-1, 1, num=40):
                testbench[5].append(np.array([x, y]))
                testbench[6].append(np.array([x, y]))
        return testbench
    
    @classmethod
    def save_default_eval_config(cls, testbench_filename):
        save_raw(cls.create_testbench(), testbench_filename)


def save_env_to_config_file(env, filename):
    dico = env.to_config()
    save_json(dico, filename)


def save_teacher(dataset, teacher_folder):
    dico = {}
    dico['type'] = dataset.__class__.__name__
    dico['y_spaces'] = []
    n_demos = 0
    for y in dataset.y_spaces:
        l = {}
        l['name'] = y.name
        l['demos'] = len(y.data)
        n_demos += len(y.data)
        dico['y_spaces'].append(l)
    if len(dataset.idEP) == 0:
        dico['procedures'] = "No"
    elif len(dataset.idEP) == n_demos:
        dico['procedures'] = "Only"
    else:
        dico['procedures'] = "Some"
    save_json(dico, teacher_folder + ".json")
    save_raw(dataset, teacher_folder + ".data")


def save_la_to_config_file(la, filename_la, filenames_teachers):
    dico = la.to_config()
    for ds, ft in zip(dico['strategies'], filenames_teachers):
        if ft and len(ft) > 0:
            ds['dataset'] = ft
    save_json(dico, filename_la)


def save_testfile_config(testfile_config, la_config, eval_config, testfiles, n_iter):
    dico = {}
    dico['learning_agent'] = la_config
    dico['evaluation'] = eval_config
    dico['outputfiles'] = testfiles
    dico['n_iter'] = n_iter
    save_json(dico, testfile_config)


def load_env_from_config_file(filename):
    dico = load_json(filename)
    return getattr(sys.modules[__name__], dico['type']).from_config(dico)


def load_la_from_config_file(filename, env):
    dico = load_json(filename)
    strat = []
    if dico['dataset']['type'] == "SimpleDataset":
        dataset = None
    else:
        dataset = getattr(sys.modules[__name__], dico['dataset']['type']).from_config(env.to_config(), dico['dataset']['options'])
    
    for s in dico['strategies']:
        dataset_t = None
        if s.has_key("dataset"):
            dataset_t = load_raw(find_teacher(s['dataset']))
        if dico['dataset']['type'] == "SimpleDataset" and dataset == None:
            action_length = s['complex_actions']
            dataset = getattr(sys.modules[__name__], dico['dataset']['type']).from_config(env.to_config(), dico['dataset']['options'], action_length)
        newStrat = getattr(sys.modules["learning_strategies"], s['type']).from_config(s, env, dataset_t, dataset)
        strat.append(newStrat)
    if dico.has_key('interest_model'):
        im = InterestModelV3(dataset, dico['interest_model']['options'])
        mods = []
        for m in dico['mods']:
            mods.append(getattr(sys.modules[__name__], m['name'])(im, m['prob']))
        return getattr(sys.modules[__name__], dico['type'])(dataset, strat, mods, im, env)
    else:
        return getattr(sys.modules[__name__], dico['type'])(dataset, strat, env)


def load_from_config(name):
    config = load_json(find_test(name))
    if config.has_key('environment'):
        env = load_env_from_config_file(find_env_file(config['environment']))
        if config.has_key('testbench'):
            testbench = load_raw(find_eval_testbench(config['environment'], config['testbench']))
        else:
            testbench = load_raw(find_eval_testbench(config['environment']))
    else:
        env = load_env_from_config_file(find_test_folder(name) + "environment.json")
        testbench = load_raw(find_test_folder(name) + "default.testbench")
    if config.has_key('learner'):
        la = load_la_from_config_file(find_la(config['learner']), env)
    else:
        la = load_la_from_config_file(find_test_folder(name) + "learner.json", env)
    if config.has_key('evaluation'):
        eval_conf = load_json(find_eval(config['evaluation']))
    else:
        eval_conf = load_json(find_test_folder(name) + "evaluation.json")
    
    return la, config, eval_conf, testbench


def load_testfiles(test):
    folder = find_test_folder(test) + "data/"
    evas = []
    for fi in os.listdir(folder):
        evas.append(load_raw(fi))
    return evas


def save_progress(eva, test, testfile, t=None):
    if t != None:
        f = find_test_folder(test) + "data/" + testfile + "_" + str(t)
    else:
        f = find_test_folder(test) + "data/" + testfile
    save_raw(eva, f)
    for fi in os.listdir(find_test_folder(test) + "data/"):
        i = fi.find(testfile)
        if i == 0 and len(fi) > len(testfile) and fi[len(testfile)] == '_':
            if t == None or int(fi[(len(testfile)+1):]) < t:
                os.remove(find_test_folder(test) + "data/" + fi)


def find_env_file(name):
    return "config/environments/" + name + "/" + name + ".json"


def find_eval_testbench(name_env, name_testbench=None):
    if name_testbench:
        return "config/environments/" + name_env + "/" + name_testbench + ".testbench"
    return "config/environments/" + name_env + "/" + name_env + ".testbench"


def find_eval(name):
    return "config/evaluations/" + name + ".json"


def find_teacher(name): # name contains one subfolder at least
    return "teachers/" + name + ".data"


def find_test(name):
    return find_test_folder(name) + "config.json"


def find_test_folder(name):
    return "data/" + name + "/"


def find_la(name):
    return "config/learners/" + name + ".json"


def remove_file(name):
    if os.path.isfile(name):
        os.remove(name)


def main():
    #parser = OptionParser()
    #parser.add_option("-c", "--config", dest="config", type="string", metavar="FICHIER",action="store", help="config file for the learning agent")
    #parser.add_option("-e", "--eval", dest="eval", type="string", metavar="FICHIER",action="store", help="config file for the evaluation")
    #(options, args) = parser.parse_args()
    pass


def get_folder(path):
    for i in range(len(path)-1, -1, -1):
        if path[i] == '/' or path[i] == '\\':
            return path[:i]
    return "."
    
    
if __name__ == "__main__":
    if len(sys.argv) > 1:
        test = sys.argv[1]
    else:
        raise Exception("No test name given!")
    
    if len(sys.argv) > 2:
        testfile = sys.argv[2]
    else:
        raise Exception("No test file given!")
    
    folder = find_test_folder(test) + "data"
    if not os.path.exists(folder):
        os.makedirs(folder)
    
    la, config, eval_conf, testbench = load_from_config(test)
    n_iter = config['n_iter']
    save_iter = config['save']
    t = eval_conf['t']
    
    eva = EvaluationV4(la, testbench)
    
    print("[" + test + "] " + testfile + " started")
    
    next_iter = 0
    while next_iter < n_iter:
        next_iter += save_iter
        next_iter = min(next_iter, n_iter)
        eva.learning_agent.run(next_iter)
        i = 0
        while i < len(t) and t[i] <= next_iter:
            i += 1
        for n in range(len(eva.evaluation), i):
            eva.evaluate(t[n])
        if next_iter < n_iter:
            save_progress(eva.get_lw(), test, testfile, next_iter)
    
    save_progress(eva.get_lw(), test, testfile)
    print("[" + test + "] " + testfile + " finished")
    
    
    
    
    
    
    