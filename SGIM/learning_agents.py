import numpy as np
import random
import copy
import math

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from interest_models import InterestRegion, InterestTree, InterestModelV3

from utils import uniform_sampling, save_json, save_str, load_json, load_str, display_progress


import time





class Mod:
    """Implements the SGIM sampling mods."""
    name = "Random"
    def __init__(self, interest_model, prob):
        """
        sample func: function called when sampling with this mod
        prob float: probability of this mod to occur
        """
        self.prob = prob
        self.interest_model = interest_model
    
    def to_config(self):
        dico = {}
        dico['name'] = self.__class__.__name__
        dico['prob'] = self.prob
        return dico
    
    def sample(self):
        return self.interest_model.sample_random()


class GoodRegionMod(Mod):
    """Mod corresponding to SGIM Mod 1."""
    name = "Good Region"
    def sample(self):
        return self.interest_model.sample_good_point()


class GoodPointMod(Mod):
    """Mod corresponding to SGIM Mod 2."""
    name = "Good Point"    
    def sample(self):
        return self.interest_model.sample_best_point()


class ReplayStats:
    
    def __init__(self, episodes):
        self.episodes = episodes
    
    def __str__(self):
        s = ""
        for i, e in enumerate(self.episodes):
            s += "Episode " + str(i) + "\n\n" + str(e) + "\n\n---------------------------\n"
        return s


class Episode:
    
    def __init__(self, iterations, strategy, task, goal, mod, goal_progress):
        self.iterations = iterations  # list of list of attempts
        self.strategy = strategy
        self.task = task
        self.goal = goal
        self.mod = mod
        self.goal_progress = goal_progress
        self.stats = None
    
    def process(self, im):
        for it in self.iterations:
            for att in it:
                att.process(im, self.strategy)
        im.add_point(self.goal, self.task, self.strategy, self.goal_progress)
        self.stats = im.get_stats()
    
    def __str__(self):
        return "Mod: " + str(self.mod) + "\nTarget: Task " + str(self.task) + " Strategy " + str(self.strategy) + \
                " Goal " + str(self.goal) + "\nGoal progress: " + str(self.goal_progress) + "\n\n" + str(self.stats)


class Attempt: # A specific action tried
    
    def __init__(self, action, outcomes, progresses, procedure=None):
        self.action = action
        self.outcomes = outcomes
        self.procedure = procedure
        self.progresses = progresses
    
    def process(self, im, strat):
        for outcome, progress in zip(self.outcomes, self.progresses):
            im.add_point(outcome[1], outcome[0], strat, progress)


class LearningAgentV5:
    
    def __init__(self, dataset, strat, env, verbose=False):
        self.dataset = dataset
        self.strategies = strat
        self.environment = env
        self.learning_process = []
        self.epmem_length = [0]  # Track the correspondence between iteration and episodic memory length (necessary for complex actions)
        self.n_iter = 0
        self.verbose = verbose
        self.lastEpisode = None
    
    def run(self, n_iter):
        """Run the learner until max number of iterations."""
        while self.n_iter < n_iter:
            # Choose learning strategy randomly
            strat = random.randint(0, len(self.strategies)-1)
            if self.verbose:
                print self.n_iter
                print("Strategy used: " + str(strat))
            # Add choices to the learning process (same shape as the more advance learners)
            self.learning_process.append([self.n_iter, -1, strat, -1, []])
            # Run one learning episode
            self.episode(strat)
    
    def adapt_memory(self, newEpmem):
        """Shift outcomes and y_types to fix delay."""
        last_mem = newEpmem[0][0]
        for i, mem in reversed(list(enumerate(self.lastEpisode))):
            for j, mm in reversed(list(enumerate(mem))):
                curMem = copy.deepcopy(mm)
                mm[2] = copy.deepcopy(last_mem[2])
                mm[3] = copy.deepcopy(last_mem[3])
                last_mem = curMem
    
    def episode(self, strat):
        """Run one learning episode."""
        
        # Run an episode of the given strategy
        self.strategies[strat].run()
        
        if self.lastEpisode:
            lastStrat = self.learning_process[-2][2]
            self.adapt_memory(self.strategies[strat].memory)
            
            # For each strategy iterations
            for m in self.lastEpisode:
                # For each step of the complex action
                for x in m:
                    # x on the form [a, atype, y_list, ytypes]
                    if self.strategies[lastStrat].complex_actions == 0:
                        self.dataset.create_a_space(x[1])
                    if len(x) > 4:
                        # A procedure has been tried
                        self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                                self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
                    else:
                        self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                                self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
                self.epmem_length.append(len(self.dataset.idA))
            self.lastEpisode = copy.deepcopy(self.strategies[strat].memory)
        
        # Increment number of iterations by the number of actions performed
        self.n_iter += self.strategies[strat].n


#### TO DO add iterators to traverse correctly episodes and such
class LearningAgentV4:
    """Default LearningAgent, learns by episode but without any choice of task and goal, choose strategy randomly."""
    
    def __init__(self, dataset, strat, env, verbose=False):
        """
        dataset DatasetV2: dataset of the agent
        strat StrategyV4 list: list of learning strategies available to the agent
        env EnvironmentV4: environment of the experiment
        """
        self.dataset = dataset
        self.strategies = strat
        self.environment = env
        self.learning_process = []
        self.epmem_length = [0]  # Track the correspondence between iteration and episodic memory length (necessary for complex actions)
        self.n_iter = 0
        self.verbose = verbose
    
    def to_config(self):
        dico = {}
        dico['type'] = self.__class__.__name__
        dico['strategies'] = []
        for s in self.strategies:
            dico['strategies'].append(s.to_config())
        dico['dataset'] = {}
        dico['dataset']['options'] = self.dataset.options
        dico['dataset']['type'] = self.dataset.__class__.__name__
        return dico
    
    def _replayer_lp(self):
        i = 0
        while i < len(self.learning_process):
            yield self.learning_process[i]
            i += 1
    
    def _replayer_epmem_range(self, first, last):
        i = first
        while i < last:
            yield self.epmem_length[i]
            i += 1
    
    def _replayer_all_epmem(self):
        i = 0
        j = 0
        while i < len(self.learning_process):
            lp = self.learning_process[i]
            n_iter = lp[0]
            for epmem in self._replayer_epmem_range(j, n_iter):
                yield epmem
            j = n_iter
            i += 1
    
    def _replayer_idE_range(self, first, last):
        i = first
        while i < last:
            yield i
            i += 1
    
    def _replayer_all_idE(self):
        i = 0
        j = 0
        k = 0
        while i < len(self.learning_process):
            lp = self.learning_process[i]
            n_iter = lp[0]
            while j < n_iter:
                size = self.epmem[j]
                for ide in self._replayer_idE_range(k, size):
                    yield ide
                k = size
                j += 1
            i += 1
    
    def _replayer_all_idE_test(self):
        i = 0
        j = 0
        k = 0
        while i < len(self.learning_process):
            lp = self.learning_process[i]
            n_iter = len(self.epmem_length)-1
            if i < len(self.learning_process) - 1:
                n_iter = self.learning_process[i+1][0]
            while j <= n_iter:
                size = self.epmem_length[j]
                for ide in range(k, size):
                    yield i, ide
                k = size
                j += 1
            i += 1
    
    def run(self, n_iter):
        """Run the learner until max number of iterations."""
        while self.n_iter < n_iter:
            # Choose learning strategy randomly
            strat = random.randint(0, len(self.strategies)-1)
            if self.verbose:
                print self.n_iter
                print("Strategy used: " + str(strat))
            # Add choices to the learning process (same shape as the more advance learners)
            self.learning_process.append([self.n_iter, -1, strat, -1, []])
            # Run one learning episode
            self.episode(strat)
        
    def episode(self, strat):
        """Run one learning episode."""
        
        # Run an episode of the given strategy
        self.strategies[strat].run()
        
        # For each strategy iterations
        for m in self.strategies[strat].memory:
            # For each step of the complex action
            for x in m:
                # x on the form [a, atype, y_list, ytypes]
                if self.strategies[strat].complex_actions == 0:
                    self.dataset.create_a_space(x[1])
                if len(x) > 4:
                    # A procedure has been tried
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
                else:
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
            self.epmem_length.append(len(self.dataset.idA))
            
        # Increment number of iterations by the number of actions performed
        self.n_iter += self.strategies[strat].n
    
    def plot_strategy(self, strat, window, ax, options):
        """Plot the choices of strategy over time."""
        t = np.array(range(self.n_iter))
        strat_choice = []
        last_first_j = 0
        for i in range(self.n_iter):
            choice = np.zeros(len(self.strategies))
            for it in self.learning_process[last_first_j:]:
                if it[0] <= i - window:
                    last_first_j += 1
                elif it[0] < i + window:
                    choice[it[2]] += 1
                else:
                    break
            norm = np.sum(choice)
            if norm > 0:
                choice = choice / norm
            strat_choice.append(choice)
        
        strat_choice = np.array(strat_choice)
        ax.plot(t, strat_choice[:,strat], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
    
    def plot_task_strategy_points(self, task, strat, ax, options):
        """Plot each reached outcomes of the given task with a color indicating the strategy used to acquire it."""
        points = []
        n = 0
        for i in range(len(self.learning_process)):
            s = self.learning_process[i][2]
            if s == strat:
                n0 = self.epmem_length[self.learning_process[i][0]]
                if i < len(self.learning_process)-1:
                    n1 = self.epmem_length[self.learning_process[i+1][0]]
                else:
                    n1 = len(self.dataset.idY)
                while n < len(self.dataset.y_spaces[task].ids) and self.dataset.y_spaces[task].ids[n] < n0:
                    n += 1
                while n < len(self.dataset.y_spaces[task].ids) and self.dataset.y_spaces[task].ids[n] < n1:
                    points.append(self.dataset.y_spaces[task].data[n])
                    n += 1
        if self.dataset.y_spaces[task].dim < 4:
            p = np.array(points)
            if self.dataset.y_spaces[task].dim == 1:
                ax.plot(p, np.zeros(len(p)), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
            elif self.dataset.y_spaces[task].dim == 2:
                ax.plot(p[:,0], p[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
            elif self.dataset.y_spaces[task].dim == 3:
                try:
                    ax.scatter(p[:,0], p[:,1], p[:,2], marker=options['marker'], color=options['color'])
                except IndexError:
                    print p
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_strategies_visualizer(self, window, prefix=""):
        """Return a dictionary used to visualize the choices of strategy."""
        dico = {}
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.strategies)):
            if len(self.strategies) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.strategies)-1)))))
            markers.append('')
            lines.append('solid')
            legends.append(self.strategies[i].name)
            plots.append(lambda ax, options, i=i: self.plot_strategy(i, window, ax, options))
        
        dico['limits'] = {'min': [None, 0.0], 'max': [None, 1.0]}
        dico['title'] = prefix + "Strategy choice"
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['axes'] = ['Iterations', '%']
        dico['plots'] = plots
        
        return dico
    
    def get_strategies_points_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the points reached by a given strategy."""
        dico = {}
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.strategies)):
            if len(self.strategies) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.strategies)-1)))))
            markers.append('.')
            lines.append('None')
            legends.append(self.strategies[i].name)
            plots.append(lambda ax, options, i=i: self.plot_task_strategy_points(task, i, ax, options))
        
        dico['limits'] = {'min': self.dataset.y_spaces[task].bounds['min'], \
            'max': self.dataset.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Points reached task " + str(task)
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    

class InterestAgentV4(LearningAgentV4):
    """Complete learning agent with an interest model and mods."""
    
    def __init__(self, dataset, strat, mods, interest_model, env, verbose=False):
        """
        dataset DatasetV2: dataset of the agent
        strat StrategyV4 list: list of learning strategies available to the agent
        mods Mod list: list of available mods (used to determine wich task/goal/strategy to explore
        interest_model InterestModelV3: interest model used by the agent to guide its learning process
        env EnvironmentV4: environment of the experiment
        """
        LearningAgentV4.__init__(self, dataset, strat, env, verbose)
        self.mods = mods
        self.interest_model = interest_model
        self.progresses = []
        self.goal_progresses = []
    
    def to_config(self):
        dico = LearningAgentV4.to_config(self)
        dico['mods'] = []
        for m in self.mods:
            dico['mods'].append(m.to_config())
        dico['interest_model'] = self.interest_model.to_config()
        return dico
    
    def get_attempt(self, iteration, i, divide=False):
        idE = self.epmem_length[iteration] + i
        #print idE, iteration, i
        progresses = self.progresses[iteration][i]
        a_type, idA = self.dataset.idA[idE]
        action = (a_type, self.dataset.a_spaces[a_type[0]][a_type[1]].data[idA])
        idY = self.dataset.idY[idE]
        idP = self.dataset.idP[idE]
        proc = None
        if idP:
            p_type = idP[0]
            proc = (p_type, self.dataset.p_spaces[p_type[0]][p_type[1]].data[idP[1]])
        outcomes = []
        if divide:
            for j in range(len(progresses)):
                progresses[j] /= float(a_type[1] + 1)
        for task, iy in idY:
            outcomes.append((task, self.dataset.y_spaces[task].data[iy]))
        return Attempt(action, outcomes, progresses, proc)
    
    def get_episode(self, i, divide=False):
        lp = self.learning_process[i]
        start = lp[0]
        end = self.n_iter
        if i < len(self.learning_process)-1:
            end = self.learning_process[i+1][0]
        iterations = []
        n_p = 0
        for j in range(end-start):
            iterations.append([])
            idE_start = self.epmem_length[i+j]
            idE_end = len(self.dataset.idA)
            if (i+j) < len(self.epmem_length)-1:
                idE_end = self.epmem_length[i+j+1]
            for k in range(idE_end - idE_start):
                iterations[-1].append(self.get_attempt(i+j, k, divide))
                n_p += 1
        g_p = self.goal_progresses[i]
        if divide:
            g_p /= float(n_p)
        return Episode(iterations, lp[2], lp[3], lp[4], lp[1], g_p)
    
    def replay(self, divide=False):
        im = InterestModelV3(self.dataset, self.interest_model.options)
        episodes = []
        for i in range(len(self.learning_process)):
            display_progress(i*100/len(self.learning_process), 25)
            episodes.append(self.get_episode(i, divide))
            episodes[-1].process(im)
        return ReplayStats(episodes)
    
    def quick_replay(self, divide=False):
        episodes = []
        for i in range(len(self.learning_process)):
            episodes.append(self.get_episode(i, divide))
        return ReplayStats(episodes)
    
    def run(self, n_iter):
        """Run the learner until max number of iterations."""
        while self.n_iter < n_iter:
            # Select stochastically mod
            mod = self.choose_mod()
            # Select task, strategy and goal outcome according to chosen mod
            task, strat, goal = self.choose_strat_goal(mod)
            # Make sure strategy chosen is available
            while not self.strategies[strat].available(task):
                task, strat, goal = self.choose_strat_goal(mod)
            if self.verbose:
                print("Iterations: " + str(self.n_iter))
                print("Mod " + str(mod) + " chose space " + str(task) + ", strategy " + str(strat) + " and goal " + str(goal))
            # Add choices to the learning process (same shape as the less advance learners)
            self.learning_process.append([self.n_iter, mod, strat, task, goal])
            # Run one episode
            self.episode(strat, goal, task)
        
    def episode(self, strat, goal, task):
        """Run one episode of given strategy to reach the given goal in given task space."""
        
        # Run the episode
        self.strategies[strat].run_goal(goal, task)
        
        # Compute previous goal competence
        goal_comp = self.dataset.competence(goal, task)
        
        list_points = []
        
        # For each strategy iterations
        for m in self.strategies[strat].memory:
            # For each step of the complex action
            all_progresses = []
            for x in m:
                # x on the form [a, atype, y_list, ytypes]
                all_progresses.append([])
                old_comp_list = []
                for y, t in zip(x[2], x[3]):
                    old_comp_list.append(self.dataset.competence(y, t))
                if self.strategies[strat].complex_actions == 0:
                    self.dataset.create_a_space(x[1])
                if len(x) > 4:
                    # A procedure has been tried
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
                else:
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
                for y, t, old_comp in zip(x[2], x[3], old_comp_list):
                    new_comp = self.dataset.competence(y, t)
                    progress = self.progress(old_comp, new_comp)
                    # Add point in interest region
                    self.interest_model.add_point(y, t, strat, progress)
                    all_progresses[-1].append(progress)
            self.progresses.append(all_progresses)
            self.epmem_length.append(len(self.dataset.idA))
        
        # Compute goal progress and add goal to interest region        
        new_goal_comp = self.dataset.competence(goal, task)
        goal_progress = self.progress(goal_comp, new_goal_comp)
        self.interest_model.add_point(goal, task, strat, goal_progress)
        self.goal_progresses.append(goal_progress)
        
        # Increment number of iterations
        self.n_iter += self.strategies[strat].n
    
    def compute_progress(self, y, task, before):
        """Compute progress in reaching a given target between now and before."""
        old_comp = self.dataset.competence(y, task, n_iter=before)
        new_comp = self.dataset.competence(y, task)
        #print("Task " + str(task) + ", point " + str(y) + ", comp(" + str(before) + "): " + str(old_comp) + " -> " + str(new_comp))
        return self.progress(old_comp, new_comp)   
        
    def choose_mod(self):
        """Choose mod stochastically."""
        probs = []
        for m in self.mods:
            probs.append(m.prob)
        return uniform_sampling(probs)
    
    def choose_strat_goal(self, mod):
        """Sample strategy and goal according to chosen mod."""
        return self.mods[mod].sample()
        
    @staticmethod
    def progress(old_comp, new_comp):
        """Static method returning progress computed with old and new competence."""
        return new_comp - old_comp
    
    def compute_strategy_task(self):
        """Return a matrix giving choices per task (rows) and strategies (columns)."""
        choices = np.zeros((len(self.dataset.y_spaces), len(self.strategies)), dtype=np.float64)
        for it in self.learning_process:
            if it[1] > 0:
                choices[it[3], it[2]] += 1
        #by_task = np.sum(choices, axis=1)
        #by_task = np.reshape(by_task, (len(self.dataset.y_spaces), 1))
        #choices /= by_task
        return choices
    
    def compute_strategies(self, window=500):
        res = np.zeros((self.n_iter, len(self.strategies)))
        for t in range(self.n_iter):
            first = max(0, t-window)
            last = min(self.n_iter, t+window)
            choice = np.zeros(len(self.strategies))
            for it in self.learning_process[first:last]:
                if it[1] > 0: # Only IM mods
                    choice[it[2]] += 1
            norm = np.sum(choice)
            choice /= max(1., norm)
            res[t,:] = choice
        return res
    
    def compute_tasks(self, window=500):
        res = np.zeros((self.n_iter, len(self.dataset.y_spaces)))
        for t in range(self.n_iter):
            first = max(0, t-window)
            last = min(self.n_iter, t+window)
            choice = np.zeros(len(self.dataset.y_spaces))
            for it in self.learning_process[first:last]:
                if it[1] > 0: # Only IM mods
                    choice[it[3]] += 1
            norm = np.sum(choice)
            choice /= max(1., norm)
            res[t,:] = choice
        return res
    
    def plot_tasks(self, task, window, ax, options): # Careful, removing mod 0
        """Plot the choices of task over time."""
        t = np.array(range(self.n_iter))
        task_choice = []
        last_first_j = 0
        for i in range(self.n_iter):
            choice = np.zeros(len(self.interest_model.interest_trees))
            for it in self.learning_process[last_first_j:]:
                if it[0] <= i - window:
                    last_first_j += 1
                elif it[0] < i + window:
                    choice[it[3]] += 1
                else:
                    break
            norm = np.sum(choice)
            if norm > 0:
                choice = choice / norm 
            task_choice.append(choice)
        
        task_choice = np.array(task_choice)
        ax.plot(t, task_choice[:,task], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
    
    def plot_goals_task(self, task, ax, options): # Careful, removing mod 0
        """Plot the goals chosen for a given task space."""
        goals = []
        if self.dataset.y_spaces[task].dim < 4:
            for it in self.learning_process:
                if it[3] == task and it[1] > 0:
                    goals.append(it[4])
            goals = np.array(goals)
            if len(goals) > 0:
                if self.dataset.y_spaces[task].dim == 1:
                    ax.plot(goals, np.zeros(len(goals)), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 2:
                    ax.plot(goals[:,0], goals[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 3:
                    ax.scatter(goals[:,0], goals[:,1], goals[:,2], marker=options['marker'], color=options['color'])
    
    def plot_task_strategy_goal(self, task, strat, ax, options): # Careful, removing mod 0
        """Plot the goals chosen for a specific task space and strategy."""
        goals = []
        if self.dataset.y_spaces[task].dim < 4:
            for it in self.learning_process:
                if it[3] == task and it[2] == strat and it[1] > 0:
                    goals.append(it[4])
            goals = np.array(goals)
            if len(goals) > 0:
                if self.dataset.y_spaces[task].dim == 1:
                    ax.plot(goals, np.zeros(len(goals)), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 2:
                    ax.plot(goals[:,0], goals[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 3:
                    ax.scatter(goals[:,0], goals[:,1], goals[:,2], marker=options['marker'], color=options['color'])
    
    def plot_task_mod_goal(self, task, mod, ax, options):
        """Plot the goals chosen for a specific task space and by a specific mod."""
        goals = []
        if self.dataset.y_spaces[task].dim < 4:
            for it in self.learning_process:
                if it[3] == task and it[1] == mod:
                    goals.append(it[4])
            goals = np.array(goals)
            if len(goals) > 0:
                if self.dataset.y_spaces[task].dim == 1:
                    ax.plot(goals, np.zeros(len(goals)), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 2:
                    ax.plot(goals[:,0], goals[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 3:
                    ax.scatter(goals[:,0], goals[:,1], goals[:,2], marker=options['marker'], color=options['color'])
    
    def plot_mod(self, mod, window, ax, options):
        """Plot the mod chosen over time."""
        t = np.array(range(self.n_iter))
        mod_choice = []
        for i in range(self.n_iter):
            choice = np.zeros(len(self.mods))
            for it in self.learning_process:
                if it[0] > i - window and it[0] < i + window:
                    choice[it[1]] += 1
                if it[0] >= i + window:
                    break
            norm = np.sum(choice)
            if norm > 0:
                choice = choice / norm 
            mod_choice.append(choice)
        
        mod_choice = np.array(mod_choice)
        ax.plot(t, mod_choice[:,mod], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_tasks_visualizer(self, window, prefix=""):
        """Return a dictionary used to visualize the choices of tasks."""
        dico = {}
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.interest_model.interest_trees)):
            if len(self.interest_model.interest_trees) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.interest_model.interest_trees)-1)))))
            markers.append('')
            lines.append('solid')
            legends.append("Task " + str(i))
            plots.append(lambda ax, options, i=i: self.plot_tasks(i, window, ax, options))
        
        dico['limits'] = {'min': [None, 0.0], 'max': [None, 1.0]}
        dico['title'] = prefix + "Task choice"
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['axes'] = ['Iterations', '%']
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    
    def get_mods_visualizer(self, window, prefix=""):
        """Return a dictionary used to visualize the choices of mod."""
        dico = {}
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.mods)):
            if len(self.mods) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.mods)-1)))))
            markers.append('')
            lines.append('solid')
            legends.append(self.mods[i].name)
            plots.append(lambda ax, options, i=i: self.plot_mod(i, window, ax, options))
        
        dico['limits'] = {'min': [None, 0.0], 'max': [None, 1.0]}
        dico['title'] = prefix + "Mod choice"
        dico['color'] = colors
        dico['marker'] = markers
        dico['axes'] = ['Iterations', '%']
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    
    def get_goals_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the goals of a specific task space."""
        dico = {}
        dico['limits'] = {'min': self.dataset.y_spaces[task].bounds['min'], \
            'max': self.dataset.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Goals chosen task " + str(task)
        dico['color'] = ['k']
        dico['marker'] = ['.']
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_goals_task(task, ax, options)]
        
        return dico
    
    def get_goals_strategy_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the goals chosen for a specific task space sorted by strategy."""
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.strategies)):
            if len(self.strategies) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.strategies)-1)))))
            markers.append('.')
            lines.append('None')
            legends.append(self.strategies[i].name)
            plots.append(lambda ax, options, i=i: self.plot_task_strategy_goal(task, i, ax, options))
        
        dico = {}
        dico['limits'] = {'min': self.dataset.y_spaces[task].bounds['min'], \
            'max': self.dataset.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Goals chosen task " + str(task)
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    
    def get_goals_mod_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the goals on a specific task space chosen by mods."""
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.mods)):
            if len(self.mods) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.mods)-1)))))
            markers.append('.')
            lines.append('None')
            legends.append(self.mods[i].name)
            plots.append(lambda ax, options, i=i: self.plot_task_mod_goal(task, i, ax, options))
        
        dico = {}
        dico['limits'] = {'min': self.dataset.y_spaces[task].bounds['min'], \
            'max': self.dataset.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Goals chosen task " + str(task)
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico


"""
class Episode:
    
    def __init__(self, iterations, strategy, task, goal, mod, goal_progress):
        self.iterations = iterations  # list of list of attempts
        self.strategy = strategy
        self.task = task
        self.goal = goal
        self.mod = mod
        self.goal_progress = goal_progress
        self.stats = None


class Attempt: # A specific action tried
    
    def __init__(self, action, outcomes, progresses, procedure=None):
        self.action = action
        self.outcomes = outcomes
        self.procedure = procedure
        self.progresses = progresses
"""

class InterestAgentV5(InterestAgentV4):
    """Complete learning agent with an interest model and mods.
    Does not include intermediate outcomes in interest model."""
    
    def complete_replay(self, episodes):
        all_episodes = []
        for i, e in enumerate(episodes):
            display_progress(i*100/len(episodes), 25)
            all_episodes.append(self.replay_episode(e))
            all_episodes[-1].stats = self.interest_model.get_stats()
        return ReplayStats(all_episodes)
    
    def replay_episode(self, episode):
        #new_ep = Episode()
        all_att = []
        
        # Compute previous goal competence
        goal_comp = self.dataset.competence(episode.goal, episode.task)
        
        # For each strategy iterations
        for iter in episode.iterations:
            # For each step of the complex action
            all_att.append([])
            all_progresses = []
            for at in iter[:-1]:
                # x on the form [a, atype, y_list, ytypes]
                self.dataset.create_a_space(at.action[0])
                y_list = []
                y_types = []
                for o in at.outcomes:
                    y_list.append(o[1])
                    y_types.append(o[0])
                all_att[-1].append(Attempt(at.action, at.outcomes, None, at.procedure))
                if at.procedure:
                    # A procedure has been tried
                    self.dataset.add_entity(at.action[1], at.action[0], y_list, y_types, \
                            self.dataset.a_spaces[at.action[0][0]][at.action[0][1]].options['cost'], \
                            at.procedure[1], at.procedure[0])
                else:
                    self.dataset.add_entity(at.action[1], at.action[0], y_list, y_types, \
                            self.dataset.a_spaces[at.action[0][0]][at.action[0][1]].options['cost'])
            # Only the last step is used to record progress
            at = iter[-1]
            # x on the form [a, atype, y_list, ytypes]
            all_progresses.append([])
            old_comp_list = []
            y_list = []
            y_types = []
            for (t, y) in at.outcomes:
                old_comp_list.append(self.dataset.competence(y, t))
                y_list.append(y)
                y_types.append(t)
            self.dataset.create_a_space(at.action[0])
            if at.procedure:
                # A procedure has been tried
                self.dataset.add_entity(at.action[1], at.action[0], y_list, y_types, \
                        self.dataset.a_spaces[at.action[0][0]][at.action[0][1]].options['cost'], \
                        at.procedure[1], at.procedure[0])
            else:
                self.dataset.add_entity(at.action[1], at.action[0], y_list, y_types, \
                        self.dataset.a_spaces[at.action[0][0]][at.action[0][1]].options['cost'])
            for (t, y), old_comp in zip(at.outcomes, old_comp_list):
                new_comp = self.dataset.competence(y, t)
                progress = self.progress(old_comp, new_comp)
                # Add point in interest region
                self.interest_model.add_point(y, t, episode.strategy, progress)
                all_progresses[-1].append(progress)
            self.progresses.append(all_progresses)
            self.epmem_length.append(len(self.dataset.idA))
            all_att[-1].append(Attempt(at.action, at.outcomes, all_progresses[-1], at.procedure))
        
        # Compute goal progress and add goal to interest region        
        new_goal_comp = self.dataset.competence(episode.goal, episode.task)
        goal_progress = self.progress(goal_comp, new_goal_comp)
        self.interest_model.add_point(episode.goal, episode.task, episode.strategy, goal_progress)
        self.goal_progresses.append(goal_progress)
        
        # Increment number of iterations
        self.n_iter += len(episode.iterations)
        
        return Episode(all_att, episode.strategy, episode.task, episode.goal, episode.mod, goal_progress)
    
    def episode(self, strat, goal, task):
        """Run one episode of given strategy to reach the given goal in given task space."""
        
        # Run the episode
        self.strategies[strat].run_goal(goal, task)
        
        # Compute previous goal competence
        goal_comp = self.dataset.competence(goal, task)
        
        list_points = []
        
        # For each strategy iterations
        for m in self.strategies[strat].memory:
            # For each step of the complex action
            all_progresses = []
            for x in m[:-1]:
                # x on the form [a, atype, y_list, ytypes]
                if self.strategies[strat].complex_actions == 0:
                    self.dataset.create_a_space(x[1])
                if len(x) > 4:
                    # A procedure has been tried
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
                else:
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
            # Only the last step is used to record progress
            x = m[-1]
            # x on the form [a, atype, y_list, ytypes]
            all_progresses.append([])
            old_comp_list = []
            for y, t in zip(x[2], x[3]):
                old_comp_list.append(self.dataset.competence(y, t))
            if self.strategies[strat].complex_actions == 0:
                self.dataset.create_a_space(x[1])
            if len(x) > 4:
                # A procedure has been tried
                self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                        self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
            else:
                self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                        self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
            for y, t, old_comp in zip(x[2], x[3], old_comp_list):
                new_comp = self.dataset.competence(y, t)
                progress = self.progress(old_comp, new_comp)
                # Add point in interest region
                self.interest_model.add_point(y, t, strat, progress)
                all_progresses[-1].append(progress)
            self.progresses.append(all_progresses)
            self.epmem_length.append(len(self.dataset.idA))
        
        # Compute goal progress and add goal to interest region        
        new_goal_comp = self.dataset.competence(goal, task)
        goal_progress = self.progress(goal_comp, new_goal_comp)
        self.interest_model.add_point(goal, task, strat, goal_progress)
        self.goal_progresses.append(goal_progress)
        
        # Increment number of iterations
        self.n_iter += self.strategies[strat].n


class InterestAgentV6(InterestAgentV4):
    """Complete learning agent with an interest model and mods.
    Divides progress per nb_primitives to advantage easy spaces."""
    def episode(self, strat, goal, task):
        """Run one episode of given strategy to reach the given goal in given task space."""
        
        # Run the episode
        self.strategies[strat].run_goal(goal, task)
        
        # Compute previous goal competence
        goal_comp = self.dataset.competence(goal, task)
        
        list_points = []
        n_prim = 0
        
        # For each strategy iterations
        for m in self.strategies[strat].memory:
            # For each step of the complex action
            all_progresses = []
            for i, x in enumerate(m):
                # x on the form [a, atype, y_list, ytypes]
                all_progresses.append([])
                old_comp_list = []
                for y, t in zip(x[2], x[3]):
                    old_comp_list.append(self.dataset.competence(y, t))
                if self.strategies[strat].complex_actions == 0:
                    self.dataset.create_a_space(x[1])
                if len(x) > 4:
                    # A procedure has been tried
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
                else:
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
                for y, t, old_comp in zip(x[2], x[3], old_comp_list):
                    new_comp = self.dataset.competence(y, t)
                    progress = self.progress(old_comp, new_comp, i+1)
                    # Add point in interest region
                    self.interest_model.add_point(y, t, strat, progress)
                    all_progresses[-1].append(progress)
            self.progresses.append(all_progresses)
            self.epmem_length.append(len(self.dataset.idA))
            n_prim += len(m)
        
        # Compute goal progress and add goal to interest region        
        new_goal_comp = self.dataset.competence(goal, task)
        goal_progress = self.progress(goal_comp, new_goal_comp, n_prim)
        self.interest_model.add_point(goal, task, strat, goal_progress)
        self.goal_progresses.append(goal_progress)
        
        # Increment number of iterations
        self.n_iter += self.strategies[strat].n
    
    @staticmethod
    def progress(old_comp, new_comp, n):
        """Static method returning progress computed with old and new competence."""
        return (new_comp - old_comp)/n



class InterestAgentV7(InterestAgentV6):
    """Complete learning agent with an interest model and mods.
    Divides progress per nb_primitives to advantage easy spaces.
    Also use delayed outcomes for real Yumi setup."""
    
    def __init__(self, dataset, strat, mods, interest_model, env, verbose=False):
        """
        dataset DatasetV2: dataset of the agent
        strat StrategyV4 list: list of learning strategies available to the agent
        mods Mod list: list of available mods (used to determine wich task/goal/strategy to explore
        interest_model InterestModelV3: interest model used by the agent to guide its learning process
        env EnvironmentV4: environment of the experiment
        """
        InterestAgentV6.__init__(self, dataset, strat, mods, interest_model, env, verbose)
        self.lastEpisode = None
    
    def adapt_memory(self, newEpmem):
        """Shift outcomes and y_types to fix delay."""
        last_mem = newEpmem[0][0]
        for i, mem in reversed(list(enumerate(self.lastEpisode))):
            for j, mm in reversed(list(enumerate(mem))):
                curMem = copy.deepcopy(mm)
                mm[2] = copy.deepcopy(last_mem[2])
                mm[3] = copy.deepcopy(last_mem[3])
                last_mem = curMem
    
    def episode(self, strat, goal, task):
        """Run one episode of given strategy to reach the given goal in given task space."""
        
        # Run the episode
        self.strategies[strat].run_goal(goal, task)
        
        if self.lastEpisode:
            lastStrat = self.learning_process[-2][2]
            lastGoal = self.learning_process[-2][4]
            lastTask = self.learning_process[-2][3]
            self.adapt_memory(self.strategies[strat].memory)
            
            # Compute previous goal competence
            goal_comp = self.dataset.competence(lastGoal, lastTask)
            
            list_points = []
            n_prim = 0
            
            # For each strategy iterations
            for m in self.lastEpisode:
                # For each step of the complex action
                all_progresses = []
                for i, x in enumerate(m):
                    # x on the form [a, atype, y_list, ytypes]
                    all_progresses.append([])
                    old_comp_list = []
                    for y, t in zip(x[2], x[3]):
                        old_comp_list.append(self.dataset.competence(y, t))
                    if self.strategies[lastStrat].complex_actions == 0:
                        self.dataset.create_a_space(x[1])
                    if len(x) > 4:
                        # A procedure has been tried
                        self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                                self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
                    else:
                        self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                                self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
                    for y, t, old_comp in zip(x[2], x[3], old_comp_list):
                        new_comp = self.dataset.competence(y, t)
                        progress = self.progress(old_comp, new_comp, i+1)
                        # Add point in interest region
                        self.interest_model.add_point(y, t, strat, progress)
                        all_progresses[-1].append(progress)
                self.progresses.append(all_progresses)
                self.epmem_length.append(len(self.dataset.idA))
                n_prim += len(m)
            
            # Compute goal progress and add goal to interest region        
            new_goal_comp = self.dataset.competence(lastGoal, lastTask)
            goal_progress = self.progress(goal_comp, new_goal_comp, n_prim)
            self.interest_model.add_point(lastGoal, lastTask, lastStrat, goal_progress)
            self.goal_progresses.append(goal_progress)
            self.lastEpisode = self.strategies[strat].memory
        
        # Increment number of iterations
        self.n_iter += self.strategies[strat].n


###### NEVER TESTED

class VisualizableAgent:
    """Learning agent that can be visualized in real time."""
    def __init__(self, learner, visualizers, t):
        """
        learner LearningAgentV4: the learner
        visualizers list of Visualizer: the visualizers that we want to observe at runtime
        t float list: the different steps to run learner and update visualization
        """
        self.learner = learner
        self.visualizers = visualizers
        self.t = t
    
    def run(self):
        """Run the visualizable learner on each step."""
        for t in self.t:
            self.learner.run(t)
            for v in self.visualizers:
                v.plot()



