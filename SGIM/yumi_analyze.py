from yumi_control import *
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from utils import *

import os

from analyze import *
from visualize_bunches import *


folder = "Yumi_Simus_Results_bin"

ind = 0
"""
# Load all evaluation files
dico = {}
for fo in os.listdir(folder):
    k = fo[:-2]
    if fo[-2] != "_":
        k = fo[:-3]
    if k != "Yumi_SGIM_HL_4" and k != "Yumi_SAGG_HL_4":
        continue
    if not dico.has_key(k):
        dico[k] = []
    fi = folder + "/" + fo + "/eval_test"
    print("Loading " + fi)
    eva = load_bin(fi)
    dico[k].append(eva)
print ind
ind += 1
"""
"""
# Perform evaluations combinations
results = {}
for k in dico.keys():
    print("Combining evaluations for " + k)
    eva, t_list = combine_evaluations(dico[k])
    results[k] = [t_list, eva]
save_bin(results, "Analyzes/evaluations.bin")
print ind
ind += 1

"""

# Combined evaluation has following form [[mean_task_0, std_task_0], ..., [..], mean, std]


testbench = SimuYumiExperimentatorV2.create_testbench()

"""
# Compute repartition of actions size per task
k = "Yumi_SGIM_HL_4"
results = []
for kk in dico[k]:
    results.append([])
    for task in range(5):
        results[-1].append(compute_evaluation_interpol(kk.learning_agent.dataset, testbench[task], task))
res = compute_mean_evaluation_atypes(results)
save_bin(res, "Analyzes/SGIM_PB_action_repartition.bin")
print ind
ind += 1

# Compute repartition of actions size per task
k = "Yumi_SAGG_HL_4"
results = []
for kk in dico[k]:
    results.append([])
    for task in range(5):
        results[-1].append(compute_evaluation_interpol(kk.learning_agent.dataset, testbench[task], task))
res = compute_mean_evaluation_atypes(results)
save_bin(res, "Analyzes/IM_PB_action_repartition.bin")
print ind
ind += 1

# Compute choice of task & strategy for mod 1/2
k = "Yumi_SGIM_HL_4"
results = []
for kk in dico[k]:
    results.append(kk.learning_agent.compute_strategy_task())
res = compute_mean_task_strat_repartition(results)
save_bin(res, "Analyzes/SGIM_PB_task_strategy.bin")
print ind
ind += 1
"""

"""
# Compute repartition of procedures per task
k = "Yumi_SGIM_HL_4"
results = []
for kk in dico[k]:
    results.append([])
    for task in range(5):
        results[-1].append(compute_p_types_interpol(kk.learning_agent.dataset, testbench[task], task))
res = compute_mean_evaluation_ptypes(results)
save_bin(res, "Analyzes/SGIM_PB_procedure_repartition.bin")
print ind
ind += 1

# Compute repartition of procedures per task
k = "Yumi_SAGG_HL_4"
results = []
for kk in dico[k]:
    results.append([])
    for task in range(5):
        results[-1].append(compute_p_types_interpol(kk.learning_agent.dataset, testbench[task], task))
res = compute_mean_evaluation_ptypes(results)
save_bin(res, "Analyzes/IM_PB_procedure_repartition.bin")
print ind
ind += 1
"""

######## Save all information in adequate files for matlab #############

"""
# Evaluations
evals = load_bin("Analyzes/evaluations.bin")

global_evals = []
task_evals = [[],[],[],[],[]]
names = evals.keys()

for k in names:
    eva = evals[k][1]
    t_list = evals[k][0]
    global_evals.append([])
    for i in range(5):
        task_evals[i].append([])
    for t, e in zip(t_list, eva):
        global_evals[-1].append([t, e[-2]])
        for i in range(5):
            task_evals[i][-1].append([t, e[i][0]])


prepare_plot(global_evals, names, "Analyzes/evaluation/", options={"y": "log"})

for i in range(5):
    prepare_plot(task_evals[i], names, "Analyzes/evaluation_task_"+str(i)+"/", options={"y": "log"})
"""



"""
# evals = {'name1': Evaluation1, ...}

for k in evals.keys():
    e, t = combine_evaluations([evals[k]])
    evals[k] = [t, e]

global_evals = []
task_evals = [[],[],[],[],[]]
names = evals.keys()

for k in names:
    eva = evals[k][1]
    t_list = evals[k][0]
    global_evals.append([])
    for i in range(5):
        task_evals[i].append([])
    for t, e in zip(t_list, eva):
        global_evals[-1].append([t, e[-2]])
        for i in range(5):
            task_evals[i][-1].append([t, e[i][0]])


prepare_plot(global_evals, names, "Analyzes/evaluation/", options={"y": "log"})

for i in range(5):
    prepare_plot(task_evals[i], names, "Analyzes/evaluation_task_"+str(i)+"/", options={"y": "log"})
"""



"""
# Action repartition
data = load_bin("Analyzes/SGIM_PB_action_repartition.bin")
for i in range(len(data)):
    data[i] = data[i].tolist()
save_json(data, "Analyzes/SGIM_PB_action_repartition.json")

data = load_bin("Analyzes/IM_PB_action_repartition.bin")
for i in range(len(data)):
    data[i] = data[i].tolist()
save_json(data, "Analyzes/IM_PB_action_repartition.json")

# Procedure repartition

data = load_bin("Analyzes/SGIM_PB_procedure_repartition.bin")
save_json(data.tolist(), "Analyzes/SGIM_PB_procedure_repartition.json")

data = load_bin("Analyzes/IM_PB_procedure_repartition.bin")
save_json(data.tolist(), "Analyzes/IM_PB_procedure_repartition.json")

# Task/strategy

data = load_bin("Analyzes/SGIM_PB_task_strategy.bin")
save_csv(data.tolist(), "Analyzes/SGIM_PB_task_strategy.csv")
"""

data = load_bin("Analyzes/SGIM_PB_procedure_repartition.bin")
for task in range(1,5):
    save_csv(data[task].tolist(), "Analyzes/SGIM_PB_procedure_repartition_task_"+str(task)+".csv")



