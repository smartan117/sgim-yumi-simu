from yumi_control import *
from utils import *
import numpy as np

"""
This script selects the best outcomes to show two learners comparative progression.

This quality is based around different factors:
 - relative closeness to a policy teacher demo
 - evaluation bad first, then better
 - best progression difference between both algos
"""


# SGIM-ACTS converges around 100 - 500
# SGIM-PB around 500-1000

# t_list = [0, 100, 500, 1000, 2000, 5000, 10000, 15000, 20000, 25000]

# For each algo, for each testpoint
# - error at 100
# - error at 25000
# - improvement 100 - 25000
# Global note: error100 * w1 + error25000


def compute_evaluation_interpol(data, testbench, task):
    """Compute evaluation for a given task."""
    results = []
    
    # Will contain all evaluation information
    eva = []
    # Error sum on the testbench
    error_sum = 0.
    # Sum of quadratic error on testbench
    quad_error_sum = 0.
    for i, point in enumerate(testbench):
        #sys.stdout.write("\rProgress: " + str(i) + "/" + str(len(testbench)))
        #sys.stdout.flush()
        if len(data.y_spaces[task].data) == 0:
            dist = data.y_spaces[task].options['out_dist']
            results.append(None)
        else:
            _,_,a_type = data.best_locality(point, task)
            results.append(a_type[1])
    
    #print ""
    
    return results


def compute_evaluation_perf(data, testbench, task):
    """Compute evaluation for a given task."""
    results = []
    
    # Will contain all evaluation information
    eva = []
    # Error sum on the testbench
    error_sum = 0.
    # Sum of quadratic error on testbench
    quad_error_sum = 0.
    for i, point in enumerate(testbench):
        #sys.stdout.write("\rProgress: " + str(i) + "/" + str(len(testbench)))
        #sys.stdout.flush()
        if len(data.y_spaces[task].data) == 0:
            dist = data.y_spaces[task].options['out_dist']
            p = dist
            results.append(None)
        else:
            ids,dist = data.nn_y(point, task)
            p = dist[0]
            idE = data.y_spaces[task].ids[ids[0]]
            a_type = data.idA[idE][0]
            results.append(a_type[1])
        eva.append(p)
        error_sum += p
        quad_error_sum += p**2
    error_sum /= float(len(eva))
    quad_error_sum /= float(len(eva))
    std = np.sqrt(quad_error_sum - error_sum**2)
    
    #print ""
    
    return results


"""

acts_results = []
pb_results = []

f = "Yumi_Simus_Results_bin/Yumi_SGIM_ACTS_4_1/eval_test"
acts_results = np.zeros((8000, 2))
eva = load_bin(f)
e100 = eva.evaluation[1]
e500 = eva.evaluation[2]
e25000 = eva.evaluation[-1]
for i, ee in enumerate(e100[1][4][0]):
    acts_results[i][0] = ee
for i, ee in enumerate(e25000[1][4][0]):
    acts_results[i][1] = ee


f = "Yumi_Simus_Results_bin/Yumi_SGIM_HL_4_1/eval_test"
pb_results = np.zeros((8000, 2))
eva = load_bin(f)
e100 = eva.evaluation[1]
e500 = eva.evaluation[2]
e25000 = eva.evaluation[-1]
for i, ee in enumerate(e100[1][4][0]):
    pb_results[i][0] = ee
for i, ee in enumerate(e25000[1][4][0]):
    pb_results[i][1] = ee

testbench = SimuYumiExperimentatorV2.create_testbench()
a_types = compute_evaluation_interpol(eva.learning_agent.dataset, testbench[4], 4)


# First, eliminate testpoints done using long policies (more than size 4)

a = np.array(a_types)
candidates = np.array(range(len(a)))

candidates = candidates[a < 4]


###### Build different scores

for i in candidates:
    print(str(i) + ", ACTS: " + str(acts_results[i][0]) + " -> " + str(acts_results[i][1]))
    print(str(i) + ", PB: " + str(pb_results[i][0]) + " -> " + str(pb_results[i][1]))
    raw_input()

"""

from evaluation_multiprocesses import *


f = "Yumi_Simus_Results_bin/Yumi_SGIM_HL_4_1/eval_test"
pb_results = np.zeros((8000, 2))
eva_pb = load_bin(f)

f = "Yumi_Simus_Results_bin/Yumi_SGIM_ACTS_4_1/eval_test"
acts_results = np.zeros((8000, 2))
eva_acts = load_bin(f)


testbench = SimuYumiExperimentatorV2.create_testbench()
testbench1 = []
for x1 in np.linspace(-1, 1, num=10):
    for y1 in np.linspace(-1, 1, num=10):
        for x2 in np.linspace(-1, 1, num=10):
            for y2 in np.linspace(-1, 1, num=10):
                testbench1.append(np.array([x1, y1]))
testbench2 = []
for x1 in np.linspace(-1, 1, num=10):
    for y1 in np.linspace(-1, 1, num=10):
        for x2 in np.linspace(-1, 1, num=10):
            for y2 in np.linspace(-1, 1, num=10):
                testbench2.append(np.array([x2, y2]))

testbench[1] = testbench1
testbench[2] = testbench2

new_eva_pb = EvaluationV4(eva_pb.learning_agent, testbench)
new_eva_acts = EvaluationV4(eva_acts.learning_agent, testbench)
new_eva_pb.evaluate(25000)
new_eva_acts.evaluate(25000)


a_types1 = compute_evaluation_interpol(new_eva_pb.learning_agent.dataset, testbench[1], 1)
a_types2 = compute_evaluation_interpol(new_eva_pb.learning_agent.dataset, testbench[2], 2)
a_types3 = compute_evaluation_interpol(new_eva_pb.learning_agent.dataset, testbench[3], 3)


result = []
for ee in new_eva_acts.evaluation[0][1][1][0]:
    result.append([])
    result[-1].append(ee)
    
for i, ee in enumerate(new_eva_acts.evaluation[0][1][2][0]):
    result[i].append(ee)

for i, ee in enumerate(new_eva_acts.evaluation[0][1][3][0]):
    result[i].append(ee)
    
for i, ee in enumerate(new_eva_pb.evaluation[0][1][1][0]):
    result[i].append(ee)
    
for i, ee in enumerate(new_eva_pb.evaluation[0][1][2][0]):
    result[i].append(ee)
    
for i, ee in enumerate(new_eva_pb.evaluation[0][1][3][0]):
    result[i].append(ee)

for i, ee in enumerate(a_types1):
    result[i].append(ee)
for i, ee in enumerate(a_types2):
    result[i].append(ee)
for i, ee in enumerate(a_types3):
    result[i].append(ee)


compute_evaluation_perf

save_csv(result, "Demo_DGA/evaluations_analyzes.csv")







