import numpy as np
import math

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.spatial import cKDTree
from scipy.spatial.distance import euclidean

import multiprocessing as mp





class LightWeightEvaluation:
    
    def __init__(self, learning_agent, evaluation):
        """
        learning_agent LearningAgentV4
        """
        self.learning_agent = learning_agent
        # Will contain all evaluation performed
        # Each evaluation structured as below:
        #    [t, [[[perf_point0, perf_point1,...], error_sum_task_0, std_task_0], ...], mean_error, mean_std]
        self.evaluation = evaluation
    
    def plot(self, ax, options):
        """Plot evaluation on all iterations."""
        x = []
        y = []
        for i in self.evaluation:
            x.append(i[0])
            y.append(i[2])
        ax.plot(np.array(x), np.array(y), marker=options['marker'], linestyle=options['linestyle'], color=options['color'])
        plt.yscale('log')
        plt.grid(True)
    
    def plot_mean_task(self, task, ax, options):
        """Plot evaluation for a specific task space."""
        x = []
        y = []
        for i in self.evaluation:
            x.append(i[0])                
            y.append(i[1][task][1])
        ax.plot(np.array(x), np.array(y), marker=options['marker'], linestyle=options['linestyle'], color=options['color'])
        plt.yscale('log')
        plt.grid(True)
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_complete_visualizer(self, prefix=""):
        """Create dictionary to visualize evaluation."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = prefix + "Evaluations of the algo"
        dico['color'] = ['k']
        dico['marker'] = ['']
        dico['linestyle'] = ['solid']
        dico['axes'] = ['Iterations', 'Mean error on testbench']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot(ax, options)]
        
        return dico
    
    def get_task_visualizer(self, task, prefix=""):
        """Create dictionary to visualize evaluation of a specific task space."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = prefix + "Evaluations of the algo on task " + str(task)
        dico['color'] = ['k']
        dico['marker'] = ['']
        dico['linestyle'] = ['solid']
        dico['axes'] = ['Iterations', 'Mean error on testbench']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_mean_task(task, ax, options)]
        
        return dico


class EvaluationV4:
    """Evaluation of a learning agent on a testbench."""
    def __init__(self, learning_agent, testbench):
        """
        learning_agent LearningAgentV4
        testbench list of list of float array: list for each outcome space the test points
        """
        self.learning_agent = learning_agent
        self.testbench = testbench
        # Will contain all evaluation performed
        # Each evaluation structured as below:
        #    [t, [[[perf_point0, perf_point1,...], error_sum_task_0, std_task_0], ...], mean_error, mean_std]
        self.evaluation = []
    
    def compute_evaluation(self, task, t):
        """Compute evaluation for a given task."""        
        assert len(self.testbench[task]) > 0
        
        # Find the size of the task space at the time
        exeId = self.learning_agent.epmem_length[t]        
        max_i = len(self.learning_agent.dataset.y_spaces[task].data)
        for i in range(len(self.learning_agent.dataset.y_spaces[task].data)):
            if self.learning_agent.dataset.y_spaces[task].ids[i] > exeId:
                max_i = i
                break
        
        # Retrieve dataset at the time
        data = np.array(self.learning_agent.dataset.y_spaces[task].data[0:max_i])
        #print("Retrieve dataset")
        
        # Create KD-Tree for evaluation
        tree = None
        if len(data) > 0:
            data = np.unique(data, axis=0)
            np.random.shuffle(data)
            tree = cKDTree(data)
        #print("Built KD-Tree")
        
        # Will contain all evaluation information
        eva = []
        # Error sum on the testbench
        error_sum = 0.
        # Sum of quadratic error on testbench
        quad_error_sum = 0.
        for point in self.testbench[task]:
            if tree == None:
                dist = self.learning_agent.dataset.y_spaces[task].options['out_dist']
            else:
                dist,_ = tree.query(point, k=1, eps=0.0, p=2)
            p = dist / self.learning_agent.dataset.y_spaces[task].options['max_dist']
            eva.append(p)
            error_sum += p
            quad_error_sum += p**2
        error_sum /= float(len(eva))
        quad_error_sum /= float(len(eva))
        std = np.sqrt(quad_error_sum - error_sum**2)
        
        return eva, error_sum, std
    
    def evaluate(self, t):
        """Evaluate performance and record it with time given."""
        
        # Will contain all evaluation information
        all_eva = []
        # Error sum on the testbench
        mean_error = 0.
        # Mean of the task spaces error standard deviations
        mean_std = 0.
        nb_task_eval = 0
        for task in range(len(self.testbench)):
            if len(self.testbench[task]) > 0:
                eva, error_sum, std = self.compute_evaluation(task, t)
                all_eva.append([eva, error_sum, std])
                mean_error += error_sum
                mean_std += std
                nb_task_eval += 1
            else:
                all_eva.append([[], 0., 0.])
            #print("Evaluated task " + str(task))
        mean_error /= float(nb_task_eval)
        mean_std /= float(nb_task_eval)
        self.evaluation.append([t, all_eva, mean_error, mean_std])
    
    def evaluate_all(self, t, verbose=False):
        """Evaluate the learning agent at different times."""
        for tt in t:
            if verbose:
                print("Evaluation t=" + str(tt))
            self.evaluate(tt)
    
    def get_lw(self):
        return LightWeightEvaluation(self.learning_agent, self.evaluation)
    
    def plot(self, ax, options):
        """Plot evaluation on all iterations."""
        x = []
        y = []
        for i in self.evaluation:
            x.append(i[0])
            y.append(i[2])
        ax.plot(np.array(x), np.array(y), marker=options['marker'], linestyle=options['linestyle'], color=options['color'])
        plt.yscale('log')
        plt.grid(True)
    
    def plot_mean_task(self, task, ax, options):
        """Plot evaluation for a specific task space."""
        x = []
        y = []
        for i in self.evaluation:
            x.append(i[0])                
            y.append(i[1][task][1])
        ax.plot(np.array(x), np.array(y), marker=options['marker'], linestyle=options['linestyle'], color=options['color'])
        plt.yscale('log')
        plt.grid(True)
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_complete_visualizer(self, prefix=""):
        """Create dictionary to visualize evaluation."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = prefix + "Evaluations of the algo"
        dico['color'] = ['k']
        dico['marker'] = ['']
        dico['linestyle'] = ['solid']
        dico['axes'] = ['Iterations', 'Mean error on testbench']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot(ax, options)]
        
        return dico
    
    def get_task_visualizer(self, task, prefix=""):
        """Create dictionary to visualize evaluation of a specific task space."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = prefix + "Evaluations of the algo on task " + str(task)
        dico['color'] = ['k']
        dico['marker'] = ['']
        dico['linestyle'] = ['solid']
        dico['axes'] = ['Iterations', 'Mean error on testbench']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_mean_task(task, ax, options)]
        
        return dico


class EvaluationV5:
    """Evaluation that uses execution order instead of iterations as time."""
    def __init__(self, learning_agent, testbench):
        """
        learning_agent LearningAgentV4: the learning agent
        testbench list of list of float arrays: contains the points used to compute error for each task
        """
        self.learning_agent = learning_agent
        self.testbench = testbench
        # Will contain all evaluation performed
        # Each evaluation structured as below:
        #    [t, [[[perf_point0, perf_point1,...], error_sum_task_0, std_task_0], ...], mean_error, mean_std]
        self.evaluation = []
    
    def compute_evaluation(self, task, t):
        """Compute evaluation for a given task."""        
        assert len(self.testbench[task]) > 0
        
        # Find the size of the task space at the time
        exeId = t-1
        max_i = len(self.learning_agent.dataset.y_spaces[task].data)
        for i in range(len(self.learning_agent.dataset.y_spaces[task].data)):
            if self.learning_agent.dataset.y_spaces[task].ids[i] > exeId:
                max_i = i
                break
        
        # Retrieve dataset at the time
        data = np.array(self.learning_agent.dataset.y_spaces[task].data[0:max_i])
        
        # Create KD-Tree for evaluation
        tree = None
        if len(data) > 0:
            tree = cKDTree(data)
        
        # Will contain all evaluation information
        eva = []
        # Error sum on the testbench
        error_sum = 0.
        # Sum of quadratic error on testbench
        quad_error_sum = 0.
        for point in self.testbench[task]:
            if tree == None:
                dist = self.learning_agent.dataset.y_spaces[task].options['out_dist']
            else:
                dist,_ = tree.query(point, k=1, eps=0.0, p=2)
            p = dist / self.learning_agent.dataset.y_spaces[task].options['max_dist']
            eva.append(p)
            error_sum += p
            quad_error_sum += p**2
        error_sum /= float(len(eva))
        quad_error_sum /= float(len(eva))
        std = np.sqrt(quad_error_sum - error_sum**2)
        
        return eva, error_sum, std
    
    def evaluate(self, t):
        """Evaluate performance and record it with time given."""
        
        # Will contain all evaluation information
        all_eva = []
        # Error sum on the testbench
        mean_error = 0.
        # Mean of the task spaces error standard deviations
        mean_std = 0.
        nb_task_eval = 0
        for task in range(len(self.testbench)):
            if len(self.testbench[task]) > 0:
                eva, error_sum, std = self.compute_evaluation(task, t)
                all_eva.append([eva, error_sum, std])
                mean_error += error_sum
                mean_std += std
                nb_task_eval += 1
            else:
                all_eva.append([[], 0., 0.])
        mean_error /= float(nb_task_eval)
        mean_std /= float(nb_task_eval)
        self.evaluation.append([t, all_eva, mean_error, mean_std])
    
    def evaluate_all(self, t, verbose=False):
        """Evaluate performance for every t given."""
        for tt in t:
            if verbose:
                print("Evaluation t=" + str(tt))
            self.evaluate(tt)
    
    def plot(self, ax, options):
        """Plot evaluation on all iterations."""
        x = []
        y = []
        for i in self.evaluation:
            x.append(i[0])
            y.append(i[2])
        ax.plot(np.array(x), np.array(y), marker=options['marker'], linestyle=options['linestyle'], color=options['color'])
        plt.yscale('log')
        plt.grid(True)
    
    def plot_mean_task(self, task, ax, options):
        """Plot evaluation on all iterations for a specific task space."""
        x = []
        y = []
        for i in self.evaluation:
            x.append(i[0])                
            y.append(i[1][task][1])
        ax.plot(np.array(x), np.array(y), marker=options['marker'], linestyle=options['linestyle'], color=options['color'])
        plt.yscale('log')
        plt.grid(True)
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_complete_visualizer(self, prefix=""):
        """Create dictionary to visualize evaluation."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = prefix + "Evaluations of the algo"
        dico['color'] = ['k']
        dico['marker'] = ['']
        dico['linestyle'] = ['solid']
        dico['axes'] = ['Iterations', 'Mean error on testbench']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot(ax, options)]
        
        return dico
    
    def get_task_visualizer(self, task, prefix=""):
        """Create dictionary to visualize evaluation for a specific task."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = prefix + "Evaluations of the algo on task " + str(task)
        dico['color'] = ['k']
        dico['marker'] = ['']
        dico['linestyle'] = ['solid']
        dico['axes'] = ['Iterations', 'Mean error on testbench']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_mean_task(task, ax, options)]
        
        return dico




    