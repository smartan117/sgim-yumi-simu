from utils import *
from yumi_control import *
from evaluation_multiprocesses import *
from analyze import *
from visualize_bunches import *
import sys
import os



def analyze_la(file, testbench):
    la = load_bin(file).learning_agent
    print("[" + file + "] loaded")
    #eva = EvaluationV4(la, testbench)
    #eva.evaluate_all([0, 100, 500, 1000, 2000, 5000, 10000])
    #print("[" + file + "] evaluated")
    #save_bin(eva.get_lw(), file+"_eval")

    data = la.dataset
    res = {}
    res['proc_l'] = []
    for task in range(1,len(data.y_spaces)):
        res['proc_l'].append(compute_p_types_interpol(data, testbench[task], task))
        print("[" + file + "] procedures learned for task " + str(task))
    res['proc_u'] = count_will_p_types_tasks(la).tolist()
    print("[" + file + "] procedures used during learning process computed")
    res['strat_tasks'] = la.compute_strategy_task().tolist()
    res['tasks'] = la.compute_tasks().tolist()
    res['strats'] = la.compute_strategies().tolist()
    print("[" + file + "] strategical choices computed")
    save_bin(res, file+"_analyzed")
    print("[" + file + "] terminated")


def analyze_stats(folders, res_folder="Analyze"):
    #evas = {}
    res = {}
    nb_tasks = 5  # -1
    if not os.path.exists(res_folder):
        os.makedirs(res_folder)
    for folder in os.listdir("Yumi_Simus_Results_bin/."):
        test = folder[:-3]  # DIRTY
        if folder[-2] == "_":
            test = folder[:-2]
        if folder[:len(folders)] == folders:
            if not res.has_key(test):
                #evas[test] = []
                res[test] = []
            if not os.path.exists("Yumi_Simus_Results_bin/" + folder + "/eval_test_analyzed"):
                print("Warning! Test file analyzed not present!")
                continue
            #eva = load_bin(folder + "/test_eval")
            #if nb_tasks < 0:
            #    nb_tasks = len(eva.learning_agent.dataset.y_spaces)
            #eva.learning_agent = None
            #evas[test].append(eva)
            r = load_bin("Yumi_Simus_Results_bin/" + folder + "/eval_test_analyzed")
            res[test].append(r)
    """
    global_evals = []
    task_evals = []
    for i in range(nb_tasks):
        task_evals.append([])

    for k in evas.keys():
        eva, t_list = combine_evaluations(evas[k])
        print("["+k+"] final std: " + str(eva[-1][-1]))
        global_evals.append([])
        for i in range(nb_tasks):
            task_evals[i].append([])
        for t, e in zip(t_list, eva):
            global_evals[-1].append([t, e[-2]])
            for i in range(nb_tasks):
                task_evals[i][-1].append([t, e[i][0]])

    prepare_plot(global_evals, evas.keys(), res_folder+"/evaluation/", options={"y": "log"})
    for i in range(nb_tasks):
        prepare_plot(task_evals[i], evas.keys(), res_folder+"/evaluation_task_"+str(i)+"/", options={"y": "log"})
    """

    for k in res.keys(): # for each type of results
        #l = {'proc_l': [], 'proc_u': [], 'strat_tasks': [], 'tasks': [], 'strats': []}
        l = {}
        l['proc_l'] = np.zeros((nb_tasks-1, nb_tasks, nb_tasks), dtype=np.float64)
        l['proc_u'] = np.zeros((nb_tasks-1, nb_tasks, nb_tasks), dtype=np.float64)
        for r in res[k]: # for each test results
            for i in range(nb_tasks-1): # need to normalize the distributions
                l['proc_l'][i,:,:] += np.array(r['proc_l'],dtype=np.float64)[i,:,:] / np.sum(np.array(r['proc_l'])[i,:,:])
                l['proc_u'][i,:,:] += np.array(r['proc_u'],dtype=np.float64)[i,:,:] / np.sum(np.array(r['proc_u'])[i,:,:])
            if not l.has_key('strats'):
                l['strats'] = np.array(r['strats'], dtype=np.float64)
            else:
                l['strats'] += np.array(r['strats'], dtype=np.float64)
            if not l.has_key('tasks'):
                l['tasks'] = np.array(r['tasks'], dtype=np.float64)
            else:
                l['tasks'] += np.array(r['tasks'], dtype=np.float64)
            if not l.has_key('strat_tasks'):
                l['strat_tasks'] = np.array(r['strat_tasks'], dtype=np.float64)
            else:
                l['strat_tasks'] += np.array(r['strat_tasks'], dtype=np.float64)

        for kk in l.keys():
            l[kk] /= float(len(res[k]))

        if not os.path.exists(res_folder+"/"+k):
            os.makedirs(res_folder+"/"+k)
        for i in range(1, nb_tasks):
            save_csv(l['proc_l'][i-1,:,:].tolist(), res_folder+"/"+k+"/procedure_repartition_task_"+str(i)+".csv")
            save_csv(l['proc_u'][i-1,:,:].tolist(), res_folder+"/"+k+"/procedure_used_task_"+str(i)+".csv")
        save_csv(l['strat_tasks'].tolist(), res_folder+"/"+k+"/strat_tasks.csv")
        data = []
        for j in range(l['strats'].shape[1]):
            data.append([])
            for i in range(l['strats'].shape[0]):
                data[-1].append([i, l['strats'][i,j]])
        prepare_plot(data, [str(i) for i in range(len(data))], res_folder+"/"+k+"/strats/")
        data = []
        for j in range(l['tasks'].shape[1]):
            data.append([])
            for i in range(l['tasks'].shape[0]):
                data[-1].append([i, l['tasks'][i,j]])
        prepare_plot(data, [str(i) for i in range(len(data))], res_folder+"/"+k+"/tasks/")


folders = "Yumi_SGIM_HL"



analyze_stats(folders, "Analyse_New2")

"""

testbench = SimuYumiExperimentatorV2.create_testbench()
print("Testbench created")

for folder in os.listdir("Yumi_Simus_Results_bin/."):
    if folder[:len(folders)] == folders and not os.path.exists(folder+"/eval_test_analyzed"):
        analyze_la("Yumi_Simus_Results_bin/" + folder + "/eval_test", testbench)
"""
