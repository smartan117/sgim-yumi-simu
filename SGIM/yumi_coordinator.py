import zmq
import time
import json
import copy

from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')

# For debug purposes
from test_table import *


# TO DO add error handling for motion in simu and real robot


class YumiCoordinator:
    
    def __init__(self):
        self.client1 = None
        self.client2 = None
        self.client3 = None
        self.client4 = None
        self.server = None
        self.order = None
        self.ack = None
        self.real_yumi_running = False  # indicates whether the real Yumi is running sthing
        self.simu_yumi_running = False  # indicates whether the simu Yumi is running sthing
        self.next_order = None
        self.simu_order = None
        self.real_order = None
        self.running = True  # indicates whether the experiment must be running
        self.real_error = False  # indicates if the last motion results or should result in an error
        self.simu_error = False  # indicates if the last motion results in an error
        # For debug purposes
        self.dummyTable = DummyTable()
    
    def connect(self):
        ctx = zmq.Context()
        self.server = ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5551")
        self.client1 = ctx.socket(zmq.REQ)  # with ML node
        self.client1.connect("tcp://127.0.0.1:5552")
        self.client2 = ctx.socket(zmq.REQ)  # with Table node
        self.client2.connect("tcp://127.0.0.1:5553")
        self.client3 = ctx.socket(zmq.REQ)  # with simu Yumi node
        self.client3.connect("tcp://127.0.0.1:5554")
        self.client4 = ctx.socket(zmq.REQ)  # with real Yumi node
        self.client4.connect("tcp://127.0.0.1:5555")
    
    def run(self):
        while True:
            #try:
            msg = self.server.recv()
            print("Received: " + msg)
            valid = self.analyze(msg)
            if not valid:
                ack = {"type": "error", "payload": msg, "valid": False}
            else:
                ack = {"type": self.order['type'], "valid": True}
            print("Acking: " + str(ack))
            self.server.send(json.dumps(ack))
            self.apply_order()
            self.check_state()
            #except Exception:
            #    print("Connexion error!")
            #    return
    
    def analyze(self, msg):
        try:
            self.order = json.loads(msg)
            return (self.order.has_key("type") and self.order.has_key("valid") and self.order.has_key("from"))
        except Exception:
            return False
    
    def analyze_ack(self, msg):
        try:
            order = json.loads(msg)
            return (order.has_key("type") and self.order.has_key("valid"))
        except Exception:
            return False
    
    def analyze_table(self, msg):
        try:
            order = json.loads(msg)
            if (order.has_key("type") and order.has_key("payload") and order['payload'].has_key('y_list') \
                    and order['payload'].has_key('y_types')):
                return order['payload']['y_list'], order['payload']['y_types']
            print("An error occured with the table controller...")
            return [], []
        except Exception:
            print("An error occured with the table controller...")
            return [], []
    
    def apply_order(self):
        if self.order['from'] == 1:
            # Order comes from ML
            self.next_order = copy.deepcopy(self.order)
            """
            if self.order['type'] == "move" and self.order.has_key('payload'):
                self.next_order = copy.deepcopy(self.order)
            elif self.order['type'] == "reset" and self.order.has_key('payload'):
                self.next_order = copy.deepcopy(self.order)
            #elif self.order['type'] == "reset_js" and  self.order.has_key('payload'):
            #
            #elif self.order['type'] == "init":
            #
            elif self.order['type'] == "pause":
                self.next_order = copy.deepcopy(self.order)
            elif self.order['type'] == "resume":
                self.next_order = copy.deepcopy(self.order)
            else:
                pass
            """
            
        elif self.order['from'] == 3:  # and self.order['type'] == self.simu_order['type']:
            # Order comes from simu Yumi node
            self.simu_yumi_running = False
            self.simu_order['valid'] = self.order['valid']
            if self.order.has_key("payload"):
                self.simu_order['payload'].update(self.order['payload'])
            """
            if self.order['type'] == "move" and self.order.has_key('payload') and self.order.has_key('valid'):
                self.simu_yumi_running = False
            elif self.order['type'] == "reset" and self.order.has_key('payload') and self.order.has_key('valid'):
                self.simu_yumi_running = False
            else:
            """    
        elif self.order['from'] == 4:  #  and self.order['type'] == self.real_order['type']:
            # Order comes from real Yumi node
            self.real_yumi_running = False
            self.real_order = copy.deepcopy(self.order)
            
            if self.real_order['type'] == "move" and self.real_order['valid']:
                dummyTable.send(np.random.uniform(0. 0.5, 2).tolist())
                order = {"type": "outcomes", "from": 0}
                client2.send(json.dumps(order))
                msg = client2.recv()
                y_list, y_types = self.analyze_table(msg)
                self.real_order['payload']['y_list'] = y_list
                self.real_order['payload']['y_types'] = y_types
            elif self.real_order['type'] == "reset":
                dummyTable.sendLeave()
                order = {"type": "reset", "from": 0}
                client2.send(json.dumps(order))
                msg = client2.recv()
            
            """
            if self.order['type'] == "move" and self.order.has_key('payload') and self.order.has_key('valid'):
                pass
            elif self.order['type'] == "reset_js" and self.order.has_key('payload') and self.order.has_key('valid'):
                pass
            elif self.order['type'] == "init" and self.order.has_key('valid'):
                pass
            else:
                pass
            """
    
    def check_state(self):
        if not (self.real_yumi_running or self.simu_yumi_running):
            # Both robots not running
            if self.next_order != None:
                # A new order is available
                self.real_order = copy.deepcopy(self.simu_order)
                self.simu_order = copy.deepcopy(self.next_order)
                self.next_order = None
                if not self.real_order or self.real_order['valid'] or self.simu_order["type"] == "reset":
                    # Means the simu robot is not in error
                    print("Sending to simu: " + str(self.simu_order))
                    self.client3.send(json.dumps(self.simu_order))
                    msg = self.client3.recv()
                    print("Ack: " + msg)
                    self.simu_yumi_running = self.analyze_ack(msg)
                else:
                    self.simu_order['valid'] = False
                if self.real_order and self.real_order['valid']:
                    # The simu worked so the order can be sent to real one
                    print("Sending to simu: " + str(self.real_order))
                    self.client4.send(json.dumps(self.real_order))
                    msg = self.client4.recv()
                    print("Ack: " + msg)
                    self.real_yumi_running = self.analyze_ack(msg)
        if not (self.real_yumi_running or self.simu_yumi_running):
            # Both robots not running
            if self.next_order == None:
                # No new order available
                # Means we must ack the ML node
                if self.real_order == None:
                    ack = {"type": "Nothing", "valid": True}
                    if not ack.has_key('payload'):
                        ack['payload'] = {}
                else:
                    ack = copy.deepcopy(self.real_order)
                    if not ack.has_key('payload'):
                        ack['payload'] = {}
                    if self.simu_order['type'] == "move" and self.simu_order['valid']:
                        ack['payload']['joints'] = self.simu_order['payload']['joints']
                print("Responding: " + str(ack))
                self.client1.send(json.dumps(ack))
                msg = self.client1.recv()
                print("Ack: " + msg)
                if not self.analyze_ack(msg):
                    print("ML node indicates an error...")
                print("")
                    


if __name__ == "__main__":
    node = YumiCoordinator()
    node.connect()  
    node.run()          























