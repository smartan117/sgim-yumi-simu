import zmq
import time
import json
import copy

from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')



class DummyTable:
    
    def __init__(self):
        self.client = None
    
    def connect(self):
        ctx = zmq.Context()
        self.client = ctx.socket(zmq.REQ)  # with Table node
        self.client.connect("tcp://127.0.0.1:5553")
    
    def sendPosition(self, pose):
        order = {'type': "pose", "value": pose}
        self.client.send(json.dumps(order))
        msg = self.client.recv()
        try:
            ack = json.loads(msg)
            return ack.has_key('type') and ack.has_key('valid') and ack['type'] == "pose" and ack["valid"]
        except Exception:
           return False
    
    def sendLeave(self):
        order = {'type': "leave"}
        self.client.send(json.dumps(order))
        msg = self.client.recv()
        try:
            ack = json.loads(msg)
            return ack.has_key('type') and ack.has_key('valid') and ack['type'] == "leave" and ack["valid"]
        except Exception:
            return False


