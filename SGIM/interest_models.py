import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import math
import copy
import random

import pickle


from utils import uniform_sampling, save_json, save_str, load_json, load_str, save_raw, load_raw



class RegStats:
    def __init__(self):
        self.task = -1
        self.strategy = -1
        self.id = -1
        self.progress = 0.0
    
    def __str__(self):
        return "T" + str(self.task) + " - Strategy " + str(self.strategy) + \
                " (ID " + str(self.id) + ") -> " + str(self.progress)


class TaskStats:
    n_show = 10
    def __init__(self):
        self.best_regions = []
        self.progress = 0.0
        self.nb_regions = 0
    
    def __str__(self):
        s = "Nb regions: " + str(self.nb_regions) + "\nSum progress: " + str(self.progress) + "\nBest regions: \n"
        for r in self.best_regions:
            s += str(r) + "\n"
        return s


class ModelStats:
    
    def __init__(self, nb_tasks):
        self.best_regions = []
        self.task_stats = []
        self.task_strat_stats = None
        for i in range(nb_tasks):
            self.task_stats.append(TaskStats())
    
    def __str__(self):
        s = "Best overall regions: \n"
        for r in self.best_regions:
            s += str(r) + "\n"
        s += "\n"
        for i, t in enumerate(self.task_stats):
            s += "Task " + str(i) + ":\n" + str(t) + "\n"
        return s


class InterestModelV3:
    """Model of interest for the intrinsic motivated learner."""
    def __init__(self, dataset, options):
        """
        dataset DatasetV2: dataset of the learner
        options dict: contain parameters of the model
        """
        # Different keys of options
        ## nb_regions: maximum number of best regions considered when choosing with intrinsic motivation
        ## around: radius of the ball used to sample around a specific point
        ## costs: list containing the cost of each available strategy for the learner
        ## ## contains other options described in InterestRegion class documentation
        self.options = options
        self.interest_trees = []
        self.debug_sampling = []
        for y in dataset.y_spaces:
            bounds = copy.deepcopy(y.bounds)
            bounds['min'] = bounds['min']
            bounds['max'] = bounds['max']
            reg = InterestRegion(bounds, self.options)
            self.interest_trees.append(InterestTree(reg, [reg]))
    
    def to_config(self):
        dico = {}
        dico['options'] = self.options
        return dico
    
    def add_point(self, point, task, strat, progress):
        """Add a point and its corresponding progress in the model."""
        self.interest_trees[task].add_point(point, strat, progress)
        
    def sample_random(self):
        """Sample one point at random in the regions."""
        task = random.randint(0, len(self.interest_trees)-1)
        strat = random.randint(0, len(self.options['costs'])-1)
        goal = self.interest_trees[task].region.choose_random_goal()
        
        ### DEBUG START HERE
        self.debug_sampling.append(None)
        ### DEBUG END HERE
        
        return task, strat, goal
        
    def sample_good_point(self):
        """Sample one point at random in one of the best regions."""
        ir, best_prob = self.choose_region_stochastic()
        if ir == None:
            return self.sample_random()
        else:
            task = ir[0]
            strat = ir[1]
            goal = ir[2].choose_random_goal()
            ### DEBUG START HERE
            self.debug_sampling.append((ir[2].interests[strat], best_prob))
            ### DEBUG END HERE
            return task, strat, goal
        
    def sample_best_point(self):
        """Sample one point around the best point in one of the best regions."""
        ir, best_prob = self.choose_region_stochastic()
        if ir == None:
            return self.sample_random()
        else:
            task = ir[0]
            strat = ir[1]
            point = ir[2].best_point(strat)
            goal = ir[2].choose_goal_around(point, self.options['around'])
            ### DEBUG START HERE
            self.debug_sampling.append((ir[2].interests[strat], best_prob))
            ### DEBUG END HERE
            return task, strat, goal
    
    def get_regions(self):
        ir = []
        for task in range(len(self.interest_trees)):
            for id, i in enumerate(self.interest_trees[task].list_regions):
                for strat in range(len(i.interests)):
                    if i.interests[strat] != 0.0:
                        ir.append(RegStats())
                        ir[-1].task = task
                        ir[-1].strategy = strat
                        ir[-1].id = id
                        ir[-1].progress = math.fabs(i.interests[strat])
        return ir
    
    def get_stats(self):
        ir = self.get_regions()
        ir.sort(key= lambda i: -math.fabs(i.progress))
        stats = ModelStats(len(self.interest_trees))
        stats.best_regions = ir[:min(len(ir),TaskStats.n_show)]
        stats.task_strat_stats = np.zeros((len(self.interest_trees), len(self.options['costs'])))
        n_reg = 0
        for i in ir:
            if len(stats.task_stats[i.task].best_regions) < TaskStats.n_show:
                stats.task_stats[i.task].best_regions.append(i)
            stats.task_stats[i.task].progress += math.fabs(i.progress)
            stats.task_stats[i.task].nb_regions += 1
            if n_reg < self.options['nb_regions']:
                stats.task_strat_stats[i.task, i.strategy] += math.fabs(i.progress)
            n_reg += 1
        return stats
    
    def choose_region_stochastic(self):
        """Choose one of the best regions and its most adapted strategy."""
        probs = []
        ir = []
        interests = []
        for task in range(len(self.interest_trees)):
            for i in self.interest_trees[task].list_regions:
                for strat in range(len(i.interests)):
                    if i.interests[strat] != 0.0:
                        ir.append([task, strat, i])
                        interests.append(i.interests[strat])
                        probs.append(math.fabs(i.interests[strat])) # Progresses are positive or negative
                        #prob_sum += math.fabs(i.interests[strat])
        #ids = range(len(probs))
        #ids.sort(key= lambda i: probs[i])
        probs = np.array(probs)
        #n = max(0, len(probs)-self.options['nb_candidates'])
        #ids = ids[n:len(ids)]
        #probs = probs[ids]
        #prob_sum = np.sum(probs)
        #probs /= prob_sum
        ids = range(len(probs))
        ids.sort(key= lambda i: -probs[i])
        ids = ids[0:min(len(ids), self.options['nb_regions'])]
        if len(probs) > 0 and probs[ids[0]] > 0.0:
            # At least one region is non-empty & could be chosen
            #for p in probs:
            #    p /= prob_sum
            probs = probs[ids]/np.sum(probs[ids])
            n = ids[uniform_sampling(probs)]
            best_interest = ir[ids[0]][2].interests[ir[ids[0]][1]]
            
            #print probs[n], ir[n]
            # Return result as ([task, strat, interest_region], max_interest)
            return ir[n], best_interest
        else:
            # All regions are empty or could not be chosen
            return None, 0.0
    
    ### deprecated
    def plot_list_reg_v2(self, task, strat, ax, options):
        """Plot the regions for the given task space and strategy (only for 1D or 2D task spaces)."""
        norm = 0.0
        for r in self.interest_trees[task].list_regions:
            for i in r.interests:
                norm = max(-i, norm)
        for r in self.interest_trees[task].list_regions:
            if r.split == False:
                r.plot_v2(strat, norm, ax, options)


class InterestModelV4:
    """Model of interest for the intrinsic motivated learner."""
    def __init__(self, dataset, options):
        """
        dataset DatasetV2: dataset of the learner
        options dict: contain parameters of the model
        """
        # Different keys of options
        ## nb_regions: maximum number of best regions considered when choosing with intrinsic motivation
        ## around: radius of the ball used to sample around a specific point
        ## costs: list containing the cost of each available strategy for the learner
        ## ## contains other options described in InterestRegion class documentation
        self.options = options
        self.interest_trees = []
        self.debug_sampling = []
        for y in dataset.y_spaces:
            self.interest_trees.append([])
            for cost in self.options['costs']:
                bounds = copy.deepcopy(y.bounds)
                bounds['min'] = bounds['min']
                bounds['max'] = bounds['max']
                op = copy.deepcopy(self.options)
                op['cost'] = cost
                del op['costs']
                reg = InterestRegionV2(bounds, op)
                self.interest_trees[-1].append(InterestTreeV2(reg, [reg]))
    
    def to_config(self):
        dico = {}
        dico['options'] = self.options
        return dico
    
    def add_point(self, point, task, strat, progress):
        """Add a point and its corresponding progress in the model."""
        self.interest_trees[task][strat].add_point(point, progress)
        
    def sample_random(self):
        """Sample one point at random in the regions."""
        task = random.randint(0, len(self.interest_trees)-1)
        strat = random.randint(0, len(self.options['costs'])-1)
        goal = self.interest_trees[task][strat].region.choose_random_goal()
        
        ### DEBUG START HERE
        self.debug_sampling.append(None)
        ### DEBUG END HERE
        
        return task, strat, goal
        
    def sample_good_point(self):
        """Sample one point at random in one of the best regions."""
        ir, best_prob = self.choose_region_stochastic()
        if ir == None:
            return self.sample_random()
        else:
            task = ir[0]
            strat = ir[1]
            goal = ir[2].choose_random_goal()
            ### DEBUG START HERE
            self.debug_sampling.append((ir[2].interest, best_prob))
            ### DEBUG END HERE
            return task, strat, goal
        
    def sample_best_point(self):
        """Sample one point around the best point in one of the best regions."""
        ir, best_prob = self.choose_region_stochastic()
        if ir == None:
            return self.sample_random()
        else:
            task = ir[0]
            strat = ir[1]
            point = ir[2].best_point(strat)
            goal = ir[2].choose_goal_around(point, self.options['around'])
            ### DEBUG START HERE
            self.debug_sampling.append((ir[2].interest, best_prob))
            ### DEBUG END HERE
            return task, strat, goal
    
    def get_regions(self):
        ir = []
        for task in range(len(self.interest_trees)):
            for strat in range(len(self.interest_trees[task])):
                for id, i in enumerate(self.interest_trees[task][strat].list_regions):
                    if i.interest != 0.0:
                        ir.append(RegStats())
                        ir[-1].task = task
                        ir[-1].strategy = strat
                        ir[-1].id = id
                        ir[-1].progress = math.fabs(i.interest)
        return ir
    
    def get_stats(self):
        ir = self.get_regions()
        ir.sort(key= lambda i: -math.fabs(i.progress))
        stats = ModelStats(len(self.interest_trees))
        stats.best_regions = ir[:min(len(ir),TaskStats.n_show)]
        stats.task_strat_stats = np.zeros((len(self.interest_trees), len(self.options['costs'])))
        for i in ir:
            if len(stats.task_stats[i.task].best_regions) < TaskStats.n_show:
                stats.task_stats[i.task].best_regions.append(i)
            stats.task_stats[i.task].progress += math.fabs(i.progress)
            stats.task_stats[i.task].nb_regions += 1
            stats.task_strat_stats[i.task, i.strategy] += math.fabs(i.progress)
        return stats
    
    def choose_region_stochastic(self):
        """Choose one of the best regions and its most adapted strategy."""
        probs = []
        ir = []
        interests = []
        for task in range(len(self.interest_trees)):
            for strat in range(len(self.interest_trees[task])):
                for i in self.interest_trees[task][strat].list_regions:
                    if i.interest != 0.0:
                        ir.append([task, strat, i])
                        interests.append(i.interest)
                        probs.append(math.fabs(i.interest)) # Progresses are positive or negative
                        #prob_sum += math.fabs(i.interests[strat])
        #ids = range(len(probs))
        #ids.sort(key= lambda i: probs[i])
        probs = np.array(probs)
        #n = max(0, len(probs)-self.options['nb_candidates'])
        #ids = ids[n:len(ids)]
        #probs = probs[ids]
        #prob_sum = np.sum(probs)
        #probs /= prob_sum
        ids = range(len(probs))
        ids.sort(key= lambda i: -probs[i])
        ids = ids[0:min(len(ids), self.options['nb_regions'])]
        if len(probs) > 0 and probs[ids[0]] > 0.0:
            # At least one region is non-empty & could be chosen
            #for p in probs:
            #    p /= prob_sum
            probs = probs[ids]/np.sum(probs[ids])
            n = ids[uniform_sampling(probs)]
            best_interest = ir[ids[0]][2].interest
            
            #print probs[n], ir[n]
            # Return result as ([task, strat, interest_region], max_interest)
            return ir[n], best_interest
        else:
            # All regions are empty or could not be chosen
            return None, 0.0


class InterestRegion:
    """Implements an interest region."""
    
    def __init__(self, bounds, options):
        """
        bounds float list dict: contains min and max boundaries of the region
        options dict: different options used by the interest model
        """
        assert len(bounds['min']) == len(bounds['max'])
        self.points = []
        self.progresses = []
        self.interests = []
        for i in range(len(options['costs'])):
            self.points.append([])
            self.progresses.append([])
            self.interests.append(0.0)
        self.bounds = bounds
        # Contains the following keys:
        #   'min_surface': the minimum surface of a region to be still splittable
        #   'max_points': the maximum of points contained in a region (used to split)
        #   'interest_window': the number of last progress measures to consider for computing region interest (to take newer points only)
        #   'costs': cost per strategy (usually strategies involving a teacher have a higher cost)
        self.options = options
        self.split = False
        self.set_splittable()
        
    def set_splittable(self): # Change it to use max depth tree instead ???
        """Check if the region is splittable according to its surface."""
        surface = 1.
        for bmin, bmax in zip(self.bounds['min'], self.bounds['max']):
            surface *= (bmax - bmin)
        self.splittable = (surface > self.options['min_surface'])
        
    def add_point(self, point, strat, progress):
        """Add a point and its progress in the lists of the given strategy."""
        assert len(point) == len(self.bounds['min'])
        assert strat < len(self.points)
        
        self.points[strat].append(point)
        self.progresses[strat].append(progress)       
        # If enough points to split
        if len(self.points[strat]) > self.options['max_points']:
            # If region can be split
            if self.splittable and not self.split:
                self.split = True
            else:
                # Remove oldest points and progresses
                self.points[strat] = self.points[strat][(len(self.points[strat]) - self.options['max_points']):len(self.points[strat])]
                self.progresses[strat] = self.progresses[strat][(len(self.progresses[strat]) - self.options['max_points']):len(self.progresses[strat])]
        # Compute interest of the region for the strategy
        self.compute_interest(strat)
            
    def compute_interest(self, strat):
        """Compute interest of the region for given strategy."""
        self.interests[strat] = (self.mean_progress(self.progresses[strat]) / self.options['costs'][strat])
        
    def mean_progress(self, progresses):
        """Compute mean progress according to the interest window."""
        n = min(len(progresses), self.options['interest_window'])
        mean_progress = 0.
        for i in range(n):
            mean_progress += progresses[len(progresses)-i-1]
        return (mean_progress / float(n))
    
    def best_point(self, strat):
        """Find point with the highest progress inside region for given strategy."""
        n = min(len(self.progresses[strat]), self.options['interest_window'])
        if n == 0:
            print("Error: best_point should not be used for empty region.")
        i = range(n)
        i.sort(key= lambda j: self.progresses[strat][j])
        return self.points[strat][i[0]]
    
    def choose_goal_around(self, point, around):
        """Sample a goal around the given point inside region and a ball."""
        goal = copy.deepcopy(point)
        v = []
        norm = 0.
        r_max = float("inf")
        # Compute a random normalized vector indicating exploration direction
        comp = np.random.uniform(-1.0, 1.0, len(goal))
        comp = comp / np.sqrt(np.sum(comp**2))
        # Compute max norm to still be in the interest region
        for i in range(len(goal)):
            if comp[i] > 0.0:
                r_max = min((self.bounds['max'][i] - goal[i])/comp[i], r_max)
            elif comp[i] < 0.0:
                r_max = min((self.bounds['min'][i] - goal[i])/comp[i], r_max)
        # Make sure the norm is smaller than around parameter
        r_max = min(r_max, around)
        r = random.uniform(0., r_max)
        for i in range(len(goal)):
            goal[i] += comp[i] * r
        return np.array(goal)
    
    def choose_random_goal(self):
        """Choose a random goal inside the region."""
        goal = []
        for bmin, bmax in zip(self.bounds['min'], self.bounds['max']):
            goal.append(random.uniform(bmin, bmax))
        return np.array(goal)
    
    ### Deprecated
    
    def plot(self, fig_id, color, norm):
        """
        Plot interest region as a rectangle patch which transparency indicates interest.
        
        fig_id int
        color string: indicates the colour of the patch
        norm float: used to normalize the patches transparency
        """
        num_plots = min(len(self.interests), 10)
        plt.ion()        
        fig = plt.figure(fig_id)
        plt.show()
        if norm == 0.0:
            norm2 = 1.0
        else:
            norm2 = norm
        n = (num_plots + 1) / 2
        for s in range(num_plots):
            ax = fig.add_subplot(str(n) + "2" + str(s+1))
            if len(self.bounds['min']) == 1:
                p = patches.Rectangle( \
                    (self.bounds['min'][0], -0.5), (self.bounds['max'][0]-self.bounds['min'][0]), 1, \
                    facecolor=color, alpha=-self.interests[s]/norm2)
                ax.add_patch(p)
            elif len(self.bounds['min']) == 2:
                p = patches.Rectangle( \
                    (self.bounds['min'][0], self.bounds['min'][1]), (self.bounds['max'][0]-self.bounds['min'][0]), \
                    (self.bounds['max'][1]-self.bounds['min'][1]), \
                    facecolor=color, alpha=-self.interests[s]/norm2)
                ax.add_patch(p)
        plt.draw()
    
    def plot_v2(self, strat, norm, ax, options):
        """Plot interest region as a rectangle patch with transparency indicating interest."""
        num_plots = min(len(self.interests), 10)
        if norm == 0.0:
            norm2 = 1.0
        else:
            norm2 = norm
        n = (num_plots + 1) / 2
        if len(self.bounds['min']) == 1:
            p = patches.Rectangle( \
                (self.bounds['min'][0], -0.5), (self.bounds['max'][0]-self.bounds['min'][0]), 1, \
                facecolor=options['color'], alpha=-self.interests[s]/norm2)
            ax.add_patch(p)
        elif len(self.bounds['min']) == 2:
            p = patches.Rectangle( \
                (self.bounds['min'][0], self.bounds['min'][1]), (self.bounds['max'][0]-self.bounds['min'][0]), \
                (self.bounds['max'][1]-self.bounds['min'][1]), \
                facecolor=options['color'], alpha=-self.interests[s]/norm2)
            ax.add_patch(p)


class InterestRegionV2:
    """Implements an interest region."""
    
    def __init__(self, bounds, options):
        """
        bounds float list dict: contains min and max boundaries of the region
        options dict: different options used by the interest model
        """
        assert len(bounds['min']) == len(bounds['max'])
        self.points = []
        self.progresses = []
        self.interest = 0.0
        self.bounds = bounds
        # Contains the following keys:
        #   'min_surface': the minimum surface of a region to be still splittable
        #   'max_points': the maximum of points contained in a region (used to split)
        #   'interest_window': the number of last progress measures to consider for computing region interest (to take newer points only)
        #   'cost': cost of strategy (usually strategies involving a teacher have a higher cost)
        self.options = options
        self.split = False
        self.set_splittable()
        
    def set_splittable(self): # Change it to use max depth tree instead ???
        """Check if the region is splittable according to its surface."""
        surface = 1.
        for bmin, bmax in zip(self.bounds['min'], self.bounds['max']):
            surface *= (bmax - bmin)
        self.splittable = (surface > self.options['min_surface'])
        
    def add_point(self, point, progress):
        """Add a point and its progress in the lists of the given strategy."""
        assert len(point) == len(self.bounds['min'])
        assert strat < len(self.points)
        
        self.points.append(point)
        self.progresses.append(progress)       
        # If enough points to split
        if len(self.points) > self.options['max_points']:
            # If region can be split
            if self.splittable and not self.split:
                self.split = True
            else:
                # Remove oldest points and progresses
                self.points = self.points[(len(self.points) - self.options['max_points']):len(self.points)]
                self.progresses = self.progresses[(len(self.progresses) - self.options['max_points']):len(self.progresses)]
        # Compute interest of the region for the strategy
        self.compute_interest()
            
    def compute_interest(self):
        """Compute interest of the region for given strategy."""
        self.interest = (self.mean_progress(self.progresses) / self.options['cost'])
        
    def mean_progress(self, progresses):
        """Compute mean progress according to the interest window."""
        n = min(len(progresses), self.options['interest_window'])
        mean_progress = 0.
        for i in range(n):
            mean_progress += progresses[len(progresses)-i-1]
        return (mean_progress / float(n))
    
    def best_point(self):
        """Find point with the highest progress inside region for given strategy."""
        n = min(len(self.progresses), self.options['interest_window'])
        if n == 0:
            print("Error: best_point should not be used for empty region.")
        i = range(n)
        i.sort(key= lambda j: self.progresses[j])
        return self.points[i[0]]
    
    def choose_goal_around(self, point, around):
        """Sample a goal around the given point inside region and a ball."""
        goal = copy.deepcopy(point)
        v = []
        norm = 0.
        r_max = float("inf")
        # Compute a random normalized vector indicating exploration direction
        comp = np.random.uniform(-1.0, 1.0, len(goal))
        comp = comp / np.sqrt(np.sum(comp**2))
        # Compute max norm to still be in the interest region
        for i in range(len(goal)):
            if comp[i] > 0.0:
                r_max = min((self.bounds['max'][i] - goal[i])/comp[i], r_max)
            elif comp[i] < 0.0:
                r_max = min((self.bounds['min'][i] - goal[i])/comp[i], r_max)
        # Make sure the norm is smaller than around parameter
        r_max = min(r_max, around)
        r = random.uniform(0., r_max)
        for i in range(len(goal)):
            goal[i] += comp[i] * r
        return np.array(goal)
    
    def choose_random_goal(self):
        """Choose a random goal inside the region."""
        goal = []
        for bmin, bmax in zip(self.bounds['min'], self.bounds['max']):
            goal.append(random.uniform(bmin, bmax))
        return np.array(goal)

    
class InterestTree:
    """Tree used to store the interest regions in a BST like structure."""
    
    def __init__(self, region, list_regions):
        """
        region InterestRegion: region attached to this tree
        list_regions InterestRegion list: list of all regions of the task space
        """
        self.left_child = None
        self.right_child = None
        self.list_regions = list_regions
        self.region = region
        self.split_value = 0.
        self.split_dim = -1
    
    def analyze(self):
        # returns nb_of_leaves_splittable vs nb_leaves
        if self.split_dim < 0:
            if self.region.splittable:
                return [1, 1]
            else:
                return [0, 1]
        l1 = self.left_child.analyze()
        l2 = self.right_child.analyze()
        return [l1[0] + l2[0], l1[1] + l2[1]]
        
    def add_point(self, point, strat, progress):
        """Add a point and its progress in the attached interest region."""
        self.region.add_point(point, strat, progress)
        if self.left_child == None:
            # Leave reached            
            if self.region.split:
                # Region must be splat
                self.define_cut_random(strat, self.region.options['max_attempts'])
                #self.define_cut_greedy(strat)
                # Making sure a cut has been found
                if self.split_dim == -1:
                    self.region.split = False
                else:
                    self.split(strat)
        else:
            # Traverse tree
            if point[self.split_dim] < self.split_value:
                self.left_child.add_point(point, strat, progress)
            else:
                self.right_child.add_point(point, strat, progress)
                
    def split(self, strat):
        """Split region according to the cut decided beforehand."""
        
        # Create child regions boundaries
        left_bounds = copy.deepcopy(self.region.bounds)
        left_bounds['max'][self.split_dim] = self.split_value
        right_bounds = copy.deepcopy(self.region.bounds)
        right_bounds['min'][self.split_dim] = self.split_value
        
        # Create empty child regions
        left_region = InterestRegion(left_bounds, self.region.options)
        right_region = InterestRegion(right_bounds, self.region.options)
        
        # Add them in the list of regions (that's the only reason we need access to the list of all regions!!!)
        self.list_regions.append(left_region)
        self.list_regions.append(right_region)
        
        # Create child interest tree
        self.left_child = InterestTree(left_region, self.list_regions)
        self.right_child = InterestTree(right_region, self.list_regions)
        
        #print("-----------------------------")
        #print("Split done")
        #print("Region [" + str(self.region.bounds['min']) + ", " + str(self.region.bounds['max']) + "] ----> Left ["
        #      + str(left_region.bounds['min']) + ", " + str(left_region.bounds['max']) + "] (")
        
        #left = 0
        #right = 0
        #for s in range(len(self.region.points)):
        #    for i in range(len(self.region.points[s])):
        #        point = self.region.points[s][i]
        #        if point[self.split_dim] < self.split_value:
        #            left += 1
        #        else:
        #            right += 1
        
        #print("Split put " + str(left) + " points left, " + str(right) + " points right.")
        #print("Split done on dimension " + str(self.split_dim) + " at value: " + str(self.split_value))
        
        # Add all points of the parent region in the child regions according to the cut
        for s in range(len(self.region.points)):
            for i in range(len(self.region.points[s])):
                point = self.region.points[s][i]
                progress = self.region.progresses[s][i]
                if point[self.split_dim] < self.split_value:
                    self.left_child.add_point(point, s, progress)
                else:
                    self.right_child.add_point(point, s, progress)
                
    def define_cut_greedy(self, strat):
        """UNTESTED method to define a cut greedily."""
        i = range(len(self.region.points[strat]))        
        max_Q = 0.
        
        # For each dimension
        for d in range(len(self.region.bounds['min'])):
            # Sort the points in the dimension
            i.sort(key = lambda j: self.region.points[strat][j][d])
            for p in range(len(i)-1):
                progress_left = 0.
                progress_right = 0.
                
                left = copy.deepcopy(i[0:(p+1)])
                right = copy.deepcopy(i[(p+1):len(i)])
                
                n = min(len(left), self.region.options['interest_window'])
                if n < len(left):
                    left.sort()
                for k in range(n):
                    progress_left += self.region.progresses[strat][left[len(left)-k-1]]
                progress_left /= float(n)
                    
                n = min(len(right), self.region.options['interest_window'])
                if n < len(right):
                    right.sort()
                for k in range(n):
                    progress_right += self.region.progresses[strat][right[len(right)-k-1]]
                progress_right /= float(n)
                
                delta_p = (progress_right - progress_left)**2
                Q = delta_p * len(left) * len(right)
                
                if Q > max_Q:
                    # Choose mean between two points as the cut
                    self.split_dim = d
                    self.split_value = (self.region.points[strat][i[p]][d] + self.region.points[strat][i[p+1]][d]) / 2.
                    
    def define_cut_random(self, strat, n_attempts): # Careful !!! The tree can't handle it if it has only one value multiple times !!!
        """Define the cut by testing a few cuts per dimension."""
        i = range(len(self.region.points[strat]))
        #n = int(math.ceil(len(self.region.points[strat])/(n_attempts+1)))
        n = float(len(self.region.points[strat]))/float(n_attempts+1)
        max_Q = 0. #-1.
        #max_card = 0
        #split_value_card = 0.
        #split_dim_card = -1
        
        # For each dimension
        for d in range(len(self.region.bounds['min'])):
            i.sort(key = lambda j: self.region.points[strat][j][d])
            # For each attempt
            for k in range(n_attempts):
                progress_left = 0.
                progress_right = 0.
                
                # Sort points by age
                #left = copy.deepcopy(i[0:((k+1)*n)])
                #right = copy.deepcopy(i[((k+1)*n):len(i)])
                #left.sort()
                #right.sort()
                
                # Id of the item following cut
                idcut = int(math.floor((k+1)*n))
                
                split_value = (self.region.points[strat][i[idcut-1]][d] + self.region.points[strat][i[idcut]][d]) / 2.
                
                # Make sure we are not trying to split something unsplittable
                if self.region.points[strat][i[idcut-1]][d] == split_value:
                    continue
                
                left = []
                right = []
                for j in range(len(self.region.points[strat])):
                    if self.region.points[strat][j][d] < split_value:
                        left.append(j)
                    else:
                        right.append(j)
                
                # Retain only the points inside interest window
                left_filtered = left[(len(left)-min(len(left),self.region.options['interest_window'])):len(left)]
                right_filtered = right[(len(right)-min(len(right),self.region.options['interest_window'])):len(right)]
                
                # Compute progress for each part
                for j in left_filtered:
                    progress_left += self.region.progresses[strat][j]
                progress_left /= float(max(len(left_filtered), 1))
                for j in right_filtered:
                    progress_right += self.region.progresses[strat][j]
                progress_right /= float(max(len(right_filtered), 1))
                
                # Compute delta_p and Q
                delta_p = (progress_left - progress_right)**2
                card = len(left) * len(right)
                if len(left) > 0 and len(right) > 0:
                    Q = delta_p * float(card)
                else:
                    Q = -float("inf")
                
                if Q > max_Q:
                    # Choose mean between two points as the cut
                    self.split_dim = d
                    self.split_value = split_value
                    max_Q = Q


class InterestTreeV2:
    """Tree used to store the interest regions in a BST like structure."""
    
    def __init__(self, region, list_regions):
        """
        region InterestRegion: region attached to this tree
        list_regions InterestRegion list: list of all regions of the task space
        """
        self.left_child = None
        self.right_child = None
        self.list_regions = list_regions
        self.region = region
        self.split_value = 0.
        self.split_dim = -1
    
    def analyze(self):
        # returns nb_of_leaves_splittable vs nb_leaves
        if self.split_dim < 0:
            if self.region.splittable:
                return [1, 1]
            else:
                return [0, 1]
        l1 = self.left_child.analyze()
        l2 = self.right_child.analyze()
        return [l1[0] + l2[0], l1[1] + l2[1]]
        
    def add_point(self, point, progress):
        """Add a point and its progress in the attached interest region."""
        self.region.add_point(point, progress)
        if self.left_child == None:
            # Leave reached            
            if self.region.split:
                # Region must be splat
                self.define_cut_random(self.region.options['max_attempts'])
                #self.define_cut_greedy(strat)
                # Making sure a cut has been found
                if self.split_dim == -1:
                    self.region.split = False
                else:
                    self.split(strat)
        else:
            # Traverse tree
            if point[self.split_dim] < self.split_value:
                self.left_child.add_point(point, progress)
            else:
                self.right_child.add_point(point, progress)
                
    def split(self, strat):
        """Split region according to the cut decided beforehand."""
        
        # Create child regions boundaries
        left_bounds = copy.deepcopy(self.region.bounds)
        left_bounds['max'][self.split_dim] = self.split_value
        right_bounds = copy.deepcopy(self.region.bounds)
        right_bounds['min'][self.split_dim] = self.split_value
        
        # Create empty child regions
        left_region = InterestRegionV2(left_bounds, self.region.options)
        right_region = InterestRegionV2(right_bounds, self.region.options)
        
        # Add them in the list of regions (that's the only reason we need access to the list of all regions!!!)
        self.list_regions.append(left_region)
        self.list_regions.append(right_region)
        
        # Create child interest tree
        self.left_child = InterestTree(left_region, self.list_regions)
        self.right_child = InterestTree(right_region, self.list_regions)
        
        #print("-----------------------------")
        #print("Split done")
        #print("Region [" + str(self.region.bounds['min']) + ", " + str(self.region.bounds['max']) + "] ----> Left ["
        #      + str(left_region.bounds['min']) + ", " + str(left_region.bounds['max']) + "] (")
        
        #left = 0
        #right = 0
        #for s in range(len(self.region.points)):
        #    for i in range(len(self.region.points[s])):
        #        point = self.region.points[s][i]
        #        if point[self.split_dim] < self.split_value:
        #            left += 1
        #        else:
        #            right += 1
        
        #print("Split put " + str(left) + " points left, " + str(right) + " points right.")
        #print("Split done on dimension " + str(self.split_dim) + " at value: " + str(self.split_value))
        
        # Add all points of the parent region in the child regions according to the cut
        for i in range(len(self.region.points)):
            point = self.region.points[i]
            progress = self.region.progresses[i]
            if point[self.split_dim] < self.split_value:
                self.left_child.add_point(point, progress)
            else:
                self.right_child.add_point(point, progress)
    
    def define_cut_random(self, n_attempts): # Careful !!! The tree can't handle it if it has only one value multiple times !!!
        """Define the cut by testing a few cuts per dimension."""
        i = range(len(self.region.points))
        #n = int(math.ceil(len(self.region.points[strat])/(n_attempts+1)))
        n = float(len(self.region.points))/float(n_attempts+1)
        max_Q = 0. #-1.
        #max_card = 0
        #split_value_card = 0.
        #split_dim_card = -1
        
        # For each dimension
        for d in range(len(self.region.bounds['min'])):
            i.sort(key = lambda j: self.region.points[j][d])
            # For each attempt
            for k in range(n_attempts):
                progress_left = 0.
                progress_right = 0.
                
                # Sort points by age
                #left = copy.deepcopy(i[0:((k+1)*n)])
                #right = copy.deepcopy(i[((k+1)*n):len(i)])
                #left.sort()
                #right.sort()
                
                # Id of the item following cut
                idcut = int(math.floor((k+1)*n))
                
                split_value = (self.region.points[i[idcut-1]][d] + self.region.points[i[idcut]][d]) / 2.
                
                # Make sure we are not trying to split something unsplittable
                if self.region.points[i[idcut-1]][d] == split_value:
                    continue
                
                left = []
                right = []
                for j in range(len(self.region.points)):
                    if self.region.points[j][d] < split_value:
                        left.append(j)
                    else:
                        right.append(j)
                
                # Retain only the points inside interest window
                left_filtered = left[(len(left)-min(len(left),self.region.options['interest_window'])):len(left)]
                right_filtered = right[(len(right)-min(len(right),self.region.options['interest_window'])):len(right)]
                
                # Compute progress for each part
                for j in left_filtered:
                    progress_left += self.region.progresses[strat][j]
                progress_left /= float(max(len(left_filtered), 1))
                for j in right_filtered:
                    progress_right += self.region.progresses[strat][j]
                progress_right /= float(max(len(right_filtered), 1))
                
                # Compute delta_p and Q
                delta_p = (progress_left - progress_right)**2
                card = len(left) * len(right)
                if len(left) > 0 and len(right) > 0:
                    Q = delta_p * float(card)
                else:
                    Q = -float("inf")
                
                if Q > max_Q:
                    # Choose mean between two points as the cut
                    self.split_dim = d
                    self.split_value = split_value
                    max_Q = Q


    