import random

import numpy as np
import warnings
import csv
import json
import pickle
import scipy.optimize
from scipy.optimize import OptimizeResult
import os
import sys






def give_progress(progress, nb_lines):
    result = "["
    symbol = 100 / nb_lines
    result += "=" * (progress / symbol)
    result += " " * (nb_lines - (progress / symbol)) + "] "
    result += str(progress)
    result += " " * (3 - len(str(progress))) + "%"
    return result


def display_progress(progress, nb_equals):
    sys.stdout.write("\r" + give_progress(progress, nb_equals))
    sys.stdout.flush()


def prepare_plot(data_list, name_list, folder, options={}):
    if not os.path.exists(folder):
        os.makedirs(folder)
    n = 1
    info = {'plots' : []}
    for data, name in zip(data_list, name_list):
        save_csv(data, folder + "plot" + str(n))
        info['plots'].append(name)
        n += 1
    info.update(options)
    save_json(info, folder + "plot.json")


def save_csv(data, filename):
    csvfile = open(filename, 'wb')
    writer = csv.writer(csvfile)
    writer.writerows(data)
    csvfile.close()


def load_csv(filename, dataType=str):
    data = []
    csvfile = open(filename, 'r')
    lines = csv.reader(csvfile)
    for row in lines:
        data.append([])
        for col in row:
            data[-1].append(dataType(col))
    csvfile.close()
    return data


def save_raw(data, filename):
    """Save data as python objects using pickle package."""
    f = open(filename, "w")
    pickle.dump(data, f)
    f.close()


def save_bin(data, filename):
    """Save data as python objects using pickle package."""
    f = open(filename, "wb")
    pickle.dump(data, f)
    f.close()


def save_json(data, filename):
    """Save data in JSON format."""
    f = open(filename, "w")
    json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))
    f.close()


def save_str(data, filename):
    """Save data as a string."""
    f = open(filename, "w")
    f.write(data)
    f.close()


def load_raw(filename):
    """Load data stored using pickle."""
    f = open(filename)
    data = pickle.load(f)
    f.close()    
    return data


def load_bin(filename):
    """Load data stored using pickle."""
    f = open(filename, "rb")
    data = pickle.load(f)
    f.close()    
    return data


def load_json(filename):
    """Load data from json file."""
    f = open(filename)
    data = json.load(f)
    f.close()
    return data


def load_str(filename):
    """Read file and return content as a string."""
    f = open(filename)
    data = f.read()
    f.close()
    return data


def sigmoid(x):
    """Compute a sigmoid."""
    y = 1.0 / (1.0 + np.exp(-x))
    return y


def uniform_sampling(probs):
    """Uniform sampler."""    
    n = range(len(probs))
    random.shuffle(n)
    r_sum = 0.
    i = -1
    r = random.uniform(0, 1)
    while r_sum < r and i < (len(probs)-1):
        i += 1
        r_sum += probs[n[i]]
    return n[i]


def multivariate_regression(X, y, xg):
    """Compute a multivariate linear regression model y = f(x) using (X,y) and use it to compute f(xg)."""
    m = y.shape[0]
    X = np.hstack((np.ones((m, 1)), X))
    theta = normal_equation(X, y)
    return np.hstack((1, xg)).dot(theta)
    

def normal_equation(X, y):
    """Compute the multivariate linear regression parameters using normal equation method."""
    theta = np.zeros((X.shape[1], y.shape[1]))    
    X2 = X.transpose()
    Xinv = np.linalg.pinv(X2.dot(X))
    theta = Xinv.dot(X2).dot(y)
    return theta


# Taken from scipy function with modifications flagged
def custom_nelder_mead(func, x0, args=(), callback=None, \
    maxiter=None, maxfev=None, disp=False, \
    return_all=False, initial_simplex=None, \
    xatol=1e-4, fatol=1e-4, **unknown_options):
    
    """
    Minimization of scalar function of one or more variables using the
    Nelder-Mead algorithm.
    Tweaked to not recompute distances on known points.
    Options
    -------
    disp : bool
        Set to True to print convergence messages.
    maxiter, maxfev : int
        Maximum allowed number of iterations and function evaluations.
        Will default to ``N*200``, where ``N`` is the number of
        variables, if neither `maxiter` or `maxfev` is set. If both
        `maxiter` and `maxfev` are set, minimization will stop at the
        first reached.
    initial_simplex : array_like of shape (N + 1, N)
        Initial simplex. If given, overrides `x0`.
        ``initial_simplex[j,:]`` should contain the coordinates of
        the j-th vertex of the ``N+1`` vertices in the simplex, where
        ``N`` is the dimension.
    xatol : float, optional
        Absolute error in xopt between iterations that is acceptable for
        convergence.
    fatol : number, optional
        Absolute error in func(xopt) between iterations that is acceptable for
        convergence.
    """
    if 'ftol' in unknown_options:
        warnings.warn("ftol is deprecated for Nelder-Mead,"
                      " use fatol instead. If you specified both, only"
                      " fatol is used.",
                      DeprecationWarning)
        if (np.isclose(fatol, 1e-4) and
                not np.isclose(unknown_options['ftol'], 1e-4)):
            # only ftol was probably specified, use it.
            fatol = unknown_options['ftol']
        unknown_options.pop('ftol')
    if 'xtol' in unknown_options:
        warnings.warn("xtol is deprecated for Nelder-Mead,"
                      " use xatol instead. If you specified both, only"
                      " xatol is used.",
                      DeprecationWarning)
        if (np.isclose(xatol, 1e-4) and
                not np.isclose(unknown_options['xtol'], 1e-4)):
            # only xtol was probably specified, use it.
            xatol = unknown_options['xtol']
        unknown_options.pop('xtol')

    scipy.optimize.optimize._check_unknown_options(unknown_options)
    maxfun = maxfev
    retall = return_all

    fcalls, func = scipy.optimize.optimize.wrap_function(func, args)

    rho = 1
    chi = 2
    psi = 0.5
    sigma = 0.5
    nonzdelt = 0.05
    zdelt = 0.00025

    x0 = np.asfarray(x0).flatten()
    
    ######## Start modifications HERE ###################
    N = len(x0)
    sim = np.zeros((N + 1, N), dtype=x0.dtype)
    fsim = np.zeros((N + 1,), float)
    
    sim[0] = np.array(x0, copy=True)
    fsim[0] = func(sim[0])
    
    ######## End modifications HERE ####################
    if initial_simplex is None:
        for k in range(N):
            y = np.array(x0, copy=True)
            if y[k] != 0:
                y[k] = (1 + nonzdelt)*y[k]
            else:
                y[k] = zdelt
            sim[k + 1] = y
            
        ######## Start modifications HERE ###################
        # Compute performance for initial points
        for k in range(N + 1):
            fsim[k] = func(sim[k])
        ######## End modifications HERE ####################
    else:        
        #sim = np.asfarray(initial_simplex[0]).copy()
        ######## Start modifications HERE ###################
        # Retrieve initial simplex and corresponding performances
        n = min(len(initial_simplex[0]), N)
        sim[1:(n+1),:] = initial_simplex[0][0:n,:]
        fsim[1:(n+1)] = initial_simplex[1][0:n]
        k = n+1
        #if k < N + 1:
        #    print("Nelder-Mead: Not enough neighbours (" + str(k) + "). Adding random points.")
        while k < N + 1: # Do it before nelder-mead
            # Add samples
            i = random.randint(0, n)
            sim[k] = sim[i,:] + 0.02 * np.random.uniform(-0.5, 0.5, N)  #Values fixed arbitrarly according to Matlab code
            fsim[k] = func(sim[k])
            k += 1
        ######## End modifications HERE ###################
        if sim.ndim != 2 or sim.shape[0] != sim.shape[1] + 1:
            raise ValueError("`initial_simplex` should be an array of shape (N+1,N)")
        if len(x0) != sim.shape[1]:
            raise ValueError("Size of `initial_simplex` is not consistent with `x0`")
        
    if retall:
        allvecs = [sim[0]]

    # If neither are set, then set both to default
    if maxiter is None and maxfun is None:
        maxiter = N * 200
        maxfun = N * 200
    elif maxiter is None:
        # Convert remaining Nones, to np.inf, unless the other is np.inf, in
        # which case use the default to avoid unbounded iteration
        if maxfun == np.inf:
            maxiter = N * 200
        else:
            maxiter = np.inf
    elif maxfun is None:
        if maxiter == np.inf:
            maxfun = N * 200
        else:
            maxfun = np.inf

    one2np1 = list(range(1, N + 1))
    

    ######## Start modifications HERE ###################
    #fsim = numpy.zeros((N + 1,), float)
    #
    #for k in range(N + 1):
    #    fsim[k] = func(sim[k])
    ######## End modifications HERE ####################

    ind = np.argsort(fsim)
    fsim = np.take(fsim, ind, 0)
    # sort so sim[0,:] has the lowest function value
    sim = np.take(sim, ind, 0)

    iterations = 1

    while (fcalls[0] < maxfun and iterations < maxiter):
        if (np.max(np.ravel(np.abs(sim[1:] - sim[0]))) <= xatol and
                np.max(np.abs(fsim[0] - fsim[1:])) <= fatol):
            break

        xbar = np.add.reduce(sim[:-1], 0) / N
        xr = (1 + rho) * xbar - rho * sim[-1]
        fxr = func(xr)
        doshrink = 0

        if fxr < fsim[0]:
            xe = (1 + rho * chi) * xbar - rho * chi * sim[-1]
            fxe = func(xe)

            if fxe < fxr:
                sim[-1] = xe
                fsim[-1] = fxe
            else:
                sim[-1] = xr
                fsim[-1] = fxr
        else:  # fsim[0] <= fxr
            if fxr < fsim[-2]:
                sim[-1] = xr
                fsim[-1] = fxr
            else:  # fxr >= fsim[-2]
                # Perform contraction
                if fxr < fsim[-1]:
                    xc = (1 + psi * rho) * xbar - psi * rho * sim[-1]
                    fxc = func(xc)

                    if fxc <= fxr:
                        sim[-1] = xc
                        fsim[-1] = fxc
                    else:
                        doshrink = 1
                else:
                    # Perform an inside contraction
                    xcc = (1 - psi) * xbar + psi * sim[-1]
                    fxcc = func(xcc)

                    if fxcc < fsim[-1]:
                        sim[-1] = xcc
                        fsim[-1] = fxcc
                    else:
                        doshrink = 1

                if doshrink:
                    for j in one2np1:
                        ######## Start modifications HERE ###################
                        if (fcalls[0] >= maxfun or iterations >= maxiter): # Could make far too much eval otherwise
                            break
                        ######## End modifications HERE ####################
                        sim[j] = sim[0] + sigma * (sim[j] - sim[0])
                        fsim[j] = func(sim[j])

        ind = np.argsort(fsim)
        sim = np.take(sim, ind, 0)
        fsim = np.take(fsim, ind, 0)
        if callback is not None:
            callback(sim[0])
        iterations += 1
        if retall:
            allvecs.append(sim[0])

    x = sim[0]
    fval = np.min(fsim)
    warnflag = 0

    if fcalls[0] >= maxfun:
        warnflag = 1
        msg = scipy.optimize.optimize._status_message['maxfev']
        if disp:
            print('Warning: ' + msg)
    elif iterations >= maxiter:
        warnflag = 2
        msg = scipy.optimize.optimize._status_message['maxiter']
        if disp:
            print('Warning: ' + msg)
    else:
        msg = scipy.optimize.optimize._status_message['success']
        if disp:
            print(msg)
            print("         Current function value: %f" % fval)
            print("         Iterations: %d" % iterations)
            print("         Function evaluations: %d" % fcalls[0])

    ######## Start modifications HERE ###################
    result = OptimizeResult(fun=fval, nit=iterations, nfev=fcalls[0],
                            status=warnflag, success=(warnflag == 0),
                            message=msg, x=x, final_simplex=(sim, fsim))
    
    ######## End modifications HERE ###################
    if retall:
        result['allvecs'] = allvecs
    return result



