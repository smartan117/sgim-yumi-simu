import zmq
import time
import sys



ctx = zmq.Context()
socket = ctx.socket(zmq.REP)
socket.bind("tcp://*:5555")

while(True):
    msg = socket.recv()
    print msg
    socket.send("Received " + msg)
