import zmq
import time
import sys
import json
import os

import copy

from environment import EnvironmentV4
from dataset import ActionSpace, OutcomeSpace, DatasetV2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from learning_strategies import ProceduralTeacher
from utils import load_raw, save_raw


import sys
sys.path.insert(0, "../poppycontrol")
import DMP

from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')


def q_mult(q1, q2):
    w1, x1, y1, z1 = q1
    w2, x2, y2, z2 = q2
    w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
    x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
    y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
    z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
    return (w, x, y, z)


def q_conjugate(q):
    w, x, y, z = q
    return (w, -x, -y, -z)


def qv_mult(q1, v1):
    q2 = (0.0,) + v1
    return q_mult(q_mult(q1, q2), q_conjugate(q1))[1:]


def normalize(u):
    norm = np.sqrt(u[0] ** 2 + u[1] ** 2 + u[2] ** 2)
    return (u[0]/norm, u[1]/norm, u[2]/norm)


def diff_angle(u, v):
    x1, y1, z1 = normalize(u)
    x2, y2, z2 = normalize(v)
    ps = x1 * x2 + y1 * y2 + z1 * z2
    return np.arccos(ps)


def compute_rotation_matrix(q_in):
    q = q_in / np.sqrt(np.sum(q_in**2))
    r, i, j, k = q
    R = np.zeros((3, 3))
    R[0,0] = 1 - 2 * (j**2 + k**2)
    R[0,1] = 2 * (i*j - k*r)
    R[0,2] = 2 * (i*k + j*r)
    R[1,0] = 2 * (i*j + k*r)
    R[1,1] = 1 - 2 * (i**2 + k**2)
    R[1,2] = 2 * (j*k - i*r)
    R[2,0] = 2 * (i*k - j*r)
    R[2,1] = 2 * (j*k + i*r)
    R[2,2] = 1 - 2 * (i**2 + j**2)
    return R


def transform(points, u, q):
    R = compute_rotation_matrix(q)
    pt = np.transpose(points)
    pr = np.dot(R, pt)
    pr = np.transpose(pr)
    return pr + u


def scalar_product(u, v):
    return np.sum(u * v)


def mixte_product(u, v, w):
    return scalar_product(np.cross(u, v), w)


def merge_datasets(d1, d2):
    for idY, idA in zip(d2.idY, d2.idA):
        a_type = idA[0]
        a_id = idA[1]
        a = d2.a_spaces[a_type[0]][a_type[1]].data[a_id]
        y_list = []
        y_types = []
        cost = 1.0
        for (t, y_id) in idY:
            y_list.append(d2.y_spaces[t].data[y_id])
            y_types.append(t)
            cost = d2.y_spaces[t].costs[y_id]
        d1.create_a_space(a_type)
        d1.add_entity(a, a_type, y_list, y_types, cost)


class SimuYumiController:

    def __init__(self, reqSrv):
        self.reqSrv = reqSrv
        self.req = None
        self.result = None

    def connect(self):
        ctx = zmq.Context()
        self.req = ctx.socket(zmq.REQ)
        self.req.RCVTIMEO = 120000
        self.req.connect(self.reqSrv)
        print("Connection established!")

    def sendOrder(self, t, payload="", return_order=False):
        msg = json.dumps({"type": t, "payload": payload})
        self.result = None
        try:
            self.req.send(msg)
            ack = self.req.recv()
            dico = json.loads(ack)
            if dico['type'] == t and dico['valid']:
                if return_order:
                    self.result = dico['payload']
                return True
            else:
                return False
        except Exception:
            print("Connexion error! Reconnecting...")
            self.connect()
            return False

    def sendMove(self, path, dt):
        return self.sendOrder("move", {'path': path, 'dt': dt}, return_order=True)

    def sendGoTo(self, pose, dt): # TO TEST ONLY
        return self.sendOrder("goto", {'pose': pose, 't': dt}, return_order=False)

    def sendReset(self, pose):
        return self.sendOrder("reset", pose)


class YumiController:

    def __init__(self, subSrv, reqSrv):
        self.subSrv = subSrv
        self.reqSrv = reqSrv
        self.sub = None
        self.req = None
        self.nb_call = 0
        self.result = None

    def connect(self):
        ctx = zmq.Context()
        self.sub = ctx.socket(zmq.SUB)
        self.sub.setsockopt(zmq.SUBSCRIBE, "")
        self.sub.RCVTIMEO = 3000
        self.sub.connect(self.subSrv)
        self.req = ctx.socket(zmq.REQ)
        self.req.RCVTIMEO = 10000
        self.req.connect(self.reqSrv)

    def sendOrder(self, t, payload="", long_order=False, return_order=False):
        msg = json.dumps({"type": t, "payload": payload})
        self.result = None
        try:
            self.req.send(msg)
            ack = self.req.recv()
            dico = json.loads(ack)
            if dico['type'] == t and dico['valid']:
                #if t in self.LONG_ORDERS:
                if long_order:
                    self.nb_call = dico['payload']
                if return_order:
                    self.result = dico['payload']
                return True
            else:
                return False
        except Exception:
            return False

    def waitCompletion(self, max_dt=30, wait=False):
        nb_end = -1
        t0 = time.time()
        t1 = time.time()
        while nb_end < self.nb_call and (t1 - t0) < max_dt:
            try:
                [address, msg] = self.sub.recv_multipart()
                if address == "nb_end":
                    dico = json.loads(msg)
                    nb_end = dico['payload']
                if wait and address == "running":
                    dico = json.loads(msg)
                    if dico['uptodate'] and not dico['payload']:
                        t0 = time.time()
            except Exception:
                pass
            t1 = time.time()
        return (nb_end == self.nb_call)

    def getStatus(self, max_dt=5):
        status = -10
        t0 = time.time()
        t1 = t0
        try:
            while status == -10 and (t1 - t0) < max_dt:
                [address, msg] = self.sub.recv_multipart()
                if address == "status":
                    dico = json.loads(msg)
                    if dico['uptodate']:
                        status = dico['payload']
                t1 = time.time()
        except Exception:
            pass
        return status

    def getPose(self):
        return self.sendOrder("getPose", return_order=True)

    def getJoints(self):
        return self.sendOrder("getJoints", return_order=True)

    def getRecord(self):
        return self.sendOrder("getRecord", return_order=True)

    def setPose(self, pose, duration):
        if self.sendOrder("setPose", {'pose': pose, 'duration': duration}, long_order=True):
            self.waitCompletion(wait=True)
            status = self.getStatus()
            return (status == 0)
        return False

    def setGoToPose(self, pose, duration):
        if self.sendOrder("goToPose", {'pose': pose, 'duration': duration}, long_order=True):
            self.waitCompletion(wait=True)
            status = self.getStatus()
            return (status == 0)
        return False

    def setJoints(self, pose, duration):
        if self.sendOrder("setJoints", {'pose': pose, 'duration': duration}, long_order=True):
            self.waitCompletion(wait=True)
            status = self.getStatus()
            return (status == 0)
        return False

    def setGoTo(self, pose, duration):
        if self.sendOrder("goTo", {'pose': pose, 'duration': duration}, long_order=True):
            self.waitCompletion(wait=True)
            status = self.getStatus()
            return (status == 0)
        return False

    def setPosePath(self, path, dt):
        if self.sendOrder("setPosePath", {'path': path, 'dt': dt}, long_order=True):
            self.waitCompletion(wait=True)
            status = self.getStatus()
            return (status == 0)
        return False

    def setJointsPath(self, path, dt):
        if self.sendOrder("setJointsPath", {'path': path, 'dt': dt}, long_order=True):
            self.waitCompletion(wait=True)
            status = self.getStatus()
            return (status == 0)
        return False

    def setJointsPath2(self, path, dt):
        if self.sendOrder("setJointsPath2", {'path': path, 'dt': dt}, long_order=True):
            self.waitCompletion(wait=True)
            status = self.getStatus()
            return (status == 0)
        return False

    def setJointsPath3(self, path, dt):
        if self.sendOrder("setJointsPath3", {'path': path, 'dt': dt}, long_order=True):
            self.waitCompletion(wait=True)
            status = self.getStatus()
            return (status == 0)
        return False


class YumiExperiment(EnvironmentV4):

    def __init__(self, a_spaces, y_spaces, controller, start, complex_env=True):
        """
        a_spaces list of ActionSpace: the list of primitive action spaces available to the learner
        y_spaces list of OutcomeSpace: the list of outcome spaces
        """
        self.a_spaces = a_spaces
        self.y_spaces = y_spaces
        self.range = np.array([337.0, 187.0, 203.5, 580.0, 226.0, 458.0, 337.0])
        self.zeros = np.array([0.0, -50.0, -21.75, 0.0, 25.0, 0.0, 0.0])
        self.controller = controller
        self.successes = 0
        self.start = start

    def normalize_angles(self, angles):
        return (angles - self.zeros) * 2.0 / self.range

    def a_to_dmp(self, a, a_type):
        """Extract DMP parameters from a primitive action."""
        assert a_type == 0
        n_bfs = 1
        cs = DMP.CanonicalSystem()
        dmp = DMP.DynamicSystem(cs=cs, tau=5.0)

        for j in range(7):
            goal = a[(n_bfs+1)*j + n_bfs] * self.range[j]/2.0 + self.zeros[j]
            ts = DMP.TransformationSystem(str(j), n_bfs=n_bfs, start=0., goal=goal, method="2012")
            weights = []
            for k in range(n_bfs):
                weights.append(100.0*a[(n_bfs+1)*j + k])
            c, h = ts.compute_bfs()
            ts.parameters = DMP.WeightedLinearRegression(c, h, np.array(weights))
            dmp.add_dmp(ts)

        return dmp

    def dmp_to_path(self, dmp, start, dt=0.02):
        """Execute the DMP on the arm."""
        real_start = {}
        for i in range(7):
            real_start[str(i)] = start[i]  # * self.range[i]/2.0 + self.zeros[i]
        dmp.set_start(real_start)
        move = dmp.integrate(dt, tf=5.0)[1]  # dictionary with arrays ided by joint name
        # But we want a list of joint angles (each has all joint angles at a timestep)
        path = []
        for i in range(7):
            for j in range(len(move[str(i)])):
                val_min = self.zeros[i] - self.range[i] / 2
                val_max = self.zeros[i] + self.range[i] / 2
                if j >= len(path):
                    path.append([])
                pose = move[str(i)][j]
                pose = min(max(pose, val_min), val_max)
                path[j].append(pose)

        return path

    def execute(self, a, a_type):
        """Execute the action and send outcomes reached."""
        self.normalize_a(a, a_type)
        y_list = []
        y_types = []

        dmp = self.a_to_dmp(a, a_type)
        path = self.dmp_to_path(dmp, self.start)

        status = self.controller.getStatus()

        if status == 0 or status == -1:
            # Controller available
            if self.controller.setJointsPath3(path, 0.02):
                self.successes += 1
        else:
            # Controller unavailable for some reason
            pass

        return y_list, y_types


class SimuYumiExperiment(EnvironmentV4):

    def __init__(self, a_spaces, y_spaces, controller, start, complex_env=True):
        """
        a_spaces list of ActionSpace: the list of primitive action spaces available to the learner
        y_spaces list of OutcomeSpace: the list of outcome spaces
        """
        self.a_spaces = a_spaces
        self.y_spaces = y_spaces
        self.m_range = np.array([337.0, 187.0, 203.5, 580.0, 226.0, 458.0, 337.0])
        self.m_zeros = np.array([0.0, -50.0, -21.75, 0.0, 25.0, 0.0, 0.0])
        self.f_zeros = np.array([137.9, -107.0, 462.0, np.pi/2])
        #self.f_range = np.array([1085.8, 1115.0, 1112.0, np.pi] + [1085.8, 1115.0, 1112.0, np.pi] * 252)
        self.f_range = np.array([1500.0, 1500.0, 1500.0, np.pi])
        self.controller = controller
        self.start = start
        self.rest_pose = copy.deepcopy(start)
        self.keep_going = True
        self.simulator = None
        #self.trajectories = []

    def set_simulator(self, simulator):
        self.simulator = simulator

    def normalize_angles(self, angles):
        return (angles - self.m_zeros) * 2.0 / self.m_range

    def normalize_f(self, f):
        return (f - self.f_zeros.tolist()) * 2.0 / self.f_range

    def a_to_dmp(self, a, a_type):
        """Extract DMP parameters from a primitive action."""
        assert a_type == 0
        n_bfs = 1
        cs = DMP.CanonicalSystem()
        dmp = DMP.DynamicSystem(cs=cs, tau=5.0)

        for j in range(7):
            goal = a[(n_bfs+1)*j + n_bfs] * self.m_range[j]/2.0 + self.m_zeros[j]
            ts = DMP.TransformationSystem(str(j), n_bfs=n_bfs, start=0., goal=goal, method="2012")
            weights = []
            for k in range(n_bfs):
                weights.append(100.0*a[(n_bfs+1)*j + k])
            c, h = ts.compute_bfs()
            ts.parameters = DMP.WeightedLinearRegression(c, h, np.array(weights))
            dmp.add_dmp(ts)

        return dmp

    def dmp_to_path(self, dmp, start, dt=0.02):
        """Execute the DMP on the arm."""
        #start_n = self.normalize_angles(start)
        real_start = {}
        for i in range(7):
            real_start[str(i)] = start[i]  # * self.range[i]/2.0 + self.zeros[i]
        dmp.set_start(real_start)
        move = dmp.integrate(dt, tf=5.0)[1]  # dictionary with arrays ided by joint name
        # But we want a list of joint angles (each has all joint angles at a timestep)
        path = []
        for i in range(7):
            for j in range(len(move[str(i)])):
                val_min = self.m_zeros[i] - self.m_range[i] / 2
                val_max = self.m_zeros[i] + self.m_range[i] / 2
                if j >= len(path):
                    path.append([])
                pose = move[str(i)][j]
                pose = min(max(pose, val_min), val_max)
                path[j].append(pose)

        return path

    def execute(self, a, a_type):
        """Execute the action and send outcomes reached."""

        self.normalize_a(a, a_type)
        y_list = []
        y_types = []

        dmp = self.a_to_dmp(a, a_type)
        path = self.dmp_to_path(dmp, self.start)

        if self.keep_going and self.controller.sendMove(path, 0.02):
            pose = self.controller.result['pose']
            q = pose[3:]
            u = qv_mult(q, (0, 0, 1))  # Compute orientation of TCP
            theta = diff_angle(u, (0, 0, -1))
            oldTraj = self.controller.result['trajectory']
            traj = []
            for t in oldTraj:
                traj.append(np.array(t))
            #self.trajectories.append(traj)
            features = np.array(pose[:3] + [theta])
            if self.simulator:
                self.simulator.process(traj)
                if self.simulator.bad:
                    self.keep_going = False
                    return y_list, y_types
            f = self.normalize_f(features)
            y_list.append(f)
            y_types.append(0)

            if self.simulator:
                all_objects_detected = True
                all_poses = []
                for i, obj in enumerate(self.simulator.objects):
                    if obj.detected:
                        y = self.simulator.table.normalize(obj.pose[:2])
                        all_poses += y.tolist()
                        y_types.append(i+1)
                        y_list.append(y)
                    else:
                        all_objects_detected = False
                if all_objects_detected:
                    y_types.append(len(self.simulator.objects) + 1)
                    y_list.append(np.array(all_poses))
                if self.simulator.sound[0]:
                    y_types.append(len(self.simulator.objects) + 2)
                    y_list.append(np.array(self.simulator.sound[1:]))

            self.start = self.controller.result['joints']
            print("Ok")
        else:
            print("Fail")
            self.keep_going = False
            #self.trajectories.append([])


        return y_list, y_types

    def reset(self):
        self.controller.sendReset(self.rest_pose.tolist())
        self.start = self.rest_pose
        self.keep_going = True
        if self.simulator:
            self.simulator.reset()


class SimuYumiExperimentV2(EnvironmentV4):

    def __init__(self, a_spaces, y_spaces, controller, start, complex_env=True):
        """
        a_spaces list of ActionSpace: the list of primitive action spaces available to the learner
        y_spaces list of OutcomeSpace: the list of outcome spaces
        """
        self.a_spaces = a_spaces
        self.y_spaces = y_spaces
        self.m_range = np.array([337.0, 187.0, 203.5, 580.0, 226.0, 458.0, 337.0])
        self.m_zeros = np.array([0.0, -50.0, -21.75, 0.0, 25.0, 0.0, 0.0])
        self.f_zeros = np.array([137.9, -107.0, 462.0, np.pi/2])
        #self.f_range = np.array([1085.8, 1115.0, 1112.0, np.pi] + [1085.8, 1115.0, 1112.0, np.pi] * 252)
        self.f_range = np.array([1500.0, 1500.0, 1500.0, np.pi])
        self.controller = controller
        self.start = start
        self.rest_pose = copy.deepcopy(start)
        self.keep_going = True
        self.simulator = None
        #self.trajectories = []

    def set_simulator(self, simulator):
        self.simulator = simulator

    def normalize_angles(self, angles):
        return (angles - self.m_zeros) * 2.0 / self.m_range

    def normalize_f(self, f):
        return (f - self.f_zeros.tolist()) * 2.0 / self.f_range

    def a_to_dmp(self, a, a_type):
        """Extract DMP parameters from a primitive action."""
        assert a_type == 0
        n_bfs = 1
        cs = DMP.CanonicalSystem()
        dmp = DMP.DynamicSystem(cs=cs, tau=5.0)

        for j in range(7):
            goal = a[(n_bfs+1)*j + n_bfs] * self.m_range[j]/2.0 + self.m_zeros[j]
            ts = DMP.TransformationSystem(str(j), n_bfs=n_bfs, start=0., goal=goal, method="2012")
            weights = []
            for k in range(n_bfs):
                weights.append(100.0*a[(n_bfs+1)*j + k])
            c, h = ts.compute_bfs()
            ts.parameters = DMP.WeightedLinearRegression(c, h, np.array(weights))
            dmp.add_dmp(ts)

        return dmp

    def dmp_to_path(self, dmp, start, dt=0.02):
        """Execute the DMP on the arm."""
        #start_n = self.normalize_angles(start)
        real_start = {}
        for i in range(7):
            real_start[str(i)] = start[i]  # * self.range[i]/2.0 + self.zeros[i]
        dmp.set_start(real_start)
        move = dmp.integrate(dt, tf=5.0)[1]  # dictionary with arrays ided by joint name
        # But we want a list of joint angles (each has all joint angles at a timestep)
        path = []
        for i in range(7):
            for j in range(len(move[str(i)])):
                val_min = self.m_zeros[i] - self.m_range[i] / 2
                val_max = self.m_zeros[i] + self.m_range[i] / 2
                if j >= len(path):
                    path.append([])
                pose = move[str(i)][j]
                pose = min(max(pose, val_min), val_max)
                path[j].append(pose)

        return path

    def execute(self, a, a_type):
        """Execute the action and send outcomes reached."""

        self.normalize_a(a, a_type)
        y_list = []
        y_types = []

        dmp = self.a_to_dmp(a, a_type)
        path = self.dmp_to_path(dmp, self.start)

        if self.keep_going and self.controller.sendMove(path, 0.02):
            pose = self.controller.result['pose']
            q = pose[3:]
            u = qv_mult(q, (0, 0, 1))  # Compute orientation of TCP
            theta = diff_angle(u, (0, 0, -1))
            oldTraj = self.controller.result['trajectory']
            traj = []
            for t in oldTraj:
                traj.append(np.array(t))
            if self.simulator:
                self.simulator.process(traj)
                if self.simulator.bad:
                    self.keep_going = False
                    return y_list, y_types
                if self.simulator.grabber.detected:
                    y = self.simulator.table.normalize(self.simulator.grabber.pose[:2])
                    y_list.append(y)
                    y_types.append(0)

                all_objects_detected = True
                all_poses = []
                for i, obj in enumerate(self.simulator.objects):
                    if obj.detected:
                        y = self.simulator.table.normalize(obj.pose[:2])
                        all_poses += y.tolist()
                        y_types.append(i+1)
                        y_list.append(y)
                    else:
                        all_objects_detected = False
                if all_objects_detected:
                    y_types.append(len(self.simulator.objects) + 1)
                    y_list.append(np.array(all_poses))
                if self.simulator.sound[0]:
                    y_types.append(len(self.simulator.objects) + 2)
                    y_list.append(np.array(self.simulator.sound[1:]))

            self.start = self.controller.result['joints']
            print("Ok")
        else:
            print("Fail")
            self.keep_going = False
            #self.trajectories.append([])


        return y_list, y_types

    def reset(self):
        self.controller.sendReset(self.rest_pose.tolist())
        self.start = self.rest_pose
        self.keep_going = True
        if self.simulator:
            self.simulator.reset()


class SimuYumiExperimentV3(SimuYumiExperimentV2):

    def __init__(self, a_spaces, y_spaces, controller, start, complex_env=True):
        """
        a_spaces list of ActionSpace: the list of primitive action spaces available to the learner
        y_spaces list of OutcomeSpace: the list of outcome spaces
        """
        self.a_spaces = a_spaces
        self.y_spaces = y_spaces
        self.controller = controller
        self.start = start
        self.rest_pose = copy.deepcopy(start)
        #self.trajectories = []
        # New attributes
        self.reset_outcomes = []
        self.reset_types = []

    def reset(self):
        if self.controller.sendReset(self.rest_pose.tolist()):
            y_list = []
            for y in self.result['y_list']:
                y_list.append(np.array(y))
            self.reset_outcomes = y_list
            self.reset_types = self.result['types']
        else:
            self.reset_outcomes = []
            self.reset_types = []
        self.start = self.rest_pose

    def execute(self, a, a_type):
        """Execute the action and send outcomes reached."""

        self.normalize_a(a, a_type)
        y_list = []
        y_types = []

        dmp = self.a_to_dmp(a, a_type)
        path = self.dmp_to_path(dmp, self.start)

        y_list = []
        y_types = []

        if self.controller.sendMove(path, 0.02):
            for y in self.result['y_list']:
                y_list.append(np.array(y))
            y_types = copy.deepcopy(self.result['y_types'])
        else:
            if len(self.reset_outcomes) > 0:
                y_list = copy.deepcopy(self.reset_outcomes)
                y_types = copy.deepcopy(self.reset_types)
        if self.controller.result.has_key('joints'):
            self.start = self.controller.result['joints']
        self.reset_outcomes = []
        self.reset_types = []
        return y_list, y_types


class Cylinder:

    def __init__(self, r, h, pose, q):
        self.r = r
        self.h = h
        self.pose = pose
        self.q = q
        self.mesh = []
        self.detected = True

    def compute_local_mesh(self, dtheta, dz, dr):
        mesh = []
        # Compute base surface mesh
        z = -self.h/2.0
        mesh.append(np.array([0.0, 0.0, z]))
        r = dr
        while r < self.r:
            theta = 0.0
            while theta < 2 * np.pi:
                mesh.append(np.array([r*np.cos(theta), r*np.sin(theta), z]))
                theta += dtheta
            r += dr
        r = self.r
        while theta < 2 * np.pi:
            mesh.append(np.array([r*np.cos(theta), r*np.sin(theta), z]))
            theta += dtheta
        # Compute contour mesh
        z += dz
        while z < self.h/2.0:
            theta = 0.0
            while theta < 2 * np.pi:
                mesh.append(np.array([r*np.cos(theta), r*np.sin(theta), z]))
                theta += dtheta
            z += dz
        z = self.h/2.0
        mesh.append(np.array([0.0, 0.0, z]))
        r = dr
        while r < self.r:
            theta = 0.0
            while theta < 2 * np.pi:
                mesh.append(np.array([r*np.cos(theta), r*np.sin(theta), z]))
                theta += dtheta
            r += dr
        r = self.r
        while theta < 2 * np.pi:
            mesh.append(np.array([r*np.cos(theta), r*np.sin(theta), z]))
            theta += dtheta
        return mesh

    def inside(self, pose): # Only for vertical cylinder
        if self.onPlane(pose[2]):
            r = np.sqrt(np.sum((pose[:2] - self.pose[:2])**2))
            return (r < self.r)
        return False

    def onPlane(self, z):
        return (z < self.pose[2] + self.h/2.0 and z > self.pose[2] - self.h/2.0)

    @classmethod
    def collision(cls, obj1, obj2): # obj1 is the object taken
        v0 = qv_mult(obj1.q, (0, 0, 1))
        v1 = qv_mult(obj2.q, (0, 0, 1))
        v0 = np.array([v0[0], v0[1], v0[2]])
        v1 = np.array([v1[0], v1[1], v1[2]])
        w = np.cross(v0, v1)
        w = np.sqrt(np.sum(w**2))
        M0M1 = obj2.pose - obj1.pose
        if w == 0.0:
            w = np.cross(M0M1, v0)
            d = np.sqrt(np.sum(w**2))/np.sqrt(np.sum(v0**2))
            if d > (obj1.r + obj2.r):
                return False
            a = scalar_product(obj1.pose, v0)
            b = a + obj1.h/2.0
            a -= obj1.h/2
            c = scalar_product(obj2.pose, v0)
            d = c + obj2.h/2.0
            c -= obj2.h/2.0
            if b >= d:
                return (d >= a)
            return (b >= c)
        else:
            k = mixte_product(M0M1,v0,v1)
            d = np.fabs(k)/w
            if d > (obj1.r + obj2.r):
                return False
            mesh = np.array(obj1.mesh)
            mesh = transform(mesh, obj1.pose, obj1.q)
            for m in mesh:
                if obj2.inside(m):
                    return True
            return False


class Disk:

    def __init__(self, r, pose):
        self.r = r
        self.pose = pose
        self.detected = False

    def inside(self, pose):
        d = self.pose[:2] - pose[:2]
        r = np.sqrt(np.sum(d**2))
        return r <= self.r

    @classmethod
    def collision(cls, obj1, obj2):
        d = obj1.pose[:2] - obj2.pose[:2]
        r = np.sqrt(np.sum(d**2))
        return r <= obj1.r + obj2.r


class Grabber:

    def __init__(self, pose, q, tolerance=0.0, dh=20.0):
        self.pose = pose # pose has an extra cell with theta
        self.q = q
        self.object = None
        self.tolerance = tolerance
        self.detected = False

    def release(self):
        self.object = None

    def catchy(self):
        return (self.pose[3] <= self.tolerance)


class Table:

    def __init__(self, bounds, z, tolerance=20.0):
        self.bounds = bounds # only bottom left and top right
        self.z = z
        self.range = np.array([bounds[1][0] - bounds[0][0], bounds[1][1] - bounds[0][1]])
        self.zero = np.array(self.bounds[0]) + self.range/2.0
        self.corners = [np.array(bounds[0]), np.array(bounds[1]), np.array([bounds[0][0], bounds[1][1]]), \
                np.array([bounds[1][0], bounds[0][1]])]
        self.semi_diagonal = np.sqrt(np.sum((np.array(bounds[1]) - np.array(bounds[0]))**2))/2.0
        self.dh = tolerance

    def onSquare(self, p):
        return (p[0] >= self.bounds[0][0] and p[0] <= self.bounds[1][0] and p[1] >= self.bounds[0][1] and p[1] <= self.bounds[1][1])

    def onTable(self, p, tolerant=False):
        if (not tolerant and p[2] > self.z) or (tolerant and p[2] > self.z + self.dh):
            return False
        else: # TO DO Make plan not infinite
            return self.onSquare(p)

    def normalize(self, p):
        return (p - self.zero) * 2.0 / self.range

    def denormalize(self, p):
        return p * self.range / 2.0 + self.zero


class TableReal:

    def __init__(self, bounds, d_bounds):
        # Actual size
        self.bounds = bounds # only bottom left and top right
        self.range = np.array([bounds[1][0] - bounds[0][0], bounds[1][1] - bounds[0][1]])
        self.zero = np.array(self.bounds[0]) + self.range/2.0
        self.corners = [np.array(bounds[0]), np.array(bounds[1]), np.array([bounds[0][0], bounds[1][1]]), \
                np.array([bounds[1][0], bounds[0][1]])]
        self.semi_diagonal = np.sqrt(np.sum((np.array(bounds[1]) - np.array(bounds[0]))**2))/2.0
        # Detection size
        self.d_bounds = d_bounds # only bottom left and top right
        self.d_range = np.array([d_bounds[1][0] - d_bounds[0][0], d_bounds[1][1] - d_bounds[0][1]])
        self.d_zero = np.array(self.d_bounds[0]) + self.d_range/2.0

    def normalize_d(self, p):
        p1 = (p - self.d_zero) * 2.0 / self.d_range
        return np.array([p1[1], p1[0]])

    def denormalize_d(self, p):
        p1 = p * self.d_range / 2.0 + self.d_zero
        return np.array([p1[1], p1[0]])

    def normalize(self, p):
        return (p - self.zero) * 2.0 / self.range

    def denormalize(self, p):
        return p * self.range / 2.0 + self.zero

    """
    def normalize(self, p):
        return (p - self.zero) * 2.0 / self.range

    def denormalize(self, p):
        return p * self.range / 2.0 + self.zero
    """


class YumiExperimentSimulator:

    def __init__(self, objects, grabber, table, r_thr=np.float("inf")):
        self.objects = objects
        self.grabber = grabber
        self.table = table
        self.bad = False
        self.poses = []
        self.sound = [False, 0.0, 0.0, 0.0]   # Make sound, frequency, intensity, beat
        self.r_thr = r_thr
        for obj in self.objects:
            self.poses.append({"pose": obj.pose, "q": np.array([1, 0, 0, 0])})
        self.r_min = self.objects[0].r + self.objects[1].r
        self.A = np.log10(self.r_min)
        self.N = 2.0 / (np.log10(self.table.semi_diagonal * 2.0) - self.A)

    def compute_sound(self, a, b):
        u = b - a
        r = np.sqrt(np.sum(u**2))
        rho = np.arccos(np.sum(u * np.array([1.0, 0.0])) / r)
        dist = []
        for p in self.table.corners:
            dist.append(np.sqrt(np.sum((p - a)**2)))
        self.sound[1] = (self.table.semi_diagonal/2.0 - min(dist)) * 2.0 / self.table.semi_diagonal
        self.sound[2] = 1.0 - 2.0 * (np.log10(r) - np.log10(self.r_min)) / (np.log10(self.table.semi_diagonal*2.0) - np.log10(self.r_min))
        self.sound[3] = (rho / np.pi) * 0.95 + 0.05
        self.sound[0] = (r <= self.r_thr)

    def reset(self):
        self.grabber.release()
        self.bad = False
        self.sound = [False, 0.0, 0.0, 0.0]
        for pose, obj in zip(self.poses, self.objects):
            obj.pose = pose["pose"]
            obj.q = pose["q"]
            obj.detected = True

    def evaluate_onfly(self):
        if self.grabber.object:
            self.grabber.object.q = self.grabber.q
            self.grabber.object.detected = False
            u = qv_mult(self.grabber.object.q, (0, 0, 1))  # Compute orientation of TCP
            u = normalize(u)
            u = np.array([u[0], u[1], u[2]])
            self.grabber.object.pose = self.grabber.pose[:3] + u * self.grabber.object.h/2.0
            for obj in self.objects:
                if obj != self.grabber.object:
                    if Cylinder.collision(self.grabber.object, obj):
                        self.bad = True
                        return

    def evaluate_end(self):
        if not self.bad:
            for obj in self.objects:
                if not self.grabber.object and self.grabber.catchy() and obj.inside(self.grabber.pose):  # object can be taken
                    self.grabber.object = obj
                elif obj == self.grabber.object and self.table.onSquare(obj.pose):
                    mesh = np.array(obj.mesh)
                    mesh = transform(mesh, obj.pose, obj.q)
                    for m in mesh:
                        if self.table.onTable(m):
                            self.grabber.release()
                            obj.pose[2] = self.table.z + obj.h/2.0
                            obj.q = np.array([1.0, 0, 0, 0])
                            obj.detected = True
            if self.objects[0].detected and self.objects[1].detected:
                self.compute_sound(self.objects[0].pose[:2], self.objects[1].pose[:2])
            else:
                self.sound = [False, 0.0, 0.0, 0.0]

    def process(self, traj):
        for pose in traj:
            p = pose[:3]
            q = pose[3:]
            u = qv_mult(q, (0, 0, 1))  # Compute orientation of TCP
            theta = diff_angle(u, (0, 0, -1))
            self.grabber.pose = np.array(p.tolist() + [theta])
            self.grabber.q = q
            self.evaluate_onfly()
            if self.bad:
                return
        self.evaluate_end()


class YumiExperimentSimulatorV2:

    def __init__(self, objects, grabber, table, r_thr=np.float("inf")):
        self.objects = objects
        self.grabber = grabber
        self.table = table
        self.bad = False
        self.poses = []
        self.sound = [False, 0.0, 0.0, 0.0]   # Make sound, frequency, intensity, beat
        self.r_thr = r_thr
        for obj in self.objects:
            self.poses.append({"pose": obj.pose, "q": np.array([1, 0, 0, 0])})
        self.r_min = self.objects[0].r + self.objects[1].r
        self.A = np.log10(self.r_min)
        self.N = 2.0 / (np.log10(self.table.semi_diagonal * 2.0) - self.A)

    def compute_sound(self, a, b):
        u = b - a
        r = np.sqrt(np.sum(u**2))
        rho = np.arccos(np.sum(u * np.array([1.0, 0.0])) / r)
        dist = []
        for p in self.table.corners:
            dist.append(np.sqrt(np.sum((p - a)**2)))
        self.sound[1] = (self.table.semi_diagonal/2.0 - min(dist)) * 2.0 / self.table.semi_diagonal
        self.sound[2] = 1.0 - 2.0 * (np.log10(r) - np.log10(self.r_min)) / (np.log10(self.table.semi_diagonal*2.0) - np.log10(self.r_min))
        self.sound[3] = (rho / np.pi) * 0.95 + 0.05
        self.sound[0] = (r <= self.r_thr)

    def reset(self):
        self.grabber.release()
        self.bad = False
        self.sound = [False, 0.0, 0.0, 0.0]
        self.grabber.detected = False
        for pose, obj in zip(self.poses, self.objects):
            obj.pose = pose["pose"]
            obj.detected = False

    def evaluate_end_old(self):
        if not self.bad:
            self.grabber.detected = self.table.onTable(self.grabber.pose)
            if self.grabber.object:
                self.grabber.object.pose = np.array([self.grabber.pose[0],self.grabber.pose[1],self.grabber.pose[2] - self.grabber.object.dh])
            for obj in self.objects:
                if not self.grabber.object and self.grabber.catchy() and obj.inside(self.grabber.pose):  # object can be taken
                    self.grabber.object = obj
                    obj.detected = False
                elif obj == self.grabber.object and self.table.onTable(obj.pose):
                    self.grabber.release()
                    obj.pose[2] = self.table.z
                    obj.detected = True
                    for obj2 in self.objects:
                        if obj is not obj2 and Disk.collision(obj, obj2):
                            self.bad = True
        if not self.bad:
            if self.objects[0].detected and self.objects[1].detected:
                self.compute_sound(self.objects[0].pose[:2], self.objects[1].pose[:2])
            else:
                self.sound = [False, 0.0, 0.0, 0.0]

    def evaluate_end(self):
        if not self.bad:
            self.grabber.detected = self.table.onTable(self.grabber.pose, True)
            if self.grabber.detected:
                # Grabber detected by table
                if self.grabber.object:
                    # Grabber has an object in hand to release
                    self.grabber.object.pose = np.array([self.grabber.pose[0],self.grabber.pose[1]])
                    self.grabber.object.detected = True
                    for obj2 in self.objects:
                        if self.grabber.object is not obj2 and Disk.collision(self.grabber.object, obj2):
                            self.bad = True
                    self.grabber.release()
                elif self.grabber.catchy():
                    for obj in self.objects:
                        if obj.inside(self.grabber.pose):
                            self.grabber.object = obj
                            obj.detected = False
        if not self.bad:
            if self.objects[0].detected and self.objects[1].detected:
                self.compute_sound(self.objects[0].pose[:2], self.objects[1].pose[:2])
            else:
                self.sound = [False, 0.0, 0.0, 0.0]

    def process(self, traj):
        pose = traj[-1]
        p = pose[:3]
        q = pose[3:]
        u = qv_mult(q, (0, 0, 1))  # Compute orientation of TCP
        theta = diff_angle(u, (0, 0, -1))
        self.grabber.pose = np.array(p.tolist() + [theta])
        self.grabber.q = q
        if self.bad:
            return
        self.evaluate_end()


class SimuYumiExperimentator:

    def __init__(self):
        #server = None
        server = SimuYumiController("tcp://127.0.0.1:5555")
        server.connect()

        #start_pose = np.array([145.0, -64.0, 12.0, 1.0, 23.0, 53.0, -131.0])
        start_pose = np.array([141.926, -91.9894, 14.8817, 146.197, 57.2076, 34.6444, -52.1023])

        table = Table([[200.0, -570.0], [885.0, 570.0]], 50.0)
        hand = Grabber(np.array([0.0, 0.0, 0.0, np.pi]), np.array([1.0, 0, 0, 0]), tolerance=np.pi/18.0)
        objA = Cylinder(40.0, 40.0, np.array([400.0, -350.0, 70.0]), np.array([1.0, 0, 0, 0]))
        objA.mesh = objA.compute_local_mesh(np.pi/20.0, 5.0, 5.0)
        objB = Cylinder(40.0, 40.0, np.array([350.0, 100.0, 70.0]), np.array([1.0, 0, 0, 0]))
        objB.mesh = objB.compute_local_mesh(np.pi/20.0, 5.0, 5.0)
        simu = YumiExperimentSimulator([objA, objB], hand, table, 300.0)


        a1 = ActionSpace({'min': -np.ones(14), 'max': np.ones(14)}, 1, "A")
        y1 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "endPose")
        y2 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "objA")
        y3 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "objB")
        y4 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "objects")
        y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "sound")
        self.server = server
        self.env = SimuYumiExperiment([a1], [y1, y2, y3, y4, y5], server, start_pose)
        self.env.simulator = simu
        self.data = DatasetV2([a1], [y1, y2, y3, y4, y5])

    @classmethod
    def create_testbench(cls):
        testbench = [[],[],[],[],[]]
        # Testbench for task space 0 and 1 and 3
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                for z in np.linspace(-1, 1, num=20):
                    for t in np.linspace(-1, 1, num=10):
                        testbench[0].append(np.array([x, y, z, t]))
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                testbench[1].append(np.array([x, y]))
                testbench[2].append(np.array([x, y]))
        for x1 in np.linspace(-1, 1, num=10):
            for y1 in np.linspace(-1, 1, num=10):
                for x2 in np.linspace(-1, 1, num=10):
                    for y2 in np.linspace(-1, 1, num=10):
                        testbench[3].append(np.array([x1, y1, x2, y2]))
        for f in np.linspace(-1, 1, num=20):
            for v in np.linspace(-1, 1, num=20):
                for b in np.linspace(0.05, 1, num=20):
                    testbench[4].append(np.array([f, v, b]))
        return testbench


class SimuYumiExperimentatorV2:

    def __init__(self, connection=True):
        #server = None
        if connection:
            server = SimuYumiController("tcp://127.0.0.1:5555")
            server.connect()
        else:
            server = None

        #start_pose = np.array([145.0, -64.0, 12.0, 1.0, 23.0, 53.0, -131.0])
        start_pose = np.array([141.926, -91.9894, 14.8817, 146.197, 57.2076, 34.6444, -52.1023])

        table = Table([[200.0, -570.0], [885.0, 570.0]], 80.0)
        hand = Grabber(np.array([0.0, 0.0, 0.0, np.pi]), np.array([1.0, 0, 0, 0]), tolerance=np.pi/18.0)
        objA = Disk(40.0, np.array([400.0, -350.0]))
        objB = Disk(40.0, np.array([350.0, 100.0]))
        simu = YumiExperimentSimulatorV2([objA, objB], hand, table, 300.0)


        a1 = ActionSpace({'min': -np.ones(14), 'max': np.ones(14)}, 1, "A")
        y1 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "endPose")
        y2 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "objA")
        y3 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "objB")
        y4 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "objects")
        y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "sound")
        self.server = server
        self.env = SimuYumiExperimentV2([a1], [y1, y2, y3, y4, y5], server, start_pose)
        self.env.simulator = simu
        self.data = DatasetV2([a1], [y1, y2, y3, y4, y5])

    @classmethod
    def create_testbench(cls):
        testbench = [[],[],[],[],[]]
        # Testbench for task space 0 and 1 and 3
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                testbench[0].append(np.array([x, y]))
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                testbench[1].append(np.array([x, y]))
                testbench[2].append(np.array([x, y]))
        for x1 in np.linspace(-1, 1, num=10):
            for y1 in np.linspace(-1, 1, num=10):
                for x2 in np.linspace(-1, 1, num=10):
                    for y2 in np.linspace(-1, 1, num=10):
                        testbench[3].append(np.array([x1, y1, x2, y2]))
        for f in np.linspace(-1, 1, num=20):
            for v in np.linspace(-1, 1, num=20):
                for b in np.linspace(0.05, 1, num=20):
                    testbench[4].append(np.array([f, v, b]))
        return testbench


class SimuYumiExperimentatorV3:

    def __init__(self, connection=True):
        from test_ml import MLNode

        if connection:
            server = MLNode()
            server.connect()
        else:
            server = None

        #start_pose = np.array([145.0, -64.0, 12.0, 1.0, 23.0, 53.0, -131.0])
        start_pose = np.array([141.926, -91.9894, 14.8817, 146.197, 57.2076, 34.6444, -52.1023])

        a1 = ActionSpace({'min': -np.ones(14), 'max': np.ones(14)}, 1, "A")
        y1 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "endPose")
        y2 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "objA")
        y3 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "objB")
        y4 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "objects")
        y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "sound")
        self.server = server
        self.env = SimuYumiExperimentV3([a1], [y1, y2, y3, y4, y5], server, start_pose)
        self.data = DatasetV2([a1], [y1, y2, y3, y4, y5])

    @classmethod
    def create_testbench(cls):
        testbench = [[],[],[],[],[]]
        # Testbench for task space 0 and 1 and 3
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                testbench[0].append(np.array([x, y]))
        for x in np.linspace(-1, 1, num=20):
            for y in np.linspace(-1, 1, num=20):
                testbench[1].append(np.array([x, y]))
                testbench[2].append(np.array([x, y]))
        for x1 in np.linspace(-1, 1, num=10):
            for y1 in np.linspace(-1, 1, num=10):
                for x2 in np.linspace(-1, 1, num=10):
                    for y2 in np.linspace(-1, 1, num=10):
                        testbench[3].append(np.array([x1, y1, x2, y2]))
        for f in np.linspace(-1, 1, num=20):
            for v in np.linspace(-1, 1, num=20):
                for b in np.linspace(0.05, 1, num=20):
                    testbench[4].append(np.array([f, v, b]))
        return testbench


class ProceduralTeacher1(ProceduralTeacher):

    def rule(self, goal):
        p = np.array(self.env.simulator.poses[0]["pose"][:2].tolist() + [85.0, 0.0])
        g1 = self.env.normalize_f(p)
        p = self.env.simulator.table.denormalize(goal)
        p = np.array(p.tolist() + [85.0, 0.0])
        g2 = self.env.normalize_f(p)
        y = np.array(g1.tolist() + g2.tolist())
        p_type = (0, 0)
        return (y, p_type)

    def choose_demo(self, goal, task):
        if task == 1:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(1)
        return self.rule(real_goal)


class ProceduralTeacher1V2(ProceduralTeacher):

    def rule(self, goal):
        #p = np.array(self.env.simulator.poses[0]["pose"][:2].tolist() + [85.0, 0.0])
        g1 = self.env.simulator.table.normalize(self.env.simulator.poses[0]["pose"][:2])
        y = np.array(g1[:2].tolist() + goal.tolist())
        p_type = (0, 0)
        return (y, p_type)

    def choose_demo(self, goal, task):
        if task == 1:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(1)
        return self.rule(real_goal)


class ProceduralTeacher2(ProceduralTeacher):

    def rule(self, goal):
        p = np.array(self.env.simulator.poses[1]["pose"][:2].tolist() + [85.0, 0.0])
        g1 = self.env.normalize_f(p)
        p = self.env.simulator.table.denormalize(goal)
        p = np.array(p.tolist() + [85.0, 0.0])
        g2 = self.env.normalize_f(p)
        y = np.array(g1.tolist() + g2.tolist())
        p_type = (0, 0)
        return (y, p_type)

    def choose_demo(self, goal, task):
        if task == 2:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(2)
        return self.rule(real_goal)


class ProceduralTeacher2V2(ProceduralTeacher):

    def rule(self, goal):
        g1 = self.env.simulator.table.normalize(self.env.simulator.poses[1]["pose"][:2])
        y = np.array(g1[:2].tolist() + goal.tolist())
        p_type = (0, 0)
        return (y, p_type)

    def choose_demo(self, goal, task):
        if task == 2:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(2)
        return self.rule(real_goal)


class ProceduralTeacher3(ProceduralTeacher):

    def rule(self, goal):
        p1 = goal[:2]
        p2 = goal[2:]
        y = np.array(p1.tolist() + p2.tolist())
        p_type = (1, 2)
        return (y, p_type)

    def choose_demo(self, goal, task):
        if task == 3:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(3)
        return self.rule(real_goal)


class ProceduralTeacher4(ProceduralTeacher):

    def rule(self, goal):
        if goal[2] < 0.05:
            # Not good
            pass
        g1 = -np.ones(2) * (1.0 + goal[0])/2.0
        p1 = self.env.simulator.table.denormalize(g1)
        r = 10 ** (self.env.simulator.A + (1 - goal[1])/self.env.simulator.N)
        theta = (goal[2] - 0.05) * np.pi / 0.95
        p2 = p1 + r * np.array([np.cos(theta), np.sin(theta)])
        if not self.env.simulator.table.onSquare(p2):
            p2 = p1 + r * np.array([np.cos(-theta), np.sin(-theta)])
        g2 = self.env.simulator.table.normalize(p2)
        y = np.array(g1.tolist() + g2.tolist())
        p_type = (1, 2)
        return (y, p_type)

    def choose_demo(self, goal, task):
        if task == 4:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(4)
        return self.rule(real_goal)


def save_progress(la, folder, testfile, t=None):
    if t != None:
        f = folder + "/" + testfile + "_" + str(t)
    else:
        f = folder + "/" + testfile
    save_raw(la, f)
    for fi in os.listdir(folder):
        i = fi.find(testfile)
        if i == 0 and len(fi) > len(testfile) and fi[len(testfile)] == '_':
            if t == None or int(fi[(len(testfile)+1):]) < t:
                os.remove(folder + "/" + fi)


def load_progress(la, folder, testfile):
    for fi in os.listdir(folder):
        i = fi.find(testfile)
        if i == 0 and len(fi) > len(testfile) and fi[len(testfile)] == '_':
            print("Loading last record: " + folder + "/" + fi)
            res = load_raw(folder + "/" + fi)
            #print la.environment.controller
            #if hasattr(la, 'environment.controller'):
            res.environment.controller = la.environment.controller
            return res
    return la



"""
######### This main does imitation on simulation setup
if __name__ == "__main__":
    import random
    from dataset import ActionSpace, OutcomeSpace
    from learning_strategies import ImitationV4
    from learning_agents import LearningAgentV4
    import numpy as np
    import copy

    from utils import save_raw, load_raw, load_json, save_json

    print("All imports loaded")

    exp = SimuYumiExperimentatorV2()

    # Need to load the demos
    # Demos stored in dict with pointX value, point0 is return home only in raw_demos
    raw_demos = load_json("Demos/simuDemos.json")
    raw_r_demos = load_json("Demos/simuDemosReversed.json")

    # Get all motions going to no object point
    keys = []
    for k in raw_demos.keys():
        if k != "point0":  # and k != "point2" and k != "point1":
            keys.append(k)


    print("Testing A1 demos")
    data = copy.deepcopy(exp.data)
    for k1 in keys:
        print k1
        exp.env.reset()
        y_list, y_types = exp.env.execute(np.array(raw_demos[k1]), 0)
        a = np.array(raw_demos[k1])
        if len(y_list) > 0:
            data.add_entity(a, (0, 0), y_list, y_types, 1.0)
    print data.idA, data.idY
    save_raw(data, "Demos/Teacher0V2")

    print("Testing A2 demos for point1")
    data = copy.deepcopy(exp.data)
    data.create_a_space((0, 1))
    for k1 in keys:
        print k1
        exp.env.reset()
        exp.env.execute(np.array(raw_demos["point1"]), 0)
        y_list, y_types = exp.env.execute(np.array(raw_demos[k1]), 0)
        a = np.array(raw_demos["point1"] + raw_demos[k1])
        if len(y_list) > 0:
            data.add_entity(a, (0, 1), y_list, y_types, 1.0)
    print data.idA, data.idY
    save_raw(data, "Demos/Teacher1V2")

    print("Testing A2 demos for point2")
    data = copy.deepcopy(exp.data)
    data.create_a_space((0, 1))
    for k1 in keys:
        print k1
        exp.env.reset()
        exp.env.execute(np.array(raw_demos["point2"]), 0)
        y_list, y_types = exp.env.execute(np.array(raw_demos[k1]), 0)
        a = np.array(raw_demos["point2"] + raw_demos[k1])
        if len(y_list) > 0:
            data.add_entity(a, (0, 1), y_list, y_types, 1.0)
    print data.idA, data.idY
    save_raw(data, "Demos/Teacher2V2")


    print("Testing A6 demos")
    dico = {}
    for k1 in keys:
        dico[k1] = {}
        for k2 in keys:
            if k1 == k2:
                continue
            print k1, k2
            env.reset()
            env.execute(np.array(raw_demos["point1"]), 0)
            env.execute(np.array(raw_r_demos["point1"]), 0)
            env.execute(np.array(raw_demos[k1]), 0)
            env.execute(np.array(raw_demos["point2"]), 0)
            env.execute(np.array(raw_r_demos["point2"]), 0)
            env.execute(np.array(raw_demos[k2]), 0)
            dico[k1][k2] = [env.simulator.objects[0].pose[:2].tolist(), env.simulator.objects[1].pose[:2].tolist(), \
                    ((not env.simulator.bad) and env.simulator.objects[1].detected and env.simulator.objects[0].detected), \
                    env.simulator.sound]
    save_raw(dico, "A6_demos.raw")
    #save_json(dico, "A6_demos.json")


    print("Testing A7 demos")
    dico = {}
    for k1 in keys:
        dico[k1] = {}
        for k2 in keys:
            if k1 == k2:
                continue
            print k1, k2
            env.reset()
            env.execute(np.array(raw_demos["point1"]), 0)
            env.execute(np.array(raw_r_demos["point1"]), 0)
            env.execute(np.array(raw_demos[k1]), 0)
            env.execute(np.array(raw_r_demos[k1]), 0)
            env.execute(np.array(raw_demos["point2"]), 0)
            env.execute(np.array(raw_r_demos["point2"]), 0)
            env.execute(np.array(raw_demos[k2]), 0)
            dico[k1][k2] = [env.simulator.objects[0].pose[:2].tolist(), env.simulator.objects[1].pose[:2].tolist(), \
                    ((not env.simulator.bad) and env.simulator.objects[1].detected and env.simulator.objects[0].detected), \
                    env.simulator.sound]
    save_raw(dico, "A7_demos.raw")
    #save_json(dico, "A7_demos.json")

    print("Testing A5 demos")
    dico = {}
    for k1 in keys:
        dico[k1] = {}
        for k2 in keys:
            if k1 == k2:
                continue
            print k1, k2
            env.reset()
            env.execute(np.array(raw_demos["point1"]), 0)
            env.execute(np.array(raw_demos[k1]), 0)
            env.execute(np.array(raw_r_demos[k1]), 0)
            env.execute(np.array(raw_demos["point2"]), 0)
            env.execute(np.array(raw_demos[k2]), 0)
            dico[k1][k2] = [env.simulator.objects[0].pose[:2].tolist(), env.simulator.objects[1].pose[:2].tolist(), \
                    ((not env.simulator.bad) and env.simulator.objects[1].detected and env.simulator.objects[0].detected), \
                    env.simulator.sound]
    save_raw(dico, "A5_demos.raw")
    #save_json(dico, "A5_demos.json")


    print("Testing A4 demos")
    data = copy.deepcopy(exp.data)
    data.create_a_space((0, 3))
    for k1 in keys:
        for k2 in keys:
            if k1 == k2:
                continue
            print k1, k2
            exp.env.reset()
            exp.env.execute(np.array(raw_demos["point1"]), 0)
            exp.env.execute(np.array(raw_demos[k1]), 0)
            exp.env.execute(np.array(raw_demos["point2"]), 0)
            y_list, y_types = exp.env.execute(np.array(raw_demos[k2]), 0)
            a = np.array(raw_demos["point1"] + raw_demos[k1] + raw_demos["point2"] + raw_demos[k2])
            if len(y_list) > 0:
                data.add_entity(a, (0, 3), y_list, y_types, 1.0)
    print data.idA, data.idY
    save_raw(data, "Demos/Teacher34V2")
"""

"""
### Evaluation of a learner
if __name__ == "__main__":
    import numpy as np
    from learning_agents import LearningAgentV4
    from evaluation_multiprocesses import EvaluationV4, LightWeightEvaluation
    from utils import load_raw, save_raw


    folder = "Yumi_SGIM_ACTS_4_1"
    testfile = "test"

    print("Evaluation of " + "[" + folder + "] " + testfile)

    la = None
    la = load_raw(folder + "/" + testfile)
    eva = EvaluationV4(la, SimuYumiExperimentatorV2.create_testbench())
    t_list = [0, 100, 500, 1000, 2000, 5000, 10000, 15000, 20000, 25000]

    print("Press Enter to start evaluation")
    raw_input()

    i = 0
    while i < len(t_list) and t_list[i] <= la.n_iter:
        eva.evaluate(t_list[i])
        print("Evaluated at iteration: " + str(t_list[i]))
        i += 1
    eva.evaluate(la.n_iter)

    save_progress(eva.get_lw(), folder, "eval_" + testfile)
    print("[" + folder + "] " + testfile + " evaluated")
"""

"""
### Perform evaluation of imitation learner
if __name__ == "__main__":
    import numpy as np
    from learning_agents import LearningAgentV4
    from evaluation_multiprocesses import EvaluationV4, LightWeightEvaluation
    from utils import load_raw, save_raw


    testfile = "Yumi_Imitation_2_dataset"

    print("Evaluation of " + testfile)

    data = load_raw(testfile)

    la = LearningAgentV4(data, [], None)

    eva = EvaluationV4(la, SimuYumiExperimentatorV2.create_testbench())
    t_list = [25000]

    for i in range(25001):
        la.epmem_length.append(len(data.idA))

    print("Press Enter to start evaluation")
    raw_input()

    i = 0
    while i < len(t_list):
        eva.evaluate(t_list[i])
        print("Evaluated at iteration: " + str(t_list[i]))
        i += 1

    save_raw(eva.get_lw(), testfile + "_eval")
    print(testfile + " evaluated")
"""

"""
######## This main computes an ideal Imitation setup
if __name__ == "__main__":
    from dataset import ActionSpace, OutcomeSpace
    from utils import save_raw, load_raw

    exp = SimuYumiExperimentatorV2()

    data = exp.data

    t_folder = "Demos/"
    t_options = {'nb_iteration': 0, 'noise': 0.005}
    for f in ["Teacher0V2", "Teacher1V2", "Teacher2V2", "Teacher34V2"]:
        dataset = load_raw(t_folder + f)
        merge_datasets(data, dataset)
    save_raw(data, "Yumi_Imitation_dataset")
"""


def main_SGIM_PB(name):
    """This main does SGIM-PB on simulation setup."""
    import random
    from dataset import ActionSpace, OutcomeSpace
    from learning_strategies import AutonomousExplorationV6, AutonomousProcedureV2, RandomActionV4, DiscreteMimicryV4
    from learning_agents import InterestAgentV4, InterestAgentV6, Mod, GoodRegionMod, GoodPointMod
    from interest_models import InterestModelV3
    import numpy as np

    from utils import save_raw, load_raw

    print("All imports loaded")

    exp = SimuYumiExperimentatorV2()

    options = {'min_points': 500, 'nb_iteration': 1, 'noise': 0.005}
    autoPol = AutonomousExplorationV6(exp.env, exp.data, "Autonomous policies", options, 0)
    other_op = {'min_points': 100, 'nb_iteration': 1, 'noise': 0.05}
    autoProc = AutonomousProcedureV2(exp.env, exp.data, "Autonomous procedures", other_op, 0)
    strat = [autoPol, autoProc]
    costs = [1.0, 1.0]
    RandomActionV4.k = 2.0

    t_folder = "Demos/"
    t_options = {'nb_iteration': 0, 'noise': 0.005}
    for f in ["Teacher0V2"]:   #, "Teacher1V2", "Teacher2V2", "Teacher34V2"]:
        dataset = load_raw(t_folder + f)
        costs.append(10.0)
        strat.append(DiscreteMimicryV4(exp.env, f, dataset, t_options))

    p_options = {'nb_iteration': 0, 'noise': 0.01}
    strat.append(ProceduralTeacher1V2(exp.env, exp.data, "ProceduralExpert1", p_options))
    costs.append(5.0)
    strat.append(ProceduralTeacher2V2(exp.env, exp.data, "ProceduralExpert2", p_options))
    costs.append(5.0)
    strat.append(ProceduralTeacher3(exp.env, exp.data, "ProceduralExpert3", p_options))
    costs.append(5.0)
    strat.append(ProceduralTeacher4(exp.env, exp.data, "ProceduralExpert4", p_options))
    costs.append(5.0)

    im = InterestModelV3(exp.data, {'around': 0.1, 'costs': costs, 'interest_window': 25, 'max_attempts': 6, \
            'max_points': 50, 'min_surface': 0.05, 'nb_regions': 10})
    mods = [Mod(im, 0.3), GoodRegionMod(im, 0.5), GoodPointMod(im, 0.2)]


    la = InterestAgentV6(exp.data, strat, mods, im, exp.env)

    folder = name
    testfile = "test"
    save_iter = 1000
    n_iter = 25000
    if not os.path.exists(folder):
        os.makedirs(folder)

    la = load_progress(la, folder, testfile)

    next_iter = la.n_iter
    while next_iter < n_iter:
        next_iter += save_iter
        next_iter = min(next_iter, n_iter)
        la.run(next_iter)
        if next_iter < n_iter:
            la.environment.controller = None
            save_progress(la, folder, testfile, next_iter)
            la.environment.controller = exp.server

    la.environment.controller = None
    save_progress(la, folder, testfile)
    print("[" + folder + "] " + testfile + " finished")


def main_SGIM_ACTS(name):
    """This main does SGIM-ACTS on simulation setup."""
    import random
    from dataset import ActionSpace, OutcomeSpace
    from learning_strategies import AutonomousExplorationV6, RandomActionV4, DiscreteMimicryV4
    from learning_agents import InterestAgentV6, Mod, GoodRegionMod, GoodPointMod
    from interest_models import InterestModelV3
    import numpy as np

    from utils import save_raw, load_raw

    print("All imports loaded")

    exp = SimuYumiExperimentatorV2()

    options = {'min_points': 500, 'nb_iteration': 1, 'noise': 0.005}
    autoPol = AutonomousExplorationV6(exp.env, exp.data, "Autonomous policies", options, 0)
    strat = [autoPol]
    costs = [1.0]
    RandomActionV4.k = 2.0

    t_folder = "Demos/"
    t_options = {'nb_iteration': 0, 'noise': 0.005}
    for f in ["Teacher0V2", "Teacher1V2", "Teacher2V2", "Teacher34V2"]:
        dataset = load_raw(t_folder + f)
        costs.append(10.0)
        strat.append(DiscreteMimicryV4(exp.env, f, dataset, t_options))


    im = InterestModelV3(exp.data, {'around': 0.1, 'costs': costs, 'interest_window': 25, 'max_attempts': 6, \
            'max_points': 50, 'min_surface': 0.05, 'nb_regions': 10})
    mods = [Mod(im, 0.3), GoodRegionMod(im, 0.5), GoodPointMod(im, 0.2)]


    la = InterestAgentV6(exp.data, strat, mods, im, exp.env)

    folder = name
    testfile = "test"
    save_iter = 1000
    n_iter = 25000
    if not os.path.exists(folder):
        os.makedirs(folder)

    la = load_progress(la, folder, testfile)

    next_iter = la.n_iter
    while next_iter < n_iter:
        next_iter += save_iter
        next_iter = min(next_iter, n_iter)
        la.run(next_iter)
        if next_iter < n_iter:
            la.environment.controller = None
            save_progress(la, folder, testfile, next_iter)
            la.environment.controller = exp.server

    la.environment.controller = None
    save_progress(la, folder, testfile)
    print("[" + folder + "] " + testfile + " finished")


def main_IM_PB(name):
    """This main does IM-PB on simulation setup."""
    import random
    from dataset import ActionSpace, OutcomeSpace
    from learning_strategies import AutonomousExplorationV6, AutonomousProcedureV2, RandomActionV4
    from learning_agents import InterestAgentV6, Mod, GoodRegionMod, GoodPointMod
    from interest_models import InterestModelV3
    import numpy as np

    from utils import save_raw, load_raw

    print("All imports loaded")

    exp = SimuYumiExperimentatorV2()

    options = {'min_points': 500, 'nb_iteration': 1, 'noise': 0.005}
    autoPol = AutonomousExplorationV6(exp.env, exp.data, "Autonomous policies", options, 0)
    other_op = {'min_points': 100, 'nb_iteration': 1, 'noise': 0.05}
    autoProc = AutonomousProcedureV2(exp.env, exp.data, "Autonomous procedures", other_op, 0)
    strat = [autoPol, autoProc]
    RandomActionV4.k = 2.0

    im = InterestModelV3(exp.data, {'around': 0.1, 'costs': [1.0, 1.0], 'interest_window': 25, 'max_attempts': 6, \
            'max_points': 50, 'min_surface': 0.05, 'nb_regions': 10})
    mods = [Mod(im, 0.25), GoodRegionMod(im, 0.6), GoodPointMod(im, 0.15)]


    la = InterestAgentV6(exp.data, strat, mods, im, exp.env)


    folder = name
    testfile = "test"
    save_iter = 1000
    n_iter = 25000
    if not os.path.exists(folder):
        os.makedirs(folder)

    la = load_progress(la, folder, testfile)

    next_iter = la.n_iter
    while next_iter < n_iter:
        next_iter += save_iter
        next_iter = min(next_iter, n_iter)
        la.run(next_iter)
        if next_iter < n_iter:
            la.environment.controller = None
            save_progress(la, folder, testfile, next_iter)
            la.environment.controller = exp.server

    la.environment.controller = None
    save_progress(la, folder, testfile)
    print("[" + folder + "] " + testfile + " finished")


def main_SAGG_RIAC(name):
    """This main does SAGG-RIAC on simulation setup."""
    import random
    from dataset import ActionSpace, OutcomeSpace
    from learning_strategies import AutonomousExplorationV6, AutonomousProcedureV2, RandomActionV4
    from learning_agents import InterestAgentV6, Mod, GoodRegionMod, GoodPointMod
    from interest_models import InterestModelV3
    import numpy as np

    from utils import save_raw, load_raw

    print("All imports loaded")

    exp = SimuYumiExperimentatorV2()

    options = {'min_points': 500, 'nb_iteration': 1, 'noise': 0.005}
    autoPol = AutonomousExplorationV6(exp.env, exp.data, "Autonomous policies", options, 0)
    strat = [autoPol]
    RandomActionV4.k = 2.0

    im = InterestModelV3(exp.data, {'around': 0.1, 'costs': [1.0], 'interest_window': 25, 'max_attempts': 6, \
            'max_points': 50, 'min_surface': 0.05, 'nb_regions': 10})
    mods = [Mod(im, 0.25), GoodRegionMod(im, 0.6), GoodPointMod(im, 0.15)]


    la = InterestAgentV6(exp.data, strat, mods, im, exp.env)

    folder = name
    testfile = "test"
    save_iter = 1000
    n_iter = 25000
    if not os.path.exists(folder):
        os.makedirs(folder)

    la = load_progress(la, folder, testfile)

    next_iter = la.n_iter
    while next_iter < n_iter:
        next_iter += save_iter
        next_iter = min(next_iter, n_iter)
        la.run(next_iter)
        if next_iter < n_iter:
            la.environment.controller = None
            save_progress(la, folder, testfile, next_iter)
            la.environment.controller = exp.server

    la.environment.controller = None
    save_progress(la, folder, testfile)
    print("[" + folder + "] " + testfile + " finished")


def main_RandomPolicy(name):
    """This main does RandomPolicy on simulation setup."""
    import random
    from dataset import ActionSpace, OutcomeSpace
    from learning_strategies import AutonomousExplorationV6, AutonomousProcedureV2, RandomActionV4
    from learning_agents import LearningAgentV4, Mod, GoodRegionMod, GoodPointMod
    from interest_models import InterestModelV3
    import numpy as np

    from utils import save_raw, load_raw

    print("All imports loaded")

    exp = SimuYumiExperimentatorV2()

    strat = RandomActionV4(exp.env, "Random")
    la = LearningAgentV4(exp.data, [strat], exp.env)

    folder = name
    testfile = "test"
    save_iter = 1000
    n_iter = 25000
    if not os.path.exists(folder):
        os.makedirs(folder)

    la = load_progress(la, folder, testfile)

    next_iter = la.n_iter
    while next_iter < n_iter:
        next_iter += save_iter
        next_iter = min(next_iter, n_iter)
        la.run(next_iter)
        if next_iter < n_iter:
            la.environment.controller = None
            save_progress(la, folder, testfile, next_iter)
            la.environment.controller = exp.server

    la.environment.controller = None
    save_progress(la, folder, testfile)
    print("[" + folder + "] " + testfile + " finished")


if __name__ == "__main__":
    algo = "Yumi_SGIM_PB_"
    # Test 10 runs of SGIM-PB on the Yumi simulation setup
    for i in range(1, 11):
        main_SGIM_PB(algo + str(i))
