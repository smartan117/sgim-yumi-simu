from utils import load_raw, save_raw
from dataset import SimpleDataset
from learning_agents import LearningAgentV4
import sys
import numpy as np
import math




def merge_datasets(d1, d2):
    for idY, idA in zip(d2.idY, d2.idA):
        a_type = idA[0]
        a_id = idA[1]
        a = d2.a_spaces[a_type[0]][a_type[1]].data[a_id]
        y_list = []
        y_types = []
        cost = 1.0
        for (t, y_id) in idY:
            y_list.append(d2.y_spaces[t].data[y_id])
            y_types.append(t)
            cost = d2.y_spaces[t].costs[y_id]
        d1.add_entity(a, a_type, y_list, y_types, cost)


def give_progress(progress, nb_lines):
    result = "["
    symbol = 100 / nb_lines
    result += "=" * (progress / symbol)
    result += " " * (nb_lines - (progress / symbol)) + "] "
    result += str(progress)
    result += " " * (3 - len(str(progress))) + "%"
    return result


if len(sys.argv) == 1:
    raise Exception("No output datafile given!")
outputfile = sys.argv[1]

if len(sys.argv) == 2:
    raise Exception("No datasets given!")

print("Starting merging datasets.")

nb_equals = 50
steps = (len(sys.argv) - 1) * 2 - 1
step = 100.0 / float(steps)
s = 0
sys.stdout.write("\r" + give_progress(0, nb_equals))
sys.stdout.flush()

la = load_raw(sys.argv[2])
data = la.dataset

s += 1
progress = int(math.floor(s * step))
sys.stdout.write("\r" + give_progress(progress, nb_equals))
sys.stdout.flush()

if not isinstance(la, LearningAgentV4):
    raise Exception("File gven does not contain a learning agent.")

for datafile in sys.argv[3:]:
    la2 = load_raw(datafile)
    s += 1
    progress = int(math.floor(s * step))
    sys.stdout.write("\r" + give_progress(progress, nb_equals))
    sys.stdout.flush()
    if not isinstance(la2, LearningAgentV4):
        s += 1
        progress = int(math.floor(s * step))
        sys.stdout.write("\r" + give_progress(progress, nb_equals))
        sys.stdout.flush()
        continue
    merge_datasets(data, la2.dataset)
    s += 1

print("Done merging datasets.")

# Goals for teachers
"""
# Dense
hor_grid1 = [[-0.9, 0.0],[-0.45, -0.45],[-0.45, 0.45],[0.0, -0.9], \
    [0.0, 0.0],[0.0, 0.9], [0.45, -0.45],[0.45, 0.45],[0.9, 0.0]]
z_grid1 = [0.0, 0.3, 0.6, 0.9]
"""
# Sparse
hor_grid1 = [[-0.9, 0.0],[0.0, -0.9],[0.0, 0.0],[0.0, 0.9],[0.9, 0.0]]
z_grid1 = [0.0, 0.9]

goals1 = []

for z in z_grid1:
    for d in hor_grid1:
        p = np.array(d + [z])
        goals1.append(p + np.random.uniform(-0.1, 0.1, 3))

"""
# Dense
hor_grid4 = [[-0.8, -0.8],[-0.8, 0.0],[-0.8, 0.8],[0.0, -0.8],[0.0, 0.0], \
    [0.0, 0.8],[0.8, -0.8],[0.8, 0.0], [0.8, 0.8]]
z_grid4 = [-0.9, -0.3, 0.3, 0.9]
"""
# Sparse
hor_grid4 = [[-0.8, -0.8],[-0.8, 0.0],[-0.8, 0.8],[0.0, -0.8],[0.0, 0.0], \
    [0.0, 0.8],[0.8, -0.8],[0.8, 0.0], [0.8, 0.8]]
z_grid4 = [-0.9, -0.3, 0.3, 0.9]

goals4 = []

for z in z_grid4:
    for d in hor_grid4:
        p = np.array(d + [z])
        goals4.append(p + np.random.uniform(-0.1, 0.1, 3))

grid2 = [[-0.9, -0.9],[-0.9, 0.0],[-0.9, 0.9],[0.0, -0.9],[0.0, 0.0], \
    [0.0, 0.9],[0.9, -0.9],[0.9, 0.0],[0.9,0.9]]
goals2 = []

for i, d_i in enumerate(grid2):
    for j, d_j in enumerate(grid2[(i+1):]):
        p = np.array(d_i + d_j)
        goals2.append(p + np.random.uniform(-0.1, 0.1, 4))


print("Building teacher datasets.")

data1 = SimpleDataset.clone_without_content(data)
data2 = SimpleDataset.clone_without_content(data)
data4 = SimpleDataset.clone_without_content(data)


for g in goals1:
    ids,_ = data.nn_y(g, 1)
    y = data.y_spaces[1].data[ids[0]]
    cost = data.y_spaces[1].costs[ids[0]]
    idE = data.y_spaces[1].ids[ids[0]]
    a_type = data.idA[idE][0]
    idA = data.idA[idE][1]
    a = data.a_spaces[a_type[0]][a_type[1]].data[idA]
    data1.add_entity(a, a_type, [y], [1], cost)
    
for g in goals2:
    ids,_ = data.nn_y(g, 2)
    y = data.y_spaces[2].data[ids[0]]
    cost = data.y_spaces[2].costs[ids[0]]
    idE = data.y_spaces[2].ids[ids[0]]
    a_type = data.idA[idE][0]
    idA = data.idA[idE][1]
    a = data.a_spaces[a_type[0]][a_type[1]].data[idA]
    data2.add_entity(a, a_type, [y], [2], cost)

for g in goals4:
    ids,_ = data.nn_y(g, 4)
    y = data.y_spaces[4].data[ids[0]]
    cost = data.y_spaces[4].costs[ids[0]]
    idE = data.y_spaces[4].ids[ids[0]]
    a_type = data.idA[idE][0]
    idA = data.idA[idE][1]
    a = data.a_spaces[a_type[0]][a_type[1]].data[idA]
    data4.add_entity(a, a_type, [y], [4], cost)

print("Done building teacher datasets.")

print("Saving teachers.")

save_raw(data1, outputfile+"1")
save_raw(data2, outputfile+"2")
save_raw(data4, outputfile+"4")

print("Done saving teachers.")








