import zmq
import time
import json
import copy

import random

from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')


class SimuNode:
    
    def __init__(self):
        self.server = None
        self.client = None
        self.order = None
        self.working = True
        self.n = 0.
    
    def connect(self):
        ctx = zmq.Context()
        self.server = ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5554")
        self.client = ctx.socket(zmq.REQ)
        self.client.connect("tcp://127.0.0.1:5551")
    
    def move(self, path):
        print("Moving!")
        for p in path:
            if len(p) != 7:
                self.working = False
        r = random.uniform(0., 1.)
        if r < 0.33:
            self.working = False
        
        time.sleep(5)
        self.n += 1.
    
    def reset(self):
        print("Resetting!")
        self.working = True
        time.sleep(0.1)
    
    def analyze(self, msg):
        try:
            self.order = json.loads(msg)
            return (self.order.has_key("type") and self.order.has_key("valid") and self.order.has_key('payload'))
        except Exception:
            return False
    
    def analyze_ack(self, msg):
        try:
            order = json.loads(msg)
            return (order.has_key("type") and self.order.has_key("valid"))
        except Exception:
            return False
    
    def applyOrder(self):
        if self.order['type'] == "move" and self.order['payload'].has_key('path') and self.order['payload'].has_key('dt'):
            self.move(self.order['payload']['path'])
        elif self.order['type'] == "reset" and self.order['payload'].has_key('joints'):
            self.reset()
    
    def check_state(self):
        ack = {'type': self.order['type'], 'from': 3}
        if self.order['type'] == "move":
            ack['valid'] = self.working
            ack['payload'] = {'joints': [self.n, 0., 0., 0., 0., 0., 0.]}
        elif self.order['type'] == "reset":
            ack['valid'] = self.working
        print("Sending: " + str(ack))
        self.client.send(json.dumps(ack))
        msg = self.client.recv()
        print("Ack: " + msg)
        if not self.analyze_ack(msg):
            print("Coordinator node indicates an error...")
    
    def run(self):
        while True:
            try:
                msg = self.server.recv()
                print("Received order: " + msg)
                valid = self.analyze(msg)
                if not valid:
                    ack = {"type": "error", "payload": msg, "from": 3}
                    print("Acking: " + str(ack))
                    self.server.send(json.dumps(ack))
                else:
                    ack = {"type": self.order['type'], "valid": True, "from": 3}
                    print("Acking: " + str(ack))
                    self.server.send(json.dumps(ack))
                    self.applyOrder()
                    self.check_state()
                print("")
            except Exception:
                print("Connexion error!")
                return


if __name__ == "__main__":
    node = SimuNode()
    node.connect()
    node.run()
    
    
        
            
