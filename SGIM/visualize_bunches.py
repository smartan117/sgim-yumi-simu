from visualization import Visualizer

from learning_agents import InterestAgentV4, LearningAgentV4
from learning_strategies import AutonomousExplorationV4
from evaluation_multiprocesses import LightWeightEvaluation
from utils import load_raw, prepare_plot

import matplotlib.pyplot as plt
import numpy as np

import sys, os
import math
import copy

from skaro_experiment import *





def plot_best_possible_evaluation(mean, t_list, ax, options):
    y = np.ones(len(t_list)) * mean
    ax.plot(np.array(t_list), y, marker=options['marker'], linestyle=options['linestyle'], color=options['color'])
    plt.yscale('log')
    plt.grid(True)


def superpose_all_evaluations(evals, algos):
    cmap = plt.cm.jet
    colors = []
    markers = []
    lines = []
    legends = []
    plots = []
    potential_markers = ['', 'o', '^', '*', 's']
    max_colours = 5
    div = min(len(evals)-1, max_colours)
    for i in range(len(evals)):
        if len(evals) == 1:
            colors.append('b')
        else:
            colors.append(cmap(int(math.floor(float(i % max_colours)*float(cmap.N - 1)/float(div)))))
        markers.append(potential_markers[i/max_colours])
        lines.append('solid')
        legends.append(algos[i])
        plots.append(lambda ax, options, eva=evals[i]: eva.plot(ax, options))
    
    dico = {}
    dico['limits'] = {'min': [None, None], 'max': [None, None]}
    dico['title'] = "Evaluations of all algos"
    dico['color'] = colors
    dico['marker'] = markers
    dico['axes'] = ['Iterations', 'Mean error on testbench']
    dico['linestyle'] = lines
    dico['legend'] = legends
    dico['plots'] = plots
    
    return dico


def superpose_task_evaluations(evals, algos, task):
    cmap = plt.cm.jet
    colors = []
    markers = []
    lines = []
    legends = []
    plots = []
    potential_markers = ['', 'o', '^', '*', 's']
    max_colours = 5
    div = min(len(evals)-1, max_colours)
    for i in range(len(evals)):
        if len(evals) == 1:
            colors.append('b')
        else:
            colors.append(cmap(int(math.floor(float(i % max_colours)*float(cmap.N - 1)/float(div)))))
        markers.append(potential_markers[i/max_colours])
        lines.append('solid')
        legends.append(algos[i])
        plots.append(lambda ax, options, eva=evals[i]: eva.plot_mean_task(task, ax, options))
    
    dico = {}
    dico['limits'] = {'min': [None, None], 'max': [None, None]}
    dico['title'] = "Evaluations of all algos on task " + str(task)
    dico['color'] = colors
    dico['marker'] = markers
    dico['axes'] = ['Iterations', 'Mean error on testbench']
    dico['linestyle'] = lines
    dico['legend'] = legends
    dico['plots'] = plots
    
    return dico


def combine_evaluations(evals):
    evaluation = []
    t_list = []
    n_t = len(evals[0].evaluation)
    n_tasks = len(evals[0].evaluation[0][1])
    for t in range(n_t):
        t_list.append(evals[0].evaluation[t][0])
        new_eval = []
        for task in range(n_tasks):
            new_eval.append([0.0, 0.0])
        new_eval.append(0.0)
        new_eval.append(0.0)
        evaluation.append(new_eval)
    
    for evali in evals:
        for t in range(len(evali.evaluation)):
            evaluation[t][-2] += evali.evaluation[t][-2] # mean
            #print evali.evaluation[t][-1]
            for task in range(n_tasks):
                #print evali.evaluation[t][task][-2]
                evaluation[t][task][0] += evali.evaluation[t][1][task][-2]
    
    for t in range(n_t):
        evaluation[t][-2] /= float(len(evals)) # mean
        for evali in evals:
            evaluation[t][-1] += (evali.evaluation[t][-2] - evaluation[t][-2])**2
        evaluation[t][-1] /= float(len(evals))
        evaluation[t][-1] = np.sqrt(evaluation[t][-1])
        for task in range(n_tasks):
            evaluation[t][task][0] /= float(len(evals)) # mean_task
            for evali in evals:
                evaluation[t][task][1] += (evali.evaluation[t][1][task][-2] - evaluation[t][task][0])**2
            evaluation[t][task][1] /= float(len(evals))
            evaluation[t][task][1] = np.sqrt(evaluation[t][task][1])
    
    return evaluation, t_list


def action_repartition(evals, ax, options):
    n = []
    dim_max = 0
    for ev in evals:
        n.append([])
        for j in range(len(ev.learning_agent.dataset.a_spaces[0])):
            n[-1].append(len(ev.learning_agent.dataset.a_spaces[0][j].data))
            dim_max = max(j+1, dim_max)
    nb = np.zeros((len(n), dim_max))
    for i in range(len(n)):
        nb[i,0:len(n[i])] = n[i]
    means = np.mean(nb, axis=0)
    stds = np.std(nb, axis=0)
    ax.plot(np.array(range(dim_max)), means, \
            color=options['color'], linestyle=options['linestyle'], marker=options['marker'])
    ax.errorbar(np.array(range(dim_max)), means, stds, \
            color=options['color'], linestyle=options['linestyle'], marker=options['marker'])


def superpose_action_repartitions(evals, algos):
    cmap = plt.cm.jet
    colors = []
    markers = []
    lines = []
    legends = []
    plots = []
    potential_markers = ['', 'o', '^', '*', 's']
    max_colours = 5
    div = min(len(evals)-1, max_colours)
    for i in range(len(evals)):
        if len(evals) == 1:
            colors.append('k')
        else:
            colors.append(cmap(int(math.floor(float(i % max_colours)*float(cmap.N - 1)/float(div)))))
        markers.append(potential_markers[i/max_colours])
        lines.append('solid')
        legends.append(algos[i])
        plots.append(lambda ax, options, ev=evals[i]: action_repartition(ev, ax, options))
    
    dico = {}
    dico['limits'] = {'min': [None, None], 'max': [None, None]}
    dico['title'] = "Action repartition"
    dico['color'] = colors
    dico['marker'] = markers
    dico['axes'] = ['Action Spaces', 'Mean actions']
    dico['linestyle'] = lines
    dico['legend'] = legends
    dico['plots'] = plots
    
    return dico


def plot_combined_evaluation(evals, t_list, ax, options):
    t = []
    m = []
    s = []
    for i in range(len(t_list)):
        t.append(t_list[i])
        m.append(evals[i][-2])
        s.append(evals[i][-1])
    #ax.errorbar(t, m, s, color=options['color'], linestyle=options['linestyle'], marker=options['marker'])
    ax.plot(t, m, color=options['color'], linestyle=options['linestyle'], marker=options['marker'])
    plt.yscale('log')
    plt.grid(True)


def prepare_combined_eval_plot(evals, t_list):
    data = []
    for i in range(len(t_list)):
        data.append([t_list[i], evals[i][-2]])
    return data


def plot_combined_evaluation_task(evals, t_list, task, ax, options):
    t = []
    m = []
    s = []
    for i in range(len(t_list)):
        t.append(t_list[i])
        m.append(evals[i][task][0])
        s.append(evals[i][task][1])
    #ax.errorbar(t, m, s, color=options['color'], linestyle=options['linestyle'], marker=options['marker'])
    ax.plot(t, m, color=options['color'], linestyle=options['linestyle'], marker=options['marker'])
    plt.yscale('log')
    plt.grid(True)


def prepare_combined_eval_task_plot(evals, t_list, task):
    data = []
    for i in range(len(t_list)):
        data.append([t_list[i], evals[i][task][0]])
    return data


def superpose_combines_evals(evals, t_lists, algos, best=None):
    cmap = plt.cm.jet
    colors = []
    markers = []
    lines = []
    legends = []
    plots = []
    potential_markers = ['', 'o', '^', '*', 's']
    max_colours = 5
    div = min(len(evals)-1, max_colours)
    for i in range(len(evals)):
        if len(evals) == 1:
            colors.append('b')
        else:
            colors.append(cmap(int(math.floor(float(i % max_colours)*float(cmap.N - 1)/float(div)))))
        markers.append(potential_markers[i/max_colours])
        lines.append('solid')
        legends.append(algos[i])
        plots.append(lambda ax, options, ev=evals[i], t=t_lists[i]: plot_combined_evaluation(ev, t, ax, options))
    
    if best != None:
        colors.append('k')
        markers.append('')
        lines.append('solid')
        legends.append("Ideal best")
        plots.append(lambda ax, options, mean=best, t_list=t_lists[0]: plot_best_possible_evaluation(mean, t_list, ax, options))
    
    dico = {}
    dico['limits'] = {'min': [None, None], 'max': [None, None]}
    dico['title'] = "Evaluations of all algos"
    dico['color'] = colors
    dico['marker'] = markers
    dico['axes'] = ['Iterations', 'Mean error on testbench']
    dico['linestyle'] = lines
    dico['legend'] = legends
    dico['plots'] = plots
    
    return dico


def superpose_combines_evals_task(evals, t_lists, task, algos, best=None):
    cmap = plt.cm.jet
    colors = []
    markers = []
    lines = []
    legends = []
    plots = []
    potential_markers = ['', 'o', '^', '*', 's']
    max_colours = 5
    div = min(len(evals)-1, max_colours)
    for i in range(len(evals)):
        if len(evals) == 1:
            colors.append('b')
        else:
            colors.append(cmap(int(math.floor(float(i % max_colours)*float(cmap.N - 1)/float(div)))))
        markers.append(potential_markers[i/max_colours])
        lines.append('solid')
        legends.append(algos[i])
        plots.append(lambda ax, options, ev=evals[i], t=t_lists[i], task=task: plot_combined_evaluation_task(ev, t, task, ax, options))
    
    if best != None:
        colors.append('k')
        markers.append('')
        lines.append('solid')
        legends.append("Ideal best")
        plots.append(lambda ax, options, mean=best, t_list=t_lists[0]: plot_best_possible_evaluation(mean, t_list, ax, options))
    
    dico = {}
    dico['limits'] = {'min': [None, None], 'max': [None, None]}
    dico['title'] = "Evaluations of all algos on task " + str(task)
    dico['color'] = colors
    dico['marker'] = markers
    dico['axes'] = ['Iterations', 'Mean error on testbench']
    dico['linestyle'] = lines
    dico['legend'] = legends
    dico['plots'] = plots
    
    return dico


##### Test functions added in future commits directly inside corresponding classes

def compute_strategy_task(la):
    choices = np.zeros((len(la.dataset.y_spaces), len(la.strategies)), dtype=np.int64)
    for it in la.learning_process:
        if it[1] > 0:
            choices[it[3], it[2]] += 1
    #by_task = np.sum(choices, axis=1)
    #by_task = np.reshape(by_task, (len(la.dataset.y_spaces), 1))
    #choices /= by_task
    return choices


def get_reached_one_atype_visualizer(data, task, a_type, prefix=""):
    dico = {}
    dico['limits'] = {'min': data.y_spaces[task].bounds['min'][data.y_spaces[task].targetable], \
        'max': data.y_spaces[task].bounds['max'][data.y_spaces[task].targetable]}
    dico['title'] = prefix + "Points reached task " + str(task) + " with A" + str(a_type+1)
    dico['color'] = ['k']
    dico['marker'] = ['.']
    dico['linestyle'] = ['None']
    dico['legend'] = ["A"+str(a_type+1)]
    dico['plots'] = [lambda ax, options, i=i: data.plot_outcomes_atype(task, a_type, ax, options)]
    
    return dico


def load_best_eval(test, other=None): # REALLY DIRTY!!!
    dico = load_json("best_evaluations_ideal.json")
    if other == None:
        config = load_json(find_test(test))
        return dico[config['environment']]
    else:
        return dico[other]




if __name__ == "__main__":
    
    fig = 1
    all_evas = []
    names = []
    
    best_eval = None
    
    testname_l = sys.argv[1:]
    names = sys.argv[1:]
    if len(testname_l) == 0:
        #testname_l = ["Weird_Random", "Weird_Random_HL", "Weird_SAGG_RIAC", "Weird_SAGG_Single_HL", \
        #    "Weird_SGIM_Single_ACTS_2", "Weird_SGIM_Special_HL", "Weird_SGIM_Special_Full"]
        #names = ["RandomAction", "RandomProcedures", "SAGG_RIAC", "SAGG_HL", \
        #    "SGIM_ACTS", "SGIM_HL", "SGIM_Full"]
        testname_l = ["Old_RandomAction"]
        names = ["Random"]
    
    for testname in testname_l:
        # Extract base of filename
        folder = find_test_folder(testname) + "data"
        if best_eval == None:
            try:
                best_eval = load_best_eval(testname, "skaro_new")
            except Exception:
                pass
        
        print("Checking " + testname)
        
        all_evas.append([])
        
        for f in os.listdir(folder):
            eva = None
            try:
                eva = load_raw(folder + "/" + f)
            except Exception:
                print("Loading " + f + " failed with no reasons!")
                continue
            la = eva.learning_agent
            
            # Optimize memory by removing useless features
            for evali in eva.evaluation:
                for eval_taski in evali[1]:
                    eval_taski[0] = []
            eva.learning_agent = None
            
            # Store evaluation
            all_evas[-1].append(eva)
            
            final_mean = eva.evaluation[-1][-2]
            print(f + " --> error: " + str(final_mean))
            
            """
            vis = Visualizer(fig, [la.dataset.get_reached_one_atype_visualizer(0, (0, 0), prefix=f+" - ")])
            vis.plot()
            fig += 1
            """
            
            """
            vis = Visualizer(fig, [la.get_goals_strategy_visualizer(3, prefix=f+" - ")])
            vis.plot()
            fig += 1
            """
            
            """
            vis = Visualizer(fig, [la.dataset.get_reached_atype_visualizer(3, prefix=f+" - ")])
            vis.plot()
            fig += 1
            """
            
            """
            ####### TESTS START HERE
            
            for task in range(len(la.dataset.y_spaces)):
                print("Task " + str(task) + ":")
                for a_type in range(len(la.dataset.y_spaces[task].sub_mappings)):
                    print(str(a_type+1) + "-actions moves: " \
                        + str(len(la.dataset.y_spaces[task].sub_mappings[a_type])) \
                        + "/" + str(len(la.dataset.y_spaces[task].mapping)))
            
            print(compute_strategy_task(la))
            
            vis = Visualizer(fig, [get_reached_one_atype_visualizer(la.dataset, 1, 0, prefix=filename+" - "), \
                get_reached_one_atype_visualizer(la.dataset, 1, 1, prefix=filename+" - "), \
                get_reached_one_atype_visualizer(la.dataset, 1, 2, prefix=filename+" - ")])
            vis.plot()
            fig += 1
            
            ####### TESTS END HERE
            
            """
            # Show the choices of strategy
            if len(la.strategies) > 1:
                vis = Visualizer(fig, [la.get_strategies_visualizer(250, prefix=f+" - ")])
                vis.plot()
                fig += 1        
            
            
            if isinstance(la, InterestAgentV4):
                # Show the choices of task
                vis = Visualizer(fig, [la.get_tasks_visualizer(250, prefix=f+" - ")])
                vis.plot()
                fig += 1
            
        
        """
        # Plot action repartition for each algo
        vis = Visualizer(fig, [superpose_action_repartitions(all_evas, names)])
        vis.plot()
        fig += 1
        """
    
    # Compute evaluations superpositions
    evals = []
    t_lists = []
    nb_tasks = 0
    for i in range(len(names)):
        comb_eval, comb_t = combine_evaluations(all_evas[i])
        nb_tasks = len(comb_eval[0])-2
        evals.append(comb_eval)
        t_lists.append(comb_t)
    
    
    """
    data_list = []
    for ev, t in zip(evals, t_lists):
        data_list.append(prepare_combined_eval_plot(ev, t))
    prepare_plot(data_list, names, "default_plot/default_plot/", {'y': "log"})
    
    for i in range(nb_tasks):
        data_list = []
        for ev, t in zip(evals, t_lists):
            data_list.append(prepare_combined_eval_task_plot(ev, t, i))
        prepare_plot(data_list, names, "default_plot/default_plot_task_"+str(i)+"/", {'y': "log"})
    """
    
    
    #### Plottings
    
    """
    # Combine overall evaluations for each base test
    #vis = Visualizer(fig, [superpose_combines_evals(evals, t_lists, names, best_eval['mean'])])
    vis = Visualizer(fig, [superpose_combines_evals(evals, t_lists, names)])
    vis.plot()
    fig += 1
    
    # Combine each task evaluation for each base test
    plot_func = []
    for i in range(6):
        #plot_func.append(superpose_combines_evals_task(evals, t_lists, i, names, best_eval['mean_tasks'][i]))
        plot_func.append(superpose_combines_evals_task(evals, t_lists, i, names))
    vis = Visualizer(fig, plot_func)
    #vis = Visualizer(fig, [superpose_combines_evals_task(evals, t_lists, 0, names), \
    #    superpose_combines_evals_task(evals, t_lists, 1, names), superpose_combines_evals_task(evals, t_lists, 2, names), \
    #    superpose_combines_evals_task(evals, t_lists, 3, names)]), superpose_combines_evals_task(evals, t_lists, 4, names), \
    #    superpose_combines_evals_task(evals, t_lists, 5, names), superpose_combines_evals_task(evals, t_lists, 6, names)])
    vis.plot()
    fig += 1
    """
    
    raw_input()
    





