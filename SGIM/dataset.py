import numpy as np
import math
from scipy.spatial import cKDTree
from scipy.spatial.distance import euclidean

from utils import multivariate_regression

import matplotlib as plt

import copy




def nn_kdtree(data, g, k):
    """Return k nearest neighbours distances and ids using KD-Tree."""
    tree = cKDTree(data)
    return tree.query(g, k=k, eps=0.0, p=2)


def distance(x, y):
    """Return the euclidean distance between the two points given."""
    return euclidean(x, y)


class OutcomeSpace:
    """Contains the parameters and data of an outcome space."""
    def __init__(self, bounds, name, options=None):
        """
        bounds dict: dictionary containing the boundaries of the space (keys 'min' and 'max)
        name string: the name of the outcome space
        options dict: a dictionary containing different parameters of the outcome space
        """
        self.data = []
        self.costs = []
        self.ids = []  # execution id (order)
        self.bounds = bounds
        self.name = name
        self.dim = len(self.bounds['min'])
        # Different keys of options
        ## max_dist: used to normalize distances in NN-searches
        ## out_dist: used as a default distance for out of bounds points
        ## dist: max distance to look for neighbours in best locality
        ## err: distance corresponding to the transition between random and optimization by some strategies
        ## range: parameter used to determine the steepness of the transition between random and optimization
        ## nn_best_locality: the number of NN-neighbours to look for in best locality computations
        if options:
            self.options = options
        else:
            self.options = {}
        self.set_options()
    
    def to_config(self):
        dico = {}
        dico['name'] = self.name
        dico['bounds'] = {'min': self.bounds['min'].tolist(), 'max': self.bounds['max'].tolist()}
        dico['options'] = self.options
        return dico
    
    @classmethod
    def from_config(cls, dico):
        bounds = {'min': np.array(dico['bounds']['min']), 'max': np.array(dico['bounds']['max'])}
        return OutcomeSpace(bounds, dico['name'], dico['options'])
    
    @classmethod
    def clone_without_content(cls, space):
        """Function that will copy an other outcome space except without any data."""
        result = OutcomeSpace(copy.deepcopy(space.bounds), copy.deepcopy(space.name), \
                copy.deepcopy(space.options))
        return result
    
    def set_options(self):
        if not self.options.has_key("max_dist"):
            self.options['max_dist'] = distance(self.bounds['min'], self.bounds['max'])
        if not self.options.has_key("out_dist"):
            self.options['out_dist'] = 5.0 * self.options['max_dist']
        if not self.options.has_key('dist'):
            self.options['dist'] = (np.log(self.dim) + 1)*0.05
        if not self.options.has_key('err'):
            self.options['err'] = (np.log(self.dim) + 1)*0.05
        if not self.options.has_key('range'):
            self.options['range'] = self.options['err'] / 4.0
        if not self.options.has_key('nn_best_locality'):
            self.options['nn_best_locality'] = self.dim
    
    def valid(self, x):
        """Check if a point has good dimensions and is within bounds."""
        if len(x) != self.dim:
            return False
        for i in range(self.dim):
            if x[i] < self.bounds['min'][i] or x[i] > self.bounds['max'][i]:
                return False
        return True
    
    def add_point(self, x, idx, cost):
        """Add a point in the dataset and if valid return id."""
        if self.valid(x):
            self.data.append(x)
            self.costs.append(cost)
            self.ids.append(idx)
            return len(self.data)-1
        return -1
    
    def compute_distances(self, x):
        """Compute array of normalized distances between data and the point given."""
        if len(self.data) == 0:
            return np.array([])
        data = np.array(self.data)
        return np.sum((data - x)**2, axis=1)/self.options['max_dist']
    
    def compute_performances(self, x):
        """Compute performances for reaching the given point."""
        return self.compute_distances(x) * np.array(self.costs)
    
    def nn(self, x, n=1):
        """Computes Nearest Neighbours based on performances."""
        perf = self.compute_performances(x)
        i = range(len(perf))
        i.sort(key=lambda j: perf[j])
        i = i[0:min(len(perf), n)]
        return i, perf[i]
    
    def nn_d(self, x, n=1):
        """Compute Nearest Neighbours based on distance."""
        dist = self.compute_distances(x)
        i = range(len(dist))
        i.sort(key=lambda j: dist[j])
        i = i[0:min(len(dist), n)]
        return i, dist[i]
    
    def plot_outcomes(self, ax, options):
        """Plot the outcomes recorded on the axis given wrt options."""
        if self.dim == 1:
            pp = np.array(self.data)
            ax.plot(pp,np.zeros_like(pp), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
        elif self.dim == 2:
            pp = np.array(self.data)
            ax.plot(pp[:,0],pp[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
        elif self.dim == 3:
            pp = np.array(self.data)
            ax.scatter(pp[:,0], pp[:,1], pp[:,2], marker=options['marker'], color=options['color'])
        else:
            pass


class ActionSpace:
    """Contains the parameters and data of an action space."""
    gamma = 1.2  # The gamma parameter used to compute perfomances
    def __init__(self, bounds, n, name, options=None):
        """
        bounds dict: dictionary containing the boundaries of the primitive action space (keys 'min' and 'max)
        n int: the number of primitives of each action of this space
        name string: the name of the action space
        options dict: a dictionary containing different parameters of the action space
        """
        self.data = []
        self.ids = []  # execution id (order)
        self.n = n  # 0 means procedure, 1 means primitive
        if n > 0:
            boundsMin = copy.deepcopy(bounds['min'].tolist()) * n
            boundsMax = copy.deepcopy(bounds['max'].tolist()) * n
            self.bounds = {'min': np.array(boundsMin), 'max': np.array(boundsMax)}
        else:
            self.bounds = copy.deepcopy(bounds)
        self.dim = len(self.bounds['min'])
        self.name = name
        # Different keys of options
        ## max_dist: used to normalize distances in NN-searches
        ## out_dist: used as a default distance for out of bounds points
        ## dist: max distance to look for neighbours in best locality
        ## err: distance corresponding to the transition between random and optimization by some strategies
        ## range: parameter used to determine the steepness of the transition between random and optimization
        ## nn_best_locality: the number of NN-neighbours to look for in best locality computations
        ## cost: cost of executing this action (used in performances computation)
        if options:
            self.options = options
        else:
            self.options = {}
        self.set_options()
    
    def to_config(self):
        dico = {}
        dico['name'] = self.name
        dico['bounds'] = {'min': self.bounds['min'].tolist(), 'max': self.bounds['max'].tolist()}
        dico['options'] = self.options
        dico['gamma'] = self.gamma
        return dico
    
    @classmethod
    def from_config(cls, dico, a_length=1):
        bounds = {'min': np.array(dico['bounds']['min']), 'max': np.array(dico['bounds']['max'])}
        ActionSpace.gamma = dico['gamma']
        return ActionSpace(bounds, a_length, dico['name'], dico['options'])
    
    @classmethod
    def clone_without_content(cls, space):
        """Function that will copy an other action space except without any data."""
        result = ActionSpace(copy.deepcopy(space.bounds), space.n, copy.deepcopy(space.name), \
                copy.deepcopy(space.options))
        result.bounds = copy.deepcopy(space.bounds)
        result.dim = len(result.bounds['min'])
        return result
    
    def set_options(self):
        if not self.options.has_key("max_dist"):
            self.options['max_dist'] = distance(self.bounds['min'], self.bounds['max'])
        if not self.options.has_key("out_dist"):
            self.options['out_dist'] = 5.0 * self.options['max_dist']
        if not self.options.has_key('dist'):
            self.options['dist'] = (np.log(self.dim) + 1)*0.05
        if not self.options.has_key('err'):
            self.options['err'] = (np.log(self.dim) + 1)*0.05
        if not self.options.has_key('range'):
            self.options['range'] = self.options['err'] / 2.0
        if not self.options.has_key('nn_best_locality'):
            self.options['nn_best_locality'] = self.dim
        self.options['cost'] = ActionSpace.get_cost(self.n)
    
    def valid(self, x):
        """Check if a point has good dimensions and is within bounds."""
        if len(x) != self.dim:
            return False
        for i in range(self.dim):
            if x[i] < self.bounds['min'][i] and x[i] > self.bounds['max'][i]:
                return False
        return True
    
    def add_point(self, x, idx):
        """Add a point in the dataset and if valid return id."""
        if self.valid(x):
            self.data.append(x)
            self.ids.append(idx)
            return len(self.data)-1
        return -1
    
    def compute_distances(self, x):
        """Compute array of normalized distances between data and the point given."""
        if len(self.data) == 0:
            return np.array([])
        data = np.array(self.data)
        return np.sum((data - x)**2, axis=1)/self.options['max_dist']
    
    def nn(self, x, n=1):
        """Computes Nearest Neighbours based on distances."""
        perf = self.compute_distances(x)
        i = range(len(perf))
        i.sort(key=lambda j: perf[j])
        i = i[0:min(len(perf), n)]
        return i, perf[i]
    
    @classmethod
    def get_cost(cls, n):
        """Return cost of an action space based on its number of primitives."""
        return cls.gamma ** n


class ProcedureSpace(ActionSpace):
    """Contains the parameters and data of a procedural space."""
    def __init__(self, bounds, name, y_spaces, options=None):
        """
        bounds dict: dictionnary containing the boundaries of the primitive action space (keys 'min' and 'max)
        name string: the name of the action space
        y_spaces tuple of OutcomeSpace: pair of outcome spaces describing the procedural space
        options dict: a dictionnary containing different parameters of the action space
        """
        self.y_spaces = y_spaces  # (y_space1, y_space2)
        ActionSpace.__init__(self, bounds, 0, name, options)
    
    def nn_procedure(self, p):
        """Computes nearest neighbours of each subpart of the procedure."""
        y1 = p[0:self.y_spaces[0].dim]
        y2 = p[self.y_spaces[0].dim:len(p)]
        
        ids1,_ = self.y_spaces[0].nn(y1)
        ids2,_ = self.y_spaces[1].nn(y2)
        
        if len(ids1) == 0 or len(ids2) == 0:
            return []
        
        return (ids1[0], ids2[0])


class DatasetV2:
    """The dataset used to record actions, outcomes and procedures."""
    def __init__(self, a_spaces, y_spaces, options=None):
        """
        a_spaces list of ActionSpace: primitive action spaces available
        y_spaces list of OutcomeSpace: outcome spaces
        options dict: parameters for the dataset
        """
        self.y_spaces = y_spaces
        self.a_spaces = []
        for a in a_spaces: # Adding primitive motor spaces
            self.a_spaces.append([a])
        # Add procedure spaces
        self.p_spaces = []
        for i, y1 in enumerate(self.y_spaces):
            bmin = copy.deepcopy(y1.bounds['min'].tolist())
            bmax = copy.deepcopy(y1.bounds['max'].tolist())
            self.p_spaces.append([])
            for j, y2 in enumerate(self.y_spaces):
                b = {'min': np.array(bmin + copy.deepcopy(y2.bounds['min'].tolist())), \
                        'max': np.array(bmax + copy.deepcopy(y2.bounds['max'].tolist()))}
                name = "T" + str(i) + "+T" + str(j)
                self.p_spaces[-1].append(ProcedureSpace(b, name, (y1, y2)))
        self.idA = []  # list of (a_type, idA)
        self.idP = []  # list of (p_type, idP) or None
        self.idY = []  # list of list of (y_type, idY)
        self.idEP = []  # list of id when a procedure was used
        # Contains the following keys
        ## min_y_best_locality: minimum of outcomes to keep in best locality searches
        ## min_a_best_locality: minimum of actions to keep in best locality searches
        if options:
            self.options = options
        else:
            self.options = {}
        self.set_options()
    
    @classmethod
    def from_config(cls, dico, options):
        a_spaces = []
        y_spaces = []
        for a in dico['a_spaces']:
            a_spaces.append(ActionSpace.from_config(a))
        for y in dico['y_spaces']:
            y_spaces.append(OutcomeSpace.from_config(y))
        return DatasetV2(a_spaces, y_spaces, options)
    
    @classmethod
    def clone_without_content(cls, dataset):
        """Function that will copy an other dataset except without any data."""
        result = DatasetV2([], [], copy.deepcopy(dataset.options))
        for a_s in dataset.a_spaces:
            result.a_spaces.append([])
            for a in a_s:
                result.a_spaces[-1].append(ActionSpace.clone_without_content(a))
        for y in dataset.y_spaces:
            result.y_spaces.append(OutcomeSpace.clone_without_content(y))
        return result
    
    def set_options(self):
        if not self.options.has_key('min_y_best_locality'):
            self.options['min_y_best_locality'] = 5
        if not self.options.has_key('min_a_best_locality'):
            self.options['min_a_best_locality'] = 4
    
    def create_a_space(self, a_type):
        """When a new length of actions is encountered, creates all action spaces recquired."""
        while a_type[1] >= len(self.a_spaces[a_type[0]]):
            self.a_spaces[a_type[0]].append(ActionSpace(self.a_spaces[a_type[0]][0].bounds, \
                len(self.a_spaces[a_type[0]])+1, self.a_spaces[a_type[0]][0].name))
    
    def add_entity(self, a, a_type, y_list, y_types, cost, p=None, p_type=None):
        """Add data to the dataset"""
        """
        a array of float: action executed
        a_type tuple of int: indicates the action space (i, j)
        y_list list of arrays of float: indicates the outcomes reached by the action
        y_types list of int: indicates which outcome spaces were reached by this action (same order than y_list)
        cost float: cost of the action used
        p array of float: procedure executed (if a procedure was indeed executed)
        p_type tuple of int: indicates which procedural space the procedure p is in
        """
        assert a_type[0] < len(self.a_spaces)
        idE = len(self.idA) # used to identify the execution order
        idA = self.a_spaces[a_type[0]][a_type[1]].add_point(a, idE)
        self.idA.append((a_type, idA))
        if p_type:
            idP = self.p_spaces[p_type[0]][p_type[1]].add_point(p, idE)
            self.idP.append((p_type, idP))
            self.idEP.append(idE)
        else:
            self.idP.append(None)
        newIds = []
        for i in range(len(y_list)):
            assert y_types[i] < len(self.y_spaces)
            idY = self.y_spaces[y_types[i]].add_point(y_list[i], idE, cost)
            newIds.append((y_types[i], idY))
        self.idY.append(newIds)
    
    def competence(self, y, y_type):
        """Compute competence to reach a specific outcome as the distance to nearest neighbour."""
        _,dist = self.y_spaces[y_type].nn_d(y, n=1)
        if len(dist) == 0:
            return self.y_spaces[y_type].options['out_dist']/self.y_spaces[y_type].options['max_dist']
        return dist[0]
    
    def nn_y(self, y, y_type, n=1):
        """Compute nearest neighbours of an outcome and return ids and performances."""
        return self.y_spaces[y_type].nn(y, n=n)
    
    def nn_a(self, a, a_type, n=1):
        """Compute nearest neighbours of an action and return ids and distances."""
        return self.a_spaces[a_type[0]][a_type[1]].nn(a, n=n)
    
    def nn_p(self, p, p_type, n=1):
        """Compute nearest neighbours of a procedure and return ids and distances."""
        return self.p_spaces[p_type[0]][p_type[1]].nn(p, n=n)
    
    def nn_y_procedural(self, y, task, n=1):
        """Compute nearest neghbours of an outcome among outcomes reached by a procedure only."""
        # get outcome ids of the outcomes reached by a procedure
        ids = np.in1d(self.y_spaces[task].ids, self.idEP)
        all_ids = np.arange(len(self.y_spaces[task].ids))
        ids = all_ids[ids]
        
        if len(ids) == 0:
            return [], []
        
        data = np.array(self.y_spaces[task].data)[ids]
        dist = np.sum((data - y)**2, axis=1)/self.y_spaces[task].options['max_dist']
        perf = dist * np.array(self.y_spaces[task].costs)[ids]
        i = range(len(perf))
        i.sort(key=lambda j: perf[j])
        i = i[0:min(len(perf), n)]
        return ids[i], perf[i]
    
    def refine_procedure(self, p, p_type):
        """Refine a procedure to return the ids of the nearest outcomes reached for both subparts of the procedure."""
        return self.p_spaces[p_type[0]][p_type[1]].nn_procedure(p)
    
    def y_id(self, idE, y_type):
        """Return outcome id reached at execution id given in outcome space specified."""
        for couple in self.idY[idE]:
            if couple[0] == y_type:
                return couple[1]
        return -1
    
    def best_locality(self, yg, y_type):
        """Compute most stable local action-outcome model around goal outcome."""
        # Compute best locality candidates
        idx, dist = self.nn_y(yg, y_type, n=self.y_spaces[y_type].options['nn_best_locality'])
        
        for i in range(len(idx)):
            # Check if distance to goal is to big and enough neighbours studied already
            if dist[i] > self.y_spaces[y_type].options['dist'] and i > self.options['min_y_best_locality']: # 5 in matlab code
                break
            
            idE = self.y_spaces[y_type].ids[idx[i]]
            a_type = self.idA[idE][0]
            idA = self.idA[idE][1]
            a = self.a_spaces[a_type[0]][a_type[1]].data[idA]
            
            idsA, distA = self.nn_a(a, a_type, n=self.a_spaces[a_type[0]][a_type[1]].options['nn_best_locality'])
            
            y = []
            all_a = []
            
            for j in range(len(idsA)):
                idEp = self.a_spaces[a_type[0]][a_type[1]].ids[idsA[j]]
                # Check if distance to a is to big and enough neighbours studied already
                if distA[j] > self.a_spaces[a_type[0]][a_type[1]].options['dist'] \
                        and j > self.options['min_a_best_locality']: # 4 in matlab code                  
                    idsA = idsA[0:j]
                    break
                else:
                    idY = self.y_id(idEp, y_type)
                    if idY >= 0:
                        # outcome space reached by this action
                        y.append(self.y_spaces[y_type].data[idY])
                    else:
                        dummy = self.y_spaces[y_type].bounds['max'] * 1000.0
                        y.append(dummy)
                    all_a.append(self.a_spaces[a_type[0]][a_type[1]].data[idsA[j]])
            
            y = np.array(y)
            all_a = np.array(all_a)
            dd = 0.         
            # Create Multivariate Regression Model to compute mean model error
            for j in range(len(idsA)):
                # Compute regression model containing all but current action
                # Then compute action error for using model at y(current action)
                a_gen = multivariate_regression(y[np.arange(len(y)) != j,:], all_a[np.arange(len(all_a)) != j,:], y[j,:])
                dd += distance(a_gen, all_a[j,:]) / self.a_spaces[a_type[0]][a_type[1]].options['max_dist']
                
            dd /= float(max(1, len(idsA)))
            
            if i == 0 or dd < minDist:
                minDist = dd
                best_y = y
                best_a = all_a
                best_a_type = a_type
        
        return best_y, best_a, best_a_type
    
    def best_locality_procedures(self, yg, y_type):
        """Compute most stable local procedure-outcome model around goal outcome."""
        # Compute best locality candidates
        idx, dist = self.nn_y_procedural(yg, y_type, n=self.y_spaces[y_type].options['nn_best_locality'])
        
        best_y = []
        best_p = []
        best_p_type = []
        
        for i in range(len(idx)):
            # Check if distance to goal is to big and enough neighbours studied already
            if dist[i] > self.y_spaces[y_type].options['dist'] and i > self.options['min_y_best_locality']: # 5 in matlab code
                break
            
            idE = self.y_spaces[y_type].ids[idx[i]]
            pro_ep = self.idP[idE]
            
            p_type = pro_ep[0]
            idP = pro_ep[1]
            
            p = self.p_spaces[p_type[0]][p_type[1]].data[idP]
            
            idsP, distP = self.nn_p(p, p_type, n=self.p_spaces[p_type[0]][p_type[1]].options['nn_best_locality'])
            
            y = []
            all_p = []
            
            for j in range(len(idsP)):
                idEp = self.p_spaces[p_type[0]][p_type[1]].ids[idsP[j]]
                # Check if distance to a is to big and enough neighbours studied already
                if distP[j] > self.p_spaces[p_type[0]][p_type[1]].options['dist'] \
                        and j > self.options['min_a_best_locality']: # 4 in matlab code                  
                    idsP = idsP[0:j]
                    break
                else:
                    idY = self.y_id(idEp, y_type)
                    if idY >= 0:
                        # outcome space reached by this action
                        y.append(self.y_spaces[y_type].data[idY])
                    else:
                        dummy = self.y_spaces[y_type].bounds['max'] * 1000.0
                        y.append(dummy)
                    all_p.append(self.p_spaces[p_type[0]][p_type[1]].data[idsP[j]])
            
            y = np.array(y)
            all_p = np.array(all_p)
            dd = 0.         
            # Create Multivariate Regression Model to compute mean model error
            for j in range(len(idsP)):
                # Compute regression model containing all but current action
                # Then compute action error for using model at y(current action)
                p_gen = multivariate_regression(y[np.arange(len(y)) != j,:], all_p[np.arange(len(all_p)) != j,:], y[j,:])
                dd += distance(p_gen, all_p[j,:]) / self.p_spaces[p_type[0]][p_type[1]].options['max_dist']
                
            dd /= float(max(1, len(idsP)))
            
            if i == 0 or dd < minDist:
                minDist = dd
                best_y = y
                best_p = all_p
                best_p_type = p_type
        
        return best_y, best_p, best_p_type
    
    def best_locality_complete(self, yg, y_type):
        """Compute most stable local action/procedure-outcome model around goal outcome."""
        # Compute best locality candidates
        idx, dist = self.nn_y(yg, y_type, n=self.y_spaces[y_type].options['nn_best_locality'])
        
        for i in range(len(idx)):
            # Check if distance to goal is to big and enough neighbours studied already
            if dist[i] > self.y_spaces[y_type].options['dist'] and i > self.options['min_y_best_locality']: # 5 in matlab code
                break
            
            idE = self.y_spaces[y_type].ids[idx[i]]
            
            # Check whether a procedure was made or not
            idPep = self.idP[idE]
            if idPep:
                # A procedure was built
                p_type = idPep[0]
                idP = idPep[1]
                
                p = self.p_spaces[p_type[0]][p_type[1]].data[idP]
                
                idsP, distP = self.nn_p(p, p_type, n=self.p_spaces[p_type[0]][p_type[1]].options['nn_best_locality'])
                
                y = []
                all_p = []
                
                for j in range(len(idsP)):
                    idEp = self.p_spaces[p_type[0]][p_type[1]].ids[idsP[j]]
                    # Check if distance to a is to big and enough neighbours studied already
                    if distP[j] > self.p_spaces[p_type[0]][p_type[1]].options['dist'] \
                            and j > self.options['min_a_best_locality']: # 4 in matlab code                  
                        idsP = idsP[0:j]
                        break
                    else:
                        idY = self.y_id(idEp, y_type)
                        if idY >= 0:
                            # outcome space reached by this action
                            y.append(self.y_spaces[y_type].data[idY])
                        else:
                            dummy = self.y_spaces[y_type].bounds['max'] * 1000.0
                            y.append(dummy)
                        all_p.append(self.p_spaces[p_type[0]][p_type[1]].data[idsP[j]])
                
                y = np.array(y)
                all_p = np.array(all_p)
                dd = 0.         
                # Create Multivariate Regression Model to compute mean model error
                for j in range(len(idsP)):
                    # Compute regression model containing all but current action
                    # Then compute action error for using model at y(current action)
                    p_gen = multivariate_regression(y[np.arange(len(y)) != j,:], all_p[np.arange(len(all_p)) != j,:], y[j,:])
                    dd += distance(p_gen, all_p[j,:]) / self.p_spaces[p_type[0]][p_type[1]].options['max_dist']
                    
                dd /= float(max(1, len(idsP)))
                
                if i == 0 or dd < minDist:
                    minDist = dd
                    best_y = y
                    best_a = all_p
                    best_a_type = p_type
                    proc_done = True
            
            a_type = self.idA[idE][0]
            idA = self.idA[idE][1]
            a = self.a_spaces[a_type[0]][a_type[1]].data[idA]
            
            idsA, distA = self.nn_a(a, a_type, n=self.a_spaces[a_type[0]][a_type[1]].options['nn_best_locality'])
            
            y = []
            all_a = []
            
            for j in range(len(idsA)):
                idEp = self.a_spaces[a_type[0]][a_type[1]].ids[idsA[j]]
                # Check if distance to a is to big and enough neighbours studied already
                if distA[j] > self.a_spaces[a_type[0]][a_type[1]].options['dist'] \
                        and j > self.options['min_a_best_locality']: # 4 in matlab code                  
                    idsA = idsA[0:j]
                    break
                else:
                    idY = self.y_id(idEp, y_type)
                    if idY >= 0:
                        # outcome space reached by this action
                        y.append(self.y_spaces[y_type].data[idY])
                    else:
                        dummy = self.y_spaces[y_type].bounds['max'] * 1000.0
                        y.append(dummy)
                    all_a.append(self.a_spaces[a_type[0]][a_type[1]].data[idsA[j]])
            
            y = np.array(y)
            all_a = np.array(all_a)
            dd = 0.         
            # Create Multivariate Regression Model to compute mean model error
            for j in range(len(idsA)):
                # Compute regression model containing all but current action
                # Then compute action error for using model at y(current action)
                a_gen = multivariate_regression(y[np.arange(len(y)) != j,:], all_a[np.arange(len(all_a)) != j,:], y[j,:])
                dd += distance(a_gen, all_a[j,:]) / self.a_spaces[a_type[0]][a_type[1]].options['max_dist']
                
            dd /= float(max(1, len(idsA)))
            
            if i == 0 or dd < minDist:
                minDist = dd
                best_y = y
                best_a = all_a
                best_a_type = a_type
                proc_done = False
        
        return best_y, best_a, best_a_type, proc_done
    
    def plot_actions_hist(self, ax, options):
        """Plot number of actions tried by action space."""
        width = 0.5
        i = 0
        x = []
        ticks = []
        y = []
        for a in self.a_spaces:
            for a_s in a:
                x.append(i)
                y.append(len(a_s.data))
                ticks.append(a_s.name + str(a_s.n))
                i += 1
        ax.bar(x, y, width, color=options['color'])
        ax.set_xticks(np.array(x) + width/2.0)
        ax.set_xticklabels(ticks)
    
    def plot_procedures_hist(self, ax, options):
        """Plot number of procedures tried by action space."""
        width = 0.5
        i = 0
        x = []
        ticks = []
        y = []
        for p in self.p_spaces:
            for p_s in p:
                x.append(i)
                y.append(len(p_s.data))
                ticks.append(p_s.name)
                i += 1
        ax.bar(x, y, width, color=options['color'])
        ax.set_xticks(np.array(x) + width/2.0)
        ax.set_xticklabels(ticks)
    
    def plot_actions_hist_task(self, task, ax, options):
        """Plot number of actions tried by action space that reached given outcome space."""
        nb_a = []
        width = 0.5
        i = 0
        x = []
        ticks = []
        for a in self.a_spaces:
            nb_a.append([])
            for a_s in a:
                nb_a[-1].append(0)
                x.append(i)
                ticks.append(a_s.name + str(a_s.n))
                i += 1
        idE = self.y_spaces[task].ids
        for i in idE:
            a_type = self.idA[i][0]
            nb_a[a_type[0]][a_type[1]] += 1
        y = []
        for nn in nb_a:
            for n in nn:
                y.append(n)
        ax.bar(x, y, width, color=options['color'])
        ax.set_xticks(np.array(x) + width/2.0)
        ax.set_xticklabels(ticks)
    
    def plot_procedures_hist_task(self, task, ax, options):
        """Plot number of procedures tried by action space that reached given outcome space."""
        nb_p = []
        width = 0.5
        i = 0
        x = []
        ticks = []
        for p in self.p_spaces:
            nb_p.append([])
            for p_s in p:
                nb_p[-1].append(0)
                x.append(i)
                ticks.append(p_s.name)
                i += 1
        idE = self.y_spaces[task].ids
        for i in idE:
            if not self.idP[i]:
                continue
            p_type = self.idP[i][0]
            nb_p[p_type[0]][p_type[1]] += 1
        y = []
        for nn in nb_p:
            for n in nn:
                y.append(n)
        ax.bar(x, y, width, color=options['color'])
        ax.set_xticks(np.array(x) + width/2.0)
        ax.set_xticklabels(ticks)
    
    def plot_outcomes(self, task, ax, options):
        """Plot outcomes in given outcome space;"""
        self.y_spaces[task].plot_outcomes(ax, options)
    
    def plot_outcomes_atype(self, task, a_type, ax, options):
        """Plot outcomes reached by the given action space on the given outcome space."""
        if self.y_spaces[task].dim == 1:
            pp = np.array(self.y_spaces[task].data)
            idY = np.array(self.y_spaces[task].ids)
            idA = np.array(self.a_spaces[a_type[0]][a_type[1]].ids)
            coms = np.in1d(idY, idA)
            if len(pp) > 0:
                pp = pp[coms,:]
                ax.plot(pp,np.zeros_like(pp), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])                
        elif self.y_spaces[task].dim == 2:
            pp = np.array(self.y_spaces[task].data)
            idY = np.array(self.y_spaces[task].ids)
            idA = np.array(self.a_spaces[a_type[0]][a_type[1]].ids)
            coms = np.in1d(idY, idA)
            if len(pp) > 0:
                pp = pp[coms,:]
                ax.plot(pp[:,0],pp[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])                
        elif self.y_spaces[task].dim == 3:
            pp = np.array(self.y_spaces[task].data)
            idY = np.array(self.y_spaces[task].ids)
            idA = np.array(self.a_spaces[a_type[0]][a_type[1]].ids)
            coms = np.in1d(idY, idA)
            if len(pp) > 0:
                pp = pp[coms,:]
                ax.scatter(pp[:,0], pp[:,1], pp[:,2], marker=options['marker'], color=options['color'])  #marker= '.', color='b'                
        else:
            pass
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_reached_one_atype_visualizer(self, task, a_type, prefix=""):
        """Return a dictionary used to visualize outcomes reached by one action space."""
        dico = {}
        dico['limits'] = {'min': self.y_spaces[task].bounds['min'], \
            'max': self.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Points reached task " + str(task) + " with " + self.a_spaces[a_type[0]][a_type[1]].name + \
                str(self.a_spaces[a_type[0]][a_type[1]].n)
        dico['color'] = ['k']
        dico['marker'] = ['.']
        dico['linestyle'] = ['None']
        dico['legend'] = [self.a_spaces[a_type[0]][a_type[1]].name]
        dico['plots'] = [lambda ax, options: self.plot_outcomes_atype(task, a_type, ax, options)]
        
        return dico
    
    def get_reached_atype_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize outcomes reached for each action space used."""
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        n_a_spaces = 0
        n = 0
        for i in range(len(self.a_spaces)):
            n_a_spaces += len(self.a_spaces[i])
        for i in range(len(self.a_spaces)):
            for j in range(len(self.a_spaces[i])):
                if n_a_spaces == 1:
                    colors.append('k')
                else:
                    colors.append(cmap(int(math.floor(float(n)*float(cmap.N - 1)/float(n_a_spaces-1)))))
                markers.append('.')
                lines.append('None')
                legends.append(self.a_spaces[i][j].name + str(self.a_spaces[i][j].n))
                plots.append(lambda ax, options, i=i, j=j: self.plot_outcomes_atype(task, (i, j), ax, options))
                n += 1
        dico = {}
        dico['limits'] = {'min': self.y_spaces[task].bounds['min'], \
            'max': self.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Points reached task " + str(task)
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    
    def get_reached_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize outcomes reached for the specified outcome space."""
        dico = {}
        dico['limits'] = {'min': self.y_spaces[task].bounds['min'], \
            'max': self.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Points reached task " + str(task)
        dico['color'] = ['k']
        dico['marker'] = ['.']
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_outcomes(task, ax, options)]
        
        return dico
    
    def get_actionspaces_visualizer(self, prefix=""):
        """Return a dictionary used to visualize the number of actions tried per action space."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = "Action spaces distribution " + prefix
        dico['color'] = ['b']
        dico['marker'] = [None]
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_actions_hist(ax, options)]
        
        return dico
    
    def get_actionspaces_task_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the number of actions tried per action space for given outcome space."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = "Action spaces distribution for task " + str(task) + " for " + prefix
        dico['color'] = ['b']
        dico['marker'] = [None]
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_actions_hist_task(ax, task, options)]
        
        return dico
    
    def get_proceduralspaces_visualizer(self, prefix=""):
        """Return a dictionary used to visualize the number of procedures tried per procedure space."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = "Procedural spaces distribution " + prefix
        dico['color'] = ['b']
        dico['marker'] = [None]
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_procedures_hist(ax, options)]
        
        return dico
    
    def get_proceduralspaces_task_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the number of procedures tried per procedure space for given outcome space."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = "Procedural spaces distribution for task " + str(task) + " for " + prefix
        dico['color'] = ['b']
        dico['marker'] = [None]
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_procedures_hist_task(ax, task, options)]
        
        return dico


class SimpleDataset:
    """The dataset used to record simple actions, outcomes."""
    def __init__(self, a_spaces, y_spaces, options=None):
        """
        a_spaces list of ActionSpace: action spaces available
        y_spaces list of OutcomeSpace: outcome spaces
        options dict: parameters for the dataset
        """
        self.y_spaces = y_spaces
        self.a_spaces = a_spaces
        self.idA = []  # list of (a_type, idA)
        self.idY = []  # list of list of (y_type, idY)
        # Contains the following keys
        ## min_y_best_locality: minimum of outcomes to keep in best locality searches
        ## min_a_best_locality: minimum of actions to keep in best locality searches
        if options:
            self.options = options
        else:
            self.options = {}
        self.set_options()
    
    @classmethod
    def from_config(cls, dico, options, action_length):
        a_spaces = []
        y_spaces = []
        for a in dico['a_spaces']:
            a_spaces.append([])
            for i in range(action_length):
                a_spaces[-1].append(ActionSpace.from_config(a, i+1))
        for y in dico['y_spaces']:
            y_spaces.append(OutcomeSpace.from_config(y))
        return SimpleDataset(a_spaces, y_spaces, options)
    
    @classmethod
    def clone_without_content(cls, dataset):
        """Function that will copy an other dataset except without any data."""
        result = DatasetV2([], [], copy.deepcopy(dataset.options))
        for a_s in dataset.a_spaces:
            result.a_spaces.append([])
            for a in a_s:
                result.a_spaces[-1].append(ActionSpace.clone_without_content(a))
        for y in dataset.y_spaces:
            result.y_spaces.append(OutcomeSpace.clone_without_content(y))
        return result
    
    def set_options(self):
        if not self.options.has_key('min_y_best_locality'):
            self.options['min_y_best_locality'] = 5
        if not self.options.has_key('min_a_best_locality'):
            self.options['min_a_best_locality'] = 4
    
    def add_entity(self, a, a_type, y_list, y_types, cost=1.0):
        """Add data to the dataset"""
        """
        a array of float: action executed
        a_type tuple of int: indicates the action space (i, j)
        y_list list of arrays of float: indicates the outcomes reached by the action
        y_types list of int: indicates which outcome spaces were reached by this action (same order than y_list)
        """
        assert a_type[0] < len(self.a_spaces)
        idE = len(self.idA) # used to identify the execution order
        idA = self.a_spaces[a_type[0]][a_type[1]].add_point(a, idE)
        self.idA.append((a_type, idA))
        newIds = []
        for i in range(len(y_list)):
            assert y_types[i] < len(self.y_spaces)
            idY = self.y_spaces[y_types[i]].add_point(y_list[i], idE, cost)
            newIds.append((y_types[i], idY))
        self.idY.append(newIds)
    
    def competence(self, y, y_type):
        """Compute competence to reach a specific outcome as the distance to nearest neighbour."""
        _,dist = self.nn_y(y, y_type, 1)
        if len(dist) == 0:
            return self.y_spaces[y_type].options['out_dist']/self.y_spaces[y_type].options['max_dist']
        return dist[0]
    
    def nn_y(self, y, y_type, n=1):
        """Compute nearest neighbours of an outcome and return ids and performances."""
        return self.y_spaces[y_type].nn(y, n=n)
    
    def nn_a(self, a, a_type, n=1):
        """Compute nearest neighbours of an action and return ids and distances."""
        return self.a_spaces[a_type[0]][a_type[1]].nn(a, n=n)
    
    def y_id(self, idE, y_type):
        """Return outcome id reached at execution id given in outcome space specified."""
        for couple in self.idY[idE]:
            if couple[0] == y_type:
                return couple[1]
        return -1
    
    def best_locality(self, yg, y_type):
        """Compute most stable local action-outcome model around goal outcome."""
        # Compute best locality candidates
        idx, dist = self.nn_y(yg, y_type, n=self.y_spaces[y_type].options['nn_best_locality'])
        
        for i in range(len(idx)):
            # Check if distance to goal is to big and enough neighbours studied already
            if dist[i] > self.y_spaces[y_type].options['dist'] and i > self.options['min_y_best_locality']: # 5 in matlab code
                break
            
            idE = self.y_spaces[y_type].ids[idx[i]]
            a_type = self.idA[idE][0]
            idA = self.idA[idE][1]
            a = self.a_spaces[a_type[0]][a_type[1]].data[idA]
            
            idsA, distA = self.nn_a(a, a_type, n=self.a_spaces[a_type[0]][a_type[1]].options['nn_best_locality'])
            
            y = []
            all_a = []
            
            for j in range(len(idsA)):
                idEp = self.a_spaces[a_type[0]][a_type[1]].ids[idsA[j]]
                # Check if distance to a is to big and enough neighbours studied already
                if distA[j] > self.a_spaces[a_type[0]][a_type[1]].options['dist'] \
                        and j > self.options['min_a_best_locality']: # 4 in matlab code                  
                    idsA = idsA[0:j]
                    break
                else:
                    idY = self.y_id(idEp, y_type)
                    if idY >= 0:
                        # outcome space reached by this action
                        y.append(self.y_spaces[y_type].data[idY])
                    else:
                        dummy = self.y_spaces[y_type].bounds['max'] * 1000.0
                        y.append(dummy)
                    all_a.append(self.a_spaces[a_type[0]][a_type[1]].data[idsA[j]])
            
            y = np.array(y)
            all_a = np.array(all_a)
            dd = 0.         
            # Create Multivariate Regression Model to compute mean model error
            for j in range(len(idsA)):
                # Compute regression model containing all but current action
                # Then compute action error for using model at y(current action)
                a_gen = multivariate_regression(y[np.arange(len(y)) != j,:], all_a[np.arange(len(all_a)) != j,:], y[j,:])
                dd += distance(a_gen, all_a[j,:]) / self.a_spaces[a_type[0]][a_type[1]].options['max_dist']
                
            dd /= float(max(1, len(idsA)))
            
            if i == 0 or dd < minDist:
                minDist = dd
                best_y = y
                best_a = all_a
                best_a_type = a_type
        
        return best_y, best_a, best_a_type
    
    def plot_actions_hist(self, ax, options):
        """Plot number of actions tried by action space."""
        width = 0.5
        i = 0
        x = []
        ticks = []
        y = []
        for a in self.a_spaces:
            for a_s in a:
                x.append(i)
                y.append(len(a_s.data))
                ticks.append(a_s.name + str(a_s.n))
                i += 1
        ax.bar(x, y, width, color=options['color'])
        ax.set_xticks(np.array(x) + width/2.0)
        ax.set_xticklabels(ticks)
    
    def plot_actions_hist_task(self, task, ax, options):
        """Plot number of actions tried by action space that reached given outcome space."""
        nb_a = []
        width = 0.5
        i = 0
        x = []
        ticks = []
        for a in self.a_spaces:
            nb_a.append([])
            for a_s in a:
                nb_a[-1].append(0)
                x.append(i)
                ticks.append(a_s.name + str(a_s.n))
                i += 1
        idE = self.y_spaces[task].ids
        for i in idE:
            a_type = self.idA[i][0]
            nb_a[a_type[0]][a_type[1]] += 1
        y = []
        for nn in nb_a:
            for n in nn:
                y.append(n)
        ax.bar(x, y, width, color=options['color'])
        ax.set_xticks(np.array(x) + width/2.0)
        ax.set_xticklabels(ticks)
    
    def plot_outcomes(self, task, ax, options):
        """Plot outcomes in given outcome space;"""
        self.y_spaces[task].plot_outcomes(ax, options)
    
    def plot_outcomes_atype(self, task, a_type, ax, options):
        """Plot outcomes reached by the given action space on the given outcome space."""
        if self.y_spaces[task].dim == 1:
            pp = np.array(self.y_spaces[task].data)
            idY = np.array(self.y_spaces[task].ids)
            idA = np.array(self.a_spaces[a_type[0]][a_type[1]].ids)
            coms = np.in1d(idY, idA)
            if len(pp) > 0:
                pp = pp[coms,:]
                ax.plot(pp,np.zeros_like(pp), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])                
        elif self.y_spaces[task].dim == 2:
            pp = np.array(self.y_spaces[task].data)
            idY = np.array(self.y_spaces[task].ids)
            idA = np.array(self.a_spaces[a_type[0]][a_type[1]].ids)
            coms = np.in1d(idY, idA)
            if len(pp) > 0:
                pp = pp[coms,:]
                ax.plot(pp[:,0],pp[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])                
        elif self.y_spaces[task].dim == 3:
            pp = np.array(self.y_spaces[task].data)
            idY = np.array(self.y_spaces[task].ids)
            idA = np.array(self.a_spaces[a_type[0]][a_type[1]].ids)
            coms = np.in1d(idY, idA)
            if len(pp) > 0:
                pp = pp[coms,:]
                ax.scatter(pp[:,0], pp[:,1], pp[:,2], marker=options['marker'], color=options['color'])  #marker= '.', color='b'                
        else:
            pass
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_reached_one_atype_visualizer(self, task, a_type, prefix=""):
        """Return a dictionary used to visualize outcomes reached by one action space."""
        dico = {}
        dico['limits'] = {'min': self.y_spaces[task].bounds['min'], \
            'max': self.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Points reached task " + str(task) + " with " + self.a_spaces[a_type[0]][a_type[1]].name + \
                str(self.a_spaces[a_type[0]][a_type[1]].n)
        dico['color'] = ['k']
        dico['marker'] = ['.']
        dico['linestyle'] = ['None']
        dico['legend'] = [self.a_spaces[a_type[0]][a_type[1]].name]
        dico['plots'] = [lambda ax, options, i=i: self.plot_outcomes_atype(task, a_type, ax, options)]
        
        return dico
    
    def get_reached_atype_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize outcomes reached for each action space used."""
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        n_a_spaces = 0
        n = 0
        for i in range(len(self.a_spaces)):
            n_a_spaces += len(self.a_spaces[i])
        for i in range(len(self.a_spaces)):
            for j in range(len(self.a_spaces[i])):
                if n_a_spaces == 1:
                    colors.append('k')
                else:
                    colors.append(cmap(int(math.floor(float(n)*float(cmap.N - 1)/float(n_a_spaces-1)))))
                markers.append('.')
                lines.append('None')
                legends.append(self.a_spaces[i][j].name + str(self.a_spaces[i][j].n))
                plots.append(lambda ax, options, i=i, j=j: self.plot_outcomes_atype(task, (i, j), ax, options))
                n += 1
        dico = {}
        dico['limits'] = {'min': self.y_spaces[task].bounds['min'], \
            'max': self.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Points reached task " + str(task)
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    
    def get_reached_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize outcomes reached for the specified outcome space."""
        dico = {}
        dico['limits'] = {'min': self.y_spaces[task].bounds['min'], \
            'max': self.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Points reached task " + str(task)
        dico['color'] = ['k']
        dico['marker'] = ['.']
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_outcomes(task, ax, options)]
        
        return dico
    
    def get_actionspaces_visualizer(self, prefix=""):
        """Return a dictionary used to visualize the number of actions tried per action space."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = "Action spaces distribution " + prefix
        dico['color'] = ['b']
        dico['marker'] = [None]
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_actions_hist(ax, options)]
        
        return dico
    
    def get_actionspaces_task_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the number of actions tried per action space for given outcome space."""
        dico = {}
        dico['limits'] = {'min': [None, None], 'max': [None, None]}
        dico['title'] = "Action spaces distribution for task " + str(task) + " for " + prefix
        dico['color'] = ['b']
        dico['marker'] = [None]
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_actions_hist_task(ax, task, options)]
        
        return dico




if __name__ == "__main__":
    
    #### Testing regression of procedures
    # ex : task 2 reached by procedure on task 1 and 0
    
    from utils import multivariate_regression
    import random
    
    goal = np.array([0.45, 0.8, -0.1, 0.7])
    
    y = []
    p = []
    
    nb_miss = 3
    r = 0.3
    
    for i in range(6-nb_miss):
        point = goal + np.random.uniform(-r, r, 4)
        p1 = np.zeros(3)
        p0 = np.zeros(3)
        p1[0:2] = point[0:2]
        p1[2] = 0.9 + random.uniform(-0.1, 0.1)
        p0[0:2] = point[2:4]
        p0[2] = -0.1 + random.uniform(-0.1, 0.1)
        y.append(point)
        p.append(np.array(p1.tolist() + p0.tolist()))
    for i in range(nb_miss):
        point = goal + np.random.uniform(-r, r, 4)
        real_point = 1000.0 * np.ones(4)
        p1 = np.zeros(3)
        p0 = np.zeros(3)
        p1[0:2] = point[0:2]
        p1[2] = 0.9 + random.uniform(-0.1, 0.1)
        p0[0:2] = point[2:4]
        p0[2] = -0.3 + random.uniform(-0.1, 0.1)
        y.append(real_point)
        p.append(np.array(p1.tolist() + p0.tolist()))
    
    y = np.array(y)
    p = np.array(p)
    
    p0 = multivariate_regression(y, p, goal)
    print p
    print goal, p0
    
    pass









