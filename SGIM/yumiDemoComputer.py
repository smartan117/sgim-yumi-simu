import zmq
import time
import sys
import json
import os

import copy

from dataset import ActionSpace, OutcomeSpace, DatasetV2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from utils import save_json


import sys
sys.path.insert(0, "../poppycontrol")
import DMP



mypath = "RawDemoParts"


list_a = {}
w_zero = 0.0
w_range = 200.0
m_range = np.array([337.0, 187.0, 203.5, 580.0, 226.0, 458.0, 337.0])
m_zeros = np.array([0.0, -50.0, -21.75, 0.0, 25.0, 0.0, 0.0])

for file in os.listdir(mypath):
    fname = os.path.join(mypath, file)
    if os.path.isfile(fname):
        key = file.split('.')[0]
        
        with open(fname) as f:
            content = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
        content = [x.strip() for x in content]

        l = []
        for c in content:
            l.append(json.loads(c))

        n_bfs = 1
        cs = DMP.CanonicalSystem()
        dmp = DMP.DynamicSystem(cs=cs, tau=5.0)

        for j in range(7):
            ts = DMP.TransformationSystem(str(j), n_bfs=n_bfs, start=0., goal=1.0, method="2012")
            dmp.add_dmp(ts)

        t = np.linspace(0, 5.0, num=len(l))
        l = np.array(l)
        poses = {}
        for j in range(7):
            p = l[:,j].T
            poses[str(j)] = p

        dmp.train(t, poses)

        _,move = dmp.integrate(0.02, 5.0)

        a = []
        for i in range(7):
            a += [min(max(dmp.ts[str(i)].parameters.weights[0][0] * 2.0 / w_range, -1.0), 1.0)]
            a += [min(max( (dmp.ts[str(i)].goal - m_zeros[i]) * 2.0 / m_range[i], -1.0), 1.0)]
        
        list_a[key] = a

start_pose = np.array([141.926, -91.9894, 14.8817, 146.197, 57.2076, 34.6444, -52.1023])
g = (start_pose - m_zeros) * 2.0 / m_range
a = []
for gg in g:
    a += [0.0, gg]
list_a["point0"] = a

print list_a
save_json(list_a, "Demos/simuDemos.json")

list_a = {}

for file in os.listdir(mypath):
    fname = os.path.join(mypath, file)
    if os.path.isfile(fname):
        key = file.split('.')[0]
        
        with open(fname) as f:
            content = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
        content = [x.strip() for x in content]

        l = []
        for c in content:
            l.append(json.loads(c))

        n_bfs = 1
        cs = DMP.CanonicalSystem()
        dmp = DMP.DynamicSystem(cs=cs, tau=5.0)

        for j in range(7):
            ts = DMP.TransformationSystem(str(j), n_bfs=n_bfs, start=0., goal=1.0, method="2012")
            dmp.add_dmp(ts)

        t = np.linspace(0, 5.0, num=len(l))
        l = np.array(l)
        poses = {}
        for j in range(7):
            p = l[::-1,j].T
            poses[str(j)] = p

        dmp.train(t, poses)

        _,move = dmp.integrate(0.02, 5.0)

        a = []
        for i in range(7):
            a += [min(max(dmp.ts[str(i)].parameters.weights[0][0] * 2.0 / w_range, -1.0), 1.0)]
            a += [min(max( (dmp.ts[str(i)].goal - m_zeros[i]) * 2.0 / m_range[i], -1.0), 1.0)]
        
        list_a[key] = a

print list_a
save_json(list_a, "Demos/simuDemosReversed.json")







