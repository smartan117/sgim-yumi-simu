from utils import load_raw, save_str, save_raw, save_csv
from yumi_control import *
from learning_agents import *
from interest_models import *
from learning_strategies import *

import numpy as np

np.set_printoptions(suppress=True)



def replay(la, im, divide=False):
    episodes = []
    for i in range(len(la.learning_process)):
        display_progress(i*100/len(la.learning_process), 25)
        episodes.append(la.get_episode(i, divide))
        episodes[-1].process(im)
    return ReplayStats(episodes)


"""
for i, ep in enumerate(rs.episodes):
        choices = np.zeros(ep.stats.task_strat_stats)
        tot = np.sum(ep.stats.task_strat_stats)
        if ep.mod > 0 and tot > 0.0:
            ideal_choices3 += ep.stats.task_strat_stats / tot
"""


def compute_strategy_choice(rs, window=100): # axis 0
    # First compute strategical choices when using intrinsic motivation
    all_choices = []
    for ep in rs.episodes:
        choices = np.sum(ep.stats.task_strat_stats, axis=0)
        tot = np.sum(choices)
        if ep.mod > 0 and tot > 0.0:
            all_choices.append(choices / tot)
    ideal_choices = []
    for i in range(len(all_choices)):
        start = max(i - window, 0)
        end = min(i + window, len(all_choices))
        choices = np.zeros(len(all_choices[0]))
        for j in range(start, end):
            choices += all_choices[j]
        ideal_choices.append(choices)
    return ideal_choices


def compute_task_choice(rs, window=100): # axis 1
    # First compute task choices when using intrinsic motivation
    all_choices = []
    for ep in rs.episodes:
        choices = np.sum(ep.stats.task_strat_stats, axis=1)
        tot = np.sum(choices)
        if ep.mod > 0 and tot > 0.0:
            all_choices.append(choices / tot)
    ideal_choices = []
    for i in range(len(all_choices)):
        start = max(i - window, 0)
        end = min(i + window, len(all_choices))
        choices = np.zeros(len(all_choices[0]))
        for j in range(start, end):
            choices += all_choices[j]
        ideal_choices.append(choices)
    return ideal_choices


# Load test file
la = load_raw("Yumi_SGIM_HL_2/back_test_23000")

# Compute real choices
print("Real choices: ")
print la.compute_strategy_task()
save_csv(la.compute_strategy_task(), "Analyzes/actual_choices.csv")


# First test regular model
im = InterestModelV3(la.dataset, la.interest_model.options)
rs1 = replay(la, im)

ideal_choices1 = np.zeros((len(la.dataset.y_spaces), len(la.strategies)))
for ep in rs1.episodes:
    tot = np.sum(ep.stats.task_strat_stats)
    if ep.mod > 0 and tot > 0.0:
        ideal_choices1 += ep.stats.task_strat_stats / tot

print("Ideal choices with standard model: ")
print np.around(ideal_choices1)
save_csv(np.around(ideal_choices1), "Analyzes/ideal_choices.csv")


# Test regular model with progress/n
im = InterestModelV3(la.dataset, la.interest_model.options)
rs2 = replay(la, im, True)

ideal_choices2 = np.zeros((len(la.dataset.y_spaces), len(la.strategies)))
for ep in rs2.episodes:
    tot = np.sum(ep.stats.task_strat_stats)
    if ep.mod > 0 and tot > 0.0:
        ideal_choices2 += ep.stats.task_strat_stats / tot

print("Ideal choices with standard model and progress/n: ")
print np.around(ideal_choices2)
save_csv(np.around(ideal_choices2), "Analyzes/progress_over_n_choices.csv")


# Test regular model with learner that not record intermediary progresses
"""
exp = SimuYumiExperimentatorV2(False)
    
options = {'min_points': 500, 'nb_iteration': 1, 'noise': 0.005}
autoPol = AutonomousExplorationV6(exp.env, exp.data, "Autonomous policies", options, 0)
other_op = {'min_points': 100, 'nb_iteration': 1, 'noise': 0.05}
autoProc = AutonomousProcedureV2(exp.env, exp.data, "Autonomous procedures", other_op, 0)
strat = [autoPol, autoProc]
costs = [1.0, 1.0]
RandomActionV4.k = 2.0

t_folder = "Demos/"
t_options = {'nb_iteration': 0, 'noise': 0.005}
for f in ["Teacher0V2", "Teacher1V2", "Teacher2V2", "Teacher34V2"]:
    dataset = load_raw(t_folder + f)
    costs.append(10.0)
    strat.append(DiscreteMimicryV4(exp.env, f, dataset, t_options))

p_options = {'nb_iteration': 0, 'noise': 0.01}
strat.append(ProceduralTeacher1V2(exp.env, exp.data, "ProceduralExpert1", p_options))
costs.append(5.0)
strat.append(ProceduralTeacher2V2(exp.env, exp.data, "ProceduralExpert2", p_options))
costs.append(5.0)
strat.append(ProceduralTeacher3(exp.env, exp.data, "ProceduralExpert3", p_options))
costs.append(5.0)
strat.append(ProceduralTeacher4(exp.env, exp.data, "ProceduralExpert4", p_options))
costs.append(5.0)

im = InterestModelV3(exp.data, {'around': 0.1, 'costs': costs, 'interest_window': 25, 'max_attempts': 6, \
        'max_points': 50, 'min_surface': 0.05, 'nb_regions': 10})
mods = [Mod(im, 0.3), GoodRegionMod(im, 0.5), GoodPointMod(im, 0.2)]


la2 = InterestAgentV5(exp.data, strat, mods, im, exp.env)
rs3 = la2.complete_replay(la.quick_replay().episodes)
ideal_choices3 = np.zeros((len(la.dataset.y_spaces), len(la.strategies)))
for ep in rs3.episodes:
    tot = np.sum(ep.stats.task_strat_stats)
    if ep.mod > 0 and tot > 0.0:
        ideal_choices3 += ep.stats.task_strat_stats / tot

print "\n", len(la2.epmem_length), len(la2.dataset.a_spaces[0][0].data), len(la2.dataset.y_spaces[0].data)

print("Ideal choices with standard model and no intermediary progress: ")
print np.around(ideal_choices3)

save_raw(rs3, "weird.txt")
"""

rs3 = load_raw("weird.txt")
ideal_choices3 = np.zeros((len(la.dataset.y_spaces), len(la.strategies)))
for ep in rs3.episodes:
    tot = np.sum(ep.stats.task_strat_stats)
    if ep.mod > 0 and tot > 0.0:
        ideal_choices3 += ep.stats.task_strat_stats / tot
save_csv(np.around(ideal_choices3), "Analyzes/no_intermediary_choices.csv")