import random
import numpy as np
from scipy.optimize import minimize
from scipy.spatial.distance import euclidean
import copy

from utils import custom_nelder_mead, multivariate_regression, sigmoid, save_json, save_str, load_json, load_str


import time




class StrategyV4:
    """Strategy usable by a learning agent."""
    def __init__(self, env, name, complex_actions=0):
        """
        env EnvironmentV4: environment of the learner (with which it will interact)
        name string: name of the strategy
        complex_actions int: indicates length of actions if fixed (0 otherwise)
        """
        self.env = env
        self.name = name
        # Short term memory containing all actions & outcomes reached during the last learning episode
        ## on the form [memory of iteration 0, memory of iteration1,...]
        ## memory of iteration i which tried a_i = a0 a1 a2 ... an is [memory of a0, memory of a1,...,memory of an]
        ## memory of a_j is on the form [a_j, a_type_j, y_list_j, y_types_j]
        self.memory = []
        self.n = 0
        self.complex_actions = complex_actions
    
    def to_config(self):
        dico = {}
        dico['type'] = self.__class__.__name__
        dico['name'] = self.name
        dico['complex_actions'] = self.complex_actions
        return dico
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        return StrategyV4(env, dico['name'], dico['complex_actions'])
    
    def available(self, task):
        """Says if the strategy is available to the learner."""
        return True
    
    def run(self):
        """Run the strategy (function used when the strategy does not require a goal/task)."""
        raise NotImplementedError
    
    def run_goal(self, goal, task):
        """Run the strategy for reaching the chosen goal/task."""
        raise NotImplementedError
    
    def test_action(self, action, a_type):
        """Test a specific complex action and store consequences in memory."""
        
        # Resetting the environment
        self.env.reset()
        
        if a_type[0] < len(self.env.a_spaces):
            actions = np.split(action, a_type[1]+1)
            newAction = []
            self.env.reset()
            mem = []
            for i in range(len(actions)):
                y_list, y_types = self.env.execute(actions[i], a_type[0])
                newAction += actions[i].tolist()
                if self.complex_actions == 0 or i == self.complex_actions-1: # TO CHECK if it works properly for both simple and complex actions
                    mem.append([np.array(newAction), (a_type[0], i), y_list, y_types])
            self.memory.append(mem)
            self.n += 1


class RandomActionV4(StrategyV4):
    """The random exploration of actions strategy."""
    k = 2.0  # parameters used to choose the size of an action
    def run(self):
        """Run random exploration."""
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Test a random action
        self.test_random_action()
    
    def to_config(self):
        dico = StrategyV4.to_config(self)
        dico['k'] = self.k
        return dico
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return RandomActionV4(env, dico['name'], dico['complex_actions'])
    
    def run_goal(self, goal, task):
        """Run random exploration (ignore task/goal as useless)."""
        self.run()
    
    def test_random_action(self): # test modifications on both simple and complex agents
        """Build and test a random action."""
        action = []
        a_type = random.randint(0, len(self.env.a_spaces)-1)
        n = 0
        if self.complex_actions <= 0:
            cost = 1.0 / (self.k ** n)
            r = random.random()
            # Keep chaining current actions with primitives to build a complex action
            while len(action) == 0 or r < cost:
                for j in range(self.env.a_spaces[a_type].dim):
                    action.append(random.uniform(self.env.a_spaces[a_type].bounds['min'][j], \
                            self.env.a_spaces[a_type].bounds['max'][j]))
                n += 1
                r = random.random()
                cost = 1.0 / (self.k ** n)
        else:
            n = self.complex_actions
            for i in range(self.complex_actions):
                for j in range(self.env.a_spaces[a_type].dim):
                    action.append(random.uniform(self.env.a_spaces[a_type].bounds['min'][j], \
                            self.env.a_spaces[a_type].bounds['max'][j]))
        self.test_action(np.array(action), (a_type, n-1))


class AutonomousExplorationV4(RandomActionV4):
    """Autonomous exploration strategy. Used in SAGG-RIAC algorithm."""
    
    def __init__(self, environment, dataset, name, options, complex_actions=0, verbose=False):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: episodic memory of the agent using the strategy (needed for local exploration)
        name string: name of the given strategy
        options dict: options of the strategy
        verbose boolean: indicates the level of debug info
        """
        RandomActionV4.__init__(self, environment, name, complex_actions)
        self.dataset = dataset
        # Contains the following keys:
        #   'min_points': min number of iterations before enabling the use of local exploration
        #   'nb_iteration': number of iterations per learning episode
        self.options = options
        self.verbose = verbose
        # Store method chosen for every iteration with the strategy, stored by learning episode
        self.methods = []
        # Store local global criteria for every iteration with the strategy, stored by learning episode
        self.criteria = []
    
    def to_config(self):
        dico = RandomActionV4.to_config(self)
        dico['options'] = self.options
        return dico
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return AutonomousExplorationV4(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
        
    def run_goal(self, goal, task):
        """Perform an episode of autonomous exploration."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Add a list with the choices of methods for the episode
        self.methods.append([])
        self.criteria.append([])
        
        while self.n < self.options['nb_iteration']:
            # Choose between local and global exploration
            prob = self.local_global_criteria(goal, task)
            self.criteria[-1].append(prob)
            r = random.uniform(0, 1)
            
            if r <= prob:
                # We have chosen random exploration
                self.methods[-1].append(0)
                self.test_random_action()
            else:
                # We have chosen local optimizattion
                self.methods[-1].append(1)
                if self.verbose:
                    print("Using Local optimization after random for n="+ str(self.n) + ", ~prob: " + str(prob))                
                
                # Compute best locality and distances
                y, a, a_type = self.dataset.best_locality(goal, task)
                d = []
                for yi in y:
                    d.append(min(euclidean(yi, goal), \
                        self.dataset.y_spaces[task].options['out_dist'])/self.dataset.y_spaces[task].options['max_dist'])
                initial_simplex = [np.array(a), np.array(d)]
                
                # Compute first guess
                a0 = multivariate_regression(y, a, goal)
                
                # Then run Nelder-Mead to optimize action
                self.run_local_optimization(a0, goal, task, a_type, initial_simplex) 
        
    def local_global_criteria(self, goal, task):
        """Criteria used to choose between local and global exploration (Straight from Matlab SGIM code)."""
        
        prob = 1.0
        if len(self.dataset.y_spaces[task].data) > self.options['min_points']:
            _,dist = self.dataset.nn_y(goal, task, 5)
            if len(dist) < 5:
                return prob
            mean_dist = np.mean(dist)
            x = (mean_dist - self.dataset.y_spaces[task].options['err']) / self.dataset.y_spaces[task].options['range']
            prob = 0.8*(sigmoid(x) - 0.5) + 0.5
            #prob = 0.9*(sigmoid(x) - 0.5) + 0.5   # From matlab SGIM code
        return prob
        
    def run_local_optimization(self, a, goal, task, a_type, initial_simplex=None):
        """Perform local exploration using Nelder-Mead optimization method."""
        
        # Function to minimize for Nelder-Mead
        function = lambda x: self.test_nelder_mead(x, goal, task, a_type)
        minimize(function, a, method=custom_nelder_mead, options={'xtol': 1e-3, 'ftol': 1e-6, \
            'maxiter': (self.options['nb_iteration'] - self.n), 'maxfev': (self.options['nb_iteration'] - self.n), \
            'initial_simplex': initial_simplex})
        
    def test_nelder_mead(self, a, g, task, a_type):
        """Metod called when testing an action with Nelder-Mead algorithm."""
        
        self.test_action(a, a_type)
        dist = self.dataset.y_spaces[task].options['out_dist']/self.dataset.y_spaces[task].options['max_dist']
        for i in range(len(self.memory[-1][-1][3])):
            if self.memory[-1][-1][3][i] == task:
                dist = min(euclidean(g, self.memory[-1][-1][2][i]), self.dataset.y_spaces[task].options['out_dist']) \
                    / self.dataset.y_spaces[task].options['max_dist']
        return dist


class AutonomousExplorationV6(AutonomousExplorationV4):
    """Variant of Autonomous exploration performing only one iteration of local exploration."""
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return AutonomousExplorationV6(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def run_goal(self, goal, task):
        """Perform an episode of autonomous exploration."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Add a list with the choices of methods for the episode
        self.methods.append([])
        self.criteria.append([])
        
        while self.n < self.options['nb_iteration']:
            # Choose between local and global exploration
            prob = self.local_global_criteria(goal, task)
            self.criteria[-1].append(prob)
            r = random.uniform(0, 1)
            
            if r <= prob:
                # We have chosen random exploration
                self.methods[-1].append(0)
                self.test_random_action()
            else:
                # We have chosen local optimizattion
                self.methods[-1].append(1)
                if self.verbose:
                    print("Using Local optimization after random for n="+ str(self.n) + ", ~prob: " + str(prob))                
                
                # Compute best locality and distances
                y, a, a_type = self.dataset.best_locality(goal, task)
                
                # Compute first guess and test it
                a0 = multivariate_regression(y, a, goal)  
                
                s = AutonomousProcedure.compute_uniformity(a, self.dataset.a_spaces[a_type[0]][a_type[1]].options['max_dist'])
                w = 2 * (0.5 - s) * self.options['noise']
                a0 += w * np.random.uniform(-1, 1, len(a0))
                
                self.test_action(a0, a_type)


class AutonomousExplorationV5(AutonomousExplorationV4):
    """Autonomous exploration strategy (with a different local exploration sub-strategy). Used in SAGG-RIAC algorithm."""
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return AutonomousExplorationV5(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def run_goal(self, goal, task):
        """Perform an episode of autonomous exploration."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Add a list with the choices of methods for the episode
        self.methods.append([])
        self.criteria.append([])
        
        while self.n < self.options['nb_iteration']:
            # Choose between local and global exploration
            prob = self.local_global_criteria(goal, task)
            self.criteria[-1].append(prob)
            r = random.uniform(0, 1)
            
            if r <= prob:
                # We have chosen random exploration
                self.methods[-1].append(0)
                self.test_random_action()
            else:
                # We have chosen local optimizattion
                self.methods[-1].append(1)
                if self.verbose:
                    print("Using Local optimization after random for n="+ str(self.n) + ", ~prob: " + str(prob))                
                
                # Compute best locality and distances
                y, a, a_type = self.dataset.best_locality(goal, task)
                d = []
                for yi in y:
                    d.append(min(euclidean(yi, goal), \
                        self.dataset.y_spaces[task].options['out_dist'])/self.dataset.y_spaces[task].options['max_dist'])
                initial_simplex = [np.array(a), np.array(d)]
                
                # Compute first guess and test it
                a0 = multivariate_regression(y, a, goal)                
                self.test_action(a0, a_type)
                # Repete it while adding some random noise
                while self.n < self.options['nb_iteration']:
                    a = a0 + np.random.uniform(-self.options['noise'], self.options['noise'], len(a0))
                    self.test_action(a, a_type)
                
    
    def local_global_criteria(self, goal, task):
        """Criteria used to choose between local and global exploration (Straight from Matlab SGIM code)."""
        
        prob = 1.0
        if len(self.dataset.y_spaces[task].data) > self.options['min_points']:
            _,dist = self.dataset.nn_y(goal, task, 5)
            if len(dist) < 5:
                return prob
            mean_dist = np.mean(dist)
            x = (mean_dist - self.dataset.y_spaces[task].options['err']) / self.dataset.y_spaces[task].options['range']
            prob = 0.8*(sigmoid(x) - 0.5) + 0.5
            #prob = 0.9*(sigmoid(x) - 0.5) + 0.5   # From matlab SGIM code
        return prob
        
    def run_local_optimization(self, a, goal, task, a_type, initial_simplex=None):
        """Perform local exploration using Nelder-Mead optimization method."""
        
        # Function to minimize for Nelder-Mead
        function = lambda x: self.test_nelder_mead(x, goal, task, a_type)
        minimize(function, a, method=custom_nelder_mead, options={'xtol': 1e-3, 'ftol': 1e-6, \
            'maxiter': (self.options['nb_iteration'] - self.n), 'maxfev': (self.options['nb_iteration'] - self.n), \
            'initial_simplex': initial_simplex})
        
    def test_nelder_mead(self, a, g, task, a_type):
        """Metod called when testing an action with Nelder-Mead algorithm."""
        
        self.test_action(a, a_type)
        dist = self.dataset.y_spaces[task].options['out_dist']/self.dataset.y_spaces[task].options['max_dist']
        for i in range(len(self.memory[-1][-1][3])):
            if self.memory[-1][-1][3][i] == task:
                dist = min(euclidean(g, self.memory[-1][-1][2][i]), self.dataset.y_spaces[task].options['out_dist']) \
                    / self.dataset.y_spaces[task].options['max_dist']
        return dist


class RandomProcedure(RandomActionV4):
    """Random procedure strategy."""
    def __init__(self, environment, dataset, name, options, verbose=False):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: episodic memory of the agent using the strategy (needed for procedures)
        name string: name of the given strategy
        options dict: options of the strategy
        verbose boolean: indicates the level of debug info
        """
        RandomActionV4.__init__(self, environment, name)
        self.dataset = dataset
        self.options = options
        self.verbose = verbose
        self.chosen_p = []
    
    def to_config(self):
        dico = RandomActionV4.to_config(self)
        dico['options'] = self.options
        return dico
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return RandomProcedure(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def get_flat_filled_p_list(self):
        """Return available choices of procedural space."""
        l = []
        for i in range(len(self.dataset.p_spaces)):
            for j in range(len(self.dataset.p_spaces[i])):
                l.append((i, j))
        return l
    
    def random_procedure(self):
        """Build a random strategy."""
        l = self.get_flat_filled_p_list()
        if len(l) == 0:
            return None
        i = random.randint(0, len(l)-1)
        p = []
        for k in range(self.dataset.p_spaces[l[i][0]][l[i][1]].dim):
            p.append(random.uniform(self.dataset.p_spaces[l[i][0]][l[i][1]].bounds['min'][k], \
                    self.dataset.p_spaces[l[i][0]][l[i][1]].bounds['max'][k]))
        return (np.array(p), (l[i]))
    
    def run_goal(self, goal, task):
        """Run strategy for a given goal (useless as not goal oriented strategy)."""
        self.run()
    
    def run(self):
        """Run strategy."""
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        pro = self.random_procedure()
        self.chosen_p.append(pro)
        if pro:
            self.test_procedure(pro[0], pro[1])
        else:
            self.test_random_action()
    
    def test_procedure(self, p, p_type):
        """Test a procedure after refining it."""
        # Find closest combination of tasks recorded
        pro = self.dataset.refine_procedure(p, p_type)
        if len(pro) == 0:
            self.test_random_action()
        else:
            idy1 = pro[0]
            idy2 = pro[1]
            ide1 = self.dataset.p_spaces[p_type[0]][p_type[1]].y_spaces[0].ids[idy1]
            ide2 = self.dataset.p_spaces[p_type[0]][p_type[1]].y_spaces[1].ids[idy2]
            (a_type1, ida1) = self.dataset.idA[ide1]
            (a_type2, ida2) = self.dataset.idA[ide2]
            if a_type1[0] == a_type2[0]:
                a1 = self.dataset.a_spaces[a_type1[0]][a_type1[1]].data[ida1]
                a2 = self.dataset.a_spaces[a_type2[0]][a_type2[1]].data[ida2]
                # Build composition of actions
                a = np.array(a1.tolist() + a2.tolist())
                # Compute a_type of composite action
                nb = self.dataset.a_spaces[a_type1[0]][a_type1[1]].n + \
                        self.dataset.a_spaces[a_type2[0]][a_type2[1]].n
                a_type = (a_type1[0], nb-1)
                self.test_action(a, a_type)
                # Add the procedure tested and its type in the last tried action memory
                proc_tested = np.array(self.dataset.p_spaces[p_type[0]][p_type[1]].y_spaces[0].data[idy1].tolist() + \
                        self.dataset.p_spaces[p_type[0]][p_type[1]].y_spaces[1].data[idy2].tolist())
                self.memory[-1][-1].append(proc_tested)
                self.memory[-1][-1].append(p_type)
            else:
                self.test_random_action()


class AutonomousProcedure(RandomProcedure):
    """Autonomous exploration of procedural spaces. Similar to regular Autonomous exploration strategy."""
    def __init__(self, environment, dataset, name, options, verbose=False):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: episodic memory of the agent using the strategy (needed for local exploration)
        name string: name of the given strategy
        options dict: options of the strategy
        verbose boolean: indicates the level of debug info
        """
        RandomProcedure.__init__(self, environment, dataset, name, options, verbose=verbose)
        # Store method chosen for every iteration with the strategy, stored by learning episode
        self.methods = []
        # Store local global criteria for every iteration with the strategy, stored by learning episode
        self.criteria = []
        if not self.options.has_key('criteria_mod'):
            self.options['criteria_mod'] = 1.0
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return AutonomousProcedure(env, dataset_la, dico['name'], dico['options'])
    
    def run_goal(self, goal, task):
        """Run strategy for given goal."""
        self.memory = []
        self.n = 0
        
        # Add a list with the choices of methods for the episode
        self.methods.append([])
        self.criteria.append([])
        
        while self.n < self.options['nb_iteration']:
            prob = self.local_global_criteria(goal, task)
            self.criteria[-1].append(prob)
            r = random.uniform(0, 1)
            
            if r < prob:
                self.methods[-1].append(0)
                pro = self.random_procedure()
                self.chosen_p.append(pro)
                if pro:
                    self.test_procedure(pro[0], pro[1])
                else:
                    self.test_random_action()
            else:
                # Chose local exploration of procedure space
                self.methods[-1].append(1)
                if self.verbose:
                    print("Using Local optimization after random for n="+ str(self.n) + ", ~prob: " + str(prob))
                # Compute best locality
                y, p, p_type = self.dataset.best_locality_procedures(goal, task)
                
                if len(p_type) > 1:
                    while self.n < self.options['nb_iteration']:
                        # Compute first guess
                        p0 = multivariate_regression(y, p, goal)
                        
                        ### TO DO Add noise proportional to local model uniformity
                        s = AutonomousProcedure.compute_uniformity(p, self.dataset.p_spaces[p_type[0]][p_type[1]].options['max_dist'])
                        w = 2 * (0.5 - s) * self.options['noise']
                        p0 += w * np.random.uniform(-1, 1, len(p0))
                        
                        self.chosen_p.append((p0, p_type))
                        self.test_procedure(p0, p_type)
                        
                        # Recover executed procedure and reached outcome and add to local model
                        real_p = self.memory[-1][-1][-2]
                        y_list = self.memory[-1][-1][2]
                        tasks = self.memory[-1][-1][3]
                        reached_y = self.dataset.y_spaces[task].bounds['max'] * 1000.0
                        # Retrieve actual outcome reached thanks to the procedure
                        for i, t in enumerate(tasks):
                            if t == task:
                                reached_y = y_list[i]
                                break
                        p = np.vstack((p, real_p))
                        y = np.vstack((y, reached_y))
                else:
                    pro = self.random_procedure()
                    self.chosen_p.append(pro)
                    if pro:
                        self.test_procedure(pro[0], pro[1])
                    else:
                        self.test_random_action()
    
    @classmethod
    def compute_uniformity(cls, cluster, dmax):
        bar = np.mean(cluster, axis=0)
        d = np.sqrt(np.sum((cluster - bar)**2, axis=1)) / dmax
        s = np.sqrt(np.mean(d ** 2))
        return s
    
    def get_total_procedures(self):
        """Return actual number of procedures tried."""
        return len(self.dataset.idEP)
    
    def local_global_criteria(self, goal, task):
        """Criteria used to choose between local and global exploration (Straight from Matlab SGIM code)."""
        n = self.get_total_procedures()
        prob = 1.0
        if n > self.options['min_points']:
            _,dist = self.dataset.nn_y_procedural(goal, task, 5)
            if len(dist) < 5:
                return prob
            mean_dist = np.mean(dist) / self.options['criteria_mod']
            x = (mean_dist - self.dataset.y_spaces[task].options['err']) / self.dataset.y_spaces[task].options['range']
            prob = 0.8*(sigmoid(x) - 0.5) + 0.5
            #prob = 0.9*(sigmoid(x) - 0.5) + 0.5   # From matlab SGIM code
        return prob


class AutonomousProcedureV2(AutonomousProcedure):
    """Variant of Autonomous exploration of procedures performing one iteration of local exploration."""    
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return AutonomousProcedureV2(env, dataset_la, dico['name'], dico['options'])
    
    def run_goal(self, goal, task):
        """Run strategy for given goal."""
        self.memory = []
        self.n = 0
        
        # Add a list with the choices of methods for the episode
        self.methods.append([])
        self.criteria.append([])
        
        while self.n < self.options['nb_iteration']:
            prob = self.local_global_criteria(goal, task)
            self.criteria[-1].append(prob)
            r = random.uniform(0, 1)
            
            if r < prob:
                self.methods[-1].append(0)
                pro = self.random_procedure()
                self.chosen_p.append(pro)
                if pro:
                    self.test_procedure(pro[0], pro[1])
                else:
                    self.test_random_action()
            else:
                # Chose local exploration of procedure space
                self.methods[-1].append(1)
                if self.verbose:
                    print("Using Local optimization after random for n="+ str(self.n) + ", ~prob: " + str(prob))
                # Compute best locality
                y, p, p_type = self.dataset.best_locality_procedures(goal, task)
                
                if len(p_type) > 1:
                    # Compute first guess
                    p0 = multivariate_regression(y, p, goal)
                    
                    ### TO DO Add noise proportional to local model uniformity
                    s = AutonomousProcedure.compute_uniformity(p, self.dataset.p_spaces[p_type[0]][p_type[1]].options['max_dist'])
                    w = 2 * (0.5 - s) * self.options['noise']
                    p0 += w * np.random.uniform(-1, 1, len(p0))
                    
                    self.chosen_p.append((p0, p_type))
                    self.test_procedure(p0, p_type)
                else:
                    pro = self.random_procedure()
                    self.chosen_p.append(pro)
                    if pro:
                        self.test_procedure(pro[0], pro[1])
                    else:
                        self.test_random_action()


class AutonomousExplorationProcedures(RandomProcedure):
    """Autonomous exploration mixing procedures and actions."""
    def __init__(self, environment, dataset, name, options, verbose=False):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: episodic memory of the agent using the strategy (needed for local exploration)
        name string: name of the given strategy
        options dict: options of the strategy
        verbose boolean: indicates the level of debug info
        """
        RandomProcedure.__init__(self, environment, dataset, name, options, verbose=verbose)
        # Store method chosen for every iteration with the strategy, stored by learning episode
        self.methods = []
        # Store local global criteria for every iteration with the strategy, stored by learning episode
        self.criteria = []
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return AutonomousExplorationProcedures(env, dataset_la, dico['name'], dico['options'])
    
    def run_goal(self, goal, task):
        """Perform an episode of autonomous exploration."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Add a list with the choices of methods for the episode
        self.methods.append([])
        self.criteria.append([])
        
        while self.n < self.options['nb_iteration']:
            # Choose between local and global exploration
            prob = self.local_global_criteria(goal, task)
            self.criteria[-1].append(prob)
            r = random.uniform(0, 1)
            
            if r <= prob:
                # We have chosen random exploration
                self.methods[-1].append(0)
                i = random.randint(0, 1)
                if i == 0:
                    self.test_random_action()
                else:
                    pro = self.random_procedure()
                    self.chosen_p.append(pro)
                    if pro:
                        self.test_procedure(pro[0], pro[1])
                    else:
                        self.test_random_action()
            else:
                # We have chosen local optimizattion
                self.methods[-1].append(1)
                if self.verbose:
                    print("Using Local optimization after random for n="+ str(self.n) + ", ~prob: " + str(prob))                
                
                # Compute best locality and distances
                y, a, a_type, proc_done = self.dataset.best_locality_complete(goal, task)
                
                if proc_done:
                    if self.verbose:
                        print("Choosing procedures")
                    if len(a_type) > 1:
                        while self.n < self.options['nb_iteration']:
                            # Compute first guess
                            p0 = multivariate_regression(y, a, goal)
                            
                            ### TO DO Add noise proportional to local model uniformity
                            s = AutonomousProcedure.compute_uniformity(a, self.dataset.p_spaces[a_type[0]][a_type[1]].options['max_dist'])
                            w = 2 * (0.5 - s) * self.options['noise']
                            p0 += w * np.random.uniform(-1, 1, len(p0))
                            
                            self.chosen_p.append((p0, a_type))
                            self.test_procedure(p0, a_type)
                            
                            # Recover executed procedure and reached outcome and add to local model
                            real_p = self.memory[-1][-1][-2]
                            y_list = self.memory[-1][-1][2]
                            tasks = self.memory[-1][-1][3]
                            reached_y = self.dataset.y_spaces[task].bounds['max'] * 1000.0
                            # Retrieve actual outcome reached thanks to the procedure
                            for i, t in enumerate(tasks):
                                if t == task:
                                    reached_y = y_list[i]
                                    break
                            a = np.vstack((a, real_p))
                            y = np.vstack((y, reached_y))
                    else:
                        pro = self.random_procedure()
                        self.chosen_p.append(pro)
                        if pro:
                            self.test_procedure(pro[0], pro[1])
                        else:
                            self.test_random_action()
                else:
                    if self.verbose:
                        print("Choosing actions")
                    d = []
                    for yi in y:
                        d.append(min(euclidean(yi, goal), \
                            self.dataset.y_spaces[task].options['out_dist'])/self.dataset.y_spaces[task].options['max_dist'])
                    initial_simplex = [np.array(a), np.array(d)]
                    
                    # Compute first guess
                    a0 = multivariate_regression(y, a, goal)
                    
                    # Then run Nelder-Mead to optimize action
                    self.run_local_optimization(a0, goal, task, a_type, initial_simplex) 
        
    def get_total_procedures(self):
        """Return actual number of procedures tried."""
        return len(self.dataset.idEP)
    
    def local_global_criteria(self, goal, task):
        """Criteria used to choose between local and global exploration (Straight from Matlab SGIM code)."""
        
        prob = 1.0
        if len(self.dataset.y_spaces[task].data) > self.options['min_points']:
            _,dist = self.dataset.nn_y(goal, task, 5)
            if len(dist) < 5:
                return prob
            mean_dist = np.mean(dist)
            x = (mean_dist - self.dataset.y_spaces[task].options['err']) / self.dataset.y_spaces[task].options['range']
            prob = 0.8*(sigmoid(x) - 0.5) + 0.5
            #prob = 0.9*(sigmoid(x) - 0.5) + 0.5   # From matlab SGIM code
        return prob
        
    def run_local_optimization(self, a, goal, task, a_type, initial_simplex=None):
        """Perform local exploration using Nelder-Mead optimization method."""
        
        # Function to minimize for Nelder-Mead
        function = lambda x: self.test_nelder_mead(x, goal, task, a_type)
        minimize(function, a, method=custom_nelder_mead, options={'xtol': 1e-3, 'ftol': 1e-6, \
            'maxiter': (self.options['nb_iteration'] - self.n), 'maxfev': (self.options['nb_iteration'] - self.n), \
            'initial_simplex': initial_simplex})
        
    def test_nelder_mead(self, a, g, task, a_type):
        """Metod called when testing an action with Nelder-Mead algorithm."""
        
        self.test_action(a, a_type)
        dist = self.dataset.y_spaces[task].options['out_dist']/self.dataset.y_spaces[task].options['max_dist']
        for i in range(len(self.memory[-1][-1][3])):
            if self.memory[-1][-1][3][i] == task:
                dist = min(euclidean(g, self.memory[-1][-1][2][i]), self.dataset.y_spaces[task].options['out_dist']) \
                    / self.dataset.y_spaces[task].options['max_dist']
        return dist


class LocalExploration(StrategyV4):
    """Local exploration strategy."""
    def __init__(self, environment, dataset, name, options, complex_actions=0, verbose=False):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: episodic memory of the agent using the strategy (needed for local exploration)
        name string: name of the given strategy
        options dict: options of the strategy
        verbose boolean: indicates the level of debug info
        """
        StrategyV4.__init__(self, environment, name, complex_actions)
        self.dataset = dataset
        # Contains the following keys:
        #   'min_points': min number of iterations before enabling the use of local exploration
        #   'nb_iteration': number of iterations per learning episode
        self.options = options
        self.verbose = verbose
    
    def to_config(self):
        dico = StrategyV4.to_config(self)
        dico['options'] = self.options
        return dico
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        return LocalExploration(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def available(self, task):
        """Return availability of strategy (only not available when no points in targeted task space."""
        return (len(self.dataset.y_spaces[task].data) > 0)
        
    def run_goal(self, goal, task): # already augmented goal with cost 0
        """Perform an episode of autonomous exploration."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Compute best locality and distances
        y, a, a_type = self.dataset.best_locality(goal, task)
        d = []
        for yi in y:
            d.append(min(euclidean(yi, goal), \
                self.dataset.y_spaces[task].options['out_dist'])/self.dataset.y_spaces[task].options['max_dist'])
        initial_simplex = [np.array(a), np.array(d)]
        
        # Compute first guess
        a0 = multivariate_regression(y, a, goal)
        
        # Then run Nelder-Mead to optimize action
        self.run_local_optimization(a0, goal, task, a_type, initial_simplex)
        
    def run_local_optimization(self, a, goal, task, a_type, initial_simplex=None):
        """Perform local exploration using Nelder-Mead optimization method."""
        
        # Function to minimize for Nelder-Mead
        function = lambda x: self.test_nelder_mead(x, goal, task, a_type)
        minimize(function, a, method=custom_nelder_mead, options={'xtol': 1e-3, 'ftol': 1e-6, \
            'maxiter': self.options['nb_iteration'], 'maxfev': self.options['nb_iteration'], \
            'initial_simplex': initial_simplex})
        
    def test_nelder_mead(self, a, g, task, a_type):
        """Metod called when testing an action with Nelder-Mead algorithm."""
        
        self.test_action(a, a_type)
        dist = self.dataset.y_spaces[task].options['out_dist']/self.dataset.y_spaces[task].options['max_dist']
        for i in range(len(self.memory[-1][-1][3])):
            if self.memory[-1][-1][3][i] == task:
                dist = min(euclidean(g, self.memory[-1][-1][2][i]), self.dataset.y_spaces[task].options['out_dist']) \
                    / self.dataset.y_spaces[task].options['max_dist']
        return dist


class ImitationV4(StrategyV4):
    """Passive imitation strategy."""
    
    def __init__(self, environment, name, dataset, options, complex_actions=0):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: dataset of the teacher
        name string: name of the given strategy
        options dict: options of the strategy        
        """
        StrategyV4.__init__(self, environment, name, complex_actions)
        self.dataset = dataset
        # Contains the following keys:
        #   'nb_iteration': number of iterations per episode
        #   'noise': noise band used to perform radom modifications on repetitions
        self.options = options
        self.demos_given = []
        self.demos_times = []
        for a in range(len(self.dataset.idA)):
            self.demos_times.append(0)
    
    def to_config(self):
        dico = StrategyV4.to_config(self)
        dico['options'] = self.options
        dico['y_spaces'] = {}
        for y in self.dataset.y_spaces:
            dico['y_spaces'][y.name] = len(y.data)
        return dico
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        return ImitationV4(env, dico['name'], dataset_t, dico['options'], dico['complex_actions'])
        
    def choose_demo(self):
        """Choose one demonstration among the less repeted ones."""
        
        # Sort actions ids by number of times given
        ids = range(len(self.dataset.idA))
        ids.sort(key= lambda j: self.demos_times[j])
        
        # Take only the less chosen demos into account
        min_chosen = self.demos_times[ids[0]]
        j = 1
        while j < len(ids) and self.demos_times[ids[j]] == min_chosen:
            j += 1
        ids = ids[0:j]
        
        # Take one random demo from less chosen demos
        random.shuffle(ids)            
        action = ids[random.randint(0,len(ids)-1)]
        return action
        
    def run(self):
        """Run one imitation episode."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Choose one demonstration among the less repeted ones
        demo_id = self.choose_demo()
        self.demos_times[demo_id] += 1
        
        a_type = self.dataset.idA[demo_id][0]
        demo = self.dataset.a_spaces[a_type[0]][a_type[1]].data[self.dataset.idA[demo_id][1]]
        self.demos_given.append(demo)
        
        # Copy demo to make sure not changing it
        action = copy.deepcopy(demo)
        
        #self.test_action(action, a_type)    #TO DO change it everywhere    
        for i in range(self.options['nb_iteration']+1):
            a = action + np.random.uniform(-self.options['noise'], self.options['noise'], len(action))
            self.test_action(a, a_type)


class MimicryV4(StrategyV4):
    """Active mimicry strategy. The agent requests demonstration to reach specific goals."""
    
    def __init__(self, environment, name, dataset, options, complex_actions=0):
        """
        environment Environment: environment of the learning agent
        dataset DatasetV4: dataset of the teacher
        name string: name of the given strategy
        options dict: options of the strategy        
        """
        StrategyV4.__init__(self, environment, name, complex_actions)
        self.dataset = dataset
        # Contains the following keys:
        #   'nb_iteration': number of iterations per episode
        #   'noise': noise band used to perform radom modifications on repetitions
        self.options = options
        self.demos_given = []
    
    def to_config(self):
        dico = StrategyV4.to_config(self)
        dico['options'] = self.options
        dico['dataset'] = ""
        dico['y_spaces'] = {}
        for y in self.dataset.y_spaces:
            dico['y_spaces'][y.name] = len(y.data)
        return dico
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        return MimicryV4(env, dico['name'], dataset_t, dico['options'], dico['complex_actions'])
        
    def choose_demo(self, goal, task):
        """Choose the closest demonstration to the requested goal."""
        
        ids,dist = self.dataset.nn_y(goal, task)
        if len(dist) > 0:
            action = self.dataset.y_spaces[task].ids[ids[0]]
        else:
            action = random.randint(0,len(self.dataset.idA)-1)
        return action
        
    def run_goal(self, goal, task):
        """Run one episode under the mimicry strategy."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Choose one demonstration as adapted to goal as possible
        demo_id = self.choose_demo()        
        
        a_type = self.dataset.idA[demo_id][0]
        demo = self.dataset.a_spaces[a_type[0]][a_type[1]].data[self.dataset.idA[demo_id][1]]
        self.demos_given.append(demo)
        
        # Copy demo to make sure not changing it
        action = copy.deepcopy(demo)
        
        self.test_action(action, a_type)        
        for i in range(self.options['nb_iteration']):
            a = action + np.random.uniform(-self.options['noise'], self.options['noise'], len(action))
            self.test_action(a, a_type)


class DiscreteMimicryV4(MimicryV4):
    """Active mimicry strategy. Only difference, it chooses among the demos least given if teacher not adapted."""
    
    def __init__(self, environment, name, dataset, options, complex_actions=0):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: dataset of the teacher
        name string: name of the given strategy
        options dict: options of the strategy        
        """
        MimicryV4.__init__(self, environment, name, dataset, options, complex_actions)
        # options contains the following keys:
        #   'nb_iteration': number of iterations per episode
        #   'noise': noise band used to perform radom modifications on repetitions
        self.demos_times = []
        for a in range(len(self.dataset.idA)):
            self.demos_times.append(0)
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        return DiscreteMimicryV4(env, dico['name'], dataset_t, dico['options'], dico['complex_actions'])
        
    def choose_demo(self, goal, task):
        """Choose the closest demonstration to the requested goal or one of the least given ones."""
        ids,dist = self.dataset.nn_y(goal, task)
        if len(dist) > 0:
            action = self.dataset.y_spaces[task].ids[ids[0]]
        else:
            # Sort actions ids by number of times given
            ids = range(len(self.demos_times))
            ids.sort(key= lambda j: self.demos_times[j])
            
            # Take only the less chosen demos into account
            min_chosen = self.demos_times[ids[0]]
            j = 1
            while j < len(ids) and self.demos_times[ids[j]] == min_chosen:
                j += 1
            ids = ids[0:j]
            
            # Take one random demo from less chosen demos
            random.shuffle(ids)
            action = ids[random.randint(0,len(ids)-1)]
        return action
        
    def run_goal(self, goal, task):
        """Run one episode under the mimicry strategy."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Choice of a demonstration as adapted to goal as possible
        demo_id = self.choose_demo(goal, task)
        self.demos_times[demo_id] += 1
        
        a_type = self.dataset.idA[demo_id][0]
        demo = self.dataset.a_spaces[a_type[0]][a_type[1]].data[self.dataset.idA[demo_id][1]]
        self.demos_given.append(demo)
        
        # Copy demo to make sure not changing it
        action = copy.deepcopy(demo)
        
        self.test_action(action, a_type)        
        for i in range(self.options['nb_iteration']):
            a = action + np.random.uniform(-self.options['noise'], self.options['noise'], len(action))
            self.test_action(a, a_type)


class DiscreteProceduralTeacher(RandomActionV4, MimicryV4):
    """Active mimicry strategy. Only difference, it chooses among the demos least given if teacher not adapted."""
    
    def __init__(self, environment, name, dataset, la_dataset, options, complex_actions=0):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: episodic memory of the agent using the strategy (needed for local exploration)
        la_dataset DatasetV2: episodic memory of the teacher (contains only procedures)
        name string: name of the given strategy
        options dict: options of the strategy
        """
        RandomActionV4.__init__(self, environment, name, complex_actions)
        MimicryV4.__init__(self, environment, name, dataset, options, complex_actions)
        # options contains the following keys:
        #   'nb_iteration': number of iterations per episode
        #   'noise': noise band used to perform radom modifications on repetitions
        self.demos_times = []
        self.la_dataset = la_dataset
        for a in range(len(self.dataset.idA)):
            self.demos_times.append(0)
        self.chosen_p = []
    
    def to_config(self):
        dico = MimicryV4.to_config(self)
        dico['k'] = RandomActionV4.k
        return dico
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return DiscreteProceduralTeacher(env, dico['name'], dataset_t, dataset_la, dico['options'], dico['complex_actions'])
        
    def choose_demo(self, goal, task):
        """Choose the closest demonstration to the requested goal or one of the least given ones."""
        ids,dist = self.dataset.nn_y(goal, task)
        if len(dist) > 0:
            action = self.dataset.y_spaces[task].ids[ids[0]]
        else:
            # Sort actions ids by number of times given
            ids = range(len(self.demos_times))
            ids.sort(key= lambda j: self.demos_times[j])
            
            # Take only the less chosen demos into account
            min_chosen = self.demos_times[ids[0]]
            j = 1
            while j < len(ids) and self.demos_times[ids[j]] == min_chosen:
                j += 1
            ids = ids[0:j]
            
            # Take one random demo from less chosen demos
            random.shuffle(ids)
            action = ids[random.randint(0,len(ids)-1)]
        return action
        
    def run_goal(self, goal, task):
        """Run one episode under the mimicry strategy."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Choice of a demonstration as adapted to goal as possible
        demo_id = self.choose_demo(goal, task)
        self.demos_times[demo_id] += 1
        
        p_type = self.dataset.idP[demo_id][0]
        demo = self.dataset.p_spaces[p_type[0]][p_type[1]].data[self.dataset.idP[demo_id][1]]
        self.demos_given.append(demo)
        
        # Copy demo to make sure not changing it
        proce = copy.deepcopy(demo)
        self.chosen_p.append((demo, p_type))
        
        self.test_procedure(proce, p_type)        
        for i in range(self.options['nb_iteration']):
            p = proce + np.random.uniform(-self.options['noise'], self.options['noise'], len(proce))
            self.chosen_p.append((p, p_type))
            self.test_procedure(p, p_type)
    
    def test_procedure(self, p, p_type):
        # Find closest combination of tasks recorded
        pro = self.la_dataset.refine_procedure(p, p_type)
        if len(pro) == 0:
            self.test_random_action()
        else:
            idy1 = pro[0]
            idy2 = pro[1]
            ide1 = self.la_dataset.p_spaces[p_type[0]][p_type[1]].y_spaces[0].ids[idy1]
            ide2 = self.la_dataset.p_spaces[p_type[0]][p_type[1]].y_spaces[1].ids[idy2]
            (a_type1, ida1) = self.la_dataset.idA[ide1]
            (a_type2, ida2) = self.la_dataset.idA[ide2]
            if a_type1[0] == a_type2[0]:
                a1 = self.la_dataset.a_spaces[a_type1[0]][a_type1[1]].data[ida1]
                a2 = self.la_dataset.a_spaces[a_type2[0]][a_type2[1]].data[ida2]
                a = np.array(a1.tolist() + a2.tolist())
                nb = self.la_dataset.a_spaces[a_type1[0]][a_type1[1]].n + \
                        self.la_dataset.a_spaces[a_type2[0]][a_type2[1]].n
                a_type = (a_type1[0], nb-1)
                self.test_action(a, a_type)
                proc_tested = np.array(self.la_dataset.p_spaces[p_type[0]][p_type[1]].y_spaces[0].data[idy1].tolist() + \
                        self.la_dataset.p_spaces[p_type[0]][p_type[1]].y_spaces[1].data[idy2].tolist())
                self.memory[-1][-1].append(proc_tested)
                self.memory[-1][-1].append(p_type)
            else:
                self.test_random_action()


class ProceduralTeacher(RandomProcedure):
    
    def __init__(self, environment, dataset, name, options, verbose=False):
        """
        environment EnvironmentV4: environment of the learning agent
        dataset DatasetV2: episodic memory of the agent using the strategy (needed for procedures)
        name string: name of the given strategy
        options dict: options of the strategy
        verbose boolean: indicates the level of debug info
        """
        RandomProcedure.__init__(self, environment, dataset, name, options, verbose=verbose)
        self.demos_given = []
        self.chosen_p = []
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        return self.random_procedure()
    
    def determine_random_goal(self, task):
        """Use by child classes to determine a procedure when wrong task space asked."""
        goal = []
        for i in range(self.dataset.y_spaces[task].dim):
            goal.append(random.uniform(self.dataset.y_spaces[task].bounds['min'][i], \
                self.dataset.y_spaces[task].bounds['max'][i]))
        return np.array(goal)
    
    def run_goal(self, goal, task):
        """Run one episode under the mimicry strategy."""
        
        # Initialization of the episode memory
        self.memory = []
        self.n = 0
        
        # Choice of a demonstration as adapted to goal as possible
        proc = self.choose_demo(goal, task)
        
        p_type = proc[1]
        demo = proc[0]
        self.demos_given.append((demo, p_type))
        self.chosen_p.append([])
        
        # Copy demo to make sure not changing it
        proce = copy.deepcopy(demo)
        self.chosen_p[-1].append((demo, p_type))
        
        self.test_procedure(proce, p_type)        
        for i in range(self.options['nb_iteration']):
            p = proce + np.random.uniform(-self.options['noise'], self.options['noise'], len(proce))
            self.chosen_p[-1].append((p, p_type))
            self.test_procedure(p, p_type)


def rule1(goal):
    p_type = (1, 0)
    pen = np.array([-0.3, 0.5, 1.0])
    return (np.array(pen.tolist() + goal.tolist()), p_type)


def rule2(goal):
    p_type = (1, 0)
    p1 = [goal[0], goal[1], 1.0]
    p2 = [goal[2], goal[3], -0.1]
    return (np.array(p1 + p2), p_type)


def rule4(goal):
    p_type = (4, 0)
    p1 = [0.0, 0.0, 0.0]
    p2 = goal/5.0 + np.array([-0.6, 0.4 , 0.6])
    return (np.array(p1 + p2.tolist()), p_type)


def rule3v2(goal):
    p_type = (3, 0)
    p1 = [0.0, 0.0, 0.0]
    p2 = goal/5.0 + np.array([-0.6, 0.4 , 0.6])
    return (np.array(p1 + p2.tolist()), p_type)


def rule5(goal):
    p_type = (5, 0)
    p1 = [0.0, 0.0, 0.0]
    p2 = goal/5.0 + np.array([-0.1, -0.5 , 0.6])
    return (np.array(p1 + p2.tolist()), p_type)


def rule4v2(goal):
    p_type = (4, 0)
    p1 = [0.0, 0.0, 0.0]
    p2 = goal/5.0 + np.array([-0.1, -0.5 , 0.6])
    return (np.array(p1 + p2.tolist()), p_type)


def rule6(goal):
    p_type = (4, 5)
    p1 = [goal[0], 0.0, 0.0]
    p2 = [0.0, goal[1], 0.0]
    return (np.array(p1 + p2), p_type)


def rule5v2(goal):
    p_type = (3, 4)
    p1 = [goal[0], 0.0, 0.0]
    p2 = [0.0, goal[1], 0.0]
    return (np.array(p1 + p2), p_type)


def rule6v2(goal):
    p_type = (5, 1)
    p1 = [goal[0], goal[1]]
    p2 = [goal[0], goal[1], -0.1]
    return (np.array(p1 + p2), p_type)


class ProceduralTeacher1(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher1(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 1:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(1)
        return rule1(real_goal)


class ProceduralTeacher2(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher2(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 2:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(2)
        return rule2(real_goal)


class ProceduralTeacher4(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher4(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 4:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(4)
        return rule4(real_goal)


class ProceduralTeacher5(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher5(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 5:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(5)
        return rule5(real_goal)


class ProceduralTeacher6(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher6(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 6:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(6)
        return rule6(real_goal)


class ProceduralTeacher3V2(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher3V2(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 3:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(3)
        return rule3v2(real_goal)


class ProceduralTeacher4V2(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher4V2(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 4:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(4)
        return rule4v2(real_goal)


class ProceduralTeacher5V2(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher5V2(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 5:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(5)
        return rule5v2(real_goal)


class ProceduralTeacher6V2(ProceduralTeacher):
    
    @classmethod
    def from_config(cls, dico, env, dataset_t, dataset_la):
        RandomActionV4.k = dico['k']
        return ProceduralTeacher6V2(env, dataset_la, dico['name'], dico['options'], dico['complex_actions'])
    
    def choose_demo(self, goal, task):
        if task == 6:
            real_goal = goal
        else:
            real_goal = self.determine_random_goal(6)
        return rule6v2(real_goal)

