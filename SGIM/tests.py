import numpy as np




##### Compute a circle dataset
R = 600
O = np.array([70., -70.])

thetas = np.arange(-np.pi/2.0, np.pi/2.0, np.pi/1000.0)

C = []
for t in thetas:
    P = O + R * np.array([np.cos(t), np.sin(t)])
    P = (P - np.array([542.5, 0.])) / np.array([342.5, 570.])
    if np.fabs(P[0]) <= 1.0 and np.fabs(P[1]) <= 1.0:
        C.append(P)

##### Compute testbenches

testbench0 = []
for x in np.linspace(-1, 1, num=20):
    for y in np.linspace(-1, 1, num=20):
        testbench0.append(np.array([x, y]))


##### Compute best ideal evaluation of task 0

def denorm(p):
    return p * np.array([342.5, 570.]) + np.array([542.5, 0.])

for p in testbench0: