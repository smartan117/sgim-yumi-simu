import zmq
import time
import json
import copy

from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')



class MLNode:
    
    def __init__(self):
        self.server = None
        self.client = None
        self.order = None
        self.result = None
    
    def connect(self):
        ctx = zmq.Context()
        self.server = ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5552")
        self.client = ctx.socket(zmq.REQ)
        self.client.connect("tcp://127.0.0.1:5551")
    
    def connectToReal(self):
        ctx = zmq.Context()
        self.server = ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5551")
        self.client = ctx.socket(zmq.REQ)
        self.client.connect("tcp://127.0.0.1:5555")
    
    def connectToSimu(self):
        ctx = zmq.Context()
        self.server = ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5551")
        self.client = ctx.socket(zmq.REQ)
        self.client.connect("tcp://127.0.0.1:5554")
    
    def analyze(self, msg):
        try:
            self.order = json.loads(msg)
            return (self.order.has_key("type") and self.order.has_key("valid") and self.order.has_key("payload"))
        except Exception:
            return False
    
    def analyze_ack(self, msg):
        try:
            order = json.loads(msg)
            return (order.has_key("type") and order.has_key("valid"))
        except Exception:
            return False
    
    def moveOrder(self, path, ts):
        return {'type': 'move', 'valid': True, 'payload': {'path': path, "dt": ts}, 'from': 1}
    
    def resetOrder(self, joints):
        return {'type': 'reset', 'valid': True, 'payload': {'joints': joints, 'dt': 0.01, 'dsNumerator': 1, 'dsDenominator': 4}, 'from': 1}
    
    def sendOrder(self, order):
        self.client.send(json.dumps(order))
        msg = self.client.recv()
        print("Ack: " + msg)
        return self.analyze_ack(msg)
    
    def waitForAnswer(self):
        msg = self.server.recv()
        print("Received: " + msg)
        res = self.analyze(msg)
        if res:
            ack = {"type": self.order['type'], "from": 1, "valid": True}
        else:
            ack = {"type": "error", "valid": False}
        print("Acking: " + str(ack))
        self.server.send(json.dumps(ack))
        return res
    
    def handle(self, order):
        print("Sending order: " + str(order))
        if self.sendOrder(order):
            return self.waitForAnswer():
        else:
            return False
    
    def sendMove(self, path, dt):
        if self.handle(self.moveOrder(path, dt)) and self.order['payload'].has_key('y_types') \
                and self.order['payload'].has_key('y_list') and self.order['payload'].has_key('joints'):
            self.result = self.order['payload']
            return True
        else:
            return False
    
    def sendReset(self, joints):
        if self.handle(self.ResetOrder(joints)) and self.order['payload'].has_key('y_types') \
                and self.order['payload'].has_key('y_list'):
            self.result = self.order['payload']
            return True
        else:
            return False



if __name__ == "__main__":
    node = MLNode()
    node.connect()
    
    # Add tests
    raw_input()
    node.handle(node.resetOrder([0., 0., 0., 0., 0., 0., 0.]))
    
    raw_input()
    node.handle(node.moveOrder([[1., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.resetOrder([0., 0., 0., 0., 0., 0., 0.]))
    
    raw_input()
    node.handle(node.moveOrder([[2., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.moveOrder([[3., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.resetOrder([0., 0., 0., 0., 0., 0., 0.]))
    
    raw_input()
    node.handle(node.moveOrder([[4., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.moveOrder([[-3., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.moveOrder([[5., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.moveOrder([[6., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.resetOrder([0., 0., 0., 0., 0., 0., 0.]))
    
    raw_input()
    node.handle(node.moveOrder([[7., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    
    
    
    
    
    
    
    
