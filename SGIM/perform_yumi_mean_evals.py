from yumi_control import *
from evaluation_multiprocesses import *
from visualize_bunches import *
from utils import *

import os
import matplotlib.pyplot as plt


"""
dico = {}
folder = "Yumi_Simus_Results_bin"

for fo in os.listdir(folder):
    k = fo[:-2]
    if fo[-2] != "_":
        k = fo[:-3]
    if not dico.has_key(k):
        dico[k] = []
    fi = folder + "/" + fo + "/eval_test"
    print("Loading " + fi)
    eva = load_bin(fi)
    dico[k].append(eva)

results = {}
for k in dico.keys():
    print("Combining evaluations for " + k)
    eva, t_list = combine_evaluations(dico[k])
    results[k] = [t_list, eva]

save_bin(results, "Yumi_Simus_Combined_Evals_08_14")
"""


results = load_bin("Yumi_Simus_Combined_Evals_08_14")

options = [{'color': 'k', 'marker': 'None', 'linestyle': 'solid'}, \
        {'color': 'r', 'marker': 'None', 'linestyle': 'solid'}, \
        {'color': 'g', 'marker': 'None', 'linestyle': 'solid'}, \
        {'color': 'b', 'marker': 'None', 'linestyle': 'solid'}, \
        {'color': 'm', 'marker': 'None', 'linestyle': 'solid'}]

plt.ion()

ax = plt.figure().add_subplot(111)
plt.show()
for i, k in enumerate(results.keys()):
    plot_combined_evaluation(results[k][1], results[k][0], ax, options[i])
plt.legend(results.keys())
plt.title('Global evaluation')
plt.draw()

for t in range(5):
    ax = plt.figure().add_subplot(111)
    plt.show()
    for i, k in enumerate(results.keys()):
        plot_combined_evaluation_task(results[k][1], results[k][0], t, ax, options[i])
    plt.legend(results.keys())
    plt.title('Task evaluation '+str(t))
    plt.draw()


raw_input()
