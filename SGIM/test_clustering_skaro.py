from skaro_experiment import *
from utils import load_raw
from sklearn.cluster import KMeans, DBSCAN
from scipy.spatial.distance import euclidean
import numpy as np
import copy
import matplotlib.pyplot as plt



###### Module functions

# Just for testing purposes
all_e = [0.01, 0.02, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5]
all_m = [500, 200, 100, 50, 20, 10, 5, 1]


def cluster(data):
    all_ind = np.array(range(len(data)))
    labels = - np.ones(len(data), dtype=np.int32)
    
    # Dirty hack to remove out of bounds duplicates
    ind = (data[:,0] > 1.0)
    labels[ind] = -2 * np.ones(len(labels[ind]), dtype=np.int32)
    
    i_e = 0
    i_m = 0
    max_label = 0
    while i_e < len(all_e) and i_m < len(all_m):
        m = all_m[i_m]
        e = all_e[i_e] * np.sqrt(4 * len(data[0]))
        
        ind = (labels == -1)
        
        if len(data[ind]) == 0:
            break
        
        clustering = DBSCAN(eps=e, min_samples=m)
        clusters = clustering.fit_predict(data[ind])
        
        max_label = max(labels) + 1
        good_ind = clusters != -1        
        ind[ind] = good_ind
        
        labels[ind] = clusters[good_ind] + max_label
        
        if i_m > i_e:
            i_e += 1
        else:
            i_m += 1
        print("Already did " + str(i_e + i_m) + " passes (/" + str(len(all_m) + len(all_e)) + ").")
    
    return labels


###### Load dataset

eva = load_raw("data/A3_SGIM_Full/data/test4")
dataset = eva.learning_agent.dataset


###### Extract initial context

arm = eva.learning_agent.environment.scene.arm
arm.set_angles(eva.learning_agent.environment.initial_pose)
arm.compute_end()
context = np.array(arm.joints[-1,0:2].tolist() + [0.5])

all_contexts = [context.tolist()]
for y_s in dataset.y_spaces[1:]:
    all_contexts.append([1000.0] * y_s.dim) 


###### Extract relevant data

contexts = []
policies = []
outcomes = []
states = []

tasks = [0, 1, 2]

context = []
for t in tasks:
    context += all_contexts[t]
context = np.array(context)

print context

c = copy.deepcopy(context)

episode = []
n_e = -1

for i in range(len(dataset.idA)):
    (a_type, ida) = dataset.idA[i]
    a = dataset.a_spaces[a_type[0]][a_type[1]].data[ida]
    a = a[(len(a)-13):len(a)]
    full_y = []
    for t in tasks:
        idy = dataset.y_id(i, t)
        y = [1000.0] * dataset.y_spaces[t].dim
        if idy >= 0:
            y = dataset.y_spaces[t].data[idy].tolist()
        full_y += y
    full_y = np.array(full_y)
    if a_type[1] == 0:
        n_e += 1
        states.append(context)
        episode.append(n_e)
        c = copy.deepcopy(context)
    contexts.append(copy.deepcopy(c))
    policies.append(a)
    outcomes.append(full_y)
    states.append(full_y)
    episode.append(n_e)
    c = full_y

contexts = np.array(contexts)
policies = np.array(policies)
outcomes = np.array(outcomes)
states = np.array(states)


###### Clusterize data

"""
# Clusterize each individually using k-means

n_c = 5
n_o = 5
n_p = 5

clustering_c = KMeans(n_clusters=n_c)
clustering_p = KMeans(n_clusters=n_p)
clustering_o = KMeans(n_clusters=n_o)
"""

"""
# Clusterize states and policies individually using DBSCAN

m_p = 200
m_s = 100

e_p = 0.05 * np.sqrt(48)
e_s = 0.05 * np.sqrt(12)

clustering_p = DBSCAN(eps=e_p, min_samples=m_p)
clustering_s = DBSCAN(eps=e_s, min_samples=m_s)


clusters_s = clustering_s.fit_predict(states)
print("Just finished clustering states!")

clusters_c = []
clusters_o = []
n_e = -1
i_c = 0
i_o = 0
for i in range(len(states)):
    if episode[i] > n_e: # Beginning of a new episode
        n_e = episode[i]
    else:
        clusters_c.append(clusters_s[i-1])
        clusters_o.append(clusters_s[i])


clusters_p = clustering_p.fit_predict(policies)
print("Just finished clustering policies!")

n_s = max(clusters_s)+1
n_c = n_s
n_o = n_s
n_p = max(clusters_p)+1
"""


data = np.hstack((contexts, policies, outcomes))
#data = np.hstack((contexts, outcomes))

"""
# Clustering whole using k-means

n = 10

clustering = KMeans(n_clusters=n)
clusters = clustering.fit_predict(data)
"""


# Clustering outcomes using DBSCAN

#labels = cluster(states) # All tasks
labels0 = cluster(outcomes[:,:3])
labels1 = cluster(outcomes[:,3:6])
labels2 = cluster(outcomes[:,6:])

labels_p = cluster(policies)

labels = np.hstack((labels0.reshape((len(labels0), 1)), labels1.reshape((len(labels1), 1)), labels2.reshape((len(labels2), 1))))

"""
e = 0.05 * np.sqrt(4 * len(outcomes[0]))
m = 50

clustering = DBSCAN(eps=e, min_samples=m)
clusters = clustering.fit_predict(outcomes)

n_w = max(clusters)+1
"""

"""
# Clustering policies using DBSCAN

e = 0.05 * np.sqrt(48)
m = 50

clustering_p = DBSCAN(eps=e, min_samples=m)
clusters_p = clustering_p.fit_predict(policies)

n_p = max(clusters_p)+1


# Check cluster resemblance

union = np.zeros((n_w+1, n_p+1))

for c_w, c_p in zip(clusters_w, clusters_p):
    union[c_w, c_p] += 1

np.set_printoptions(suppress=True)
"""


"""
n_ind = np.zeros(n+1)
for l in clusters:
    n_ind[l] += 1

centroids = np.zeros((n, len(data[0])))
for i in range(len(data)):
    l = clusters[i]
    if l >= 0:
        centroids[l,:] += data[i,:]
for i in range(len(centroids)):
    centroids[i,:] /= n_ind[i]
"""

"""
X = []
Y = []
Z = []

for i in range(len(clusters)):
    l = clusters[i]
    if l < 0:
        continue
    while len(X) <= l:
        X.append([])
        Y.append([])
        Z.append([])
    X[l].append(data[i,16])
    Y[l].append(data[i,17])
    Z[l].append(data[i,18])
    

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(len(X)):
    ax.plot(np.array(X[i]), np.array(Y[i]), np.array(Z[i]), ".")
plt.show()
"""


"""
for i in range(len(X)):
    plt.plot(np.array(X[i]), np.array(Y[i]), ".")
    plt.show()
"""


"""
arrows = np.hstack((centroids[:,0:2], centroids[:,14:16] - centroids[:,0:2]))

fig = plt.figure()
ax = fig.add_subplot(111)

for i in range(len(arrows)):
    ax.arrow(arrows[i,0], arrows[i,1], arrows[i,2], arrows[i,3])

ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)

plt.show()
"""


###### Test policy centroid computation through outcomes

"""
centroids = np.zeros((n_o, 12))
nn = np.zeros(n_o)

for i in range(len(clusters_o)):
    lo = clusters_o[i]
    centroids[lo,:] += policies[i]
    nn[lo] += 1

for i in range(len(nn)):
    centroids[i,:] /= nn[i]

clusters_p2 = []
nb_p_cluster = np.zeros(n_o, dtype=np.int32)
for i in range(len(policies)):
    l = -1
    min_d = float("inf")
    for j in range(len(centroids)):
        d = euclidean(policies[i,:], centroids[j,:])
        if d < min_d:
            min_d = d
            l = j
    clusters_p2.append(j)
    nb_p_cluster[j] += 1
"""


###### Compute state-policy graph

"""
edges = np.zeros((n_c, n_o, n_p), dtype=np.int32)

for lc, lp, lo in zip(clusters_c, clusters_p, clusters_o):
    if lc < 0 or lp < 0 or lo < 0:
        continue
    edges[lc, lo, lp] += 1
"""







