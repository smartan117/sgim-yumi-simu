from yumi_control import *
from evaluation_multiprocesses import *
from utils import *

import os



"""
### Evaluation of a learner
if __name__ == "__main__":
    import numpy as np
    from learning_agents import LearningAgentV4
    from evaluation_multiprocesses import EvaluationV4, LightWeightEvaluation
    from utils import load_raw, save_raw
    
    
    folder = "Yumi_SGIM_ACTS_4_1"
    testfile = "test"
    
    print("Evaluation of " + "[" + folder + "] " + testfile)
    
    la = None
    la = load_raw(folder + "/" + testfile)
    eva = EvaluationV4(la, SimuYumiExperimentatorV2.create_testbench())
    t_list = [0, 100, 500, 1000, 2000, 5000, 10000, 15000, 20000, 25000]
    
    print("Press Enter to start evaluation")
    raw_input()
    
    i = 0
    while i < len(t_list) and t_list[i] <= la.n_iter:
        eva.evaluate(t_list[i])
        print("Evaluated at iteration: " + str(t_list[i]))
        i += 1
    eva.evaluate(la.n_iter)
    
    save_progress(eva.get_lw(), folder, "eval_" + testfile)
    print("[" + folder + "] " + testfile + " evaluated")
"""

testbench = SimuYumiExperimentatorV2.create_testbench()
t_list = [0, 100, 500, 1000, 2000, 5000, 10000, 15000, 20000, 25000]

folder = "Yumi_Simus_Results"
out_folder = "Yumi_Simus_Results_bin"

for test in os.listdir(folder):
    if os.path.exists(out_folder + "/" + test + "/" + "eval_test"):
        continue
    fi = folder + "/" + test + "/test"
    print("Loading test file from " + test)
    la = load_raw(fi)
    #print la.environment
    eva = EvaluationV4(la, testbench)
    print("Evaluating test file...")
    i = 0
    while i < len(t_list) and t_list[i] <= la.n_iter:
        eva.evaluate(t_list[i])
        print("Evaluated at iteration: " + str(t_list[i]))
        i += 1
    eva.evaluate(la.n_iter)
    print("Saving results...")
    os.makedirs(out_folder + "/" + test + "/")
    save_bin(eva.get_lw(), out_folder + "/" + test + "/" + "eval_test")
    

