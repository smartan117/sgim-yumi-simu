import math
import pypot.robot
import numpy as np
import copy

# Working with the simulated version
import pypot.vrep
import pypot.utils.pypot_time as ptime
from pypot.primitive import LoopPrimitive
from pypot.primitive.move import MoveRecorder, Move, MovePlayer
from DMP import DynamicSystem



class MovementPrimitive(LoopPrimitive):
    """Implements a primitive sending commands to the robot at a given timestep."""

    def __init__(self, robot, move, dt, freq):
        """
        robot pypot.robot.Robot: robot on which to apply the primitive
        move float array dict: angles list of the motion for each joint
        dt float: timestep of the motion
        freq float: half frequency of the primitive
        """

        LoopPrimitive.__init__(self, robot, freq*2.)
        self.motors = move.keys()
        self.move = move
        self.duration = dt * len(move[self.motors[0]])
        self.timestep = dt

    def update(self):
        """Send the commands to the motors."""

        # Check whether the motion ends
        if self.elapsed_time > self.duration:
            self.stop(wait=False)
            return

        # Check the motion stage
        i = int(self.elapsed_time / self.timestep)

        if i < len(self.move[self.motors[0]]):
            # Send the commands to each joint
            for k in self.motors:
                motor = getattr(self.robot, k)
                motor.compliant = False
                motor.goal_position = self.move[k][i]


class DynamicMovementPrimitive(MovementPrimitive):
    """Implements a DMP to be played on the robot by integrating it beforehand."""

    def __init__(self, robot, dmp, duration, dt, freq):
        """
        robot pypot.robot.Robot: robot on which to apply the primitive
        dmp DynamicSystem: the trained DMP to be executed
        duration float: the duration of the whole motion
        dt float: timestep of the motion
        freq float: half frequency of the primitive
        """

        self.dmp = copy.deepcopy(dmp)
        self.dmp.tau = duration
        move = self.dmp.integrate(dt,tf=duration)[1]
        MovementPrimitive.__init__(self, robot, move, dt, freq)


class ExecutableDynamicMovementPrimitive(LoopPrimitive):
    """Implements a DMP being integrated in real time."""

    def __init__(self, robot, dmp, duration, freq):
        """
        robot pypot.robot.Robot: robot on which to apply the primitive
        dmp DynamicSystem: the trained DMP to be executed
        duration float: the duration of the whole motion
        freq float: frequency of the primitive
        """

        LoopPrimitive.__init__(self, robot, freq)
        self.duration = duration
        self.dmp = copy.deepcopy(dmp)
        self.dmp.reset()
        self.t = 0.

    def update(self):
        """Update the state of the DMP and the motors goals."""

        # Check whether the motion is over
        if self.elapsed_time > self.duration:
            self.stop(wait=False)
            return

        dt = self.elapsed_time - self.t
        self.t = self.elapsed_time

        self.dmp.step(dt)
        self.send_commands()

    def send_commands(self):
        """Send the commands corresponding to the current state of the DMP."""

        for m in self.dmp.ts.keys():
            motor = getattr(self.robot, m)
            motor.goal_position = self.dmp.ts[m].y
     

class SafetyModule(LoopPrimitive):
    """Module checking safety constraints for the robot."""

    def __init__(self, robot, freq, safety_constraints):
        LoopPrimitive.__init__(self, robot, freq)
        self.safety_constraints = safety_constraints
        self.over_heating = {}
        self.reset()

    def reset(self):
        """Setup safety measures."""
        for m in self.safety_constraints.keys():
            motor = getattr(self.robot, m)
            # Constraining maximum speed to prevent dangerous collision
            motor.moving_speed = self.safety_constraints[m]['max_speed']
            self.over_heating[m] = [0, False, False]  # Last over load time and if overloaded at last frame

    def update(self):
        """Override the update method to apply safety measures."""
        self.apply_safety_commands()

    def apply_safety_commands(self):
        """Apply safety measures: freeze joint when forcing for too much time."""

        for m in self.safety_constraints.keys():
            motor = getattr(self.robot, m)
            motor.moving_speed = self.safety_constraints[m]['max_speed']
            # Check load to prevent overheating of motor
            if self.over_heating[m][2]:  # Joint frozen
                motor.compliant = True
            elif math.fabs(motor.present_load) > self.safety_constraints[m]['max_load']:  # Joint overloaded
                if not self.over_heating[m][1]:
                    self.over_heating[m][0] = ptime.time()
                    self.over_heating[m][1] = True
                elif self.over_heating[m][1] and (ptime.time() - self.over_heating[m][0]) >  \
                        self.safety_constraints[m]['max_time']:
                    self.over_heating[m][2] = True                    
            else:
                self.over_heating[m][1] = False
                motor.compliant = False


#class SafeDynamicMovementPrimitive(ExecutableDynamicMovementPrimitive):
#    """Wrap the DMP into a safety module to check load and max speed."""
#
#    def __init__(self, robot, dmp, safety_constraints, duration, freq):
#        """
#        robot pypot.robot.Robot: robot on which to apply the primitive
#        dmp DynamicSystem: the trained DMP to be executed
#        safety_constraints: dict with motor as keys containing dict with 'max_speed', 'max_load' and
#            'max_time'
#        duration float: the duration of the whole motion
#        freq float: frequency of the primitive
#        """
#
#        ExecutableDynamicMovementPrimitive.__init__(robot, dmp, duration, freq)
#        self.safety_constraints = safety_constraints
#        self.over_heating = {}
#        self.send_commands = self.apply_safety_commands
#        self.setup()
#
#    def setup(self):
#        """Setup safety measures."""
#        for m in self.dmp.ts.keys():
#            motor = getattr(self.robot, m)
#            # Constraining maximum speed to prevent dangerous collision
#            motor.moving_speed = self.safety_constraints[m]['max_speed']
#            motor.compliant = False
#            self.over_heating[m] = [0, False, False]  # Last over load time and if overloaded at last frame
#
#    def apply_safety_commands(self):
#        """Apply safety measures and send commands: freeze joint when forcing for too much time."""
#
#        for m in self.dmp.ts.keys():
#            motor = getattr(self.robot, m)
#            # Check load to prevent overheating of motor
#            if self.over_heating[m][2]:  # Joint frozen
#                motor.compliant = True
#            elif fabs(motor.present_load) > self.safety_constraints[m]['max_load']:  # Joint overloaded
#                if not self.over_heating[m][1]:
#                    self.over_heating[m][0] = ptime.time()
#                    self.over_heating[m][1] = True
#                elif self.over_heating[m][1] and (ptime.time() - self.over_heating[m][0]) >  \ 
#                        self.safety_constraints[m]['max_time']:
#                    self.over_heating[m][2] = True
#                motor.goal_position = self.dmp.ts[m].y
#            else:
#                self.over_heating[m][1] = False
#                motor.goal_position = self.dmp.ts[m].y


class MotionRecorder():
    """Wrapper for the MoveRecorder class to make it handier."""

    def __init__(self, robot, motors, duration, freq):
        """
        robot pypot.robot.Robot: robot on which to apply the primitive
        motors string list: list of the joint names
        duration int: duration of the motion
        freq float: frequency of the primitive
        """
             
        self.duration = duration
        self.motors = []
        self.robot = robot
        for m in motors:
            motor = getattr(self.robot, m)
            self.motors.append(motor)
        self.mr = MoveRecorder(robot, freq, self.motors)

    def setup(self):
        """Make sure all joints considered are compliant."""

        self.robot.compliant = False     
        for m in self.motors:
            m.compliant = True

    def run(self, countdown=True):
        """Record the motion.

        countdown bool: printing or not the countdown of the recording on the screen
        """

        # Make all involved joints compliant
        self.setup()

        # Start the motion recording
        self.mr.start()

        dt = 1.0    # The duration of the motion will be in seconds
        i = 0

        while i < self.duration:
            if countdown:
                print self.duration - i
            i += 1
            ptime.sleep(dt)  

        # Stop the recording when time is up          
        self.mr.stop()

        # Freeze all involved joints
        self.robot.compliant = False

    def get_move(self):

        return self.mr.move

    def save_as(self, name):

        with open(name, 'w') as f:
            self.mr.move.save(f)


def move2array(move):
    """Translate a Move object to something handier."""

    time = move.positions().keys()
    positions = {}
    time_f = []
    motors = move[0][1].keys()
    for t in time:
        time_f.append(float(t))
    l = move2sortedmove(time_f)
    time_list = np.array(time_f)[l]
    for m in motors:
        pt = []
        for t in time:    
            pt.append(move.positions()[t][m][0])
        positions[m] = np.array(pt)[l]

    return (motors,time_list,positions)


def poppy2origin(robot):
    """Set robot motors to zero.

    robot pypot.robot.Robot: robot on which to apply commands
    """

    for m in robot.motors:
        m.compliant = False
        m.goto_position(0, 2)


def move2sortedmove(t):
    """Return the bubble-sorted time list given."""

    l = range(len(t))
    i = len(t)
    swap = True
    while i > 0 and swap:
        swap = False
        for j in range(0,i-1):
            if float(t[l[j]]) > float(t[l[j+1]]):
                tmp = l[j]
                l[j] = l[j+1]
                l[j+1] = tmp
                swap = True
        i = i-1
    return l




if __name__ == "__main__":

    from DMP import CanonicalSystem, TransformationSystem
    import json
    import poppy_config as pc

    # Load the motion recorded in the file
    move_file = '/home/nduminy/Programmation/Poppy/Moves/yes_sir.txt'
    with open(move_file) as f:
         mo = Move.load(f)

    # Translate the motion in arrays
    joints, t, move = move2array(mo)

    # Initialize the canonical system and the DMP
    cs = CanonicalSystem()
    dmp = DynamicSystem(cs=cs,tau=4.0)

    # Initialize the transformation systems
    for i in range(len(joints)):
        ts = TransformationSystem(joints[i],cs=cs,n_bfs=5)
        dmp.add_dmp(ts)

    # Train the DMP as a whole
    dmp.train(t, move)

    # Save the DMP in a compact JSON format
    move_json = '/home/nduminy/Programmation/Poppy/Moves/yes_sir.json'
    with open(move_json, 'w') as f:
        json.dump(dmp.to_json(), f, sort_keys=True, indent=4, separators=(',', ': '))

    # Load Poppy on V-REP
    json_file = "/home/nduminy/Programmation/Poppy/poppy-humanoid/software/poppy_humanoid/configuration/poppy_humanoid.json"
    with open(json_file) as f:
        config = json.load(f)
    robot = pypot.vrep.from_vrep(config,'localhost',19997,"/home/nduminy/Programmation/Poppy/poppy_humanoid.ttt")
    pc.vrep_hack(robot)

    """
    # Execute the DMP priorly integrated
    mp = DynamicMovementPrimitive(robot, dmp, 5.0, 0.01, 50)
    mp.start()

    ptime.sleep(6)

    # Execute the DMP integrated in real time
    mp = ExecutableDynamicMovementPrimitive(robot, dmp, 5.0, 50)
    mp.start()

    ptime.sleep(6)
    """

    # Load the trained DMP from JSON file
    with open(move_json) as f:
        dmp = DynamicSystem.from_json(json.load(f))

    # Execute the DMP integrated in real time
    mp = ExecutableDynamicMovementPrimitive(robot, dmp, 5.0, 50)
    mp.start()

    ptime.sleep(6)

    
    



