MODULE FileTransfer (SYSMODULE)
    !***********************************************************
    !
    ! Module:  FileTransfer
    !
    ! Description:
    !   Contains function to exchange data using files with PC
    !
    ! Author: Nicolas Duminy
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    PROC writePathM(string fileName, robtarget data{*}, \num Size)
        VAR iodev stream;
        VAR num index;
        VAR num length;
        length := Dim(data, 1);
        IF Present(Size) THEN
            length := Size;
        ENDIF
        ! Use "TEMP:" instead of "HOME:"
        Open "HOME:" \File:=fileName, stream, \Write;
        FOR index FROM 1 TO length DO
            Write stream, ValToStr(data{index});
        ENDFOR
    ENDPROC
    
    PROC writePathMSimplified(string fileName, robtarget data{*}, \num Size)
        VAR iodev stream;
        VAR num index;
        VAR num length := Dim(data, 1)
        IF Present(Size) THEN
            length := Size;
        ENDIF
        ! Use "TEMP:" instead of "HOME:"
        Open "HOME:" \File:=fileName, stream, \Write;
        FOR index FROM 1 TO length DO
            VAR num line{7};
            line{1} := data{index}.trans.x;
            line{2} := data{index}.trans.y;
            line{3} := data{index}.trans.z;
            line{4} := data{index}.rot.q1;
            line{5} := data{index}.rot.q2;
            line{6} := data{index}.rot.q3;
            line{7} := data{index}.rot.q4;
            Write stream, ValToStr(line);
        ENDFOR
    ENDPROC
    
    PROC readPathM(string fileName, inout robtarget data{*}, inout num size)
        VAR iodev stream;
        VAR num index := 1;
        VAR string plop := "";
        VAR bool ok;
        Open "HOME:" \File:=fileName, stream, \Read;
        WHILE plop <> "EOF" and index <= Dim(data, 1) DO
            plop := ReadStr(stream);
            ok := StrToVal(plop, data{index});
            size := index;
            index := index + 1;
        ENDWHILE        
    ENDPROC
    
    PROC readPathMSimplified(string fileName, inout robtarget data{*}, inout num size)
        VAR iodev stream;
        VAR num index := 1;
        VAR string plop := "";
        VAR bool ok;
        Open "HOME:" \File:=fileName, stream, \Read;
        WHILE plop <> "EOF" and index <= Dim(data, 1) DO
            VAR num line{7};
            VAR robtarget p;
            plop := ReadStr(stream);
            ok := StrToVal(plop, line);
            p.trans.x := line{1};
            p.trans.y := line{2};
            p.trans.z := line{3};
            p.rot.q1 := line{4};
            p.rot.q2 := line{5};
            p.rot.q3 := line{6};
            p.rot.q4 := line{7};
            data{index} := p;
            size := index;
            index := index + 1;
        ENDWHILE        
    ENDPROC
    
    PROC writePathJ(string fileName, jointtarget data{*}, \num Size)
        VAR iodev stream;
        VAR num index;
        VAR num length;
        length := Dim(data, 1);
        IF Present(Size) THEN
            length := Size;
        ENDIF
        ! Use "TEMP:" instead of "HOME:"
        Open "HOME:" \File:=fileName, stream, \Write;
        FOR index FROM 1 TO length DO
            Write stream, ValToStr(data{index});
        ENDFOR
    ENDPROC
    
    PROC writePathJSimplified(string fileName, jointtarget data{*}, \num Size)
        VAR iodev stream;
        VAR num index;
        VAR num length := Dim(data, 1)
        IF Present(Size) THEN
            length := Size;
        ENDIF
        ! Use "TEMP:" instead of "HOME:"
        Open "HOME:" \File:=fileName, stream, \Write;
        FOR index FROM 1 TO length DO
            VAR num line{7};
            line{1} := data{index}.robax.rax_1;
            line{2} := data{index}.robax.rax_2;
            line{3} := data{index}.robax.rax_3;
            line{4} := data{index}.robax.rax_4;
            line{5} := data{index}.robax.rax_5;
            line{6} := data{index}.robax.rax_6;
            line{7} := data{index}.extax.eax_a;
            Write stream, ValToStr(line);
        ENDFOR
    ENDPROC
    
    PROC readPathJ(string fileName, inout jointtarget data{*}, inout num size)
        VAR iodev stream;
        VAR num index := 1;
        VAR string plop := "";
        VAR bool ok;
        Open "HOME:" \File:=fileName, stream, \Read;
        WHILE plop <> "EOF" and index <= Dim(data, 1) DO
            plop := ReadStr(stream);
            ok := StrToVal(plop, data{index});
            size := index;
            index := index + 1;
        ENDWHILE        
    ENDPROC

ENDMODULE