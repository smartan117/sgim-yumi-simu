MODULE TestModule
    !***********************************************************
    !
    ! Module:  TestModule
    !
    ! Description:
    !   Skeleton module to control a YuMi robot
    !
    ! Author: Nicolas Duminy
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    VAR jointtarget traj{1000};
    VAR num plops{7000};
    VAR num size := 1000;
    VAR rawbytes data{100};
    VAR string fileName := "test.data";
    VAR num t1;
    VAR num t2;
    VAR num t11;
    VAR num t22;
    VAR num t111;
    VAR num t222;
    VAR clock clk;
    VAR num timeWrite;
    VAR num timeRead;
    
    PROC main()
        VAR num index;
        FOR index FROM 0 TO (size-1) DO
            plops{index*7+1} := index + 0.0;
            plops{index*7+2} := index + 0.1;
            plops{index*7+3} := index + 0.2;
            plops{index*7+4} := index + 0.3;
            plops{index*7+5} := index + 0.4;
            plops{index*7+6} := index + 0.5;
            plops{index*7+7} := index + 0.6;
            traj{index+1}.robax.rax_1 := index+1 + 0.0;
            traj{index+1}.robax.rax_2 := index+1 + 0.1;
            traj{index+1}.robax.rax_3 := index+1 + 0.2;
            traj{index+1}.robax.rax_4 := index+1 + 0.3;
            traj{index+1}.robax.rax_5 := index+1 + 0.4;
            traj{index+1}.robax.rax_6 := index+1 + 0.5;
            traj{index+1}.extax.eax_a := index+1 + 0.6;
        ENDFOR
        !t1 := plops{708};
        !t2 := plops{4277};
        t1 := traj{12}.robax.rax_3;
        t2 := traj{997}.extax.eax_a;
        ClkReset clk;
        ClkStart clk;
        writeData;
        ClkStop clk;
        timeWrite := ClkRead(clk);
        Stop;
        FOR index FROM 1 TO 7000 DO
            plops{index} := 0;
        ENDFOR
        FOR index FROM 1 TO 1000 DO
            traj{index}.robax.rax_1 := 0.0;
            traj{index}.robax.rax_2 := 0.0;
            traj{index}.robax.rax_3 := 0.0;
            traj{index}.robax.rax_4 := 0.0;
            traj{index}.robax.rax_5 := 0.0;
            traj{index}.robax.rax_6 := 0.0;
            traj{index}.extax.eax_a := 0.0;
        ENDFOR
        !t111 := plops{708};
        !t222 := plops{4277};
        t111 := traj{12}.robax.rax_3;
        t222 := traj{997}.extax.eax_a;
        Stop;
        ClkReset clk;
        ClkStart clk;
        readData;
        ClkStop clk;
        timeRead := ClkRead(clk);
        !t11 := plops{708};
        !t22 := plops{4277};
        t11 := traj{12}.robax.rax_3;
        t22 := traj{997}.extax.eax_a;
        Stop;
    ENDPROC
    
    LOCAL FUNC bool unpack()
        VAR num index;
        VAR num buf_num := 1;
        VAR num already := 0;
        ! 4 bytes per num
        !UnpackRawBytes data, 1, size, \IntX := DINT;
        IF size > 1000 THEN
            RETURN false;
        ENDIF
        FOR index FROM 0 TO (size-1) DO
            if RawBytesLen(data{buf_num}) > 996 THEN
                buf_num := buf_num + 1;
                already := index;
            ENDIF
            UnpackRawBytes data{buf_num}, 28*(index-already) + 1, traj{index+1}.robax.rax_1, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 5, traj{index+1}.robax.rax_2, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 9, traj{index+1}.robax.rax_3, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 13, traj{index+1}.robax.rax_4, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 17, traj{index+1}.robax.rax_5, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 21, traj{index+1}.robax.rax_6, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 25, traj{index+1}.extax.eax_a, \Float4;
        ENDFOR
        RETURN true;
    ENDFUNC
    
    LOCAL PROC pack()
        VAR num index;
        VAR num buf_num := 1;
        ClearRawBytes data{buf_num};
        !PackRawBytes size, data{buf_num}, 1, \IntX := DINT;
        FOR index FROM 1 TO size DO
            if RawBytesLen(data{buf_num}) > 996 THEN
                buf_num := buf_num + 1;
                ClearRawBytes data{buf_num};
            ENDIF
            PackRawBytes traj{index}.robax.rax_1, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_2, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_3, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_4, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_5, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_6, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.extax.eax_a, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
        ENDFOR
        RETURN;
    ENDPROC
    
    ! TO DO Investigate
    LOCAL PROC writeOnFile()
        VAR iodev stream;
        VAR num index;
        ! Use "TEMP:" instead of "HOME:"
        Open "HOME:" \File:=fileName, stream, \Write;
        FOR index FROM 1 TO 7000 DO
            Write stream, ValToStr(plops{index});
        ENDFOR
    ENDPROC
    
    LOCAL PROC readFromFile()
        VAR iodev stream;
        VAR num index;
        Open "HOME:" \File:=fileName, stream, \Read;
        FOR index FROM 1 TO 7000 DO
            plops{index} := ReadNum(stream);
        ENDFOR        
    ENDPROC
    
    LOCAL PROC writeData()
        VAR iodev stream;
        VAR num index;
        ! Use "TEMP:" instead of "HOME:"
        Open "HOME:" \File:=fileName, stream, \Write;
        FOR index FROM 1 TO size DO
            Write stream, ValToStr(traj{index});
        ENDFOR
    ENDPROC
    
    LOCAL PROC readData()
        VAR iodev stream;
        VAR num index;
        VAR string plop;
        VAR bool ok;
        Open "HOME:" \File:=fileName, stream, \Read;
        FOR index FROM 1 TO size DO
            plop := ReadStr(stream);
            ok := StrToVal(plop, traj{index});
        ENDFOR        
    ENDPROC
    
ENDMODULE