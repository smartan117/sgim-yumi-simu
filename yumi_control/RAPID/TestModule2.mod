MODULE TestModule2
    !***********************************************************
    !
    ! Module:  TestModule2
    !
    ! Description:
    !   Skeleton module to control a YuMi robot
    !
    ! Author: Nicolas Duminy
    !
    ! Version: 1.0
    !
    !***********************************************************
	
	! Different constants used for motion control

    CONST num defaultSize := 260;
	CONST zonedata passByZone := z10;
	
	PERS num timestep := 0.02;
    VAR num duration := 2.0;
	
	VAR robtarget path{defaultSize};
    VAR robtarget goal;
	VAR num finalSize;
	VAR num curSize := 0;
	
	VAR intnum out_zonenum;
	
	VAR shapedata volume;
	CONST pos corner1 := [-1000,-1000,60];
	CONST pos corner2 := [1000,1000,1000];
	VAR wztemporary forbidden_space;
	
	VAR num exec_status := 0;
	! 0 means reset
	! 1 means initialization complete
	! i means before move i
	
	
	PROC main()
		init;
		exec_status := 1;
		WHILE TRUE DO
			Stop;
			goToPoseInterpolatedJ;
			exec_status := exec_status + 1;
		ENDWHILE
        goToPose;
	ENDPROC
	
	PROC init()
		! Declare world zones
		WZBoxDef \Outside, volume, corner1, corner2;
		WZLimSup \Temp, forbidden_space, volume;
		WZEnable forbidden_space;
		RETURN;
	ENDPROC
	
	PROC goToPoseInterpolated()
		VAR num index := 1;
        VAR robtarget start;
        VAR robtarget curTarget;
        VAR num steps;
        VAR num dx;
        VAR num dy;
        VAR num dz;
        VAR num dq1;
        VAR num dq2;
        VAR num dq3;
        VAR num dq4;
        VAR num q1;
        VAR num q2;
        VAR num q3;
        VAR num q4;
        VAR num nq;
        goal.extax.eax_a := -160.0;
        steps := duration/timestep;
        start := CRobT(\Tool:=VaccumOne);
        curTarget := start;
        dx := (goal.trans.x - start.trans.x)/steps;
        dy := (goal.trans.y - start.trans.y)/steps;
        dz := (goal.trans.z - start.trans.z)/steps;
        dq1 := (goal.rot.q1 - start.rot.q1)/steps;
        dq2 := (goal.rot.q2 - start.rot.q2)/steps;
        dq3 := (goal.rot.q3 - start.rot.q3)/steps;
        dq4 := (goal.rot.q4 - start.rot.q4)/steps;
        ConfJ\Off;
		WHILE index < steps DO
            curTarget.trans.x := start.trans.x + index * dx;
            curTarget.trans.y := start.trans.y + index * dy;
            curTarget.trans.z := start.trans.z + index * dz;
            q1 := start.rot.q1 + index * dq1;
            q2 := start.rot.q2 + index * dq2;
            q3 := start.rot.q3 + index * dq3;
            q4 := start.rot.q4 + index * dq4;
            nq := Sqrt(q1*q1 + q2*q2 + q3*q3 + q4*q4);
            curTarget.rot.q1 := q1/nq;
            curTarget.rot.q2 := q2/nq;
            curTarget.rot.q3 := q3/nq;
            curTarget.rot.q4 := q4/nq;
            MoveJ curTarget, v1000, \T:=timestep, passByZone, VaccumOne;
            index := index + 1;
		ENDWHILE
		MoveJ goal, v1000, \T:=timestep, fine, VaccumOne;
        RETURN;
	ENDPROC
    
    PROC goToPose()
        !ConfJ \Off;
        goal.extax.eax_a := -160.0;
        ConfJ\Off;
        MoveJ goal, v1000, \T:=timestep, fine, VaccumOne;
        RETURN;
    ENDPROC
    
    PROC goToPoseInterpolatedJ()
		VAR num index := 1;
        VAR robtarget start;
        VAR robtarget curTarget;
        VAR jointtarget target;
        VAR num steps;
        VAR num dx;
        VAR num dy;
        VAR num dz;
        VAR num dq1;
        VAR num dq2;
        VAR num dq3;
        VAR num dq4;
        VAR num q1;
        VAR num q2;
        VAR num q3;
        VAR num q4;
        VAR num nq;
        goal.extax.eax_a := -160.0;
        steps := duration/timestep;
        start := CRobT(\Tool:=VaccumOne);
        curTarget := start;
        dx := (goal.trans.x - start.trans.x)/steps;
        dy := (goal.trans.y - start.trans.y)/steps;
        dz := (goal.trans.z - start.trans.z)/steps;
        dq1 := (goal.rot.q1 - start.rot.q1)/steps;
        dq2 := (goal.rot.q2 - start.rot.q2)/steps;
        dq3 := (goal.rot.q3 - start.rot.q3)/steps;
        dq4 := (goal.rot.q4 - start.rot.q4)/steps;
        ConfJ \Off;
		WHILE index < steps DO
            curTarget.trans.x := curTarget.trans.x + dx;
            curTarget.trans.y := curTarget.trans.y + dy;
            curTarget.trans.z := curTarget.trans.z + dz;
            q1 := curTarget.rot.q1 + dq1;
            q2 := curTarget.rot.q2 + dq2;
            q3 := curTarget.rot.q3 + dq3;
            q4 := curTarget.rot.q4 + dq4;
            nq := Sqrt(q1*q1 + q2*q2 + q3*q3 + q4*q4);
            curTarget.rot.q1 := q1/nq;
            curTarget.rot.q2 := q2/nq;
            curTarget.rot.q3 := q3/nq;
            curTarget.rot.q4 := q4/nq;
            target := CalcJointT(curTarget, VaccumOne);
            curTarget := CalcRobT(target, VaccumOne);
            MoveAbsJ target, v1000, \T:=timestep, passByZone, VaccumOne;
            index := index + 1;
		ENDWHILE
        target := CalcJointT(goal, VaccumOne);
        MoveAbsJ target, v1000, \T:=timestep, fine, VaccumOne;
        RETURN;
	ENDPROC
	
ENDMODULE
