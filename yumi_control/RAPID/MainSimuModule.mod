MODULE MainSimuModule
    !***********************************************************
    !
    ! Module:  MainSimuModule
    !
    ! Description:
    !   Skeleton module to control a simulated YuMi robot
    !
    ! Author: Nicolas Duminy
    !
    ! Version: 1.0
    !
    !***********************************************************
	! Different constants used for motion control
	CONST num defaultSize := 260;
	CONST zonedata passByZone := z10;
	
	PERS num timestep := 0.02;
	
	VAR jointtarget path{defaultSize};
	VAR robtarget finalPose;
    VAR jointtarget finalJoints;
	VAR num finalSize;
	VAR num curSize := 0;
	
	VAR intnum out_zonenum;
	
	VAR shapedata volume;
	CONST pos corner1 := [-1000,-1000,60];
	CONST pos corner2 := [1000,1000,1000];
	VAR wztemporary forbidden_space;
	
	VAR num exec_status := 0;
	! 0 means reset
	! 1 means initialization complete
	! i means before move i
	
    PERS string pathFileName := "path.data";
    
	PERS bool startsync := FALSE;
	PERS bool stopsync := TRUE;
	
	PROC main()
		init;
		exec_status := 1;
		WHILE TRUE DO
			Stop;
            readPathJSimplified pathFileName, path, finalSize;
			easyPathExecute;
            IF IsFile(("HOME:/" + pathFileName)) THEN
                removeFile ("HOME:/" + pathFileName);
            ENDIF
            !executePath;
			exec_status := exec_status + 1;
		ENDWHILE
	ENDPROC
	
	PROC init()
        IF IsFile(("HOME:/" + pathFileName)) THEN
            removeFile ("HOME:/" + pathFileName);
        ENDIF
		! Declare world zones
		WZBoxDef \Outside, volume, corner1, corner2;
		WZLimSup \Temp, forbidden_space, volume;
		WZEnable forbidden_space;
		RETURN;
	ENDPROC
	
	PROC executePath()
		VAR num index := 1;
		startsync := TRUE;
		stopsync := FALSE;
		WHILE index < finalSize DO
			IF curSize >= index THEN
				MoveAbsJ path{index}, v1000, \T:=timestep, passByZone, VaccumOne;
				index := index + 1;
			ENDIF
		ENDWHILE
		MoveAbsJ path{curSize}, v1000, \T:=timestep, fine, VaccumOne;
		finalPose := CRobT(\Tool:=VaccumOne);
        finalJoints := CJointT();
		startsync := FALSE;
		stopsync := TRUE;
	ENDPROC
    
    PROC easyPathExecute()
        VAR num index := 1;
		startsync := TRUE;
		stopsync := FALSE;
		WHILE index < finalSize DO
            MoveAbsJ path{index}, v1000, \T:=timestep, passByZone, VaccumOne;
            index := index + 1;
		ENDWHILE
		MoveAbsJ path{finalSize}, v1000, \T:=timestep, fine, VaccumOne;
		finalPose := CRobT(\Tool:=VaccumOne);
        finalJoints := CJointT();
		startsync := FALSE;
		stopsync := TRUE;
    ENDPROC
    
	
ENDMODULE