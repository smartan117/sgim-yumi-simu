MODULE TestModule
    !***********************************************************
    !
    ! Module:  TestModule
    !
    ! Description:
    !   Skeleton module to control a YuMi robot
    !
    ! Author: Nicolas Duminy
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    VAR jointtarget traj{1000};
    VAR num size := 1000;
    VAR rawbytes data{100};
    
    PROC main()
        VAR clock clk;
        VAR bool ok;
        VAR num t0;
        VAR num t1;
        ClkReset clk;
        ClkStart clk;
        pack;
        ClkStop clk;
        t0 := ClkRead(clk);
        ClkReset clk;
        ClkStart clk;
        ok := unpack();
        ClkStop clk;
        t1 := ClkRead(clk);
        Stop;
    ENDPROC
    
    LOCAL FUNC bool unpack()
        VAR num index;
        VAR num buf_num := 1;
        VAR num already := 0;
        ! 4 bytes per num
        !UnpackRawBytes data, 1, size, \IntX := DINT;
        IF size > 1000 THEN
            RETURN false;
        ENDIF
        FOR index FROM 0 TO (size-1) DO
            if 28*(index+1-already) > RawBytesLen(data{buf_num}) THEN
                buf_num := buf_num + 1;
                already := index;
            ENDIF
            UnpackRawBytes data{buf_num}, 28*(index-already) + 1, traj{index+1}.robax.rax_1, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 5, traj{index+1}.robax.rax_2, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 9, traj{index+1}.robax.rax_3, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 13, traj{index+1}.robax.rax_4, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 17, traj{index+1}.robax.rax_5, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 21, traj{index+1}.robax.rax_6, \Float4;
            UnpackRawBytes data{buf_num}, 28*(index-already) + 25, traj{index+1}.extax.eax_a, \Float4;
        ENDFOR
        Stop;
        RETURN true;
    ENDFUNC
    
    LOCAL PROC pack()
        VAR num index;
        VAR num buf_num := 1;
        ClearRawBytes data{buf_num};
        !PackRawBytes size, data{buf_num}, 1, \IntX := DINT;
        FOR index FROM 1 TO size DO
            if RawBytesLen(data{buf_num}) > 996 THEN
                buf_num := buf_num + 1;
                ClearRawBytes data{buf_num};
            ENDIF
            PackRawBytes traj{index}.robax.rax_1, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_2, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_3, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_4, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_5, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.robax.rax_6, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
            PackRawBytes traj{index}.extax.eax_a, data{buf_num}, (RawBytesLen(data{buf_num})+1), \Float4;
        ENDFOR
        Stop;
        RETURN;
    ENDPROC
    
ENDMODULE