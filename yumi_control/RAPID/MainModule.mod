MODULE MainModule
    !***********************************************************
    !
    ! Module:  MainModule
    !
    ! Description:
    !   Skeleton module to control a YuMi robot
    !
    ! Author: Nicolas Duminy
    !
    ! Version: 1.0
    !
    !***********************************************************
	
	! Different constants used for motion control
	CONST num defaultTimestep := 0.01;
	CONST num defaultDuration := 5;
	CONST num defaultSize := 3000;
	CONST zonedata passByZone := z10;
	
	! In the following, cur*** means used value and ask*** means next ordered value
	
	! Current robot state
	VAR num curOrder := 0;
	VAR num askOrder := 0;
	
	! Common variables for trajectory execution
	VAR num askTimestep := defaultTimestep;
	VAR num curTimestep;
	
	! Variables attached to a joint trajectory execution
	VAR jointtarget curPathJ{defaultSize};
	VAR jointtarget askPathJ{defaultSize};
	VAR num curPathJSize;
	VAR num askPathJSize := defaultSize;
	VAR num finalJSize := defaultSize;
	
	! Variables attached to a pose trajectory execution
	VAR robtarget curPathP{defaultSize};
	VAR robtarget askPathP{defaultSize};
	VAR num curPathPSize;
	VAR num askPathPSize := defaultSize;
	VAR num finalPSize := defaultSize;
	
	! Variables attached to a motion recording
	VAR jointtarget recPathJ{defaultSize};
	VAR num recPathJSize;
	VAR num recPathStep;
	
	! Variables attached to a pose motion
	VAR robtarget curGoalP;
	VAR robtarget askGoalP;
	VAR num curDuration;
	VAR num askDuration := defaultDuration;
	
	! Variables attached to a joint reorientation
	VAR jointtarget curGoalJ;
	VAR jointtarget askGoalJ;
	
	! Variables used to know current robot state
	VAR robtarget curEndPose;
	VAR jointtarget curJointPose;
	
	! Variables used to record and replay motions backward
	VAR pathrecid recorder;
	
	! Variables stocking the interrupts ID
	VAR intnum cancelnum;
	VAR intnum check_posenum;
	VAR intnum soft_stopnum;
	VAR intnum out_zonenum;
	VAR intnum suspendnum;
	VAR bool initialized := FALSE;
	
	! Indicates if the robot has started to move
	VAR bool moving := FALSE;
	VAR bool motion_en := TRUE;
	! Indicates if forbidden zone is checked
	VAR bool out_zone_check := TRUE;
	VAR bool object1_check := FALSE;
	VAR bool object2_check := FALSE;
	VAR bool running := TRUE;
	VAR bool recording := FALSE;
	! Status of last ran function
	! 0: executed correctly
	! 1: being executed
	! -i: finished with an error
	VAR num execution_status := 0;
	VAR num nb_call := 0;
	VAR num nb_end := 0;
	
	! World zones and volumes
	! TO DO make volume modifiable through PC SDK
	VAR shapedata volume;
	VAR shapedata volume_object1;
	VAR shapedata volume_object2;
	CONST pos corner1 := [-1000,-1000,-1000];
	CONST pos corner2 := [1000,1000,1000];
	VAR wztemporary forbidden_space;
	VAR wztemporary wz_object1;
	VAR wztemporary wz_object2;
	
	! Debug
	VAR errnum last_error;
	
	
	PROC main()
		init;
		! Infinite loop only stopped when receiving stop order
		while TRUE do
			moving := FALSE;
			nb_end := nb_call;
			WaitUntil askOrder <> 0;
			nb_call := nb_call + 1;
			curOrder := askOrder;
			askOrder := 0;
			curTimestep := askTimestep;
			recPathStep := askTimestep;
			curDuration := askDuration;
			execution_status := 1;
			TEST curOrder
			CASE 1:
				! Receiving joint path execution order
				curPathJ := askPathJ;
				curPathJSize := askPathJSize;
				executePathJ;
			CASE 2:
				! Receiving pose motion execution order
				curGoalP := askGoalP;
				executePoseMotion;
			CASE 3:
				! Receiving joint motion execution order
				curGoalJ := askGoalJ;
				executeJointMotion;
			CASE 4:
				! Receiving pose path execution order
				curPathP := askPathP;
				curPathPSize := askPathPSize;
				executePathM;
			CASE 5:
				! Receiving joints recording order
				recordMotion;
			CASE 6:
				! Testing new joint path execution technique
				executePathJ2;
			CASE 7:
				! new pose path execution technique
			CASE 8:
				! Activate blower
			CASE 9:
				! Activate sucking
			CASE 10:
				! Deactivate vacuum
			CASE 11:
				! New way to reset joints
				curGoalJ := askGoalJ;
				go_to;
            CASE 12:
                ! New way to set end effector pose
                curGoalP := askGoalP;
                go_to_pose;
            CASE 13:
                ! New way of moving joints along path that always go back to rest pose
                curPathJ := askPathJ;
				curPathJSize := askPathJSize;
                executePathJ3;
			CASE 42:
				! Receiving stop order
				Stop;
			ENDTEST
			moving := FALSE;
		ENDWHILE
	ENDPROC
	
	PROC init()
		if  not initialized then
			! Declare all interrupts
			IDisable;
			CONNECT cancelnum WITH cancel;
			CONNECT check_posenum WITH check_pose;
			CONNECT soft_stopnum WITH soft_stop;
			CONNECT out_zonenum WITH out_handle;
			CONNECT suspendnum WITH suspend;
			ISignalDO custom_DO_1, high, suspendnum;
			ISignalDO custom_DO_2, high, soft_stopnum;
			ISignalDO custom_DO_3, high, cancelnum;
			ISignalDO custom_DO_4, high, out_zonenum;
			ITimer 0.1, check_posenum;
			
			! Declare world zones
			WZBoxDef \Outside, volume, corner1, corner2;
			WZDOSet \Temp, forbidden_space \Before, volume, custom_DO_4, 1;
			
			initialized := TRUE;
			IEnable;
		endif
		if out_zone_check then
			WZEnable forbidden_space;
		else
			WZDisable forbidden_space;
		endif
	ENDPROC
	
	TRAP suspend
        ISleep cancelnum;
		ISleep check_posenum;
		ISleep soft_stopnum;
		ISleep out_zonenum;
		ISleep suspendnum;
		running := FALSE;
		IF moving THEN
			StopMove;
		ENDIF
		WaitUntil running;
		IF moving THEN
			StartMove;
		ENDIF
        IWatch cancelnum;
		IWatch check_posenum;
		IWatch soft_stopnum;
		IWatch out_zonenum;
		IWatch suspendnum;
	ENDTRAP
	
	TRAP out_handle
		IF moving THEN
			StopMove;
			go_back;
		endif
		execution_status := -1;
		RESET custom_DO_1;
		ExitCycle;
	ENDTRAP
	
	TRAP cancel
		IF moving THEN
			StopMove;
			go_back;
		endif
		IF recording THEN
			stopMotionRecording;
		endif
		curOrder := 0;
		ExitCycle;
	ENDTRAP
	
	TRAP check_pose
		curEndPose := CRobT();
		curJointPose := CJointT();
	ENDTRAP
	
	TRAP soft_stop
		IF motion_en THEN
			StopMove \Quick;
			motion_en := FALSE;
		ELSE
			StartMove;
			motion_en := TRUE;
		ENDIF
	ENDTRAP
	
	PROC vacuum_blow()
		!RESET hand_CmdVacuum1_R;
		!SET hand_CmdBlowoff1_R;
	ENDPROC
	
	PROC vacuum_suck()
		!RESET hand_CmdBlowoff1_R;
		!SET hand_CmdVacuum1_R;
	ENDPROC
	
	PROC vacuum_stop()
		!RESET hand_CmdVacuum1_R;
		!RESET hand_CmdBlowoff1_R;
	ENDPROC
	
	PROC go_back()
		ClearPath;
		StorePath;
		PathRecMoveBwd \ID:=recorder;
		RestoPath;
		StartMove;
	ENDPROC
	
	PROC executePathJ()
		PathRecStop \Clear;
		PathRecStart recorder;
		moving := TRUE;
		FOR index FROM 1 TO (curPathJSize-1) DO
			MoveAbsJ curPathJ{index}, v1000, \T:=curTimestep, passByZone, VaccumOne;
		ENDFOR
		MoveAbsJ curPathJ{curPathJSize}, v1000, \T:=curTimestep, fine, VaccumOne;
        execution_status := 0;
		RETURN;
	ERROR
		IF ERRNO = ERR_COLL_STOP THEN
			go_back;
            execution_status := -1;
			RETURN;
		ENDIF
    ENDPROC
	
	PROC executePathJ2()
		VAR num index := 1;
		PathRecStop \Clear;
		PathRecStart recorder;
		moving := TRUE;
		WHILE index < finalJSize DO
			IF askPathJSize >= index THEN
				MoveAbsJ askPathJ{index}, v1000, \T:=curTimestep, passByZone, VaccumOne;
				index := index + 1;
			ENDIF
		ENDWHILE
		MoveAbsJ askPathJ{askPathJSize}, v1000, \T:=curTimestep, fine, VaccumOne;
        execution_status := 0;
	ERROR
		IF ERRNO = ERR_COLL_STOP THEN
			go_back;
            execution_status := -1;
			RETURN;
		ENDIF
	ENDPROC
    
    PROC executePathJ3()
		VAR num index := 1;
        VAR jointtarget initial_pose;
		PathRecStop \Clear;
		PathRecStart recorder;
        initial_pose := CJointT();
		moving := TRUE;
		WHILE index < finalJSize DO
			IF askPathJSize >= index THEN
				MoveAbsJ askPathJ{index}, v1000, \T:=curTimestep, passByZone, VaccumOne;
				index := index + 1;
			ENDIF
		ENDWHILE
		MoveAbsJ askPathJ{askPathJSize}, v1000, \T:=curTimestep, fine, VaccumOne;
        index := askPathJSize - 1;
        WHILE index > 0 DO
			MoveAbsJ askPathJ{index}, v1000, \T:=curTimestep, passByZone, VaccumOne;
			index := index - 1;
		ENDWHILE
        MoveAbsJ initial_pose, v1000, \T:=curTimestep, fine, VaccumOne;
        execution_status := 0;
	ERROR
		IF ERRNO = ERR_COLL_STOP THEN
			ClearPath;
		    StorePath;
            RestoPath;
		    StartMove;
            index := index - 1;
            WHILE index > 0 DO
    			MoveAbsJ askPathJ{index}, v1000, \T:=curTimestep, passByZone, VaccumOne;
    			index := index - 1;
		    ENDWHILE
            MoveAbsJ initial_pose, v1000, \T:=curTimestep, fine, VaccumOne;
            execution_status := -1;
			RETURN;
		ENDIF
	ENDPROC
	
	PROC executePathM()
		ConfL \Off;
		PathRecStop \Clear;
		PathRecStart recorder;
		moving := TRUE;
		FOR index FROM 1 TO (curPathJSize-1) DO
			MoveL curPathP{index}, v1000, \T:=curTimestep, passByZone, VaccumOne;
		ENDFOR
		MoveL curPathP{curPathPSize}, v1000, \T:=curTimestep, fine, VaccumOne;
        execution_status := 0;
		RETURN;
	ERROR
		IF ERRNO = ERR_COLL_STOP THEN
			go_back;
            execution_status := -1;
			RETURN;
		ENDIF
    ENDPROC
	
	PROC executePathP2()
		VAR num index := 1;
		ConfL \Off;
		PathRecStop \Clear;
		PathRecStart recorder;
		moving := TRUE;
		WHILE index < finalPSize DO
			IF askPathPSize >= index THEN
				MoveL askPathP{index}, v1000, \T:=curTimestep, passByZone, VaccumOne;
				index := index + 1;
			ENDIF
		ENDWHILE
		MoveL askPathP{askPathPSize}, v1000, \T:=curTimestep, fine, VaccumOne;
        execution_status := 0;
	ERROR
		IF ERRNO = ERR_COLL_STOP THEN
			go_back;
            execution_status := -1;
			RETURN;
		ENDIF
	ENDPROC
	
	PROC executePoseMotion()
		ConfL \Off;
		PathRecStop \Clear;
		PathRecStart recorder;
		moving := TRUE;
        MoveL curGoalP, v1000, \T:=curDuration, fine, VaccumOne;
        execution_status := 0;
		RETURN;
	ERROR
		last_error := ERRNO;
		IF ERRNO = ERR_COLL_STOP THEN
			go_back;
            execution_status := -1;
			RETURN;
		ENDIF
    ENDPROC
	
	PROC go_to()
		VAR robtarget pose;
        VAR jointtarget joints;
		VAR bool goodPose := FALSE;
		pose := CalcRobT(curGoalJ, VaccumOne);
        joints := CalcJointT(pose, VaccumOne);
		PathRecStop \Clear;
		PathRecStart recorder;
		moving := TRUE;
        MoveL pose, v1000, \T:=curDuration, fine, VaccumOne;
		goodPose := TRUE;
		PathRecStop \Clear;
		PathRecStart recorder;
		MoveAbsJ curGoalJ, v1000, \T:=1, fine, VaccumOne;
        execution_status := 0;
		RETURN;
	ERROR
		last_error := ERRNO;
		IF ERRNO = ERR_COLL_STOP THEN
			go_back;
            execution_status := -1;
			RETURN;
        ELSEIF ERRNO = ERR_OUTSIDE_REACH OR ERRNO = ERR_ROBLIMIT THEN
            execution_status := -1;
            RETURN;
		ENDIF
	ENDPROC
    
    PROC go_to_pose()
        VAR robtarget pose;
        VAR robtarget curPose;
        VAR robtarget endPose;
        VAR robtarget plop;
        VAR jointtarget joints;
        VAR jointtarget curJoints;
        VAR num vector{3};
        joints := CalcJointT(curGoalP, VaccumOne);
        endPose := CalcRobT(joints, VaccumOne);
        curPose := CRobT();
        pose := CRobT();
        ConfL \Off;
        vector{1} := curGoalP.trans.x - pose.trans.x;
        vector{2} := curGoalP.trans.y - pose.trans.y;
        vector{3} := curGoalP.trans.z - pose.trans.z;
        PathRecStop \Clear;
		PathRecStart recorder;
		moving := TRUE;
        FOR index FROM 1 TO 19 DO
            curPose := CRobT();
            curJoints := CJointT();
            pose.trans.x := pose.trans.x + vector{1}/20;
            pose.trans.y := pose.trans.y + vector{2}/20;
            pose.trans.z := pose.trans.z + vector{3}/20;
            pose.extax.eax_a := 180;
            plop := pose;
            MoveL pose, v1000, \T:=curDuration/20, passByZone, VaccumOne;
        ENDFOR
        curPose := CRobT();
        curJoints := CJointT();
        curGoalP.extax.eax_a := 180;
        curGoalP.rot := pose.rot;
        plop := curGoalP;
        MoveL curGoalP, v1000, \T:=curDuration/20, fine, VaccumOne;
        execution_status := 0;
    ERROR
        last_error := ERRNO;
        IF ERRNO = ERR_COLL_STOP THEN
			go_back;
            execution_status := -1;
			RETURN;
		ELSEIF ERRNO = ERR_OUTSIDE_REACH OR ERRNO = ERR_ROBLIMIT THEN
            execution_status := -1;
            RETURN;
        ELSE
            go_back;
            execution_status := -1;
            RETURN;
		ENDIF
    ENDPROC
	
	PROC executeJointMotion()
		PathRecStop \Clear;
		PathRecStart recorder;
		moving := TRUE;
		MoveAbsJ curGoalJ, v1000, \T:=curDuration, fine, VaccumOne;
        execution_status := 0;
		RETURN;
	ERROR
		IF ERRNO = ERR_COLL_STOP THEN
			go_back;
            execution_status := -1;
			RETURN;
		ENDIF
    ENDPROC
	
	PROC recordMotion()
		WaitTime 2;
		recording := TRUE;
		SetLeadThrough \On;
		ISleep suspendnum;
		ISleep out_zonenum;
		ISleep soft_stopnum;
		recPathJSize := 1;
		WHILE recording AND recPathJSize <= defaultSize do
			recPathJ{recPathJSize} := CJointT();
			WaitTime recPathStep;
			recPathJSize := recPathJSize + 1;
		ENDWHILE
		WaitUntil NOT recording;
		stopMotionRecording;
	ENDPROC
	
	PROC stopMotionRecording()
		SetLeadThrough \Off;
		IWatch suspendnum;
		IWatch out_zonenum;
		IWatch soft_stopnum;
		recording := FALSE;
	ENDPROC
	
ENDMODULE