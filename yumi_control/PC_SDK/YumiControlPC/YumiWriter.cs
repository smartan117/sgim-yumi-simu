﻿using System;
using System.Collections.Generic;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;

using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System.Diagnostics;
using System.Dynamic;

namespace YumiControlPC
{
    public class YumiWriter
    {
        // Attributes used to launch and manage server
        private bool working = true;
        private bool running = true;
        private bool motion_en = true;
        private int nb_call = 0;
        private int nb_end = 0;
        private string srv;
        private ResponseSocket resp = null;
        private YumiLowLevelController controller;
        private dynamic currentOrder = null;

        public YumiWriter(YumiLowLevelController controller, string srv)
        {
            this.srv = srv;
            this.controller = controller;
        }

        private void update()
        {
            controller.readNBCall(ref nb_call);
            controller.readNBEnd(ref nb_end);
            controller.readRunning(ref running);
            controller.readSoftStop(ref motion_en);
        }

        private void analyzeMsg(string msg)
        {
            currentOrder = null;
            try
            {
                dynamic dico = JsonConvert.DeserializeObject(msg);
                if (dico.type is Object && dico.payload is Object && (string)dico.type is string)
                    currentOrder = dico;
            }
            catch (Exception)
            {

            }
        }

        private bool validateMove()
        {
            return running && motion_en && (nb_call == nb_end);
        }

        private void applyOrder(ref dynamic ack)
        {
            if (currentOrder == null || controller == null)
                return;
            ack = new ExpandoObject();
            string order = currentOrder.type;
            ack.type = order;
            ack.valid = false;
            if (string.Compare(order, "setPose") == 0)
            {
                try
                {
                    float[] pose = currentOrder.payload.pose.ToObject<float[]>();
                    float duration = currentOrder.payload.duration;

                    if (validateMove())
                        if (controller.sendPoseOrder(pose, duration))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if(string.Compare(order, "goToPose") == 0)
            {
                try
                {
                    float[] pose = currentOrder.payload.pose.ToObject<float[]>();
                    float duration = currentOrder.payload.duration;

                    if (validateMove())
                        if (controller.sendGoToPoseOrder(pose, duration))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "setJoints") == 0)
            {
                try
                {
                    float[] joints = currentOrder.payload.pose.ToObject<float[]>();
                    float duration = currentOrder.payload.duration;

                    if (validateMove())
                        if (controller.sendJointOrder(joints, duration))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "goTo") == 0)
            {
                try
                {
                    float[] joints = currentOrder.payload.pose.ToObject<float[]>();
                    float duration = currentOrder.payload.duration;

                    if (validateMove())
                        if (controller.sendGoToOrder(joints, duration))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "setPosePath") == 0)
            {
                try
                {
                    float[,] path = currentOrder.payload.path.ToObject<float[,]>();
                    float dt = currentOrder.payload.dt;
                    
                    if (validateMove())
                        if (controller.sendPosePathOrder(path, dt))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "setPosePath2") == 0)
            {
                try
                {
                    float[,] path = currentOrder.payload.path.ToObject<float[,]>();
                    float dt = currentOrder.payload.dt;

                    if (validateMove())
                        if (controller.sendPosePathOrder2(path, dt))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "setJointsPath") == 0)
            {
                try
                {
                    float[,] path = currentOrder.payload.path.ToObject<float[,]>();
                    float dt = currentOrder.payload.dt;

                    Stopwatch watch = new Stopwatch();
                    watch.Start();

                    if (validateMove())
                        if (controller.sendJointPathOrder(path, dt))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                    Debug.WriteLine("Sending full joint path order took " + watch.ElapsedMilliseconds + " ms");
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "setJointsPath2") == 0)
            {
                try
                {
                    float[,] path = currentOrder.payload.path.ToObject<float[,]>();
                    float dt = currentOrder.payload.dt;

                    if (validateMove())
                        if (controller.sendJointPathOrder2(path, dt))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "setJointsPath3") == 0)
            {
                try
                {
                    float[,] path = currentOrder.payload.path.ToObject<float[,]>();
                    float dt = currentOrder.payload.dt;

                    if (validateMove())
                        if (controller.sendJointPathOrder3(path, dt))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "cancel") == 0)
            {
                ack.valid = controller.cancelOrder();
            }
            else if (string.Compare(order, "suspend") == 0)
            {
                try
                {
                    bool value = currentOrder.payload;
                    ack.valid = controller.suspendToggle(value);
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "softStop") == 0)
            {
                try
                {
                    bool value = currentOrder.payload;
                    ack.valid = controller.softStop(value);
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "recordStart") == 0)
            {
                try
                {
                    float dt = currentOrder.payload;
                    if (validateMove())
                        if (controller.sendRecordOrder(dt))
                        {
                            ack.payload = nb_call + 1;
                            ack.valid = true;
                        }
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "recordStop") == 0)
            {
                try
                {
                    ack.valid = controller.stopRecording();
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "exit") == 0)
            {
                working = false;
                ack.valid = true;
            }
            else if (string.Compare(order, "ping") == 0)
            {
                ack.valid = true;
            }
            else if (string.Compare(order, "setCollision") == 0)
            {
                try
                {
                    bool value = currentOrder.payload;
                    ack.valid = controller.setCollisionAvoidance(value);
                }
                catch (Exception) { }
            }
            else if (string.Compare(order, "getJoints") == 0)
            {
                float[] curJoints = new float[7];
                bool read = controller.readJoints(ref curJoints);
                if (read)
                {
                    ack.payload = curJoints;
                    ack.valid = true;
                }
            }
            else if (string.Compare(order, "getPose") == 0)
            {
                float[] curJoints = new float[8];
                bool read = controller.readPose(ref curJoints);
                if (read)
                {
                    ack.payload = curJoints;
                    ack.valid = true;
                }
            }
            else if (string.Compare(order, "getRecord") == 0)
            {
                float[,] record = new float[1,1];
                bool read = controller.readRecord(ref record);
                float dt = 0;
                read = read && controller.readRecordStep(ref dt);
                if (read)
                {
                    ack.valid = true;
                    ack.payload = new ExpandoObject();
                    ack.payload.path = record;
                    ack.payload.dt = dt;
                }
            }
        }

        public void run()
        {
            resp = new ResponseSocket();
            resp.Bind(srv);
            Debug.WriteLine("Writer online!");

            while (working)
            {
                string msg = resp.ReceiveFrameString();
                Debug.WriteLine(msg);
                analyzeMsg(msg);
                update();
                dynamic ack = null;
                applyOrder(ref ack);
                if (ack == null)
                {
                    ack = new ExpandoObject();
                    ack.type = "error";
                    ack.payload = msg;
                    ack.valid = false;
                }
                resp.SendFrame(JsonConvert.SerializeObject((ExpandoObject) ack));
            }
            resp.Dispose();
            Debug.WriteLine("Writer offline!");
        }
    }
}
