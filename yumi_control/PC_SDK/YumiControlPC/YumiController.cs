﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

using ABB.Robotics.Controllers;
using ABB.Robotics.Controllers.Discovery;

using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;

namespace YumiControlPC
{
    public partial class YumiController : Form
    {
        private NetworkScanner scanner = null;
        private NetworkWatcher networkWatcher = null;
        private ControllerInfo ctrlInfo = null;
        private SimuYumiController controller = null;
        //private YumiLowLevelController controller2 = null;
        private RequestSocket req = null;

        //private YumiListener listener = null;
        private SimuYumiNode writer = null;

        private bool showLogs = false;
        private bool running = true;
        private bool motion_en = true;

        public YumiController()
        {
            InitializeComponent();
            logsBox.Hide();
        }

        private void controllerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (controllerList.SelectedItems.Count > 0)
                ctrlInfo = (ControllerInfo)controllerList.SelectedItems[0].Tag;
            else
                ctrlInfo = null;
        }

        private void YumiController_Load(object sender, EventArgs e)
        {
            scanner = new NetworkScanner();
            scanner.Scan();

            ControllerInfoCollection controllers = scanner.Controllers;

            ListViewItem item = null;
            foreach (ControllerInfo controllerInfo in controllers)
            {
                item = new ListViewItem(controllerInfo.IPAddress.ToString());
                item.SubItems.Add(controllerInfo.Id);
                item.SubItems.Add(controllerInfo.Availability.ToString());
                item.SubItems.Add(controllerInfo.IsVirtual.ToString());
                item.SubItems.Add(controllerInfo.SystemName);
                item.SubItems.Add(controllerInfo.Version.ToString());
                item.SubItems.Add(controllerInfo.ControllerName);
                controllerList.Items.Add(item);
                item.Tag = controllerInfo;
            }

            networkWatcher = new NetworkWatcher(scanner.Controllers);
            networkWatcher.Found += NetworkWatcher_Found;
            networkWatcher.Lost += NetworkWatcher_Lost;
            networkWatcher.EnableRaisingEvents = true;
        }

        private void NetworkWatcher_Lost(object sender, NetworkWatcherEventArgs e)
        {
            foreach (ListViewItem item in controllerList.Items)
            {
                if ((ControllerInfo)item.Tag == e.Controller)
                    controllerList.Items.Remove(item);
            }
        }

        private void NetworkWatcher_Found(object sender, NetworkWatcherEventArgs e)
        {
            this.Invoke(new EventHandler<NetworkWatcherEventArgs>(AddControllerToListView), new Object[] { this, e });
        }

        private void AddControllerToListView(object sender, NetworkWatcherEventArgs e)
        {
            ControllerInfo info = e.Controller;
            ListViewItem item = new ListViewItem(info.IPAddress.ToString());
            item.SubItems.Add(info.Id);
            item.SubItems.Add(info.Availability.ToString());
            item.SubItems.Add(info.IsVirtual.ToString());
            item.SubItems.Add(info.SystemName);
            item.SubItems.Add(info.Version.ToString());
            item.SubItems.Add(info.ControllerName);
            controllerList.Items.Add(item);
            item.Tag = info;
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            string task = taskBox.Text;
            string module = moduleBox.Text;

            if (controller == null)  // Connection
            {
                if (ctrlInfo != null)
                {
                    controller = new SimuYumiController(task, module);
                    string user = null;
                    string pwd = null;
                    if (userBox.Text.Length > 0)
                        user = userBox.Text;
                    if (passwordBox.Text.Length > 0)
                        pwd = passwordBox.Text;
                    controller.connectRobot(ctrlInfo, user, pwd);
                    stateLabel.Text = "Connected to " + ctrlInfo.SystemName;
                    connectButton.Text = "Disconnect";
                }
            }
            else if(writer == null)
            {
                controller.disconnectRobot();
                controller = null;
                stateLabel.Text = "Offline";
                connectButton.Text = "Connect";
            }

        }

        private void anonymousBox_CheckedChanged(object sender, EventArgs e)
        {
            if(anonymousBox.Checked)
            {
                userBox.Text = "";
                passwordBox.Text = "";
                userBox.Hide();
                passwordBox.Hide();
                userLabel.Hide();
                passwordLabel.Hide();
            }
            else
            {
                userBox.Show();
                passwordBox.Show();
                userLabel.Show();
                passwordLabel.Show();
            }
        }

        private void logsButton_Click(object sender, EventArgs e)
        {
            showLogs = !showLogs;
            if (showLogs)
                logsBox.Show();
            else
                logsBox.Hide();
        }

        private void serverButton_Click(object sender, EventArgs e)
        {
            if(req == null && controller != null)
            {
                //listener = new YumiListener(controller2, "tcp://*:5556");
                writer = new SimuYumiNode(controller, "tcp://*:5555", "tcp://127.0.0.1:5556");
                Task.Factory.StartNew(() =>
                {
                    writer.run();
                });
                req = new RequestSocket();
                req.Connect("tcp://127.0.0.1:5555");

                serverButton.Text = "Stop server";
            }
            else if(req != null)
            {
                stopWriter();
                req.Dispose();
                req = null;
                writer = null;
                serverButton.Text = "Run server";
            }
        }

        private bool analyzeResponse(string msg, string order)
        {
            Debug.WriteLine(msg);
            try
            {
                dynamic dico = JsonConvert.DeserializeObject(msg);
                if (dico.type is Object && dico.valid is Object && (string)dico.type is string)
                    if (string.Compare(order, (string) dico.type) == 0)
                        return ((bool)dico.valid);
            }
            catch (Exception)
            {

            }
            return false;
        }

        private void softStop(bool value)
        {
            req.SendFrame("{\"type\": \"softStop\", \"payload\": " + JsonConvert.SerializeObject(value) + "}");
            string ack = req.ReceiveFrameString();
            if(analyzeResponse(ack, "softStop"))
                motion_en = value;
        }

        private void suspend(bool value)
        {
            req.SendFrame("{\"type\": \"suspend\", \"payload\": " + JsonConvert.SerializeObject(value) + "}");
            string ack = req.ReceiveFrameString();
            if (analyzeResponse(ack, "suspend"))
                running = value;
        }

        private void cancel()
        {
            req.SendFrame("{\"type\": \"cancel\", \"payload\": \"\"}");
            req.ReceiveFrameString();
        }

        private void stopWriter()
        {
            req.SendFrame("{\"type\": \"exit\", \"payload\": \"\"}");
            req.ReceiveFrameString();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFD = new OpenFileDialog();
            openFD.Filter = "Applications (*.exe)|*.exe";
            openFD.FilterIndex = 1;
            openFD.Multiselect = false;
            DialogResult result = openFD.ShowDialog();

            if(result == DialogResult.OK)
            {
                string file = openFD.FileName;
                Process process = new Process();
                process.StartInfo.FileName = file;
                //process.StartInfo.Arguments = "arg1 arg2 ...";
                process.Start();
            }
        }

        private void pauseRobotButton_Click(object sender, EventArgs e)
        {
            if (controller != null)
                if (running)
                {
                    suspend(false);
                }
                else
                {
                    suspend(true);
                }
            if (running)
                pauseRobotButton.Text = "Pause Robot";
            else
                pauseRobotButton.Text = "Resume Robot";
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (req != null)
                cancel();
        }

        private void softStopButton_Click(object sender, EventArgs e)
        {
            if (req != null)
                softStop(!motion_en);
            if (motion_en)
                softStopButton.Text = "Soft stop";
            else
                softStopButton.Text = "Move";
        }
    }
}
