﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using ABB.Robotics.Controllers;
using ABB.Robotics.Controllers.Discovery;
using ABB.Robotics.Controllers.RapidDomain;
using ABB.Robotics.Controllers.IOSystemDomain;
using System.Threading;
using ABB.Robotics;
using System.Diagnostics;
using Newtonsoft.Json;

namespace YumiControlPC
{
    public class SimuYumiController
    {
        private static int BATCH_SIZE = 50;

        private Controller controller = null;  //Controller of the robot
        private Mastership master = null;  //Currently held mastership (mostly used for accessing RAPID domain)
        private string task;
        private string module;

        public SimuYumiController(string task, string module)
        {
            this.task = task;
            this.module = module;
        }

        /**
         * Low-level functions
         **/

        // Connect the controller to the robot
        public bool connectRobot(ControllerInfo controllerInfo, string user = null, string pwd = null)
        {
            //scanner = new NetworkScanner();
            //scanner.Scan();
            //ControllerInfoCollection controllers = scanner.Controllers;

            //ControllerInfo controllerInfo = controllers[0];

            if
                (controllerInfo.Availability == ABB.Robotics.Controllers.Availability.Available)
            {
                if (controller != null)
                {
                    return false;
                }
                controller = ControllerFactory.CreateFrom(controllerInfo);
                if (user == null || pwd == null)
                    controller.Logon(UserInfo.DefaultUser);
                else
                {
                    UserInfo info = new UserInfo(user, pwd);
                    controller.Logon(info);
                }
                return (controller.OperatingMode == ControllerOperatingMode.Auto);
            }
            return false;
        }
        
        // Disconnect the controller from the robot
        public bool disconnectRobot()
        {
            if (controller != null)
            {
                controller.Logoff();
                controller.Dispose();
                controller = null;
            }
            return true;
        }

        // Acquire mastership to RAPID domain
        public void requestMastership()
        {
            releaseMastership();
            master = Mastership.Request(controller.Rapid);
        }

        // Release mastership
        public void releaseMastership()
        {
            if (master != null)
                master.Dispose();
            master = null;
        }

        // Return synchronized remote RAPID variable
        private RapidData synchronizeRapid(string task, string module, string varName)
        {
            return controller.Rapid.GetRapidData(task, module, varName);
        }

        // Start the main task
        public bool startTask()
        {
            //Debug.WriteLine("Starting task");
            try
            {
                Task wTask = controller.Rapid.GetTask(task);
                //Debug.WriteLine("Got task: " + wTask.ExecutionStatus);
                requestMastership();
                try
                {
                    StartResult res = controller.Rapid.Start(RegainMode.Continue, ExecutionMode.Continuous, ExecutionCycle.AsIs, StartCheck.None, false, TaskPanelExecutionMode.AllTasks);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                finally
                {
                    releaseMastership();
                }
                //Debug.WriteLine("Task status: " + wTask.ExecutionStatus);
                return (wTask.ExecutionStatus == TaskExecutionStatus.Running);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }
        }

        // Stop the main task
        public bool stopTask()
        {
            //Debug.WriteLine("Stopping task");
            try
            {
                Task wTask = controller.Rapid.GetTask(task);
                //Debug.WriteLine("Got task: " + wTask.ExecutionStatus);
                requestMastership();
                try
                {
                    controller.Rapid.Stop(StopMode.Immediate, TaskPanelExecutionMode.AllTasks);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    releaseMastership();
                }
                //Debug.WriteLine("Task status: " + wTask.ExecutionStatus);
                return (wTask.ExecutionStatus != TaskExecutionStatus.Running);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }

        }

        // Return true if the task is running
        public bool isRunning(string task = null)
        {
            //Debug.WriteLine("Getting task");
            Task wTask;
            if (task == null)
                wTask = controller.Rapid.GetTask(this.task);
            else
                wTask = controller.Rapid.GetTask(task);
            //Debug.WriteLine("Got task: " + wTask.ExecutionStatus);
            return (wTask.ExecutionStatus == TaskExecutionStatus.Running);
        }

        // Reset the program pointer of the task
        public bool resetPointer(string thatTask = null)
        {
            Task wTask;
            if (thatTask == null)
                wTask = controller.Rapid.GetTask(task);
            else
                wTask = controller.Rapid.GetTask(thatTask);

            try
            {
                requestMastership();
                try
                {
                    //Debug.WriteLine("Resetting pointer");
                    wTask.ResetProgramPointer();
                    return true;
                }
                catch (GenericControllerException)
                {
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    releaseMastership();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }

        // Write on a digital output signal
        public bool writeSignalDO(string id, bool value)
        {
            bool write = false;
            Signal signal1 = controller.IOSystem.GetSignal(id);
            DigitalSignal diSig1 = (DigitalSignal)signal1;

            try
            {
                //Signal must be set as accessible (access to ALL)
                if (value)
                    diSig1.Set();
                else
                    diSig1.Reset();
                write = true;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            return write;
        }

        // Read a digital output signal
        public bool readSignalDO(string id, ref bool value)
        {
            bool read = false;
            Signal signal1 = controller.IOSystem.GetSignal(id);
            DigitalSignal diSig1 = (DigitalSignal)signal1;

            try
            {
                value = diSig1.Get() != 0;
                read = true;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }
            return read;
        }

        // Trigger an interrupt by toggling a digital output
        public bool triggerInterrupt(string id)
        {
            bool success = false;
            bool value = true;
            if (!readSignalDO(id, ref value))
                return false;
            if (value)
            {
                success = writeSignalDO(id, false);
                if (!success)
                    return false;
            }
            success = writeSignalDO(id, true);
            if (!success)
                return false;
            success = writeSignalDO(id, false);
            return success;
        }

        // Set the program of the main task
        public bool setPointer(string routine, int line)
        {
            Task wTask = controller.Rapid.GetTask(task);
            try
            {
                requestMastership();
                ProgramPosition pos = new ProgramPosition(module, routine, new TextRange(line));
                wTask.SetProgramPointer(pos);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }
            return false;
        }

        // Read a remote RAPID T typed data and store it in var ref
        public bool readData<T>(string task, string module, string varName, ref T var)
        {
            RapidData rd = synchronizeRapid(task, module, varName);
            if (rd.Value is T)
            {
                var = (T)rd.Value;
                return true;
            }
            return false;
        }

        // Write a RAPID data on the robot
        public bool writeData(string task, string module, string varName, IRapidData var)
        {
            bool success = false;
            try
            {
                requestMastership();
                RapidData rd = synchronizeRapid(task, module, varName);
                if (rd.Value.GetType().IsAssignableFrom(var.GetType()))
                {
                    rd.Value = var;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        // Write a RAPID data on the robot in a RAPID array at position i
        public bool writeArrayData(string task, string module, string varName, IRapidData var, int i)
        {
            bool success = false;
            try
            {
                requestMastership();
                RapidData rd = synchronizeRapid(task, module, varName);
                if (rd.Value is ArrayData)
                {
                    rd.WriteItem(var, i);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        // Read a remote RAPID data from an array at position i and store it in var ref
        public bool readArrayData<T>(string task, string module, string varName, ref T var, int i)
        {
            RapidData rd = synchronizeRapid(task, module, varName);
            if (rd.Value is ArrayData)
            {
                var = (T)rd.ReadItem(i);
                return true;
            }
            return false;
        }


        /**
         * Sligthly more complex functions
         * */


        // Move robot along given trajectory and store final 
        public bool moveRobotEasy(float[,] path, float timestep, ref float[] finalPose, ref float[] joints, ref float[,] trajectory)
        {
            try
            {
                //Stopwatch watch = new Stopwatch();
                //watch.Start();

                if (isRunning())
                    stopTask();

                //Debug.WriteLine("Ready to send parameters");

                Num status = default(Num);
                readData<Num>(task, module, "exec_status", ref status);
                int intStatus = (int)status.Value;

                //Debug.WriteLine("Execution status: " + intStatus);

                Num dt = default(Num);
                dt.Value = timestep;

                bool write = writeData(task, module, "timestep", dt);
                if (!write)
                    return false;

                //Debug.WriteLine("Timestep sent");

                Num val = default(Num);
                val.Value = path.GetLength(0);
                write = writeData(task, module, "finalSize", val);
                if (!write)
                    return false;

                //string pathFile = "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\path.data";
                string pathFile = "path.data";

                //Debug.WriteLine("Final size sent");
                if (File.Exists(pathFile))
                    File.Delete(pathFile);
                serializePathAndSave(pathFile, path);
                controller.FileSystem.PutFile(pathFile, "path.data");


                //Debug.WriteLine("Current size sent");

                startTask();

                //Debug.WriteLine("Initialize motion took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Thread.Sleep(10000);

                //Debug.WriteLine("Task started: motion begins!");
                

                //Debug.WriteLine("Full motion sent");

                while (isRunning())
                    Thread.Sleep(100);

                Bool stopping = default(Bool);
                stopping.Value = true;
                write = writeData(task, module, "stopsync", stopping);

                while (isRunning("T_ROB_R_b"))
                    Thread.Sleep(100);

                //Debug.WriteLine("Moving and detecting end took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Debug.WriteLine("Motion ended");

                RobTarget endPose = default(RobTarget);
                readData<RobTarget>(task, module, "finalPose", ref endPose);
                finalPose[0] = endPose.Trans.X;
                finalPose[1] = endPose.Trans.Y;
                finalPose[2] = endPose.Trans.Z;
                finalPose[3] = (float)endPose.Rot.Q1;
                finalPose[4] = (float)endPose.Rot.Q2;
                finalPose[5] = (float)endPose.Rot.Q3;
                finalPose[6] = (float)endPose.Rot.Q4;

                //Debug.WriteLine("Got final pose");

                JointTarget endJoints = default(JointTarget);
                readData<JointTarget>(task, module, "finalJoints", ref endJoints);
                joints[0] = endJoints.RobAx.Rax_1;
                joints[1] = endJoints.RobAx.Rax_2;
                joints[2] = endJoints.RobAx.Rax_3;
                joints[3] = endJoints.RobAx.Rax_4;
                joints[4] = endJoints.RobAx.Rax_5;
                joints[5] = endJoints.RobAx.Rax_6;
                joints[6] = endJoints.ExtAx.Eax_a;

                //Debug.WriteLine("Getting final state took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Debug.WriteLine("Got final joints");

                //trajectory = new float[252, 7];

                val = default(Num);
                readData<Num>("T_ROB_R_b", "RecordingModule", "curSize", ref val);
                trajectory = new float[((int)val.Value), 7];

                Debug.WriteLine("Trajectory has size " + val.Value);

                //string trajFile = "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\trajectoryP.data";
                string trajFile = "trajectoryP.data";

                if (File.Exists(trajFile))
                    File.Delete(trajFile);
                controller.FileSystem.GetFile("trajectoryP.data", trajFile);
                deserializeTrajAndLoad(trajFile, ref trajectory);

                //watch.Stop();
                //Debug.WriteLine("Getting trajectory took " + watch.ElapsedMilliseconds + " ms");

                readData<Num>(task, module, "exec_status", ref status);
                int newStatus = (int)status.Value;

                //Debug.WriteLine("Execution status: " + newStatus);

                return (newStatus > intStatus);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }
        
        // Reset the robot controller before a motion execution
        public bool resetRobot()
        {
            if (isRunning())
                stopTask();
            resetPointer();
            resetPointer("T_ROB_R_b");
            Bool yay = default(Bool);
            yay.Value = false;
            writeData(task, module, "startsync", yay);
            yay.Value = true;
            writeData(task, module, "stopsync", yay);
            //if (resetPointer())
            //    Debug.WriteLine("Pointer reset");
            //else
            //    Debug.WriteLine("Pointer reset failed");
            return startTask();
        }

        // Serialize and save a trajectory
        public void serializePathAndSave(string fileName, float[,] path)
        {
            try
            {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(fileName);

                for (int i = 0; i < path.GetLength(0); i++)
                {
                    string line = null;
                    float[] floatLine = new float[path.GetLength(1)];
                    for (int j = 0; j < path.GetLength(1); j++)
                    {
                        floatLine[j] = path[i, j];
                    }
                    line = JsonConvert.SerializeObject(floatLine);
                    sw.WriteLine(line);
                }
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

        // Deserialize and save a trajectory
        public void deserializeTrajAndLoad(string fileName, ref float[,] path)
        {
            string line;
            dynamic dico = null;
            float[] floatLine;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(fileName);

                //Read the first line of text
                line = sr.ReadLine();

                //Continue to read until you reach end of file
                int i = 0;
                while (line != null && i < path.GetLength(0))
                {
                    dico = JsonConvert.DeserializeObject(line);
                    floatLine = dico.ToObject<float[]>();
                    for (int j = 0; j < path.GetLength(1); j++)
                        path[i, j] = floatLine[j];
                    i += 1;
                    line = sr.ReadLine();
                }

                //close the file
                sr.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
            }
        }


        /**
         *  DEPRECATED AND UNTESTED
         **/

        public bool writeArray(string task, string module, string varName, IRapidData[] var)
        {
            bool success = false;
            try
            {
                requestMastership();
                RapidData rd = synchronizeRapid(task, module, varName);

                if (rd.Value is ArrayData)
                {
                    for (int i = 0; i < var.Length; i++)
                    {
                        rd.WriteItem(var[i], i);
                    }
                }
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        public bool writePath(string task, string module, string varName, float[,] path, int end, int start = 0)
        {
            bool success = false;
            try
            {
                requestMastership();
                RapidData rd = synchronizeRapid(task, module, varName);

                if (rd.Value is ArrayData)
                {
                    JointTarget joints = default(JointTarget);
                    for (int i = start; i <= end; i++)
                    {
                        joints.RobAx.Rax_1 = path[i, 0];
                        joints.RobAx.Rax_2 = path[i, 1];
                        joints.RobAx.Rax_3 = path[i, 2];
                        joints.RobAx.Rax_4 = path[i, 3];
                        joints.RobAx.Rax_5 = path[i, 4];
                        joints.RobAx.Rax_6 = path[i, 5];
                        joints.ExtAx.Eax_a = path[i, 6];
                        rd.WriteItem(joints, i);
                    }
                }
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        // Move robot along given trajectory (OLD)
        public bool moveRobot(float[,] path, float timestep, ref float[] finalPose, ref float[] joints, ref float[,] trajectory)
        {
            try
            {
                //Stopwatch watch = new Stopwatch();
                //watch.Start();

                if (isRunning())
                    stopTask();

                //Debug.WriteLine("Ready to send parameters");

                Num status = default(Num);
                readData<Num>(task, module, "exec_status", ref status);
                int intStatus = (int)status.Value;

                //Debug.WriteLine("Execution status: " + intStatus);

                Num dt = default(Num);
                dt.Value = timestep;

                bool write = writeData(task, module, "timestep", dt);
                if (!write)
                    return false;

                //Debug.WriteLine("Timestep sent");

                Num val = default(Num);
                val.Value = path.GetLength(0);
                write = writeData(task, module, "finalSize", val);
                if (!write)
                    return false;

                //Debug.WriteLine("Final size sent");

                int step = BATCH_SIZE;
                if (step > path.GetLength(0))
                    step = path.GetLength(0);
                write = writePath(task, module, "path", path, step - 1);
                if (!write)
                    return false;

                //Debug.WriteLine("First motion batch sent");

                val.Value = step;
                write = writeData(task, module, "curSize", val);
                if (!write)
                    return false;

                //Debug.WriteLine("Current size sent");

                startTask();

                //Debug.WriteLine("Initialize motion took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Thread.Sleep(10000);

                //Debug.WriteLine("Task started: motion begins!");

                for (int i = 2; i <= path.GetLength(0) / BATCH_SIZE; i++)
                {
                    step = i * BATCH_SIZE;
                    write = writePath(task, module, "path", path, step - 1, (i - 1) * BATCH_SIZE);
                    if (!write)
                        return false;
                    val.Value = i * BATCH_SIZE;
                    write = writeData(task, module, "curSize", val);
                    if (!write)
                        return false;

                }

                if (path.GetLength(0) > step)
                {
                    write = writePath(task, module, "path", path, path.GetLength(0) - 1, step);
                    if (!write)
                        return false;
                    val.Value = path.GetLength(0);
                    write = writeData(task, module, "curSize", val);
                    if (!write)
                        return false;
                }

                //Debug.WriteLine("Full motion sent");

                while (isRunning() || isRunning("T_ROB_R_b"))
                    Thread.Sleep(100);

                //Debug.WriteLine("Moving and detecting end took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Debug.WriteLine("Motion ended");

                RobTarget endPose = default(RobTarget);
                readData<RobTarget>(task, module, "finalPose", ref endPose);
                finalPose[0] = endPose.Trans.X;
                finalPose[1] = endPose.Trans.Y;
                finalPose[2] = endPose.Trans.Z;
                finalPose[3] = (float)endPose.Rot.Q1;
                finalPose[4] = (float)endPose.Rot.Q2;
                finalPose[5] = (float)endPose.Rot.Q3;
                finalPose[6] = (float)endPose.Rot.Q4;

                //Debug.WriteLine("Got final pose");

                JointTarget endJoints = default(JointTarget);
                readData<JointTarget>(task, module, "finalJoints", ref endJoints);
                joints[0] = endJoints.RobAx.Rax_1;
                joints[1] = endJoints.RobAx.Rax_2;
                joints[2] = endJoints.RobAx.Rax_3;
                joints[3] = endJoints.RobAx.Rax_4;
                joints[4] = endJoints.RobAx.Rax_5;
                joints[5] = endJoints.RobAx.Rax_6;
                joints[6] = endJoints.ExtAx.Eax_a;

                //Debug.WriteLine("Getting final state took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Debug.WriteLine("Got final joints");

                //trajectory = new float[252, 7];

                val = default(Num);
                readData<Num>("T_ROB_R_b", "RecordingModule", "curSize", ref val);
                trajectory = new float[((int)val.Value), 7];
                RobTarget pose = default(RobTarget);
                for (int i = 0; i < trajectory.GetLength(0); i++)
                {
                    if (!readArrayData<RobTarget>("T_ROB_R_b", "RecordingModule", "trajectory", ref pose, i))
                        return false;
                    trajectory[i, 0] = pose.Trans.X;
                    trajectory[i, 1] = pose.Trans.Y;
                    trajectory[i, 2] = pose.Trans.Z;
                    trajectory[i, 3] = (float)pose.Rot.Q1;
                    trajectory[i, 4] = (float)pose.Rot.Q2;
                    trajectory[i, 5] = (float)pose.Rot.Q3;
                    trajectory[i, 6] = (float)pose.Rot.Q4;
                }

                //watch.Stop();
                //Debug.WriteLine("Getting trajectory took " + watch.ElapsedMilliseconds + " ms");

                readData<Num>(task, module, "exec_status", ref status);
                int newStatus = (int)status.Value;

                //Debug.WriteLine("Execution status: " + newStatus);

                return (newStatus > intStatus);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }

        // Move robot to final spatial position in given duration (NOT USED)
        public bool goTo(float[] pose, float duration)
        {
            try
            {
                if (isRunning())
                    stopTask();

                //Debug.WriteLine("Ready to send parameters");

                Num status = default(Num);
                readData<Num>(task, module, "exec_status", ref status);
                int intStatus = (int)status.Value;

                //Debug.WriteLine("Execution status: " + intStatus);

                Num dt = default(Num);
                dt.Value = duration;

                bool write = writeData(task, module, "duration", dt);
                if (!write)
                    return false;

                RobTarget endPose = default(RobTarget);
                endPose.Trans.X = pose[0];
                endPose.Trans.Y = pose[1];
                endPose.Trans.Z = pose[2];
                endPose.Rot.Q1 = pose[3];
                endPose.Rot.Q2 = pose[4];
                endPose.Rot.Q3 = pose[5];
                endPose.Rot.Q4 = pose[6];
                write = writeData(task, module, "goal", endPose);

                if (!write)
                    return false;

                startTask();

                while (isRunning())
                    Thread.Sleep(100);

                readData<Num>(task, module, "exec_status", ref status);
                int newStatus = (int)status.Value;

                return (newStatus > intStatus);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }

        // Serialize a Rapid data array and save it
        public void serializeAndSave(string fileName, RapidData[] data)
        {
            try
            {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(fileName);

                for(int i = 0; i < data.Length; i++)
                    sw.WriteLine(data[i].StringValue);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
            }
        }

        // Deserialize a file into a Rapid data array
        public void deserializeAndLoad(string fileName, ref RapidData[] data)
        {
            string line;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(fileName);

                //Read the first line of text
                line = sr.ReadLine();

                //Continue to read until you reach end of file
                int i = 0;
                while (line != null && i < data.Length)
                {
                    data[i] = default(RapidData);
                    data[i].StringValue = line;
                    line = sr.ReadLine();
                }

                //close the file
                sr.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
            }
        }

        // Main test function
        public static void Main(string[] args)
        {
            NetworkScanner scanner = new NetworkScanner();
            scanner.Scan();
            ControllerInfoCollection controllers = scanner.Controllers;

            Console.WriteLine("Scanning for controllers!");

            ControllerInfo controllerInfo = controllers[0];

            SimuYumiController controller = new SimuYumiController("T_ROB_R", "TestModule2");
            controller.connectRobot(controllerInfo);

            Console.WriteLine("Controller connected to robot");

            Console.ReadKey();

            //controller.controller.FileSystem.PutFile("C:\\Users\\Master_2013_2\\Documents\\yumi_control\\test_motion2.json", "motion2.json");

            string fileName = "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\example_traj.data";
            string outFile = "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\example_traj_out.data";

            float[,] path = new float[253, 7];
            controller.deserializeTrajAndLoad(fileName, ref path);
            Console.WriteLine("Path has a length of " + path.GetLength(0));
            Console.WriteLine(path[252, 6]);
            controller.serializePathAndSave(outFile, path);

            /*
            // Reading values as jointtargets
            bool read = true;
            Num val = default(Num);
            controller.readData<Num>("T_ROB_R", "TestModule", "size", ref val);
            float[,] trajectory = new float[((int)val.Value), 7];
            JointTarget pose = default(JointTarget);
            for (int i = 0; i < trajectory.GetLength(0); i++)
            {
                if (!controller.readArrayData<JointTarget>("T_ROB_R", "TestModule", "traj", ref pose, i))
                    read = false;
                trajectory[i, 0] = pose.RobAx.Rax_1;
                trajectory[i, 1] = pose.RobAx.Rax_2;
                trajectory[i, 2] = pose.RobAx.Rax_3;
                trajectory[i, 3] = pose.RobAx.Rax_4;
                trajectory[i, 4] = pose.RobAx.Rax_5;
                trajectory[i, 5] = pose.RobAx.Rax_6;
                trajectory[i, 6] = pose.ExtAx.Eax_a;
            }
            */

            // Reading values as RawBytes

            /*
            Stopwatch watch = new Stopwatch();
            watch.Start();
            controller.controller.FileSystem.FileExists("motion2.json");
            controller.controller.FileSystem.PutFile("C:\\Users\\Master_2013_2\\Documents\\yumi_control\\test_motion2.json","motion2.json");
            controller.controller.FileSystem.GetFile("motion2.json", "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\test_motion2.copy.json");
            controller.controller.FileSystem.RemoveFile("motion2.json");
            watch.Stop();
            Console.WriteLine("Took " + watch.ElapsedMilliseconds + " ms");
            */

            Console.ReadKey();
        }

    }
}
