﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;

namespace YumiControlPC
{
    public class SimuYumiNode
    {
        private int orderRcv = 0;
        private string srv;
        private string rstSrv;
        private ResponseSocket resp = null;  // Server communicating with external programs, awaiting commands
        private RequestSocket req = null;  // Client communicating with Robotstudio simulation to reset robot
        private SimuYumiController controller;  // Hand-made controller of Yumi with high-level commands
        private dynamic currentOrder = null;
        public bool working = true;

        public SimuYumiNode(SimuYumiController controller, string srv, string rstSrv)
        {
            this.srv = srv;
            this.rstSrv = rstSrv;
            this.controller = controller;
        }

        // Check if message has correct formatting and deserialize it
        private void analyzeMsg(string msg)
        {
            currentOrder = null;
            try
            {
                dynamic dico = JsonConvert.DeserializeObject(msg);
                if (dico.type is Object && dico.payload is Object && (string)dico.type is string)
                    currentOrder = dico;
            }
            catch (Exception)
            {

            }
        }

        // Apply the order received and prepare acknowledgement message
        private void applyOrder(ref dynamic ack)
        {
            if (currentOrder == null || controller == null)
                return;
            string order = currentOrder.type;
            ack = new ExpandoObject();
            ack.type = order;
            Debug.WriteLine("Order received: " + orderRcv);
            orderRcv += 1;
            try
            {
                if (string.Compare(order, "move") == 0)
                {
                    // Received a policy execution order
                    float[,] path = currentOrder.payload.path.ToObject<float[,]>();
                    float dt = currentOrder.payload.dt;
                    float[] pose = new float[7];
                    float[] joints = new float[7];
                    float[,] trajectory = null;

                    bool ok = controller.moveRobotEasy(path, dt, ref pose, ref joints, ref trajectory);
                    ack.payload = new ExpandoObject();
                    ack.payload.pose = pose;
                    ack.payload.joints = joints;
                    ack.payload.trajectory = trajectory;
                    ack.valid = ok;
                }
                else if (string.Compare(order, "goto") == 0)
                {
                    // Received a joints GoTo order
                    float[] goal = currentOrder.payload.pose.ToObject<float[]>();
                    float t = currentOrder.payload.t;

                    bool ok = controller.goTo(goal, t);
                    ack.valid = ok;
                }
                else if (string.Compare(order, "reset") == 0)
                {
                    // Received the order to reset the environmental setup
                    if (controller.stopTask())
                    {
                        while (controller.isRunning())
                            Thread.Sleep(50);

                        double[] pose = currentOrder.payload.ToObject<double[]>();
                        dynamic msg = new ExpandoObject();
                        msg.type = "reset";
                        msg.payload = pose;
                        req.SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));
                        Debug.WriteLine("Sent reset to reset node");
                        string answer = req.ReceiveFrameString();
                        Debug.WriteLine("Reset node: " + answer);
                        analyzeMsg(answer);
                        ack.payload = "";

                        if ((currentOrder != null) && (bool)currentOrder.valid && controller.resetRobot())
                        {
                            while (controller.isRunning())
                                Thread.Sleep(50);

                            ack.valid = true;
                        }
                        else
                            ack.valid = false;
                    }
                    else
                    {
                        ack.payload = "";
                        ack.valid = false;
                    }
                }
                else if (string.Compare(order, "exit") == 0)
                {
                    working = false;
                    ack.valid = true;
                    ack.payload = "";
                }
            }
            catch (Exception) { }
        }

        // Thread main function
        public void run()
        {
            resp = new ResponseSocket();
            resp.Bind(srv);
            req = new RequestSocket();
            req.Connect(rstSrv);
            Debug.WriteLine("Writer online!");

            while (working)
            {
                string msg = resp.ReceiveFrameString();
                Debug.WriteLine(msg);
                analyzeMsg(msg);
                dynamic ack = null;
                applyOrder(ref ack);
                if (ack == null)
                {
                    ack = new ExpandoObject();
                    ack.type = "error";
                    ack.payload = msg;
                    ack.valid = false;
                }
                resp.SendFrame(JsonConvert.SerializeObject((ExpandoObject)ack));
            }
            resp.Dispose();
            Debug.WriteLine("Writer offline!");
        }

    }
}
