using System;
using System.Collections.Generic;
using System.Text;
using System.Dynamic;

using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;

using ABB.Robotics.Math;
using ABB.Robotics.RobotStudio;
using ABB.Robotics.RobotStudio.Environment;
using ABB.Robotics.RobotStudio.Stations;
using System.Threading.Tasks;

//	To activate this Add-in you have to copy RobotStudioEmptyAddin1.rsaddin to the Add-In directory,
//  typically C:\Program Files (x86)\Common Files\ABB Industrial IT\Robotics IT\RobotStudio\AddIns

namespace RobotStudioEmptyAddin1
{
    public class Class1
    {
        private string srv;
        private ResponseSocket resp = null;  // Server awaiting for reset commands
        private dynamic currentOrder = null;
        private Station station = null;  // Current station loaded in Robotstudio (e.g. Yumi robot)
        private Mechanism mech = null;  // Currently controlled mechanical chain (e.g. Yumi right arm)
        public bool working = true;

        public Class1(string srv)
        {
            this.srv = srv;
        }

        // Check if message has correct formatting and deserialize it
        private void analyzeMsg(string msg)
        {
            currentOrder = null;
            try
            {
                dynamic dico = JsonConvert.DeserializeObject(msg);
                if (dico.type is Object && dico.payload is Object && (string)dico.type is string)
                    currentOrder = dico;
            }
            catch (Exception)
            {

            }
        }

        // Apply the order received and prepare acknowledgement message
        private void applyOrder(ref dynamic ack)
        {
            if (currentOrder == null || mech == null)
                return;
            string order = currentOrder.type;
            ack = new ExpandoObject();
            ack.type = order;
            try
            {
                if (string.Compare(order, "reset") == 0)
                {
                    double[] pose = currentOrder.payload.ToObject<double[]>();
                    // Hack to set the joint angles in correct order
                    double[] jointVal = new double[7];
                    jointVal[0] = pose[0];
                    jointVal[1] = pose[1];
                    jointVal[2] = pose[6];
                    jointVal[3] = pose[2];
                    jointVal[4] = pose[3];
                    jointVal[5] = pose[4];
                    jointVal[6] = pose[5];
                    ack.valid =  setJoints(ref jointVal);
                    ack.payload = "";
                }
                else if (string.Compare(order, "exit") == 0)
                {
                    working = false;
                    ack.valid = true;
                    ack.payload = "";
                }
            }
            catch (Exception) { }
        }

        // Set joints of the current mechanical unit to values (in degrees)
        private bool setJoints(ref double[] pose)
        {
            for (int i = 0; i < pose.Length; i++)
            {
                pose[i] = Globals.DegToRad(pose[i]);
            }
            return mech.SetJointValues(pose, true);
        }

        // Thread main function
        public void run()
        {
            station = Project.ActiveProject as Station;
            mech = station.ActiveTask.Mechanism;

            Logger.AddMessage(new LogMessage("Reset node linked to " + mech.ToString()));

            double[] jointVal = new double[7];

            resp = new ResponseSocket();
            resp.Bind(srv);

            Logger.AddMessage(new LogMessage("Node online listening to " + srv));

            while (working)
            {
                string msg = resp.ReceiveFrameString();
                Logger.AddMessage(new LogMessage("Node received: " + msg));
                analyzeMsg(msg);
                dynamic ack = null;
                applyOrder(ref ack);
                if (ack == null)
                {
                    ack = new ExpandoObject();
                    ack.type = "error";
                    ack.payload = msg;
                    ack.valid = false;
                }

                /*
                jointVal = mech.GetJointValues();
                Logger.AddMessage(new LogMessage("The robots joint values:"));
                int i = 1;
                foreach (double d in jointVal)
                {
                    Logger.AddMessage(new LogMessage("Joint " + i++ + ": " +
                        Globals.RadToDeg(d).ToString()));
                }
                */

                resp.SendFrame(JsonConvert.SerializeObject((ExpandoObject)ack));
            }
            resp.Dispose();

            Logger.AddMessage(new LogMessage("Node offline"));
        }

        // This is the entry point which will be called when the Add-in is loaded
        public static void AddinMain()
        {
            Class1 node = new Class1("tcp://*:5556");
            Task.Factory.StartNew(() =>
            {
                node.run();
            });
        }

    }
}