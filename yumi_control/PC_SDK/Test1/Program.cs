﻿using System;
using NetMQ;
using NetMQ.Sockets;

using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

namespace Test1
{
    class Program
    {

        static void Main(string[] args)
        {
            /*
            float[,] test = new float[4,3];
            test[1,2] = 3.5F;
            
            string jsonObject = JsonConvert.SerializeObject(test);
            Console.WriteLine(jsonObject);
            Console.ReadKey();
            Console.WriteLine(test);
            Console.ReadKey();
            float[,] test2 = JsonConvert.DeserializeObject<float[,]>(jsonObject);
            Console.WriteLine(test2);
            Console.ReadKey();
            */
            /*
            Dictionary<string, string> dico = new Dictionary<string, string>();
            dico.Add("name", "Nicolas");
            dico.Add("function", "student");
            Console.WriteLine(dico);
            Console.WriteLine(JsonConvert.SerializeObject(dico));
            dynamic json = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(dico));
            */
            /*
            string json = "{\"type\": \"setPose\", \"payload\": {\"pose\": [0.5, -1.0, 7.8], \"duration\": 5.0}}";
            Console.WriteLine(json);
            dynamic obj = JsonConvert.DeserializeObject(json);
            Console.WriteLine(obj.type is Object);
            Console.WriteLine(obj.payload is Object);
            Console.WriteLine(obj.bhgdzajhd is Object);
            Console.WriteLine((string)obj.type is string);

            string test = "{\"type\":\"softStop\",\"payload\":false}";
            dynamic obj2 = JsonConvert.DeserializeObject(test);
            Console.WriteLine(test);
            Console.WriteLine(obj.type is Object);
            Console.WriteLine(obj.payload is Object);
            */

            float[] plop = new float[5];
            Console.WriteLine(plop.Length);
            plop = new float[18];
            Console.WriteLine(plop.Length);
            Console.ReadKey();
        }
    }
}
