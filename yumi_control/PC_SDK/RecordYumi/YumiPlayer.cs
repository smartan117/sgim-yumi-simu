﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Dynamic;
using System.Windows.Forms;
using System.IO;

namespace RecordYumi
{
    public class YumiPlayer
    {
        private static long MAX_DT = 30000;
        private static long QUICK_DT = 5000;

        private string srv;
        //private string subSrv;
        private RequestSocket client = null;
        //private SubscriberSocket sub = null;

        public YumiPlayer(string srv) //, string subSrv)
        {
            this.srv = srv;
            //this.subSrv = subSrv;
        }

        //TO DO cleaner try-catch
        public void run()
        {
            client = new RequestSocket();
            client.Connect(srv);

            //sub = new SubscriberSocket();
            //sub.Connect(subSrv);

            Console.WriteLine("Connected! Press any key to start app!");
            Console.ReadKey();

            OpenFileDialog openFD = new OpenFileDialog();
            openFD.Filter = "JSON Data (*.json)|*.json";
            openFD.FilterIndex = 1;
            openFD.Multiselect = false;
            DialogResult result = openFD.ShowDialog();

            if (result == DialogResult.OK)
            {

                Console.WriteLine("Motion loaded!");

                string data = null;
                using (var reader = new StreamReader(openFD.FileName))
                {
                    data = reader.ReadToEnd();
                }

                try
                {
                    if (data != null)
                    {
                        dynamic dico = JsonConvert.DeserializeObject(data);
                        //dico.dt = 0.02F;
                        float[][] path = dico.path.ToObject<float[][]>();

                        
                        dynamic msg = new ExpandoObject();
                        dynamic response = null;
                        string ack = "";
                        /*msg.type = "goTo";
                        msg.payload = new ExpandoObject();
                        msg.payload.pose = path[0];
                        msg.payload.duration = 3.0F;

                        Console.WriteLine("Joint motion loaded! Press any key to go into initial position!");
                        Console.ReadKey();

                        client.SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));

                        Console.WriteLine("Joint order sent!");

                        ack = client.ReceiveFrameString();

                        Console.WriteLine("Joint order acknowledged!");

                        response = JsonConvert.DeserializeObject(ack);
                        if ((bool)response.valid)
                        {
                            // Motion starts now because valid
                            int nb_call = response.payload;
                            int nb_end = nb_call - 1;
                            sub.Subscribe("");
                            Stopwatch watch = new Stopwatch();
                            watch.Start();
                            while (nb_end < nb_call && watch.ElapsedMilliseconds < MAX_DT)
                            {
                                string topic = sub.ReceiveFrameString();
                                string content = sub.ReceiveFrameString();

                                if (topic == "nb_end")
                                {
                                    response = JsonConvert.DeserializeObject(content);
                                    nb_end = response.payload;
                                }
                                else if (topic == "running")
                                {
                                    response = JsonConvert.DeserializeObject(content);
                                    if ((bool)response.uptodate && !(bool)response.payload)
                                        watch.Restart();
                                }
                            }

                            if (nb_end == nb_call)
                            {
                                Console.WriteLine("Motion ended!");
                                int status = -10;
                                watch.Restart();
                                while (status == -10 && watch.ElapsedMilliseconds < QUICK_DT)
                                {
                                    string topic = sub.ReceiveFrameString();
                                    string content = sub.ReceiveFrameString();

                                    if (topic == "status")
                                    {
                                        response = JsonConvert.DeserializeObject(content);
                                        if ((bool)response.uptodate)
                                            status = response.payload;
                                    }
                                }
                                Console.WriteLine("With status " + status);
                            }
                        }
                        */

                        
                        
                        // RESET PROGRAM AND POSE 
                        msg = new ExpandoObject();
                        msg.type = "reset";
                        msg.payload = path[0];

                        Console.WriteLine("Ready to send reset order");
                        Console.ReadKey();

                        client.SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));

                        Console.WriteLine("Reset sent");

                        ack = client.ReceiveFrameString();

                        Console.WriteLine("Reset node ack: " + ack);

                        response = JsonConvert.DeserializeObject(ack);
                        if ((bool)response.valid)
                            Console.WriteLine("Reset successful");

                        

                        ///*
                        msg = new ExpandoObject();
                        msg.type = "move"; //TO CHANGE
                        msg.payload = dico;

                        Console.WriteLine("Motion trajectory ready! Press any key to start it!");
                        Console.ReadKey();

                        client.SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));

                        Console.WriteLine("Joint path order sent!");

                        ack = client.ReceiveFrameString();

                        Console.WriteLine("Joint path order acknowledged!");

                        response = JsonConvert.DeserializeObject(ack);
                        if ((bool) response.valid)
                        {
                            // Motion starts now because valid
                            /*int nb_call = response.payload;
                            int nb_end = nb_call - 1;
                            sub.Subscribe("");
                            Stopwatch watch = new Stopwatch();
                            watch.Start();
                            while (nb_end < nb_call && watch.ElapsedMilliseconds < MAX_DT)
                            {
                                string topic = sub.ReceiveFrameString();
                                string content = sub.ReceiveFrameString();

                                if (topic == "nb_end")
                                {
                                    response = JsonConvert.DeserializeObject(content);
                                    nb_end = response.payload;
                                }
                                else if (topic == "running")
                                {
                                    response = JsonConvert.DeserializeObject(content);
                                    if ((bool) response.uptodate && ! (bool) response.payload)
                                        watch.Restart();
                                }
                            }

                            if (nb_end == nb_call)
                            {

                                Console.WriteLine("Motion ended!");

                                int status = -10;
                                watch.Restart();
                                while (status == -10 && watch.ElapsedMilliseconds < QUICK_DT)
                                {
                                    string topic = sub.ReceiveFrameString();
                                    string content = sub.ReceiveFrameString();

                                    if (topic == "status")
                                    {
                                        response = JsonConvert.DeserializeObject(content);
                                        if ((bool) response.uptodate)
                                            status = response.payload;
                                    }
                                }

                                Console.WriteLine("With status " + status);
                            }
                            */
                            Console.WriteLine("Motion worked!");
                        }
                        //*/
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            Console.WriteLine("Press any key to quit");
            Console.ReadKey();
        }

        [STAThread]
        public static void Main(string[] args)
        {
            YumiPlayer program = new YumiPlayer("tcp://127.0.0.1:5555");  //, "tcp://127.0.0.1:5556");
            program.run();
        }
    }
}
