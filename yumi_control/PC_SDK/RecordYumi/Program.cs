﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Dynamic;
using System.Windows.Forms;
using System.IO;

namespace RecordYumi
{
    class Program
    {
        private string srv;
        private RequestSocket client = null;

        public Program(string srv)
        {
            this.srv = srv;
        }

        // TO DO cleaner try-catch
        public void run()
        {
            client = new RequestSocket();
            client.Connect(srv);

            Console.WriteLine("Connected! Press any key to start recording!");
            Console.ReadKey();

            
            try
            {
                dynamic msg = new ExpandoObject();
                msg.type = "recordStart";
                msg.payload = 0.01F;

                client.SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));
                string ack = client.ReceiveFrameString();

                dynamic response = JsonConvert.DeserializeObject(ack);
                if ((bool) response.valid)
                {
                    msg = new ExpandoObject();
                    msg.type = "recordStop";
                    msg.payload = "";

                    Console.WriteLine("Recording started! Press any key to stop recording!");
                    Console.ReadKey();

                    client.SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));
                    ack = client.ReceiveFrameString();

                    //Console.WriteLine(ack);

                    response = JsonConvert.DeserializeObject(ack);
                    if ((bool) response.valid)
                    {
                        Console.WriteLine("Recording was a success!");

                        msg = new ExpandoObject();
                        msg.type = "getRecord";
                        msg.payload = "";

                        client.SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));
                        ack = client.ReceiveFrameString();

                        //Console.WriteLine(ack);

                        response = JsonConvert.DeserializeObject(ack);
                        if ((bool) response.valid && response.payload is object)
                        {
                            Console.WriteLine("Got record from robot!");

                            SaveFileDialog openFD = new SaveFileDialog();
                            openFD.Filter = "JSON data (*.json)|*.json";
                            openFD.FilterIndex = 1;
                            DialogResult result = openFD.ShowDialog();

                            if (result == DialogResult.OK)
                            {
                                using (var writer = new StreamWriter(openFD.FileName))
                                {
                                    writer.WriteLine(JsonConvert.SerializeObject(response.payload, Formatting.Indented));
                                }
                                Console.WriteLine("Record saved with success!");
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.WriteLine("Press any key to quit");
            Console.ReadKey();
        }

        [STAThread]
        static void Main(string[] args)
        {
            Program program = new Program("tcp://127.0.0.1:5555");
            program.run();
        }
    }
}
